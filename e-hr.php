<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top-web.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden">
<!-- Headbar -->
<?php include("incs/header-v2.html") ?>
<script>
$(".main-menu .list-unstyled>li.active").removeClass('active');
$(".main-menu .list-unstyled>li:nth-child(4)").addClass('active');
</script>
<!-- /Headbar -->
<div class="page-checkout">

    
    <div id="toc">
		<section class="z-broadcast _self-pt0 mb0">
			<div class="bx-stepbar _self-pv20 cb-af container">
				<ul class="tabsbar">
						  <li><a href="e-hr-dashboard.php" title="Send Message"><i class="fas fa-bullhorn"></i> <span>Dashboard</span></a></li>
						  <li><a href="e-hr.php" title="Create Message" class="selected"><i class="fas fa-users"></i> <span>Employee</span></a></li>
						  <li><a href="e-hr-employee.php" title="User Detail"><i class="fas fa-users-cog"></i> <span>Import User</span></a></li>
						  <li><a href="e-hr-setting.php" title="Message Setting"><i class="fas fa-sliders-h"></i> <span>Message Setting</span></a></li>
						  <li><a href="e-hr-report.php" title="Report"><i class="fas fa-file-medical-alt"></i> <span>Report</span></a></li>
				  </ul>
			</div>
					

			
			<div class="bg-gray2 contentTabs">
				<div id="tbc-1" class="msg">
					<div class="head-bg">
					<div class="container">
						<h2>E-HR</h2>	
					</div>
					</div>

					<form method="post" class="form-checkout form-sending'">
					
					<div class="wrap-full _chd-cl-xs-12 _chd-cl-sm">
						<div class="main row center-xs">
							<div class="container">
							
								<div class="sort-bar d-flex between-xs middle-xs">
									<div class="sort">
										<select class="form-control select2" data-placeholder="Search by" style="width:auto">
										  <option></option>
										  <option>ชื่อ-นามสกุล</option>
										  <option>ฝ่าย</option>
										  <option>แผนก</option>
										  <option>ตำแหน่ง</option>
										</select>
										<div class="search-sm d-inline">
											<input class="txt-box" placeholder="ค้นหา...">
											<button type="submit" class="fas fa-search" aria-hidden="true"></button>
										</div>
									</div>
									<div class="right">
										<a class="ui-btn-black btn-xs" data-fancybox="" data-modal="modal" data-src="#modal-add-user" title="Add new">Add New</a>
									</div>

								</div>
							
								<!-- card -->
								<div class="card bg-white rounded mt20-xs border-0">
									
									<div class="card-body _self-pa30 middle-xs">
										<div class="table-resp off">
											<table class="table tb-bordered tb-skin" style="min-width: 900px" width="100%" cellspacing="0" cellpadding="0" border="0">
											<thead>
											<tr>
											<th scope="col">NO.</th>
											<th scope="col">รหัสพนักงาน</th>
											<th scope="col">ชื่อ - นามสกุล</th>
											<th scope="col">บริษัท</th>
											<th scope="col">ฝ่าย</th>
											<th scope="col">แผนก</th>
											<th scope="col">ตำแหน่ง</th>
											<th scope="col">ระดับงาน</th>
											<th scope="col">สถานะ</th>
											<th scope="col">&nbsp;</th>
											</tr>
											</thead>
											<tbody>
											<?php for($i=1;$i<=20;$i++){ ?>
											<tr>
											<td valign="middle" align="center"><?php echo $i; ?></td>
											<td valign="middle" align="center">1100200<?php echo $i+1; ?></td>
											<td valign="middle" align="center">ทวีทัรพย์ พรสัจจะ</td>
											<td valign="middle" align="center">OTO</td>
											<td valign="middle" align="center">Production</td>
											<td valign="middle" align="center">Web Design</td>
											<td valign="middle" align="center">Graphic design</td>
											<td valign="middle" align="center">ผู้จัดการ</td>
											<td valign="middle" align="center">เป็นพนักงาน</td>
											<td valign="middle" align="center">
												<a class="more ui-btn-blue-min btn-xs" href="javascrtip:;" data-fancybox="" data-src="#userdetail" title="ดูรายละเอียด"><i class="fas fa-eye"></i> view</a> 
												<a href="e-hr-report-Leavinguser.php" title="View Leave" class="ui-btn-trans-mid btn-xs"><i class="fas fa-calendar-day"></i></a>
												<a href="e-hr-report-user.php" title="View Time Attendance" class="ui-btn-trans-mid btn-xs"><i class="fas fa-user-clock"></i></a>
												<a href="javascrtip:;" data-fancybox="" data-src="#acc-edit-user" title="Edit" class="ui-btn-trans-mid btn-xs"><i class="fas fa-edit"></i></a>
											</td>
											</tr>
											<?php } ?>
											</tbody>
											</table>

										</div>
										
										<div class="_flex end-xs mt20-xs">
											<div class="pagination _self-mr0-mt0">
															<ul class=" _flex middle-xs end-xs">
															  <li>
																<a title="Previous" href="#" class="btn"><i class="fas fa-angle-left"></i></a>
															  </li>
															  <li>
																<a title="Previous" href="#" class="active">1</a>
															  </li>
															  <li>
																<a title="2" href="#">2</a>
															  </li>
															  <li>
																<a title="3" href="#">3</a>
															  </li>
															  <li>
																<a title="4" href="#">4</a>
															  </li>
															  <li>...</li>
															  <li>
																<a title="30" href="#">30</a>
															  </li>
															  <li>
																<a title="Next" href="#" class="btn"><i class="fas fa-angle-right"></i></a>
															  </li>
															  <li>
																  <span class="cv-select">
																	<select class="select-box bg-wh" id="type-page">
																		<option value="0" selected="">10 / page</option>
																		<option value="1">20 / page</option>
																		<option value="2">30 / page</option>
																	</select>
																</span>
															  </li>
															</ul>
											</div>
										</div>
									</div>
									
									
									
									<!--<div class="sticky-bottom card-footer">
									<div class="__chd-ph10 center-xs">
											<button type="button" class="ui-btn-green btn-md" onclick="$('.form-sending')[0].reset();"><i class="fas fa-file-excel"></i> Export Excel</button>
									</div>
								  </div>-->
								</div>
								<!-- /card -->
							</div>

						</div>
					</div>
				</div>
					</form>

			</div>
			
			
			
			
		</section>
    </div>
</div>
	<!-- popup -->
			<div class="popup thm-lite" id="acc-edit-user">
				<div class="box-middle">
				  <div class="modal-content">
					<div class="modal-header">
					  <h2 class="modal-title t-black mb20-xs">Edit Member</h2>
					</div>
					<div class="modal-body txt-l">
					  
					  <div class="form-group p-2 bg-light rounded" data-select2-id="72">

							<div class="row">
								<div class="col-sm-6">
									  <div class="form-group">
										<label for="edit-emid">Employee ID</label>
										<input type="text" class="txt-box w-100" id="edit-emid" value="11002004">
									  </div>
									  
									  <div class="form-group js-select">
										<label for="edit-detail1">บริษัท</label>
										<!--<input type="text" class="txt-box w-100" id="edit-detail1" value="INNOHUB">-->
										<select class="form-control js-example-tags">
										  <option>OTO</option>
										  <option selected="selected">INNOHUB</option>
										</select>
									  </div>
									  
									  
									  <div class="form-group js-select">
										<label for="edit-detail1">ฝ่าย</label>
										<!--<input type="text" class="txt-box w-100" id="edit-detail1" value="Production">-->
										<select class="form-control js-example-tags">
										  <option selected="selected">IT</option>
										  <option >Accounting</option>
										  <option>Finance</option>
										  <option >Sale</option>
										  <option>Maketing</option>
										  <option >Call Center</option>
										  <option>HR</option>
										  <option>Reseption</option>
										</select>
									  </div>
									  
									  <div class="form-group js-select">
										<label for="edit-detail1">แผนก</label>
										<!--<input type="text" class="txt-box w-100" id="edit-detail1" value="Content">-->
										<select class="form-control js-example-tags">
										  <option selected="selected">Content</option>
										  <option >Accounting</option>
										  <option>Finance</option>
										  <option >Sale</option>
										  <option>Maketing</option>
										  <option>IT</option>
										  <option>Reseption</option>
										</select>
									  </div>
									  
									  <div class="form-group js-select">
										<label for="edit-detail1">ตำแหน่ง</label>
										<!--<input type="text" class="txt-box w-100" id="edit-detail1" value="Web Design">-->
										<select class="form-control js-example-tags">
										  <option selected="selected">Web Design</option>
										  <option>UX/UI</option>
										  <option>CSS Developer</option>
										  <option>Developer</option>
										  <option>Network</option>
										  <option>Content</option>
										  <option>Admin</option>
										</select>
									  </div>
									  
									  <div class="form-group js-select">
										<label for="edit-detail1">ระดับงาน</label>
										<!--<input type="text" class="txt-box w-100" id="edit-detail1" value="Manager">-->
										<select class="form-control js-example-tags">
										  <option>Straff</option>
										  <option>Senior</option>
										  <option>Assistant Manager</option>
										  <option selected="selected">Manager</option>
										  <option>Senior Manager</option>
										  <option >AVP</option>
										  <option>VP</option>
										  <option>MD</option>
										  <option>CEO</option>
										</select>
									  </div>


								</div>

								<div class="col-sm-6">

									  <div class="form-group">
										<label for="edit-name">Display Name</label>
										<input type="text" class="txt-box w-100" id="edit-name" value="ทวีทรัพย์ พรสัจจะ">
									  </div>
									  
									  <div id="f-upload_avatar" class="f-upload form-group" style="">
										<label for="apm_img2">Upload Avatar</label>
										<div class="input-group">
												<div class="custom-file">
													<input type="file" class="custom-file-input2" id="apm_img2" accept=".jpg,.jpeg,.png">
													<img id="temp_apmimage_src" hidden="">
													<label class="custom-file-label" id="apm_labelimg2" for="apm_img2"><p>Choose file</p></label>
												</div>
												<div class="input-group-append">
													<button class="input-group-text" id="apm_uploadimg2">Upload</button>
												</div>

										</div>
									 </div>
									  
									  <div class="form-group">
										<label for="gender">Gender</label>
										<div class="btn-group btn-group-toggle w-100" data-toggle="buttons">
										  <label class="btn bg-teal active">
											<input type="radio" name="gender" id="male" autocomplete="off" checked> Male
										  </label>
										  <label class="btn bg-teal">
											<input type="radio" name="gender" id="female" autocomplete="off"> Female
										  </label>
										  <label class="btn bg-teal">
											<input type="radio" name="gender" id="lgbt" autocomplete="off"> LGBT
										  </label>
										</div>
									  </div>
									  
									  <div class="form-group">
										<label for="edit-mobile">Mobile Number</label>
										<input type="text" class="txt-box w-100" id="edit-mobile" value="0899599955">
									  </div>
									  
									  <div class="form-group">
										<label for="u-Email1">Email address</label>
										<input type="email" class="txt-box w-100" id="u-Email1" value="thaweesub.p@oto.corp.com">
									  </div>
								</div>
							</div>
						</div>
					  
					</div>
					<div class="modal-footer justify-content-between">
					  <button type="button" data-fancybox-close="" class="ui-btn-gray btn-sm" title="Close" href="javascript:;" onclick="parent.jQuery.fancybox.close();">Cancel</button>
					  <button type="button" data-fancybox-close="" class="ui-btn-green2 btn-sm" title="Close" href="javascript:;" onclick="parent.jQuery.fancybox.close();">Save</button>
					</div>
				  </div>
				  <!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			  </div>
	<!-- /popup -->
	
	<div class="popup thm-lite" id="modal-edit-user">
				<div class="box-middle">
				  <div class="modal-content">
					<div class="modal-header">
					  <h4 class="modal-title w-100 text-center">Edit Phone Number / UID</h4>
					  
					</div>
					<div class="modal-body txt-l">
					  <div class="form-group bg-light rounded p-2">
                        <label class="d-block">Phone Number</label>
                        <input type="text" class="form-control form-control-lg text-lg text-center bg-white _self-pa30" value="0899599955">
                      </div>
					  
					   <div class="form-group bg-light rounded p-2">
                        <label class="d-block">User ID</label>
                        <input type="text" class="form-control form-control-lg text-lg text-center bg-white _self-pa30" value="Ub8c6ef4a7880161aa490bd7bd11d0133">
                      </div>
	
					</div>
					<div class="modal-footer justify-content-between mt10-xs">
					   <a data-fancybox-close="" class="ui-btn-gray btn-sm" title="Cancel" href="javascript:;" onclick="parent.jQuery.fancybox.close();">Cancel</a>
					   <a data-fancybox-close="" class="ui-btn-green2 btn-sm" title="Save" href="javascript:;" onclick="parent.jQuery.fancybox.close();">Save</a>
					</div>
				  </div>
				  <!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			  </div>
			  <!-- /.modal -->
			  
	<div class="popup thm-lite" id="modal-add-user">
				<div class="box-middle">
				  <div class="modal-content">
					<div class="modal-header">
					  <h2 class="modal-title t-black mb20-xs">Add Member Detail</h2>
					</div>
					<div class="modal-body txt-l">
					  
					  <div class="form-group p-2 bg-light rounded">

							<div class="row">
								<div class="col-sm-6">
									  <div class="form-group">
										<label for="add-emid">Employee ID</label>
										<input type="text" class="txt-box w-100" id="add-emid">
									  </div>
									  
									  <div class="form-group js-select">
										<label for="add-detail1">บริษัท</label>
										<!--<input type="text" class="txt-box w-100" id="edit-detail1" value="INNOHUB">-->
										<select class="form-control js-example-tags" data-placeholder="Select">
										  <option></option>
										  <option>OTO</option>
										  <option>INNOHUB</option>
										</select>
									  </div>
									  
									  
									  <div class="form-group js-select">
										<label for="add-detail1">ฝ่าย</label>
										<!--<input type="text" class="txt-box w-100" id="edit-detail1" value="Production">-->
										<select class="form-control js-example-tags" data-placeholder="Select">
										  <option></option>
										  <option>IT</option>
										  <option >Accounting</option>
										  <option>Finance</option>
										  <option >Sale</option>
										  <option>Maketing</option>
										  <option >Call Center</option>
										  <option>HR</option>
										  <option>Reseption</option>
										</select>
									  </div>
									  
									  <div class="form-group js-select">
										<label for="add-detail1">แผนก</label>
										<!--<input type="text" class="txt-box w-100" id="edit-detail1" value="Content">-->
										<select class="form-control js-example-tags" data-placeholder="Select">
										  <option></option>
										  <option>Content</option>
										  <option >Accounting</option>
										  <option>Finance</option>
										  <option >Sale</option>
										  <option>Maketing</option>
										  <option>IT</option>
										  <option>Reseption</option>
										</select>
									  </div>
									  
									  <div class="form-group js-select">
										<label for="add-detail1">ตำแหน่ง</label>
										<!--<input type="text" class="txt-box w-100" id="edit-detail1" value="Web Design">-->
										<select class="form-control js-example-tags" data-placeholder="Select">
										  <option></option>
										  <option>Web Design</option>
										  <option>UX/UI</option>
										  <option>CSS Developer</option>
										  <option>Developer</option>
										  <option>Network</option>
										  <option>Content</option>
										  <option>Admin</option>
										</select>
									  </div>
									  
									  <div class="form-group js-select">
										<label for="add-detail1">ระดับงาน</label>
										<!--<input type="text" class="txt-box w-100" id="edit-detail1" value="Manager">-->
										<select class="form-control js-example-tags" data-placeholder="Select">
										  <option></option>
										  <option>Straff</option>
										  <option>Senior</option>
										  <option>Assistant Manager</option>
										  <option>Manager</option>
										  <option>Senior Manager</option>
										  <option >AVP</option>
										  <option>VP</option>
										  <option>MD</option>
										  <option>CEO</option>
										</select>
									  </div>


								</div>

								<div class="col-sm-6">

									  <div class="form-group">
										<label for="add-name">Display Name</label>
										<input type="text" class="txt-box w-100" id="add-name">
									  </div>
									  
									  <div id="f-upload_avatar" class="f-upload form-group" style="">
										<label for="add_img2">Upload Avatar</label>
										<div class="input-group">
												<div class="custom-file">
													<input type="file" class="custom-file-input2" id="aadd_img2" accept=".jpg,.jpeg,.png">
													<img id="temp_addimage_src" hidden="">
													<label class="custom-file-label" id="aadd_labelimg2" for="aadd_img2"><p>Choose file</p></label>
												</div>
												<div class="input-group-append">
													<button class="input-group-text" id="add_uploadimg2">Upload</button>
												</div>

										</div>
									 </div>
									  
									  <div class="form-group">
										<label for="gender">Gender</label>
										<div class="btn-group btn-group-toggle w-100" data-toggle="buttons">
										  <label class="btn bg-teal">
											<input type="radio" name="agender" id="male" autocomplete="off"> Male
										  </label>
										  <label class="btn bg-teal">
											<input type="radio" name="agender" id="female" autocomplete="off"> Female
										  </label>
										  <label class="btn bg-teal">
											<input type="radio" name="agender" id="lgbt" autocomplete="off"> LGBT
										  </label>
										</div>
									  </div>
									  
									  <div class="form-group">
										<label for="add-mobile">Mobile Number</label>
										<input type="text" class="txt-box w-100" id="add-mobile">
									  </div>
									  
									  <div class="form-group">
										<label for="u-Email1">Email address</label>
										<input type="email" class="txt-box w-100" id="ua-Email1">
									  </div>
								</div>
							</div>
						</div>
					  
					</div>
					<div class="modal-footer justify-content-between">
					  <button type="button" data-fancybox-close="" class="ui-btn-gray btn-sm" title="Close" href="javascript:;" onclick="parent.jQuery.fancybox.close();">Cancel</button>
					  <button type="button" data-fancybox-close="" class="ui-btn-green2 btn-sm" title="Close" href="javascript:;" onclick="parent.jQuery.fancybox.close();">Save</button>
					</div>
				  </div>
				  <!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			  </div>
			  <!-- /.modal -->

	<div class="thm-popup pa0-xs rounded-1" id="userdetail" style="width: 800px;display: none">
				<div id="UserMoreDetail" class="card card-widget widget-user-2">
				  <!-- Add the bg color to the header using any of the bg-* classes -->
				  <div class="widget-user-header bg-green d-flex between-xs bottom-xs">
				   <div class="main-user">
						<div class="widget-user-image">
						  <img class="img-circle elevation-2" src="https://www.w3schools.com/w3images/avatar4.png" alt="User Avatar">
						</div>
						<!-- /.widget-user-image -->
						<h3 class="widget-user-username">ทวีทรัพย์ พรสัจจะ</h3>
						<h5 class="widget-user-desc">Employee ID : 11002001</h5>
					</div>
					<div class="call-in txt-r">
						<big class="f-normal">Designer</big> 
						<small class="d-block text-black-50 text-right"><i class="fas fa-bookmark" aria-hidden="true"></i> Platform</small>
						<!--<div class="end-call mt-2"><button class="btn-danger border-dark"><i class="fas fa-phone-slash fa-rotate-90"></i> End Call</button></div>-->
					</div>

				  </div>
				  <div class="card-footer border-bottom info-call2line _self-pa0">
					<div class="d-flex">
					  <div class="col-sm-4 border-right">
						<div class="description-block">
						  <h5 class="description-header">Mobile <a href="javascript:;" data-fancybox="" data-modal="modal" data-src="#modal-edit-user" title="edit"><i class="far fa-edit text-sm text-muted" aria-hidden="true"></i></a></h5>
						  <big class="description-text">0899599955</big>
						</div>
						<!-- /.description-block -->
					  </div>
					  <!-- /.col -->
					  <div class="col-sm-4 border-right">
						<div class="description-block">
						  <h5 class="description-header">Gender  &amp; Birthdate</h5>
						  <span class="description-text"> Male | Age 38 <small class="d-block" style="margin-top: 2px">[20/07/1982]</small></span>
						</div>
						<!-- /.description-block -->
					  </div>
					  <!-- /.col -->
					  <div class="col-sm-4">
						<div class="description-block">
						  <h5 class="description-header">Email <a href="javascript:;" data-fancybox="" data-modal="modal" data-src="#modal-edit-user" title="edit"><i class="far fa-edit text-sm text-muted" aria-hidden="true"></i></a></h5>
						  <span class="description-text"><small class="d-block">thaweesub.p@oto.corp.com</small></span>
						</div>
						<!-- /.description-block -->
					  </div>
					  <!-- /.col -->
					</div>
					<!-- /.row -->
				  </div>
				<div class="card-footer card-footer _self-pa0">
					<ul class="nav flex-column">
					  <li class="nav-item">
						<a href="javascript:;" onclick="$('.list-sr-chat').addClass('show'); /*$(this).children('.status-send').addClass('hid')*/;$(this).children('.status-success').removeClass('hid');" class="nav-link text-dark">
						  <i class="fas fa-user-slash" aria-hidden="true"></i> ขาดงาน

						  <big class="status-success float-right badge text-success text-lg ml5-xs"> 0 วัน</big>
						</a>
					  </li>
					  <li class="nav-item">
						<a href="javascript:;" data-fancybox="" data-modal="modal" data-src="#modal-map" onclick="$('.list-sr-chat').addClass('show'); /*$(this).children('.status-send').addClass('hid')*/;$(this).children('.status-success').removeClass('hid');" class="nav-link text-dark">
						  <i class="fas fa-hospital-user" aria-hidden="true"></i> ลาป่วย
						  <big class="status-success float-right badge text-success text-lg ml5-xs"> 2 วัน</big>
						</a>
					  </li>
					  <li class="nav-item">
						<a href="javascript:;" onclick="$('.list-sr-chat').addClass('show'); $(this).children('.status-send').addClass('hid');$(this).children('.status-success,.status-accept').removeClass('hid');" class="nav-link text-dark">
						  <i class="fas fa-house-user" aria-hidden="true"></i> ลากิจ
						  <big class="status-success float-right badge text-success text-lg ml5-xs"> 0 วัน</big>
						</a>
					  </li>
					  <li class="nav-item">
						<a href="#" class="nav-link text-dark">
						  <i class="fas fa-user-clock" aria-hidden="true"></i> สาย
						  <big class="status-success float-right badge text-danger text-lg ml5-xs"> 15 นาที</big> 
						</a>
					  </li>
					  <li class="nav-item">
						<a href="#" class="nav-link text-dark">
						  <i class="fas fa-chalkboard-teacher" aria-hidden="true"></i> อื่นๆ
						  <big class="status-success float-right badge text-success text-lg ml5-xs"> 0 วัน</big> 
						</a>
					  </li>
					</ul>
				  </div>
				</div>
			</div>
	<!-- /popup -->			

<!--<div id="skin-loading" class="bg-wh" onclick="$(this).fadeOut();">
	<div class="lds-hourglass"></div>
</div>-->
<script>
	window.setTimeout(function(){
		$('#skin-loading').fadeOut();
	}, 3000);
</script>

<!-- footer -->
<?php include("incs/footer-web.html") ?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js-web.html") ?>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<link href="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.css" rel="stylesheet"/>
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<script type="text/javascript">
$( document ).ready( function () {
	 

	//select2
	$(".select2").select2({minimumResultsForSearch: -1});
	
	$(".js-example-tags").select2({
	   //dropdownAutoWidth : true,
	   placeholder: "Select",
	  width: '100%',
	  tags: true,
	  insertTag: function (data, tag) {
		// Insert the tag at the end of the results
		data.push(tag);
	  }
	});
	

});
  </script>
  


<!-- /js -->

</body>
</html>
