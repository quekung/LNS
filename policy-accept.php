<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top-web.html") ?>
<!-- /Top Head -->

<body class="app">
<!-- Headbar -->
<?php include("incs/header-web-en.html") ?>
<!-- /Headbar -->
  <!-- Contents -->      
    <div id="toc">
    
    <section id="terms" data-wow-delay="0.25s" class="main-terms wow fadeIn">

	<div class="bg-white _self-pt30-pb30-pl10-pr10 g-connect">
				<div class="container msg txt-c">
					<i class="fas fa-check-circle fa-5x t-green mb10"></i>
					<h2 style="line-height: 120%"><small>คุณได้อ่านและยอมรับ</small> <b class="block" style="font-weight: 400">ข้อกำหนดนโยบายคุ้มครองความเป็นส่วนตัวนี้</b> <small>แล้วค่ะ</small></h2>	
					
				</div>
			
	</div>
	
	</section>
      
      </div>
      <!-- /toc -->


<!-- footer -->
<?php include("incs/footer-web-en.html") ?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js-web.html") ?>



</body>
</html>
