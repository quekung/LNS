<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top-web.html") ?>
<!-- /Top Head -->

<body class="">
<!-- Headbar -->
<?php include("incs/header-web.html") ?>

<!-- /Headbar -->
<div id="app" class="container view-atn">
    <div class="close-page"><a href="javascript:;" onclick="clkExit()" title="close">ปิด</a></div>
    
    <div id="app-main" class="fix-h _flex middle-xs">
			<!-- box-->
					
					<div class="box-san">
							<div class="panel txt-c">
									<em><i class="fas fa-bell"></i> Not a Team member yet?. Click Activate here!</em> <a href="activate.php" title="Register" class="ui-btn-green2 btn-sm"> Activate </a>
							</div>
							
							<div class="bx-keep">
							
							<nav class="bulatin-nav">
				<ul class="_chd-cl-xs-04 txt-c">
					
					<li class="m-retext">
					<a href="recall-text.php" title="Recall by text">
						<i class="icon-r-text"><img src="di/menu-b1.png?v1"></i>
					 	<span>Recall by text <small>ค้นหาไฟล์</small></span>
					</a>
					</li>
					
					<li class="m-profile _self-cl-xs-08 bg-wh">
					<a class="bg-wh" href="index.php" title="Profile">
						<i class="icon-r-text"><img src="di/logo.png"></i>
					 	<span>Here comes your new assistant! <small>รู้จักกันมากขึ้น</small></span>
					</a>
					</li>
					
					<li class="m-reimg">
					<a href="recall-image.php" title="Recall by Image">
						<i class="icon-r-text"><img src="di/menu-b2.png?v1"></i>
					 	<span>Recall by Image <small>ค้นหาภาพ</small></span>
					</a>
					</li>
					
					<li class="m-calendar">
					<a href="calendar.php" title="Calendar">
						<i class="icon-r-text"><img src="di/menu-b3.png?v1"></i>
					 	<span>Calendar <small>ตารางนัด</small></span>
					</a>
					</li>
					
					
					
					<li class="m-contact">
					<a href="profile-team.php" title="Contact">
						<i class="icon-r-text"><img src="di/menu-b5.png?v1"></i>
					 	<span>My Profile <small>บัญชีของฉัน</small></span>
					</a>
					</li>
					
					<li class="m-membership _self-cl-xs-08">
					<a class="_flex _chd-cl-xs-06 middle-xs" href="membership.php" title="Contact">
						<i class="icon-r-text"><img src="di/menu-b7.png?v1"></i>
					 	<span class="txt-l">Membership <small>การเป็นสมาชิก</small></span>
					</a>
					</li>
					<li class="m-contact">
					<a href="broadcasts.php" title="Broadcasts">
						<i class="icon-r-text"><img src="di/menu-b6.png?v1"></i>
					 	<span>OA Broadcasts<small>ส่งข้อความกลุ่มสมาชิก</small></span>
					</a>
					</li>
					
					
					
					
				</ul>
			</nav>
			</div>
			
			</div>
			<!-- /box-->
    </div>
</div>

<div class="popup thm-trans" id="onOffBot">
	<div class="box-middle">
	<h2>You are closing the BOT?</h2>
	<p>
		<a data-fancybox-close class="ui-btn-gray" title="Cancel" href="javascript:;" onClick="parent.jQuery.fancybox.close();">Cancel</a>
		<a data-fancybox-close class="ui-btn-green" title="OK" href="javascript:;" onClick="$('.m-switch a').toggleClass('off'); parent.jQuery.fancybox.close();"> OK</a>
	</p>
	</div>
</div>


<!--<div id="skin-loading" class="bg-wh" onclick="$(this).fadeOut();">
	<div class="lds-hourglass"></div>
</div>-->
<script>
	window.setTimeout(function(){
		$('#skin-loading').fadeOut();
	}, 3000);
</script>

<!-- js -->
<?php include("incs/js.html") ?>
<!-- /js -->

</body>
</html>
