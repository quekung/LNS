<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top-web.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden">
<!-- Headbar -->
<?php include("incs/header-v2.html") ?>
<script>
$(".main-menu .list-unstyled>li.active").removeClass('active');
$(".main-menu .list-unstyled>li:nth-child(4)").addClass('active');
</script>
<!-- /Headbar -->
<div class="page-register">

    
    <div id="toc">
		<section class="z-broadcast mb0">
			<div class="bx-stepbar _self-pv20 cb-af container">
				<ul class="tabsbar">
						  <li><a href="e-hr-dashboard.php" title="Send Message"><i class="fas fa-bullhorn"></i> <span>Dashboard</span></a></li>
						  <li><a href="e-hr.php" title="Create Message" class="selected"><i class="fas fa-users"></i> <span>Employee</span></a></li>
						  <li><a href="e-hr-employee.php" title="User Detail"><i class="fas fa-users-cog"></i> <span>Import User</span></a></li>
						  <li><a href="e-hr-setting.php" title="Message Setting"><i class="fas fa-sliders-h"></i> <span>Message Setting</span></a></li>
						  <li><a href="e-hr-report.php" title="Report"><i class="fas fa-file-medical-alt"></i> <span>Report</span></a></li>
				  </ul>
			</div>
					

			
			<div class="contentTabs main-sv">
				<div id="tbc-1" class="msg">

					<div class="head-bg _self-mb0">
					<div class="container d-flex center-xs bottom-xs">
						<h2>Leave Request</h2>
					</div>
					</div>
					
					<div class="container pt30-sm pt10-xs">
					 <div class="type-register">
					 	 <h3 class="txt-c t-gray text-sm">กรุณาเลือกประเภทการลา</h3>
					 </div>
					
					  <form id="save_guest" class="form-checkout " method="POST" action="javascript:;" enctype="multipart/form-data">
						<fieldset class="fix-label">
						<ul class="form-list">   

							<li class="group">
								<div class="row">
									<div class="col-xs-12 mb20-xs">
									<div class="js-select wr">
										<select class="txt-box w-100 select2" data-placeholder="เลือกประเภทการลา" style="width:auto" onChange="$('#showResult').removeClass('hid');">
										<option value=""></option>
										<option value="A1">ลากิจ</option>
										<option value="J1">ลาอื่นๆ (Without pay) </option>
										<option value="C1">ลาพักผ่อน</option>
										<option value="S1">ลาป่วย</option>
										<option value="E1">ลาอุปสมบท</option>
										<option value="D1">ลาคลอด/ตรวจก่อนคลอด</option>
										<option value="F1">ลาทำหมัน</option>
										<option value="H1">ลาไปราชการทหาร</option>
										<option value="R1">ปฏิบัติงานนอกสถานที่</option>
										</select>
									</div>
									</div>
								</div>
								
								<div class="row">
									<div class="col-xs-12 mb20-xs">
										<textarea class="txt-area rounded-2 w-100" placeholder="เหตุผล"></textarea>
									</div>
								</div>
								
								<div class="d-flex pa10-xs bg-gray2 rounded-2">
									<label class="pl10-xs">วันที่ทำรายการ : </label>
									<span>10/08/2020</span>
								</div>

								<div class="row">
									<div class="left col-xs-12 col-sm-6">
										<div class="wr">
											<input type="text" class="txt-box bg-gray2" id="first_name" name="id_name" value="ทวีทรัพย์ พรสัจจะ"  readonly>
											<label for="first_name">ผู้ขออนุมัติ</label>
											<span class="line"></span>
										</div>
									</div>
									<div class="right col-xs-12 col-sm-6">
										<div class="wr">
											<input type="text" class="txt-box" id="last_name" name="id_emp" value="11002008" required>
											<label for="last_name">รหัสพนักงาน</label>
											<span class="line"></span>
										</div>
									</div>
								</div>
								
								<div class="row">
									 <div class="left col-xs-12 col-sm-6">
											<div class="wr">
												<input type="date" class="txt-box" id="birthdate" name="birthdate" required maxlength="10" onChange="$('#showTime').removeClass('hid')">
												<label for="StartDate">วันที่เริ่มต้น</label>
												<span class="line"></span>
											</div>
										</div>
										<div class="right col-xs-12 col-sm-6">
											<div class="wr">
												<input type="date" class="txt-box" id="birthdate" name="birthdate" required maxlength="10" onChange="$('#showTime').removeClass('hid')">
												<label for="EndDate">วันที่สิ้นสุด</label>
												<span class="line"></span>
											</div>
										</div>
										
										
								</div>
								<div id="showTime" class="hid row">
									 <div class="left col-xs-12 col-sm-6">
											<div class="wr js-select">
												<div class="_flex nowrap middle-xs">
												  <select name="ctl00$bodydetail$dropSTTime" id="bodydetail_dropSTTime" class="select-box w-100 select2">
		<option value="08:30">08:30</option>
		<option value="09:00">09:00</option>
		<option value="09:30">09:30</option>
		<option value="10:00">10:00</option>
		<option value="10:30">10:30</option>
		<option value="11:00">11:00</option>
		<option value="11:30">11:30</option>
		<option value="13:00">13:00</option>
		<option value="13:30">13:30</option>
		<option value="14:00">14:00</option>
		<option value="14:30">14:30</option>
		<option value="15:00">15:00</option>
		<option value="15:30">15:30</option>
		<option value="16:00">16:00</option>
		<option value="16:30">16:30</option>
		<option value="17:00">17:00</option>

	</select>
												  <i class="_self-ml10 far fa-clock" onClick="$('#time').focus();"></i>
												</div>
												<label for="staff_mobile">เวลาเริ่มต้น</label>
												<span class="line"></span>
											</div>
										</div>
										<div class="right col-xs-12 col-sm-6">
											<div class="wr js-select">
												<div class="_flex nowrap middle-xs">
												  <select name="ctl00$bodydetail$dropENTime" id="bodydetail_dropENTime" class="select-box w-100 select2">
		<option value="09:00">09:00</option>
		<option value="09:30">09:30</option>
		<option value="10:00">10:00</option>
		<option value="10:30">10:30</option>
		<option value="11:00">11:00</option>
		<option value="11:30">11:30</option>
		<option value="12:00">12:00</option>
		<option value="13:30">13:30</option>
		<option value="14:00">14:00</option>
		<option value="14:30">14:30</option>
		<option value="15:00">15:00</option>
		<option value="15:30">15:30</option>
		<option value="16:00">16:00</option>
		<option value="16:30">16:30</option>
		<option value="17:00">17:00</option>
		<option value="17:30">17:30</option>

	</select>
												  <i class="_self-ml10 far fa-clock" onClick="$('#time').focus();"></i>
												</div>
												<label for="email">เวลาสิ้นสุด</label>
												<span class="line"></span>
											</div>
										</div>
								</div>
							

							</li>
							
							<li class="approveBy">
							<div class="row">
								<div class="left col-xs-12 col-sm-6">
										<div class="wr">
											<div class="_flex nowrap middle-xs">
												<input type="text" class="txt-box bg-gray2" id="Approval1" name="Approval1" value="head Level1" readonly>
												<i class="_self-ml10 far fa-check-circle t-green"></i>
											</div>
											<label for="Approval1">ผู้อนุมัติ 1</label>
											<span class="line"></span>
										</div>
									</div>
									<div class="right col-xs-12 col-sm-6">
										<div class="wr">
											<div class="_flex nowrap middle-xs">
												<input type="text" class="txt-box bg-gray2" id="Approval2" name="Approval2" value="head Level2" readonly>
												<i class="_self-ml10 far fa-check-circle t-green"></i>
											</div>
											<label for="Approval2">ผู้อนุมัติ 2</label>
											<span class="line"></span>
										</div>
									</div>

							</div>
							</li>
							
							<li id="showResult" class="hid sum mt20-xs">
								<div class="bg-light pa20-xs rounded-2">
									<h2 class="text-lg f-normal">ลาป่วย : 1 วัน <small class="f-lite">(เหตุผล)</small></h2>
									<p>เริ่มวันที่ <u>6 ส.ค. 2563</u> ถึง <u>6 ส.ค. 2563</u></p>
								</div>
							</li>

							<li class="ctrl-btn txt-c">
								<button id="btnSubmit" class="ui-btn-green btn-lg" type="submit" value="submit" data-fancybox="success" data-src="#popupCredit" title="ส่งข้อมูล">ยืนยัน</button>
							</li>
							
							<li>
							<!-- card -->
								<div class="card bg-white rounded mt30-xs border-0">
									<div class="card-header">
										<h3 class="card-title center-xs" onClick="$(this).toggleClass('active').parent().next('.card-body').slideToggle();"><b>Leave rights <i class="fas fa-angle-down"></i></b></h3>
									</div>
									<div class="card-body middle-xs" style="display: none">
										<div class="table-resp">
											<table class="table tb-bordered tb-skin">
											  <thead>
                                                    <tr>
                                                        <th class="text-center">ประเภทการลา
                                                    <h6 class="text-light space-top Elable">Leave Type</h6>
                                                        </th>
                                                        <th class="text-center">สิทธิการลา/ปี
                                                    <h6 class="text-light space-top Elable">Quota</h6>
                                                        </th>
                                                        <th class="text-center">ใช้ไป
                                                   <h6 class="text-light space-top Elable">Used</h6>
                                                        </th>
                                                        <th class="text-center">กำลังขออนุมัติ
                                                    <h6 class="text-light space-top Elable">Request</h6>
                                                        </th>
                                                        <th class="text-center">คงเหลือ
                                                              <h6 class="text-light space-top Elable">Remain</h6>
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <tr>
                                                        <td>
                                                            <span id="bodydetail_lbl_name_A1">ลากิจ</span>
                                                            <h6 class="text-muted space-top Elable">Personal Leave</h6>
                                                        </td>
                                                        <td style="text-align: right">
                                                            <span id="bodydetail_lbl_total_A1">6 วัน </span>
                                                        </td>
                                                        <td style="text-align: right">
                                                            <span id="bodydetail_lbl_used_A1">2 วัน 1  ชั่วโมง 30  นาที </span></td>

                                                        <td style="text-align: right">
                                                            <span id="bodydetail_lbl_rest_A1">-</span></td>
                                                        <td style="text-align: right">
                                                            <span id="bodydetail_lbl_remain_A1">3 วัน 6  ชั่วโมง 30  นาที </span></td>





                                                        </tr><tr>
                                                            <td>
                                                                <span id="bodydetail_lbl_name_C1">ลาพักผ่อน</span>
                                                                <h6 class="text-muted space-top Elable">Vacation Leave</h6>
                                                            </td>
                                                            <td style="text-align: right">
                                                                <span id="bodydetail_lbl_total_C1">10 วัน </span>
                                                            </td>
                                                            <td style="text-align: right">
                                                                <span id="bodydetail_lbl_used_C1">5 วัน </span>
                                                            </td>
                                                            <td style="text-align: right">
                                                                <span id="bodydetail_lbl_rest_C1">-</span>
                                                            </td>
                                                            <td style="text-align: right">
                                                                <span id="bodydetail_lbl_remain_C1">5 วัน </span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <span id="bodydetail_lbl_name_S1">ลาป่วย</span>
                                                                <h6 class="text-muted space-top Elable">Sick Leave</h6>
                                                            </td>
                                                            <td style="text-align: right">
                                                                <span id="bodydetail_lbl_total_S1">30 วัน </span>
                                                            </td>
                                                            <td style="text-align: right">
                                                                <span id="bodydetail_lbl_used_S1">3 วัน </span>
                                                            </td>
                                                            <td style="text-align: right">
                                                                <span id="bodydetail_lbl_rest_S1">-</span>
                                                            </td>
                                                            <td style="text-align: right">
                                                                <span id="bodydetail_lbl_remain_S1">27 วัน </span>
                                                            </td>
                                                        </tr>
                                                        
                                                        
                                                       
                                                        <tr>
                                                            <td>
                                                                <span id="bodydetail_lbl_name_D1">ลาคลอด/ตรวจก่อนคลอด</span>
                                                                <h6 class="text-muted space-top Elable">Maternity Leave</h6>
                                                            </td>
                                                            <td style="text-align: right">
                                                                <span id="bodydetail_lbl_total_D1">-</span>
                                                            </td>
                                                            <td style="text-align: right">
                                                                <span id="bodydetail_lbl_used_D1">-</span>
                                                            </td>
                                                            <td style="text-align: right">
                                                                <span id="bodydetail_lbl_rest_D1">-</span>
                                                            </td>
                                                            <td style="text-align: right">
                                                                <span id="bodydetail_lbl_remain_D1">-</span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <span id="bodydetail_lbl_name_H1">ลารับราชการทหาร</span>
                                                                <h6 class="text-muted space-top Elable">Military Service Leave</h6>
                                                            </td>
                                                            <td style="text-align: right">
                                                                <span id="bodydetail_lbl_total_H1">-</span>

                                                            </td>
                                                            <td style="text-align: right">
                                                                <span id="bodydetail_lbl_used_H1">-</span>
                                                            </td>
                                                            <td style="text-align: right">
                                                                <span id="bodydetail_lbl_rest_H1">-</span>
                                                            </td>
                                                            <td style="text-align: right">
                                                                <span id="bodydetail_lbl_remain_H1">-</span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <span id="bodydetail_lbl_name_E1">ลาอุปสมบท</span>
                                                                <h6 class="text-muted space-top Elable">Ordination Leave</h6>
                                                            </td>
                                                            <td style="text-align: right">
                                                                <span id="bodydetail_lbl_total_E1">120 วัน </span>
                                                            </td>
                                                            <td style="text-align: right">
                                                                <span id="bodydetail_lbl_used_E1">-</span>
                                                            </td>
                                                            <td style="text-align: right">
                                                                <span id="bodydetail_lbl_rest_E1">-</span>
                                                            </td>
                                                            <td style="text-align: right">
                                                                <span id="bodydetail_lbl_remain_E1">120 วัน </span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <span id="bodydetail_lbl_name_F1">ลาทำหมัน</span>
                                                                <h6 class="text-muted space-top Elable">Sterilization Leave</h6>
                                                            </td>
                                                            <td style="text-align: right">
                                                                <span id="bodydetail_lbl_total_F1">-</span>
                                                            </td>
                                                            <td style="text-align: right">
                                                                <span id="bodydetail_lbl_used_F1">-</span>
                                                            </td>
                                                            <td style="text-align: right">
                                                                <span id="bodydetail_lbl_rest_F1">-</span>
                                                            </td>
                                                            <td style="text-align: right">
                                                                <span id="bodydetail_lbl_remain_F1">-</span>
                                                            </td>
                                                        </tr>

                                                    


                                                </tbody>

											</table>
										  </div>
										  
										  <!--<div class="sticky-bottom card-footer">
											<div class="__chd-ph10 center-xs">
													<button type="button" class="ui-btn-green btn-md" onclick="$('.form-sending')[0].reset();"><i class="fas fa-file-excel"></i> Export Excel</button>
											</div>
										  </div>-->
									</div>
									
									
								</div>
								<!-- /card -->
							</li>
						</ul>


						</fieldset>
					</form>
					
					
					
					</div>
					

				</div>
			</div>
			
			
			
		</section>
    </div>
</div>

<!--<div id="skin-loading" class="bg-wh" onclick="$(this).fadeOut();">
	<div class="lds-hourglass"></div>
</div>-->
<script>
	window.setTimeout(function(){
		$('#skin-loading').fadeOut();
	}, 3000);
</script>

<!-- footer -->
<?php include("incs/footer-web.html") ?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js-web.html") ?>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />


<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>

<script type="text/javascript">
$( document ).ready( function () {
	


	//select2
	$(".select2").select2({minimumResultsForSearch: -1});
	//check type
	$("[name=type-user]").on("click", function() {
		var isCase = this.value;
		if(isCase == 0) {
			//alert("Employee");
			$('#employee').show();
			$('#student').hide();
			$('#user').hide();
		} else if(isCase == 1) {
			//alert("Student");
			$('#employee').hide();
			$('#student').show();
			$('#user').hide();
		} else if(isCase == 2) {
			//alert("User");
			$('#employee').hide();
			$('#student').hide();
			$('#user').show();
		}

	})


});
  </script>
  

  


<!-- /js -->

</body>
</html>
