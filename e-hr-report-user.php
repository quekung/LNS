<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top-web.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden">
<!-- Headbar -->
<?php include("incs/header-v2.html") ?>
<script>
$(".main-menu .list-unstyled>li.active").removeClass('active');
$(".main-menu .list-unstyled>li:nth-child(4)").addClass('active');
</script>
<!-- /Headbar -->
<div class="page-checkout">

    
    <div id="toc">
		<section class="z-broadcast _self-pt0 mb0">
			<div class="bx-stepbar _self-pv20 cb-af container">
				<ul class="tabsbar">
						  <li><a href="e-hr-dashboard.php" title="Send Message"><i class="fas fa-bullhorn"></i> <span>Dashboard</span></a></li>
						  <li><a href="e-hr.php" title="Create Message"><i class="fas fa-users"></i> <span>Employee</span></a></li>
						  <li><a href="e-hr-employee.php" title="User Detail"><i class="fas fa-users-cog"></i> <span>Import User</span></a></li>
						  <li><a href="e-hr-setting.php" title="Message Setting"><i class="fas fa-sliders-h"></i> <span>Message Setting</span></a></li>
						  <li><a href="e-hr-report.php" title="Report" class="selected"><i class="fas fa-file-medical-alt"></i> <span>Report</span></a></li>
				  </ul>
			</div>
					

			
			<div class="bg-gray2 contentTabs">
				<div id="tbc-1" class="msg">
					<div class="head-bg">
					<div class="container">
						<h2>E-HR</h2>	
					</div>
					</div>
					<div class="wrap-full _chd-cl-xs-12 _chd-cl-sm">
						<div class="main row center-xs">
							<div class="container">
							
								<div class="sort-bar d-flex between-xs middle-xs">
									<div class="sort">
										<select class="form-control select2" data-placeholder="Search by" style="width:auto">
										  <option></option>
										  <option>ชื่อ-นามสกุล</option>
										  <option>ฝ่าย</option>
										  <option>แผนก</option>
										  <option>ตำแหน่ง</option>
										</select>
										<div class="search-sm d-inline">
											<input class="txt-box" placeholder="ค้นหา...">
											<button type="submit" class="fas fa-search" aria-hidden="true"></button>
										</div>
									</div>
									<div class="right">
										<div id="reportrange" class="date-range pr20-xs">
											<i class="fa fa-calendar"></i>&nbsp;
											<span></span> <i class="fa fa-caret-down"></i>
										</div>
									</div>

								</div>
							
								<!-- card -->
								<div class="card bg-white rounded mt20-xs border-0">
									<div class="card-header">
										<h3 class="card-title center-xs"><b>Attendance by User Report</b></h3>
									</div>
									<div class="card-body _self-pa30-pt0 middle-xs">
										<div class="table-resp">
											<table class="table tb-bordered tb-skin">
											  <thead>
											  <tr>
												<th class="text-center" align="center">no</th>
												<th class="text-center" align="center">วันที่</th>
												<th class="text-center" align="center">เข้า</th>
												<th class="text-center" align="center">ออก</th>
												<th class="text-center" align="center">เวลาเข้า</th>
												<th class="text-center" align="center">เวลาออก</th>
												<th class="text-center" align="center">สาย ชม.นาที</th> 
												<th class="text-center" align="center">ขาด ชม.นาที</th> 
												<th class="text-center" align="center">Status</th> 
												<th class="text-center" align="center">หมายเหตุ</th> 
											  </tr>
											  </thead>
											  <tbody>
											  <tr>
												<td align="center">1</td>
												<td align="center">01/06/2020</td>
												<td align="center">08.30</td>
												<td align="center">17.30</td>
												<td align="center">08.31</td>
												<td align="center">17.42</td>
												<td align="center">0.01</td>
												<td align="center">-</td>
												<td align="center">-</td>
												<td align="center">-</td>
											  </tr>
											  
											  <tr>
												<td align="center">2</td>
												<td align="center">02/06/2020</td>
												<td align="center">08.30</td>
												<td align="center">17.30</td>
												<td align="center">-</td>
												<td align="center">-</td>
												<td align="center">-</td>
												<td align="center">-</td>
												<td align="center">-</td>
												<td align="center">ลาพักผ่อน</td>
											  </tr>
											  
											  <tr>
												<td align="center">3</td>
												<td align="center">03/06/2020</td>
												<td align="center">08.30</td>
												<td align="center">17.30</td>
												<td align="center">08.11</td>
												<td align="center">12.00</td>
												<td align="center">-</td>
												<td align="center">4:30</td>
												<td align="center">-</td>
												<td align="center">-</td>
											  </tr>
											  <?php for($i=4;$i<=31;$i++){ ?>
											  <tr>
												<td align="center"><?php echo $i ?></td>
												<td align="center"><?php if($i<=9){ ?>0<? } ?><?php echo $i ?>/06/2020</td>
												<td align="center">08.30</td>
												<td align="center">17.30</td>
												<td align="center">08.30</td>
												<td align="center">17.33</td>
												<td align="center">-</td>
												<td align="center">-</td>
												<td align="center">-</td>
												<td align="center">-</td>
											  </tr>
											  <?php } ?>
											  
											  
											 
											  
											  
											  </tbody>
											</table>
										  </div>
									</div>
									
									<div class="sticky-bottom card-footer">
									<div class="__chd-ph10 center-xs">
											<button type="button" class="ui-btn-green btn-md" onclick="$('.form-sending')[0].reset();"><i class="fas fa-file-excel"></i> Export Excel</button>
									</div>
								  </div>
								</div>
								<!-- /card -->
							</div>

						</div>
					</div>
				</div>
					</form>

			</div>
			
			
			
			
		</section>
    </div>
</div>

<!--<div id="skin-loading" class="bg-wh" onclick="$(this).fadeOut();">
	<div class="lds-hourglass"></div>
</div>-->
<script>
	window.setTimeout(function(){
		$('#skin-loading').fadeOut();
	}, 3000);
</script>

<!-- footer -->
<?php include("incs/footer-web.html") ?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js-web.html") ?>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<link href="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.css" rel="stylesheet"/>
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
<link href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" rel="stylesheet" />

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript">
$( document ).ready( function () {
	 $(function() {

    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end);

});


	//select2
	$(".select2").select2({minimumResultsForSearch: -1});
	//select2
	$('.keep-select-group').select2({
    	placeholder: "Please select",
    	//allowClear: true,
		dropdownAutoWidth : true,
		width: '100%'
	});
	
	$('#uid-unlimit').change( function() {
			var isChecked = this.checked;
			if(isChecked) {
				$("#uid-limit").removeClass("bg-white");
				$("#uid-limit").prop("disabled",true); 
				$("#uid-limit").prop("required",false); 
			} else {
				$("#uid-limit").addClass("bg-white");
				$("#uid-limit").prop("disabled",false);
				$("#uid-limit").prop("required",true);
			}
		});
	

});
  </script>
  


<!-- /js -->

</body>
</html>
