<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top-web.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-hidden">
<!-- Headbar -->
<header id="gb-header">
<div class="container">
    <h1 itemscope="" itemtype="//schema.org/Organization" class="logo">
        <a href="/" itemprop="url">Inno Hub Company Limited</a>
        <img itemprop="logo" src="di/logo.png">
    </h1>
    
    <!--<div id="main-nav" class="fix-r">
		<a class="login" href="javascript:;" onclick="$(this).hide(); $('#u-member').show();">Login</a>
	</div>-->
	
</div>
</header>
<?php /*?><?php include("incs/header-v2.html") ?><?php */?>
<!-- /Headbar -->
<div class="page-login" style="min-height: calc(100vh - 85px)">

    
    <div id="toc" class="container">

		
        	<form class="form-signin form-keep" method="post" action="profile-team.php" style="width: 400px; padding: 5vh 0; margin: 0 auto; max-width: 100% <?php /*?>border: 1px solid #f0f0f0; padding:0 10px 20px;border-radius: 1em<?php */?>">
				<fieldset>
					<legend><h3 class="head txt-c"><span>Login</span></h3></legend>
					<div class="wr">
						<input type="text" class="txt-box" id="group_name" name="group_name" required="">
						<label for="group_name">Username</label>
						<span class="line"></span>
					</div>
					<div class="wr">
						<input type="password" class="txt-box" id="group_name" name="group_name" required="">
						<label for="group_name">Password</label>
						<span class="line"></span>
					</div>
					
					<div class="mt10 txt-c">
						<em class="t-white">Not a member yet?. Click Register here!</em> <a href="register-web.php" title="Register" class="ui-btn-white-mini-cr btn-xs">Register</a>
					</div>
					
					<div class="ctrl-btn _self-mt20 fix-bottom">
						<input type="submit" class="ui-btn-white btn-lg btn-block" value="Sign In">
					</div>
				</fieldset>
			</form>
	
    </div>
</div>

<!--<div id="skin-loading" class="bg-wh" onclick="$(this).fadeOut();">
	<div class="lds-hourglass"></div>
</div>-->
<script>
	window.setTimeout(function(){
		$('#skin-loading').fadeOut();
	}, 3000);
</script>


<!-- js -->
<?php include("incs/js-web.html") ?>
<!-- /js -->

</body>
</html>
