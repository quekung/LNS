<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top-web.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden">
<!-- Headbar -->
<?php include("incs/header-v2.html") ?>
<script>
$(".main-menu .list-unstyled>li.active").removeClass('active');
$(".main-menu .list-unstyled>li:nth-child(4)").addClass('active');
</script>
<!-- /Headbar -->
<div class="page-checkout">

    
    <div id="toc">
		<section class="z-broadcast _self-pt0 mb0">
			<div class="bx-stepbar _self-pv20 cb-af container">
				<ul class="tabsbar">
						  <li><a href="broadcasts.php" title="Send Message"><i class="fas fa-bullhorn"></i> <span>Send Message</span></a></li>
						  <li><a href="broadcasts-create.php" title="Create Message"><i class="fas fa-layer-group"></i> <span>Create Template</span></a></li>
						  <li><a href="broadcasts-acc.php" title="User Detail" class="selected"><i class="fas fa-users-cog"></i> <span>User Detail</span></a></li>
						  <li><a href="broadcasts-setting.php" title="Message Setting"><i class="fas fa-sliders-h"></i> <span>Message Setting</span></a></li>
						  <li><a href="broadcasts-report.php" title="Report"><i class="fas fa-file-medical-alt"></i> <span>Report</span></a></li>
						  <li><a href="broadcasts-survey.php" title="Survey"><i class="fas fa-tasks"></i> <span>Survey</span></a></li>
				  </ul>
			</div>
					

			
			<div class="bg-gray2 contentTabs">
				<div id="tbc-1" class="msg">
					<div class="form-checkout form-sending'">
					<div class="head-title container txt-c">
						<h2 class="t-black"><i class="fas fa-users-cog _self-mr10 t-black"></i> User Detail</h2>	
						<p>ระบบส่งข้อความผ่านทาง line</p>
					</div>
					<div class="wrap-full _chd-cl-xs-12 _chd-cl-sm">
						<div class="main">
							<div class="container">
								
								<!-- card -->
								<div class="card bg-white">
									<div class="card-header _flex center-xs between-xsh">
										<ul id="type-user" class="idTabs _self-pa0 tab-receiver">
											<li><a href="broadcasts-acc.php" ><i class="fas fa-users"></i> Line Groups</a></li>
											<li><a href="broadcasts-acc-user.php" class="selected"><i class="fas fa-user"></i> Users</a></li>
										</ul>
										<!--<div class="search-sm _self-mr20">
											<input class="txt-box" placeholder="ค้นหา...">
											<button type="submit" class="fas fa-search"></button>
										</div>-->
										
									</div>
									
									
												
												
									<div class="group-tab-user bg-gray3 pa20-xs _flex center-xs between-xsh">
										<ul id="type-user" class="idTabs tab-btn _chd-mr10">
											<li><a href="#lineuser" class="selected"><i class="fas fa-user-friends"></i> Line User</a></li>
											<li><a href="#registered"><i class="fas fa-user-check"></i> Registered</a></li>
											<li><a href="#import"><i class="fas fa-file-import"></i> Import User (.csv)</a></li>
										</ul>
										
										<div class="user-sort-bar d-flex flex-nowrap middle-xs">
											<a id="sw-adv-srh" href="javascript:;" onClick="$(this).toggleClass('active'); $('#advance-srh').slideToggle();"><i class="fas fa-sliders-h"></i></a>
											<div class="wrap-search">
											<div class="search-sm">
												<input class="txt-box" placeholder="ค้นหา..." style="width: 140px">
												<button type="submit" class="fas fa-search"></button>
											</div>
											<div id="advance-srh" style="display: none">
													<ul>
														
														
														<li>
															<label>Status</label>
															<div class="row">
															<div class="left _self-cl-xs-06">
																<div class="icheck-gray pl10-xs"><input type="radio" name="status" id="approve"> <label for="approve" class="f-normal">approve</label></div>
															</div>
															<div class="right _self-cl-xs-06">
																<div class="icheck-gray pl10-xs"><input type="radio" name="status" id="notApprove"> <label for="notApprove" class="f-normal">Not approved</label></div>
															</div>
															</div>
														</li>
														
														<li>
															<label>Detail</label>
															<div class="d-flex _chd-mr20">
															<div class="left">
																<div class="icheck-gray pl10-xs"><input type="radio" name="step" id="u-added"> <label for="u-added" class="f-normal">Added freind</label></div>
															</div>
															<div class="mid">
																<div class="icheck-gray pl10-xs"><input type="radio" name="step" id="u-registered"> <label for="u-registered" class="f-normal">Registered</label></div>
															</div>
															<div class="right">
																<div class="icheck-gray pl10-xs"><input type="radio" name="step" id="u-completed"> <label for="u-completed" class="f-normal">Completed</label></div>
															</div>
															</div>
														</li>
														
														<li>
															<label>Gender</label>
															<div class="d-flex _chd-mr20">
															  <div class="icheck-gray pl10-xs"><input type="checkbox" name="Male" id="Male"> <label for="Male" class="f-normal">Male</label></div>
															  <div class="icheck-gray pl10-xs"><input type="checkbox" name="Female" id="Female"> <label for="Female" class="f-normal">Female</label></div>
															  <div class="icheck-gray pl10-xs"><input type="checkbox" name="LGBT" id="LGBT"> <label for="LGBT" class="f-normal">LGBT</label></div>
															</div>
														</li>
														
														<li>
															<label>Department</label>
															<select class="form-control js-select-multi" multiple="multiple">
															  <option>IT</option>
															  <option>Accounting</option>
															  <option>Finance</option>
															  <option>Sale</option>
															  <option>Maketing</option>
															  <option>Call Center</option>
															  <option>HR</option>
															  <option>Reseption</option>
															</select>
														</li>
														
														<li class="ctrl-btn txt-c">
															<button type="reset" class="ui-btn-gray2 btn-sm">Reset</button>
															<button type="submit" class="ui-btn-green btn-sm">Submit</button>
														</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
									
									<div class="contentTabs">
										<!-- added -->
										<div id="lineuser" class="card-body _self-pa30 middle-xs">
										<div class="table-resp off">
											<table class="table tb-bordered tb-skin" style="min-width: 900px" width="100%" cellspacing="0" cellpadding="0" border="0">
											<thead>
											<tr>
											<th scope="col" align="center">NO.</th>
											<th scope="col" align="center">Line</th>
											<th scope="col" align="center">รหัสพนักงาน</th>
											<th scope="col" align="center">ชื่อ - นามสกุล</th>
											<th scope="col" align="center">บริษัท</th>
											<th scope="col" align="center">ฝ่าย</th>
											<th scope="col" align="center">แผนก</th>
											<th scope="col" align="center">ตำแหน่ง</th>
											<th scope="col" align="center">ระดับงาน</th>
											<th scope="col" align="center"><i class="fab fa-line text-success"></i> Add Friend</th>
											<th scope="col">&nbsp;</th>
											</tr>
											</thead>
											<tbody>
											<?php for($i=1;$i<=15;$i++){ ?>
											<tr>
											<td valign="middle" align="center"><?php echo $i; ?></td>
											<td valign="middle">
												<?php if($i%3==0) {?><img class="rounded2" src="https://www.w3schools.com/w3images/avatar4.png" alt="Fat Rascal" width="40"><? } else { ?><img class="rounded2" src="https://www.w3schools.com/w3images/avatar3.png" alt="Fat Rascal" width="40"><? } ?> <span class="name"><?php echo generateRandomString(); ?></span>
											</td>
											<td valign="middle"><?php if($i<=6) {?> <?php } else { ?>1100200<?php echo $i+1; ?><?php } ?></td>
											<td valign="middle"><?php if($i<=6) {?> <?php } else { ?>ทวีทรัพย์ พรสัจจะ<?php } ?></td>
											<td valign="middle" align="center"><?php if($i<=6) {?> <?php } elseif($i%3==0) {?>INNOHUB<?php } else { ?> OTO <?php } ?></td>
											<td valign="middle" align="center"><?php if($i<=6) {?> <?php } elseif($i%3==0) {?>Production<?php } elseif($i%5==0) {?> IT <?php } elseif($i%7==0) {?>HR<?php } else { ?> Admin <?php } ?></td>
											<td valign="middle" align="center"><?php if($i<=6) {?> <?php } elseif($i%3==0) {?>Content<?php } elseif($i%5==0) {?> Platform <?php } elseif($i%7==0) {?>Interview<?php } else { ?> Officer <?php } ?></td>
											<td valign="middle" align="center"><?php if($i<=6) {?> <?php } elseif($i%3==0) {?>Web Design<?php } elseif($i%5==0) {?> Developer <?php } elseif($i%7==0) {?>Supervisor<?php } else { ?> Officer <?php } ?></td>
											<td valign="middle" align="center"><?php if($i<=6) {?> <?php } elseif($i%3==0) {?>Manager<?php } elseif($i%5==0) {?> Assitant <?php } else { ?> Straff <?php } ?></td>
											<td valign="middle" align="center"><?php if($i<=2) {?> <?php } elseif($i%2==0 || $i%3==0) {?><span class="text-success">Added</span><?php } else { ?><?php } ?></td>											
											<td valign="middle" align="center">
												<a class="more ui-btn-blue-min btn-xs" href="javascrtip:;" data-fancybox="" data-src="#userdetail" title="ดูรายละเอียด"><i class="fas fa-eye"></i> view</a> 
												<!--<a href="#" title="View Leave" class="ui-btn-trans-mid btn-xs"><i class="fas fa-calendar-day"></i></a>
												<a href="#" title="View Time Attendance" class="ui-btn-trans-mid btn-xs"><i class="fas fa-user-clock"></i></a>-->
												<a href="javascrtip:;" data-fancybox="" data-src="#acc-edit-user" title="Edit" class="ui-btn-trans-mid btn-xs"><i class="fas fa-edit"></i></a>
											</td>
											</tr>
											<?php } ?>
											</tbody>
											</table>
											

										</div>
									</div>
									
										<!-- Regitered -->
										<div id="registered" class="card-body _self-pa30 middle-xs">
										<div class="table-resp off">
											<table class="table tb-bordered tb-skin" style="min-width: 900px" width="100%" cellspacing="0" cellpadding="0" border="0">
											<thead>
											<tr>
											<th scope="col" align="center">NO.</th>
											<th scope="col" align="center">รหัสพนักงาน</th>
											<th scope="col" align="center">ชื่อ - นามสกุล</th>
											<th scope="col" align="center">บริษัท</th>
											<th scope="col" align="center">ฝ่าย</th>
											<th scope="col" align="center">แผนก</th>
											<th scope="col" align="center">ตำแหน่ง</th>
											<th scope="col" align="center">ระดับงาน</th>
											<th scope="col" align="center">Line</th>
											<th scope="col" align="center"><i class="fab fa-line text-success"></i> <small>Add Friend</small></th>
											<th scope="col" align="center"><small>APPROVED</small></th>
											<th scope="col">&nbsp;</th>
											</tr>
											</thead>
											<tbody>
											<?php for($i=1;$i<=9;$i++){ ?>
											<tr>
											<td valign="middle" align="center"><?php echo $i; ?></td>
											<td valign="middle">1100200<?php echo $i+1; ?></td>
											<td valign="middle">ทวีทรัพย์ พรสัจจะ</td>
											<td valign="middle" align="center"><?php if($i%3==0) {?>INNOHUB<?php } else { ?> OTO <?php } ?></td>
											<td valign="middle" align="center"><?php if($i%3==0) {?>Production<?php } elseif($i%5==0) {?> IT <?php } elseif($i%7==0) {?>HR<?php } else { ?> Admin <?php } ?></td>
											<td valign="middle" align="center"><?php if($i%3==0) {?>Content<?php } elseif($i%5==0) {?> Platform <?php } elseif($i%7==0) {?>Interview<?php } else { ?> Officer <?php } ?></td>
											<td valign="middle" align="center"><?php if($i%3==0) {?>Web Design<?php } elseif($i%5==0) {?> Developer <?php } elseif($i%7==0) {?>Supervisor<?php } else { ?> Officer <?php } ?></td>
											<td valign="middle" align="center"><?php if($i%3==0) {?>Manager<?php } elseif($i%5==0) {?> Assitant <?php } else { ?> Straff <?php } ?></td>
											<td valign="middle">
												<?php if($i%3==0) {?><img class="rounded2" src="https://www.w3schools.com/w3images/avatar4.png" alt="Fat Rascal" width="40"><? } else { ?><img class="rounded2" src="https://www.w3schools.com/w3images/avatar3.png" alt="Fat Rascal" width="40"><? } ?> <span class="name"><?php echo generateRandomString(); ?></span>
											</td>
											<td valign="middle" align="center"><?php if($i<=2) {?> <?php } elseif($i%2==0 || $i%3==0) {?><span class="text-success">Added</span><?php } else { ?><?php } ?></td>	
											<td valign="middle" align="center">
												<div class="icheck-primary ma0">
													<input type="checkbox" name="approve<? echo($i) ?>" id="approve-chk<? echo($i) ?>"> 
													<label for="approve-chk<? echo($i) ?>"><span class="visible-xs"> Approve</span></label>
												  </div>
											</td>
											<td valign="middle" align="center">
												<a class="more ui-btn-blue-min btn-xs" href="javascrtip:;" data-fancybox="" data-src="#userdetail" title="ดูรายละเอียด"><i class="fas fa-eye"></i> view</a> 
												<!--<a href="#" title="View Leave" class="ui-btn-trans-mid btn-xs"><i class="fas fa-calendar-day"></i></a>
												<a href="#" title="View Time Attendance" class="ui-btn-trans-mid btn-xs"><i class="fas fa-user-clock"></i></a>-->
												<a href="javascrtip:;" data-fancybox="" data-src="#acc-edit-user" title="Edit" class="ui-btn-trans-mid btn-xs"><i class="fas fa-edit"></i></a>
											</td>
											</tr>
											<?php } ?>
											</tbody>
											</table>
											

										</div>
									</div>

										<!-- import csv -->
										<div id="import" class="card-body _self-pa30 middle-xs">
										
										<div id="csv-upload" class="pa20-xs ba-light rounded mb30-xs">
											<form action="/file-upload">
												<div class="dropzone">
												</div>
											</form>
											<div class="txt-c mt10-xs">
												<a href="javascript:;" onClick="$('#listImport').removeClass('hid'); $('#csv-upload').addClass('hid');" class="ui-btn-green2">Submit <i class="fas fa-angle-right"></i></a>
											</div>
										</div>	
										
										<div id="listImport" class="hid table-resp off">
											<div class="mb10-xs d-flex end-xs">
												<a href="javascrtip:;" data-fancybox="" data-src="#acc-add-user" class="ui-btn-green2 btn-sm" title="Add Menual User"><i class="fas fa-user-plus"></i> Add Menual User</a>
											</div>
											
											<table class="table tb-bordered tb-skin" style="min-width: 900px" width="100%" cellspacing="0" cellpadding="0" border="0">
											<thead>
											<tr>
											<th scope="col" align="center">NO.</th>
											<th scope="col" align="center">รหัสพนักงาน</th>
											<th scope="col" align="center">ชื่อ - นามสกุล</th>
											<th scope="col" align="center">บริษัท</th>
											<th scope="col" align="center">ฝ่าย</th>
											<th scope="col" align="center">แผนก</th>
											<th scope="col" align="center">ตำแหน่ง</th>
											<th scope="col" align="center">ระดับงาน</th>
											<th scope="col" align="center">Line</th>
											<th scope="col" align="center"><i class="fab fa-line text-success"></i> <small>Add Friend</small></th>
											<th scope="col" align="center"><small>APPROVED</small></th>
											<th scope="col">&nbsp;</th>
											</tr>
											</thead>
											<tbody>
<?php function generateRandomString($length = 10) {
    $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
} ?>
											<?php for($i=1;$i<=20;$i++){ ?>
											<tr>
											<td valign="middle" align="center"><?php echo $i; ?></td>
											<td valign="middle"><?php if($i<=2) {?> <?php } else { ?>1100200<?php echo $i+1; ?><?php } ?></td>
											<td valign="middle"><?php if($i<=2) {?> <?php } else { ?>ทวีทรัพย์ พรสัจจะ<?php } ?></td>
											<td valign="middle" align="center"><?php if($i<=2) {?> <?php } elseif($i%3==0) {?>INNOHUB<?php } else { ?> OTO <?php } ?></td>
											<td valign="middle" align="center"><?php if($i<=2) {?> <?php } elseif($i%3==0) {?>Production<?php } elseif($i%5==0) {?> IT <?php } elseif($i%7==0) {?>HR<?php } else { ?> Admin <?php } ?></td>
											<td valign="middle" align="center"><?php if($i<=2) {?> <?php } elseif($i%3==0) {?>Content<?php } elseif($i%5==0) {?> Platform <?php } elseif($i%7==0) {?>Interview<?php } else { ?> Officer <?php } ?></td>
											<td valign="middle" align="center"><?php if($i<=2) {?> <?php } elseif($i%3==0) {?>Web Design<?php } elseif($i%5==0) {?> Developer <?php } elseif($i%7==0) {?>Supervisor<?php } else { ?> Officer <?php } ?></td>
											<td valign="middle" align="center"><?php if($i<=2) {?> <?php } elseif($i%3==0) {?>Manager<?php } elseif($i%5==0) {?> Assitant <?php } else { ?> Straff <?php } ?></td>
											<td valign="middle">
												<?php if($i>17 && $i<=20){ ?>
												<?php } else { ?>
													<?php if($i%3==0) {?><img class="rounded2" src="https://www.w3schools.com/w3images/avatar4.png" alt="Fat Rascal" width="40">
													<? } else { ?><img class="rounded2" src="https://www.w3schools.com/w3images/avatar3.png" alt="Fat Rascal" width="40"><? } ?> 
													<span class="name"><?php echo generateRandomString(); ?></span>
												<?php } ?>
											</td>
											<td valign="middle" align="center"><?php if($i>=3 && $i<=17) {?> <?php if($i%2==0 || $i%3==0 ) {?><span class="text-success">Added</span><?php } else { ?><?php } ?> <?php } ?></td>	
											<td valign="middle" align="center">
												<div class="icheck-primary ma0">
													<input type="checkbox" name="approve-csv<? echo($i) ?>" id="approve-csv-chk<? echo($i) ?>" <?php if($i%3==0 && $i<15) {?>checked=""<?php } ?>> 
													<label for="approve-csv-chk<? echo($i) ?>"><span class="visible-xs"> Approve</span></label>
												  </div>
											</td>
											<td valign="middle" align="center">
												<a class="more ui-btn-blue-min btn-xs" href="javascrtip:;" data-fancybox="" data-src="#userdetail" title="ดูรายละเอียด"><i class="fas fa-eye"></i> view</a> 
												<!--<a href="#" title="View Leave" class="ui-btn-trans-mid btn-xs"><i class="fas fa-calendar-day"></i></a>
												<a href="#" title="View Time Attendance" class="ui-btn-trans-mid btn-xs"><i class="fas fa-user-clock"></i></a>-->
												<a href="javascrtip:;" data-fancybox="" data-src="#acc-edit-user"  title="Edit" class="ui-btn-trans-mid btn-xs"><i class="fas fa-edit"></i></a>
											</td>
											</tr>
											<?php } ?>
											</tbody>
											</table>
											
											<div class="mb10-xs d-flex end-xs">
												<a href="javascrtip:;" data-fancybox="" data-src="#acc-add-user" class="ui-btn-green2 btn-sm" title="Add Menual User"><i class="fas fa-user-plus"></i> Add Menual User</a>
											</div>										
											
										</div>
									</div>
									
									
									
										<div class="sticky-bottom card-footer mf-bottom">
										<div class="__chd-ph10 center-xs">
												<button type="reset" class="ui-btn-gray btn-md" data-toggle="modal" data-target="#modal-close">Cancel</button>
												<button type="button" class="ui-btn-green btn-md" onclick="$(this).children('i').removeClass('hid'); $('.form-sending')[0].reset();"><i class="hid fas fa-circle-notch fa-spin"></i> Save Changes</button>
										</div>
										</div>
								  
								  </div>
								  
								</div>
								<!-- /card -->
							</div>

						</div>
					</div>
				</div>
					</div>

			</div>
			
			
			<!-- popup -->
			<div class="thm-popup pa0-xs rounded-1" id="userdetail" style="width: 800px;display: none">
				<div id="UserMoreDetail" class="card card-widget widget-user-2">
				  <!-- Add the bg color to the header using any of the bg-* classes -->
				  <div class="widget-user-header bg-green d-flex between-xs bottom-xs">
				   <div class="main-user">
						<div class="widget-user-image">
						  <img class="img-circle elevation-2" src="https://www.w3schools.com/w3images/avatar4.png" alt="User Avatar">
						</div>
						<!-- /.widget-user-image -->
						<h3 class="widget-user-username">ทวีทรัพย์ พรสัจจะ</h3>
						<h5 class="widget-user-desc">Employee ID : 11002001</h5>
					</div>
					<div class="call-in txt-r">
						<big class="f-normal">Designer</big> 
						<small class="d-block text-black-50 text-right"><i class="fas fa-bookmark" aria-hidden="true"></i> Platform</small>
						<!--<div class="end-call mt-2"><button class="btn-danger border-dark"><i class="fas fa-phone-slash fa-rotate-90"></i> End Call</button></div>-->
					</div>

				  </div>
				  <div class="card-footer border-bottom info-call2line _self-pa0">
					<div class="d-flex">
					  <div class="col-sm-4 border-right">
						<div class="description-block">
						  <h5 class="description-header">Mobile <a href="javascript:;" data-fancybox="" data-modal="modal" data-src="#modal-edit-user" title="edit"><i class="far fa-edit text-sm text-muted" aria-hidden="true"></i></a></h5>
						  <big class="description-text">0899599955</big>
						</div>
						<!-- /.description-block -->
					  </div>
					  <!-- /.col -->
					  <div class="col-sm-4 border-right">
						<div class="description-block">
						  <h5 class="description-header">Gender  &amp; Birthdate</h5>
						  <span class="description-text"> Male | Age 38 <small class="d-block" style="margin-top: 2px">[20/07/1982]</small></span>
						</div>
						<!-- /.description-block -->
					  </div>
					  <!-- /.col -->
					  <div class="col-sm-4">
						<div class="description-block">
						  <h5 class="description-header">Email <a href="javascript:;" data-fancybox="" data-modal="modal" data-src="#modal-edit-user" title="edit"><i class="far fa-edit text-sm text-muted" aria-hidden="true"></i></a></h5>
						  <span class="description-text"><small class="d-block">thaweesub.p@oto.corp.com</small></span>
						</div>
						<!-- /.description-block -->
					  </div>
					  <!-- /.col -->
					</div>
					<!-- /.row -->
				  </div>
				<div class="card-footer card-footer _self-pa0">
					<ul class="nav flex-column">
					  <li class="nav-item">
						<a href="javascript:;" onclick="$('.list-sr-chat').addClass('show'); /*$(this).children('.status-send').addClass('hid')*/;$(this).children('.status-success').removeClass('hid');" class="nav-link text-dark">
						  <i class="fas fa-user-slash" aria-hidden="true"></i> ขาดงาน

						  <big class="status-success float-right badge text-success text-lg ml5-xs"> 0 วัน</big>
						</a>
					  </li>
					  <li class="nav-item">
						<a href="javascript:;" data-fancybox="" data-modal="modal" data-src="#modal-map" onclick="$('.list-sr-chat').addClass('show'); /*$(this).children('.status-send').addClass('hid')*/;$(this).children('.status-success').removeClass('hid');" class="nav-link text-dark">
						  <i class="fas fa-hospital-user" aria-hidden="true"></i> ลาป่วย
						  <big class="status-success float-right badge text-success text-lg ml5-xs"> 2 วัน</big>
						</a>
					  </li>
					  <li class="nav-item">
						<a href="javascript:;" onclick="$('.list-sr-chat').addClass('show'); $(this).children('.status-send').addClass('hid');$(this).children('.status-success,.status-accept').removeClass('hid');" class="nav-link text-dark">
						  <i class="fas fa-house-user" aria-hidden="true"></i> ลากิจ
						  <big class="status-success float-right badge text-success text-lg ml5-xs"> 0 วัน</big>
						</a>
					  </li>
					  <li class="nav-item">
						<a href="#" class="nav-link text-dark">
						  <i class="fas fa-user-clock" aria-hidden="true"></i> สาย
						  <big class="status-success float-right badge text-danger text-lg ml5-xs"> 15 นาที</big> 
						</a>
					  </li>
					  <li class="nav-item">
						<a href="#" class="nav-link text-dark">
						  <i class="fas fa-chalkboard-teacher" aria-hidden="true"></i> อื่นๆ
						  <big class="status-success float-right badge text-success text-lg ml5-xs"> 0 วัน</big> 
						</a>
					  </li>
					</ul>
				  </div>
				</div>
			</div>
			
			<div class="popup thm-lite" id="acc-add-user">
				<div class="box-middle">
				  <div class="modal-content">
					<div class="modal-header">
					  <h2 class="modal-title t-black mb20-xs">Add Member Detail</h2>
					</div>
					<div class="modal-body txt-l">
					  
					  <div class="form-group p-2 bg-light rounded">

							<div class="row">
								<div class="col-sm-6">
									  <div class="form-group">
										<label for="add-emid">Employee ID</label>
										<input type="text" class="txt-box w-100" id="add-emid">
									  </div>
									  
									  <div class="form-group js-select">
										<label for="add-detail1">บริษัท</label>
										<!--<input type="text" class="txt-box w-100" id="edit-detail1" value="INNOHUB">-->
										<select class="form-control js-example-tags" data-placeholder="Select">
										  <option></option>
										  <option>OTO</option>
										  <option>INNOHUB</option>
										</select>
									  </div>
									  
									  
									  <div class="form-group js-select">
										<label for="add-detail1">ฝ่าย</label>
										<!--<input type="text" class="txt-box w-100" id="edit-detail1" value="Production">-->
										<select class="form-control js-example-tags" data-placeholder="Select">
										  <option></option>
										  <option>IT</option>
										  <option >Accounting</option>
										  <option>Finance</option>
										  <option >Sale</option>
										  <option>Maketing</option>
										  <option >Call Center</option>
										  <option>HR</option>
										  <option>Reseption</option>
										</select>
									  </div>
									  
									  <div class="form-group js-select">
										<label for="add-detail1">แผนก</label>
										<!--<input type="text" class="txt-box w-100" id="edit-detail1" value="Content">-->
										<select class="form-control js-example-tags" data-placeholder="Select">
										  <option></option>
										  <option>Content</option>
										  <option >Accounting</option>
										  <option>Finance</option>
										  <option >Sale</option>
										  <option>Maketing</option>
										  <option>IT</option>
										  <option>Reseption</option>
										</select>
									  </div>
									  
									  <div class="form-group js-select">
										<label for="add-detail1">ตำแหน่ง</label>
										<!--<input type="text" class="txt-box w-100" id="edit-detail1" value="Web Design">-->
										<select class="form-control js-example-tags" data-placeholder="Select">
										  <option></option>
										  <option>Web Design</option>
										  <option>UX/UI</option>
										  <option>CSS Developer</option>
										  <option>Developer</option>
										  <option>Network</option>
										  <option>Content</option>
										  <option>Admin</option>
										</select>
									  </div>
									  
									  <div class="form-group js-select">
										<label for="add-detail1">ระดับงาน</label>
										<!--<input type="text" class="txt-box w-100" id="edit-detail1" value="Manager">-->
										<select class="form-control js-example-tags" data-placeholder="Select">
										  <option></option>
										  <option>Straff</option>
										  <option>Senior</option>
										  <option>Assistant Manager</option>
										  <option>Manager</option>
										  <option>Senior Manager</option>
										  <option >AVP</option>
										  <option>VP</option>
										  <option>MD</option>
										  <option>CEO</option>
										</select>
									  </div>


								</div>

								<div class="col-sm-6">

									  <div class="form-group">
										<label for="add-name">Display Name</label>
										<input type="text" class="txt-box w-100" id="add-name">
									  </div>
									  
									  <div id="f-upload_avatar" class="f-upload form-group" style="">
										<label for="add_img2">Upload Avatar</label>
										<div class="input-group">
												<div class="custom-file">
													<input type="file" class="custom-file-input2" id="aadd_img2" accept=".jpg,.jpeg,.png">
													<img id="temp_addimage_src" hidden="">
													<label class="custom-file-label" id="aadd_labelimg2" for="aadd_img2"><p>Choose file</p></label>
												</div>
												<div class="input-group-append">
													<button class="input-group-text" id="add_uploadimg2">Upload</button>
												</div>

										</div>
									 </div>
									  
									  <div class="form-group">
										<label for="gender">Gender</label>
										<div class="btn-group btn-group-toggle w-100" data-toggle="buttons">
										  <label class="btn bg-teal">
											<input type="radio" name="agender" id="male" autocomplete="off"> Male
										  </label>
										  <label class="btn bg-teal">
											<input type="radio" name="agender" id="female" autocomplete="off"> Female
										  </label>
										  <label class="btn bg-teal">
											<input type="radio" name="agender" id="lgbt" autocomplete="off"> LGBT
										  </label>
										</div>
									  </div>
									  
									  <div class="form-group">
										<label for="add-mobile">Mobile Number</label>
										<input type="text" class="txt-box w-100" id="add-mobile">
									  </div>
									  
									  <div class="form-group">
										<label for="u-Email1">Email address</label>
										<input type="email" class="txt-box w-100" id="ua-Email1">
									  </div>
								</div>
							</div>
						</div>
					  
					</div>
					<div class="modal-footer justify-content-between">
					  <button type="button" data-fancybox-close="" class="ui-btn-gray btn-sm" title="Close" href="javascript:;" onclick="parent.jQuery.fancybox.close();">Cancel</button>
					  <button type="button" data-fancybox-close="" class="ui-btn-green2 btn-sm" title="Close" href="javascript:;" onclick="parent.jQuery.fancybox.close();">Save</button>
					</div>
				  </div>
				  <!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			  </div>
			  <!-- /.modal -->
			
			<div class="popup thm-lite" id="acc-edit-user">
				<div class="box-middle">
				  <div class="modal-content">
					<div class="modal-header">
					  <h2 class="modal-title t-black mb20-xs">Edit Member</h2>
					</div>
					<div class="modal-body txt-l">
					  
					  <div class="form-group p-2 bg-light rounded" data-select2-id="72">

							<div class="row">
								<div class="col-sm-6">
									  <div class="form-group">
										<label for="edit-emid">Employee ID</label>
										<input type="text" class="txt-box w-100" id="edit-emid" value="11002004">
									  </div>
									  
									  <div class="form-group js-select">
										<label for="edit-detail1">บริษัท</label>
										<!--<input type="text" class="txt-box w-100" id="edit-detail1" value="INNOHUB">-->
										<select class="form-control js-example-tags">
										  <option>OTO</option>
										  <option selected="selected">INNOHUB</option>
										</select>
									  </div>
									  
									  
									  <div class="form-group js-select">
										<label for="edit-detail1">ฝ่าย</label>
										<!--<input type="text" class="txt-box w-100" id="edit-detail1" value="Production">-->
										<select class="form-control js-example-tags">
										  <option selected="selected">IT</option>
										  <option >Accounting</option>
										  <option>Finance</option>
										  <option >Sale</option>
										  <option>Maketing</option>
										  <option >Call Center</option>
										  <option>HR</option>
										  <option>Reseption</option>
										</select>
									  </div>
									  
									  <div class="form-group js-select">
										<label for="edit-detail1">แผนก</label>
										<!--<input type="text" class="txt-box w-100" id="edit-detail1" value="Content">-->
										<select class="form-control js-example-tags">
										  <option selected="selected">Content</option>
										  <option >Accounting</option>
										  <option>Finance</option>
										  <option >Sale</option>
										  <option>Maketing</option>
										  <option>IT</option>
										  <option>Reseption</option>
										</select>
									  </div>
									  
									  <div class="form-group js-select">
										<label for="edit-detail1">ตำแหน่ง</label>
										<!--<input type="text" class="txt-box w-100" id="edit-detail1" value="Web Design">-->
										<select class="form-control js-example-tags">
										  <option selected="selected">Web Design</option>
										  <option>UX/UI</option>
										  <option>CSS Developer</option>
										  <option>Developer</option>
										  <option>Network</option>
										  <option>Content</option>
										  <option>Admin</option>
										</select>
									  </div>
									  
									  <div class="form-group js-select">
										<label for="edit-detail1">ระดับงาน</label>
										<!--<input type="text" class="txt-box w-100" id="edit-detail1" value="Manager">-->
										<select class="form-control js-example-tags">
										  <option>Straff</option>
										  <option>Senior</option>
										  <option>Assistant Manager</option>
										  <option selected="selected">Manager</option>
										  <option>Senior Manager</option>
										  <option >AVP</option>
										  <option>VP</option>
										  <option>MD</option>
										  <option>CEO</option>
										</select>
									  </div>


								</div>

								<div class="col-sm-6">

									  <div class="form-group">
										<label for="edit-name">Display Name</label>
										<input type="text" class="txt-box w-100" id="edit-name" value="ทวีทรัพย์ พรสัจจะ">
									  </div>
									  
									  <div id="f-upload_avatar" class="f-upload form-group" style="">
										<label for="apm_img2">Upload Avatar</label>
										<div class="input-group">
												<div class="custom-file">
													<input type="file" class="custom-file-input2" id="apm_img2" accept=".jpg,.jpeg,.png">
													<img id="temp_apmimage_src" hidden="">
													<label class="custom-file-label" id="apm_labelimg2" for="apm_img2"><p>Choose file</p></label>
												</div>
												<div class="input-group-append">
													<button class="input-group-text" id="apm_uploadimg2">Upload</button>
												</div>

										</div>
									 </div>
									  
									  <div class="form-group">
										<label for="gender">Gender</label>
										<div class="btn-group btn-group-toggle w-100" data-toggle="buttons">
										  <label class="btn bg-teal active">
											<input type="radio" name="gender" id="male" autocomplete="off" checked> Male
										  </label>
										  <label class="btn bg-teal">
											<input type="radio" name="gender" id="female" autocomplete="off"> Female
										  </label>
										  <label class="btn bg-teal">
											<input type="radio" name="gender" id="lgbt" autocomplete="off"> LGBT
										  </label>
										</div>
									  </div>
									  
									  <div class="form-group">
										<label for="edit-mobile">Mobile Number</label>
										<input type="text" class="txt-box w-100" id="edit-mobile" value="0899599955">
									  </div>
									  
									  <div class="form-group">
										<label for="u-Email1">Email address</label>
										<input type="email" class="txt-box w-100" id="u-Email1" value="thaweesub.p@oto.corp.com">
									  </div>
								</div>
							</div>
						</div>
					  
					</div>
					<div class="modal-footer justify-content-between">
					  <button type="button" data-fancybox-close="" class="ui-btn-gray btn-sm" title="Close" href="javascript:;" onclick="parent.jQuery.fancybox.close();">Cancel</button>
					  <button type="button" data-fancybox-close="" class="ui-btn-green2 btn-sm" title="Close" href="javascript:;" onclick="parent.jQuery.fancybox.close();">Save</button>
					</div>
				  </div>
				  <!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			  </div>
			  <!-- /.modal -->
			
			 <!-- /popup -->
		</section>
    </div>
</div>

<!--<div id="skin-loading" class="bg-wh" onclick="$(this).fadeOut();">
	<div class="lds-hourglass"></div>
</div>-->
<script>
	window.setTimeout(function(){
		$('#skin-loading').fadeOut();
	}, 3000);
</script>

<!-- footer -->
<?php include("incs/footer-web.html") ?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js-web.html") ?>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<link href="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.css" rel="stylesheet"/>
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<script type="text/javascript">
$( document ).ready( function () {
	 
      $('.carousel').flexslider({
        animation: "slide",
        animationLoop: false,
        itemWidth: 210,
        itemMargin: 5,
        minItems: 1,
        maxItems: 3,
		move: 1,
        /*start: function(slider){
          $('body').removeClass('loading');
        }*/
      });

	//select2
	$('.js-select-multi').select2({
		//maximumSelectionLength: 3,
    	placeholder: "Select",
    	//allowClear: true,
		//tags: true,
		dropdownAutoWidth : true,
		width: '100%'
	});
	$(".js-adv-select").select2({
	   //dropdownAutoWidth : true,
	  width: '100%',
	  tags: true,
	  insertTag: function (data, tag) {
		// Insert the tag at the end of the results
		data.push(tag);
	  }
	  });

	$(".js-example-tags").select2({
	   //dropdownAutoWidth : true,
	   placeholder: "Select",
	  width: '100%',
	  tags: true,
	  insertTag: function (data, tag) {
		// Insert the tag at the end of the results
		data.push(tag);
	  }
	  /*createTag: function (params) {
		var term = $.trim(params.term);

		if (term === '') {
		  return null;
		}

		return {
		  id: term,
		  text: term,
		  newTag: true // add additional parameters
		}
	  }*/
	});
	
	/*toggle radio*/
	$(".btn-group-toggle input[type=radio]").on("click", function() {
		
		if ($(this).parents('.btn').hasClass( 'active' )) {
		} else {
			$(this).parents(".btn-group-toggle").find('.btn').removeClass('active');
			$(this).parents('.btn').addClass('active');
		}
		

	})
	
	

});
  </script>

<link rel="stylesheet" href="js/plugin/dropzone.min.css" />
<script src="js/plugin/dropzone.min.js"></script>

<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.2/basic.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.2/dropzone.js"></script>-->
<script>
if ($().dropzone && !$(".dropzone").hasClass("disabled")) {
      $(".dropzone").dropzone({
        url: "https://httpbin.org/post",
        init: function () {
          this.on("success", function (file, responseText) {
            console.log(responseText);
          });
        },
        thumbnailWidth: 160,
        previewTemplate: '<div class="dz-preview dz-file-preview mb-3"><div class="d-flex flex-row "><div class="p-0 w-30 position-relative"><div class="dz-error-mark"><span><i></i></span></div><div class="dz-success-mark"><span><i></i></span></div><div class="preview-container"><img data-dz-thumbnail class="img-thumbnail border-0" /><i class="simple-icon-doc preview-icon" ></i></div></div><div class="pl-3 pt-2 pr-2 pb-1 w-70 dz-details position-relative"><div><span data-dz-name></span></div><div class="text-primary text-extra-small" data-dz-size /><div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div></div><a href="#/" class="remove" data-dz-remove><i class="glyph-icon simple-icon-trash"></i></a></div>'
      });
    }
	</script>
<!-- /js -->

</body>
</html>
