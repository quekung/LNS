<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top-web.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden">
<!-- Headbar -->
<?php include("incs/header-v2.html") ?>
<!-- /Headbar -->
<div class="page-checkout">

    
    <div id="toc">
		<section class="z-success txt-c _self-pt30">

			<div class="bg-gray2">
				<div class="container msg">
					<i class="icon-error"><img src="di/icon-sorry.png" width="100"></i>
					<h2><span class="t-red2">ขออภัย!</span></h2>	
					<p>มีข้อผิดพลาดบางอย่างในระบบของเรา กรุณาลองใหม่อีกครั้ง</p>
				</div>
			</div>
			
			<div class="ctrl-btn txt-c _self-mt20">
				<p><a href="javascript:;" onclick="window.history.go(-1); return false;" title="กลับไปยังหน้าเดิม" class="ui-btn-gray2 btn-sm">กลับไปยังหน้าเดิม</a></p>
			</div>

			
		</section>
    </div>
</div>

<!--<div id="skin-loading" class="bg-wh" onclick="$(this).fadeOut();">
	<div class="lds-hourglass"></div>
</div>-->
<script>
	window.setTimeout(function(){
		$('#skin-loading').fadeOut();
	}, 3000);
</script>

<!-- footer -->
<?php include("incs/footer-web.html") ?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js-web.html") ?>
<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>
<!-- /js -->

</body>
</html>
