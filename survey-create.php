<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top-web.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden">
<!-- Headbar -->
<?php include("incs/header-v2.html") ?>
<script>
$(".main-menu .list-unstyled>li.active").removeClass('active');
$(".main-menu .list-unstyled>li:nth-child(4)").addClass('active');
</script>
<!-- /Headbar -->
<div class="page-checkout">

    
    <div id="toc">
		<section class="z-broadcast _self-pt0 mb0">
			<div class="bx-stepbar _self-pv20 cb-af container">
				<ul class="tabsbar">
						  <li><a href="broadcasts.php" title="Send Message"><i class="fas fa-bullhorn"></i> <span>Send Message</span></a></li>
						  <li><a href="broadcasts-create.php" title="Create Message"><i class="fas fa-layer-group"></i> <span>Create Template</span></a></li>
						  <li><a href="broadcasts-acc.php" title="User Detail"><i class="fas fa-users-cog"></i> <span>User Detail</span></a></li>
						  <li><a href="broadcasts-setting.php" title="Message Setting"><i class="fas fa-sliders-h"></i> <span>Message Setting</span></a></li>
						  <li><a href="broadcasts-report.php" title="Report"><i class="fas fa-file-medical-alt"></i> <span>Report</span></a></li>
						  <li><a href="broadcasts-survey.php" title="Survey" class="selected"><i class="fas fa-tasks"></i> <span>Survey</span></a></li>
				  </ul>
			</div>
					

			
			<div class="contentTabs main-sv">
				<div id="tbc-1" class="msg">

					<div class="head-bg _self-mb0">
					<div class="container d-flex between-xs bottom-xs">
						<h2>Create survey question</h2>
						<div class="ctrl-btn-tools d-flex _chd-pl05 middle-xs">
							<div><a href="survey-view.php" class="ui-btn-trans-min btn-xs" title="view"><i class="fas fa-eye fa-2x t-white"></i></a></div>
							<div>
								<a href="#" class="ui-btn-trans-min btn-xs" title="setting"><i class="fas fa-cog fa-2x t-white"></i></a>
								<div class="sw-anonymous">
									  <label class="toggle">
										  <input class="toggle-checkbox" type="checkbox" name="status">
										  <div class="toggle-switch"></div>
										  <small class="toggle-label">ไม่ระบุชื่อตอบ</small>
									 </label>
								</div>
							</div>
							<div><a href="broadcasts-push-survey.php" class="ui-btn-black-min btn-sm ml10-xs"><i class="fas fa-paper-plane"></i> Push Survey</a></div>
						</div>
					</div>
					</div>
					<form method="post" class="form-signin form-checkout form-sending'">
						
						<div class="set-name-tpl bg-gray3 pa20-sm pa10-xs mb20-xs">
						<div class="container">
							  <div class="input-group mb0-xs d-flex middle-xs">
								<h3 class="card-title m-0 input-group-prepend middle-xs f-normal"><span class="step ui-btn-green2 _self-ph10-mr10">Create</span></h3>
								<div class="col d-flex middle-xs">
									<div class="wr w-100 _self-mb0">
									<!--<input type="text" class="w-100 txt-box" id="template-title" placeholder="Template Name" autofocus="">-->
										<input type="text" class="w-100 txt-box" id="sv-name" name="sv-name" required>
										<label for="sv-name">ชื่อแบบสอบถาม</label>
									</div>
								</div>
								<!-- cover -->
								<div class="cover-survey rw pl10-xs">			
									<figure id="show_image" class="show-cover"><div class="delete"></div></figure>
									<div class="file-upload-wrapper" data-text="Add Photo">
										<input type="file" class="file-upload-field" id="file-upload" name="file-upload" accept=".jpg,.jpeg.,.gif,.png" required="">
										<label for="file-upload"><i class="far fa-image fa-3x"></i></label>    
									</div>
									<script>
									document.getElementById('file-upload').addEventListener('change', function (event) {
		  var file = event.target.files[0];
		  var fileReader = new FileReader();
		  $("#show_image img").remove();
		  if (file.type.match('image')) {
			fileReader.onload = function () {
			  var img = document.createElement('img');
			  img.src = fileReader.result;
			  document.getElementById('show_image').appendChild(img);
			};
			fileReader.readAsDataURL(file);
		  } 
		});
		 /*input file*/
		$("form").on("change", "#file-upload", function(){ 
			$(this).parent(".file-upload-wrapper").attr("data-text", $(this).val().replace(/.*(\/|\\)/, '') );
			//$(this).parents('.new-pk').addClass('load');
			$("#show_image").removeClass('hid');
			$(".cover-survey").addClass('active');
		});
		$("#show_image .delete").click(function(){
			$(".cover-survey").removeClass('active');
			$("#show_image img").remove();
		});
		



									</script>
									
								</div>
								<!-- /cover -->
							  </div>
						</div>
						</div>
						
					<div class="main container">
					
								
								<!-- card -->
								<div class="bx-question">
									<div class="bar-ctrl d-flex between-xs mb10-xs bottom-xs">
										<b>Question 1</b>
										<div class="tools d-flex flex-nowrap">
											<a class="ui-btn-gray-min-sq btn-sm btn-clone" href="javascript:;" title="Copy"><i class="fas fa-copy"></i></a>
											<a class="ui-btn-gray-min-sq btn-sm btn-moveup disable" href="javascript:;" title="Move Up"><i class="fas fa-angle-up"></i></a>
											<a class="ui-btn-gray-min-sq btn-sm btn-movedown" href="javascript:;" title="Move Down"><i class="fas fa-angle-down"></i></a>
											<a class="ui-btn-gray-min-sq btn-sm btn-delete" href="javascript:;" onClick="$(this).parents('.bx-question').remove();" title="delete"><i class="far fa-trash-alt"></i></a>
										</div>
									</div>
									
									<div class="question-zone">
										<div class="q-text d-flex between-xs middle-xs">
											<div class="question col pr15-xs">
												<!--<input class="txt-line bg f-normal" id="q1" name="q1" placeholder="Question" required>-->
												<textarea class="txt-line bg f-normal" id="q1" name="q1" placeholder="Question" required></textarea>
												<div class="line"></div>
											</div>
											<!-- cover question -->
											<div class="rw pr15-xs d-flex middle-xs">			
												<div class="thm-question">
												<div class="file-upload-wrapper" data-text="Add Photo">
													<input type="file" class="file-upload-field" id="qcover-upload" name="qcover-upload" accept=".jpg,.jpeg.,.gif,.png" required="">
													<label for="qcover-upload"><i class="far fa-image fa-2x"></i></label>    
												</div>
												</div>
											</div>
											<script>
													document.getElementById('qcover-upload').addEventListener('change', function (event) {
													  var file = event.target.files[0];
													  var fileReader = new FileReader();
													  $(".show-cover-q img").remove();
													  if (file.type.match('image')) {
														fileReader.onload = function () {
														  var img = document.createElement('img');
														  img.src = fileReader.result;
														  document.getElementById('question1_image1').appendChild(img);
														};
														fileReader.readAsDataURL(file);
													  } 
													});
													 /*input file*/
													$("form").on("change", "#qcover-upload", function(){ 
														$(this).parent(".file-upload-wrapper").attr("data-text", $(this).val().replace(/.*(\/|\\)/, '') );
														$(".show-cover-q").removeClass('hid');
														$(this).parents('.question-zone').addClass('active');
													});
													/*$(".show-thm .delete").click(function(){
														$(this).parents('.q-answer').removeClass('active');
														$(".show-thm img").remove();
													});*/

													</script>
											<!-- /cover question -->
											<div class="sv-type">
												<div class="cv-select">
													<select style="width:300px" class="select-type-sv">
													   <option value="radio" data-image="http://ui.beurguide.net/keeppro/di/choice-sv1.png">Multiple Choice</option>
													   <option value="check" data-image="http://ui.beurguide.net/keeppro/di/choice-sv2.png">Checkboxes</option>
													   <option value="text" data-image="http://ui.beurguide.net/keeppro/di/choice-sv3.png">Textbox</option>
													   <option value="rating" data-image="http://ui.beurguide.net/keeppro/di/choice-sv4.png">Rating</option>
													</select>
												</div>
											</div>
										</div>
										<figure id="question1_image1" class="show-cover-q hid"><div class="delete" onClick="$(this).next().remove(); $(this).parents('.question-zone').removeClass('active');"></div></figure>
									</div>
									
									<div class="q-answer mt20-sm mt10-xs">
										<div class="row-answer">
											<div class="input-group d-flex middle-xs">
												<div class="icheck-primary">
													<input type="radio" name="q1-chk" id="q1-chk1" disabled> 
													<label for="q1-chk1"></label>
												</div>
												<div class="col d-flex middle-xs">
													<div class="wr w-100 _self-mb0">
														<input type="text" class="w-100 txt-box" id="q1-opt1" name="q1-opt1" placeholder="Option 1" required>
													</div>
												</div>
												<!-- cover -->
												<div class="rw pl10-xs d-flex middle-xs">			
													<div class="thm-answer">
													<div class="file-upload-wrapper" data-text="Add Photo">
														<input type="file" class="file-upload-field" id="fq-upload" name="file-upload" accept=".jpg,.jpeg.,.gif,.png" required="">
														<label for="file-upload"><i class="far fa-image fa-2x"></i></label>    
													</div>
													<script>
													document.getElementById('fq-upload').addEventListener('change', function (event) {
													  var file = event.target.files[0];
													  var fileReader = new FileReader();
													  $(".show-thm img").remove();
													  if (file.type.match('image')) {
														fileReader.onload = function () {
														  var img = document.createElement('img');
														  img.src = fileReader.result;
														  document.getElementById('answer1_image1').appendChild(img);
														};
														fileReader.readAsDataURL(file);
													  } 
													});
													 /*input file*/
													$("form").on("change", "#fq-upload", function(){ 
														$(this).parent(".file-upload-wrapper").attr("data-text", $(this).val().replace(/.*(\/|\\)/, '') );
														$(".show-thm").removeClass('hid');
														$(this).parents('.q-answer').addClass('active');
													});
													/*$(".show-thm .delete").click(function(){
														$(this).parents('.q-answer').removeClass('active');
														$(".show-thm img").remove();
													});*/

													</script>
													</div>
												</div>
												<!-- /cover -->
											  </div>
											<figure id="answer1_image1" class="show-thm"><div class="delete" onClick="$(this).next().remove(); $(this).parents('.q-answer').removeClass('active');"></div></figure>
										</div>
										<div class="row-answer">
											<div class="input-group d-flex middle-xs">
												<div class="icheck-primary">
													<input type="radio" name="q1-chk" id="q1-chk2" disabled> 
													<label for="q1-chk2"></label>
												</div>
												<div class="col d-flex middle-xs">
													<div class="wr w-100 _self-mb0">
														<input type="text" class="w-100 txt-box" id="q1-opt2" name="q1-opt2" placeholder="Option 2" required>
													</div>
												</div>
												<!-- cover -->
												<div class="rw pl10-xs d-flex middle-xs">			
													<div class="thm-answer">
													<div class="file-upload-wrapper" data-text="Add Photo">
														<input type="file" class="file-upload-field" id="fq-upload" name="file-upload" accept=".jpg,.jpeg.,.gif,.png" required="">
														<label for="file-upload"><i class="far fa-image fa-2x"></i></label>    
													</div>
													</div>
												</div>
												<!-- /cover -->
											  </div>
											<figure id="answer1_image2" class="show-thm"><div class="delete" onClick="$(this).next().remove(); $(this).parents('.q-answer').removeClass('active');"></div></figure>
										</div>
										
										<div class="row-answer">
											<div class="input-group d-flex middle-xs">
												<div class="icheck-primary">
													<input type="radio" name="q1-chk" id="q1-chk3" disabled> 
													<label for="q1-chk3"></label>
												</div>
												<div class="col d-flex middle-xs">
													<div class="wr w-100 _self-mb0">
														<input type="text" class="w-100 txt-dot" id="q1-opt1" name="q1-opt1" value="Other..." readonly>
													</div>
												</div>
												<!-- cover -->
												<div class="rw pl10-xs d-flex middle-xs">			
													<div class="thm-answer">
													<i class="far fa-image fa-2x none"></i>
													</div>
												</div>
												<!-- /cover -->
											  </div>
											<figure id="answer1_image1" class="show-thm"><div class="delete" onClick="$(this).next().remove(); $(this).parents('.q-answer').removeClass('active');"></div></figure>
										</div>

									</div>
									
									<!-- Add more -->
									<div class="q-answer mt20-sm mt10-xs">
										<div class="row-answer">
											<div class="input-group d-flex middle-xs">
												<div class="icheck-primary">
													<input type="radio" name="q1-chk" id="q1-chk-create" disabled> 
													<label for="q1-chk-create"></label>
												</div>
												<div class="col d-flex middle-xs">
													<div class="text-option">
														<input type="text" class="txt-line" id="q1-optadd" name="q1-optadd" placeholder="Add option" style="width: 110px">
													</div>
													<span>or</span>
													<a class="t-blue f-normal ml10-xs" href="javascript:;">add "Other"</a>
												</div>
											  </div>

										</div>

									</div>
									
													

								</div>
								<!-- /card -->
								
								<!-- card -->
								<div class="bx-question">
									<div class="bar-ctrl d-flex between-xs mb10-xs bottom-xs">
										<b>Question 2</b>
										<div class="tools d-flex flex-nowrap">
											<a class="ui-btn-gray-min-sq btn-sm btn-clone" href="javascript:;" title="Copy"><i class="fas fa-copy"></i></a>
											<a class="ui-btn-gray-min-sq btn-sm btn-moveup" href="javascript:;" title="Move Up"><i class="fas fa-angle-up"></i></a>
											<a class="ui-btn-gray-min-sq btn-sm btn-movedown" href="javascript:;" title="Move Down"><i class="fas fa-angle-down"></i></a>
											<a class="ui-btn-gray-min-sq btn-sm btn-delete" href="javascript:;" onClick="$(this).parents('.bx-question').remove();" title="delete"><i class="far fa-trash-alt"></i></a>
										</div>
									</div>
									
									<div class="question-zone">
										<div class="q-text d-flex between-xs middle-xs">
											<div class="question col pr15-xs">
												<textarea class="txt-line bg f-normal" id="q2" name="q2" placeholder="Question" required></textarea>
												<div class="line"></div>
											</div>
											<!-- cover question -->
											<div class="rw pr15-xs d-flex middle-xs">			
												<div class="thm-question">
												<div class="file-upload-wrapper" data-text="Add Photo">
													<input type="file" class="file-upload-field" id="qcover-upload2" name="qcover-upload2" accept=".jpg,.jpeg.,.gif,.png" required="">
													<label for="qcover-upload2"><i class="far fa-image fa-2x"></i></label>    
												</div>
												</div>
											</div>
											<script>
													document.getElementById('qcover-upload2').addEventListener('change', function (event) {
													  var file = event.target.files[0];
													  var fileReader = new FileReader();
													  $(".show-cover-q2 img").remove();
													  if (file.type.match('image')) {
														fileReader.onload = function () {
														  var img = document.createElement('img');
														  img.src = fileReader.result;
														  document.getElementById('question2_image1').appendChild(img);
														};
														fileReader.readAsDataURL(file);
													  } 
													});
													 /*input file*/
													$("form").on("change", "#qcover-upload2", function(){ 
														$(this).parent(".file-upload-wrapper").attr("data-text", $(this).val().replace(/.*(\/|\\)/, '') );
														$(".show-cover-q2").removeClass('hid');
														$(this).parents('.question-zone').addClass('active');
													});
													/*$(".show-thm .delete").click(function(){
														$(this).parents('.q-answer').removeClass('active');
														$(".show-thm img").remove();
													});*/

													</script>
											<!-- /cover question -->
											<div class="sv-type">
												<div class="cv-select">
													<select style="width:300px" class="select-type-sv">
													   <option value="radio" data-image="http://ui.beurguide.net/keeppro/di/choice-sv1.png">Multiple Choice</option>
													   <option value="check" data-image="http://ui.beurguide.net/keeppro/di/choice-sv2.png" selected>Checkboxes</option>
													   <option value="text" data-image="http://ui.beurguide.net/keeppro/di/choice-sv3.png">Textbox</option>
													   <option value="rating" data-image="http://ui.beurguide.net/keeppro/di/choice-sv4.png">Rating</option>
													</select>
												</div>
											</div>
										</div>
										<figure id="question2_image1" class="show-cover-q2 hid"><div class="delete" onClick="$(this).next().remove(); $(this).parents('.question-zone').removeClass('active');"></div></figure>
									</div>
									
									<div class="q-answer mt20-sm mt10-xs">
										<div class="row-answer">
											<div class="input-group d-flex middle-xs">
												<div class="icheck-primary">
													<input type="checkbox" name="q2-chk" id="q2-chk1" disabled> 
													<label for="q2-chk1"></label>
												</div>
												<div class="col d-flex middle-xs">
													<div class="wr w-100 _self-mb0">
														<input type="text" class="w-100 txt-box" id="q1-opt1" name="q1-opt1" placeholder="Option 1" required>
													</div>
												</div>
												<!-- cover -->
												<div class="rw pl10-xs d-flex middle-xs">			
													<div class="thm-answer">
													<div class="file-upload-wrapper" data-text="Add Photo">
														<input type="file" class="file-upload-field" id="fq-upload" name="file-upload" accept=".jpg,.jpeg.,.gif,.png" required="">
														<label for="file-upload"><i class="far fa-image fa-2x"></i></label>    
													</div>
													
													</div>
												</div>
												<!-- /cover -->
											  </div>
											<figure id="answer2_image1" class="show-thm"><div class="delete" onClick="$(this).next().remove(); $(this).parents('.q-answer').removeClass('active');"></div></figure>
										</div>
										<div class="row-answer">
											<div class="input-group d-flex middle-xs">
												<div class="icheck-primary">
													<input type="checkbox" name="q2-chk2" id="q2-chk2" disabled> 
													<label for="q2-chk2"></label>
												</div>
												<div class="col d-flex middle-xs">
													<div class="wr w-100 _self-mb0">
														<input type="text" class="w-100 txt-box" id="q2-opt2" name="q2-opt2" placeholder="Option 2" required>
													</div>
												</div>
												<!-- cover -->
												<div class="rw pl10-xs d-flex middle-xs">			
													<div class="thm-answer">
													<div class="file-upload-wrapper" data-text="Add Photo">
														<input type="file" class="file-upload-field" id="fq-upload" name="file-upload" accept=".jpg,.jpeg.,.gif,.png" required="">
														<label for="file-upload"><i class="far fa-image fa-2x"></i></label>    
													</div>
													</div>
												</div>
												<!-- /cover -->
											  </div>
											<figure id="answer2_image2" class="show-thm"><div class="delete" onClick="$(this).next().remove(); $(this).parents('.q-answer').removeClass('active');"></div></figure>
										</div>
										
										

									</div>
									
									<!-- Add more -->
									<div class="q-answer mt20-sm mt10-xs">
										<div class="row-answer">
											<div class="input-group d-flex middle-xs">
												<div class="icheck-primary">
													<input type="checkbox" name="q2-chk" id="q2-chk-create" disabled> 
													<label for="q2-chk-create"></label>
												</div>
												<div class="col d-flex middle-xs">
													<div class="text-option">
														<input type="text" class="txt-line" id="q1-optadd" name="q1-optadd" placeholder="Add option" style="width: 110px">
													</div>
													<span>or</span>
													<a class="t-blue f-normal ml10-xs" href="javascript:;">add "Other"</a>
												</div>
											  </div>

										</div>

									</div>
									
													

								</div>
								<!-- /card -->
								
								<!-- card -->
								<div class="bx-question">
									<div class="bar-ctrl d-flex between-xs mb10-xs bottom-xs">
										<b>Question 3</b>
										<div class="tools d-flex flex-nowrap">
											<a class="ui-btn-gray-min-sq btn-sm btn-clone" href="javascript:;" title="Copy"><i class="fas fa-copy"></i></a>
											<a class="ui-btn-gray-min-sq btn-sm btn-moveup" href="javascript:;" title="Move Up"><i class="fas fa-angle-up"></i></a>
											<a class="ui-btn-gray-min-sq btn-sm btn-movedown" href="javascript:;" title="Move Down"><i class="fas fa-angle-down"></i></a>
											<a class="ui-btn-gray-min-sq btn-sm btn-delete" href="javascript:;" onClick="$(this).parents('.bx-question').remove();" title="delete"><i class="far fa-trash-alt"></i></a>
										</div>
									</div>
									
									<div class="question-zone">
										<div class="q-text d-flex between-xs middle-xs">
											<div class="question col pr15-xs">
												<textarea class="txt-line bg f-normal" id="q3" name="q3" placeholder="Question" required></textarea>
												<div class="line"></div>
											</div>
											<!-- cover question -->
											<div class="rw pr15-xs d-flex middle-xs">			
												<div class="thm-question">
												<div class="file-upload-wrapper" data-text="Add Photo">
													<input type="file" class="file-upload-field" id="qcover-upload3" name="qcover-upload3" accept=".jpg,.jpeg.,.gif,.png" required="">
													<label for="qcover-upload3"><i class="far fa-image fa-2x"></i></label>    
												</div>
												</div>
											</div>
											<!-- /cover question -->
											<div class="sv-type">
												<div class="cv-select">
													<select style="width:300px" class="select-type-sv">
													   <option value="radio" data-image="http://ui.beurguide.net/keeppro/di/choice-sv1.png">Multiple Choice</option>
													   <option value="check" data-image="http://ui.beurguide.net/keeppro/di/choice-sv2.png">Checkboxes</option>
													   <option value="text" data-image="http://ui.beurguide.net/keeppro/di/choice-sv3.png" selected>Textbox</option>
													   <option value="rating" data-image="http://ui.beurguide.net/keeppro/di/choice-sv4.png">Rating</option>
													</select>
												</div>
											</div>
										</div>
										<figure id="question3_image1" class="show-cover-q3 hid"><div class="delete" onClick="$(this).next().remove(); $(this).parents('.question-zone').removeClass('active');"></div></figure>
									</div>
									
									<div class="q-answer mt20-sm mt10-xs">
										<div class="row-answer">
											<div class="input-group d-flex middle-xs">
												<div class="col d-flex middle-xs">
													<textarea class="txt-dot" type="text" name="q3-answer" id="q3-answer" placeholder="Answer text" readonly></textarea> 
													<label for="q3-answer"></label>
												</div>												
											  </div>
										</div>


									</div>
								</div>
								<!-- /card -->
								
								<!-- card -->
								<div class="bx-question">
									<div class="bar-ctrl d-flex between-xs mb10-xs bottom-xs">
										<b>Question 4</b>
										<div class="tools d-flex flex-nowrap">
											<a class="ui-btn-gray-min-sq btn-sm btn-clone" href="javascript:;" title="Copy"><i class="fas fa-copy"></i></a>
											<a class="ui-btn-gray-min-sq btn-sm btn-moveup" href="javascript:;" title="Move Up"><i class="fas fa-angle-up"></i></a>
											<a class="ui-btn-gray-min-sq btn-sm btn-movedown disable" href="javascript:;" title="Move Down"><i class="fas fa-angle-down"></i></a>
											<a class="ui-btn-gray-min-sq btn-sm btn-delete" href="javascript:;" onClick="$(this).parents('.bx-question').remove();" title="delete"><i class="far fa-trash-alt"></i></a>
										</div>
									</div>
									
									<div class="question-zone">
										<div class="q-text d-flex between-xs middle-xs">
											<div class="question col pr15-xs">
												<textarea class="txt-line bg f-normal" id="q4" name="q4" placeholder="Question" required></textarea>
												<div class="line"></div>
											</div>
											<!-- cover question -->
											<div class="rw pr15-xs d-flex middle-xs">			
												<div class="thm-question">
												<div class="file-upload-wrapper" data-text="Add Photo">
													<input type="file" class="file-upload-field" id="qcover-upload4" name="qcover-upload4" accept=".jpg,.jpeg.,.gif,.png" required="">
													<label for="qcover-upload4"><i class="far fa-image fa-2x"></i></label>    
												</div>
												</div>
											</div>
											<script>
													document.getElementById('qcover-upload4').addEventListener('change', function (event) {
													  var file = event.target.files[0];
													  var fileReader = new FileReader();
													  $(".show-cover-q4 img").remove();
													  if (file.type.match('image')) {
														fileReader.onload = function () {
														  var img = document.createElement('img');
														  img.src = fileReader.result;
														  document.getElementById('question4_image1').appendChild(img);
														};
														fileReader.readAsDataURL(file);
													  } 
													});
													 /*input file*/
													$("form").on("change", "#qcover-upload4", function(){ 
														$(this).parent(".file-upload-wrapper").attr("data-text", $(this).val().replace(/.*(\/|\\)/, '') );
														$(".show-cover-q4").removeClass('hid');
														$(this).parents('.question-zone').addClass('active');
													});
													/*$(".show-thm .delete").click(function(){
														$(this).parents('.q-answer').removeClass('active');
														$(".show-thm img").remove();
													});*/

													</script>
											<!-- /cover question -->
											<div class="sv-type">
												<div class="cv-select">
													<select style="width:300px" class="select-type-sv">
													   <option value="radio" data-image="http://ui.beurguide.net/keeppro/di/choice-sv1.png">Multiple Choice</option>
													   <option value="check" data-image="http://ui.beurguide.net/keeppro/di/choice-sv2.png">Checkboxes</option>
													   <option value="text" data-image="http://ui.beurguide.net/keeppro/di/choice-sv3.png">Textbox</option>
													   <option value="rating" data-image="http://ui.beurguide.net/keeppro/di/choice-sv4.png" selected>Rating</option>
													</select>
												</div>
											</div>
										</div>
										<figure id="question4_image1" class="show-cover-q hid"><div class="delete" onClick="$(this).next().remove(); $(this).parents('.question-zone').removeClass('active');"></div></figure>
									</div>
									
									<div class="rate-minmax d-flex center-xs pa10-xs">
										<div class="d-flex center-xs middle-xs">
											<label class="d-block pl10-xs pr10-xs">Limit option : </label>
											<div class="js-select">
													<select style="width:80px" class="select-num">
													   <option value="0">0</option>
													   <option value="1" selected>1</option>
													</select>
											</div>
											<span class="d-block pl10-xs pr10-xs">to</span>
											<div class="js-select">
													<select style="width:80px" class="select-num">
													   <option value="3">3</option>
													   <option value="4">4</option>
													   <option value="5" selected>5</option>
													   <!--<option value="6">6</option>
													   <option value="7">7</option>
													   <option value="8">8</option>
													   <option value="9">9</option>
													   <option value="10">10</option>-->
													</select>
											</div>
											<label class="d-block pl10-xs pr10-xs">Style : </label>
											<div class="js-select">
												<select id="type-rating" style="width:160px" class="select-type-sv">
													   <option value="radio" data-image="http://ui.beurguide.net/keeppro/di/choice-sv1.png" selected>Normal</option>
													   <option value="rating" data-image="http://ui.beurguide.net/keeppro/di/choice-star.png">Star</option>
													   <option value="smile" data-image="http://ui.beurguide.net/keeppro/di/choice-smile.png">Smile</option>
												</select>
											</div>
										</div>
									</div>
									
									<div class="row-rating d-flex flex-nowrap center-xs top-xs">
									<!-- Add label -->

										<div class="row-answer pr10-xs pr20-md">
											<div class="input-group d-flex middle-xs">
												<div class="col d-flex middle-xs">
													<div class="text-option">
														<input type="text" class="txt-line txt-r" id="q1-optadd" name="q1-optadd" placeholder="Label (optional)" style="width: 150px">
													</div>
												</div>
											  </div>

										</div>
			
									<!-- /Add label -->
									<div id="rating-normal" class="group-rating _chd-ph10 flex-nowrap q-answer">

										<div class="row-answer">
											<div class="txt-c middle-xs">			
												<div class="col d-block">
													<div class="wr _self-col-xs-02-mb0">
														<input type="text" class="txt-dot txt-c" id="q4-opt1" name="q4-opt1" value="1" required>
													</div>
												</div>
												<div class="icheck-primary">
													<input type="radio" name="q4-chk" id="q1-chk1" disabled> 
													<label for="q4-chk1"></label>
												</div>
											  </div>
										</div>
										<div class="row-answer">
											<div class="txt-c middle-xs">			
												<div class="col d-block">
													<div class="wr _self-col-xs-02-mb0">
														<input type="text" class="txt-dot txt-c" id="q4-opt2" name="q4-opt2" value="2" required>
													</div>
												</div>
												<div class="icheck-primary">
													<input type="radio" name="q4-chk" id="q4-chk2" disabled> 
													<label for="q4-chk2"></label>
												</div>
											  </div>
										</div>
										
										<div class="row-answer">
											<div class="txt-c middle-xs">			
												<div class="col d-block">
													<div class="wr _self-col-xs-02-mb0">
														<input type="text" class="txt-dot txt-c" id="q4-opt1" name="q4-opt1" value="3" readonly>
													</div>
												</div>
												<div class="icheck-primary">
													<input type="radio" name="q4-chk" id="q4-chk3" disabled> 
													<label for="q4-chk3"></label>
												</div>
											  </div>
										</div>
										
										<div class="row-answer">
											<div class="txt-c middle-xs">			
												<div class="col d-block">
													<div class="wr _self-col-xs-02-mb0">
														<input type="text" class="txt-dot txt-c" id="q4-opt4" name="q4-opt4" value="4" readonly>
													</div>
												</div>
												<div class="icheck-primary">
													<input type="radio" name="q4-chk" id="q4-chk4" disabled> 
													<label for="q4-chk4"></label>
												</div>
											  </div>
										</div>
										
										<div class="row-answer">
											<div class="txt-c middle-xs">			
												<div class="col d-block">
													<div class="wr _self-col-xs-02-mb0">
														<input type="text" class="txt-dot txt-c" id="q4-opt5" name="q4-opt5" value="5" readonly>
													</div>
												</div>
												<div class="icheck-primary">
													<input type="radio" name="q4-chk" id="q4-chk5" disabled> 
													<label for="q4-chk5"></label>
												</div>
											  </div>
										</div>
										
										

									</div>
									<div id="rating-star" style="display: none" class="group-rating _chd-ph10 flex-nowrap q-answer">

										<div class="row-answer">
											<div class="txt-c middle-xs">			
												<div class="col d-block">
													<div class="wr _self-col-xs-02-mb0">
														<input type="text" class="txt-dot txt-c" id="q4-opt1" name="q4-opt1" value="1" required>
													</div>
												</div>
												<div class="icheck-primary">
													<input type="radio" name="q4-chk" id="q1-chk1" disabled> 
													<label for="q4-chk1"></label>
												</div>
											  </div>
										</div>
										<div class="row-answer">
											<div class="txt-c middle-xs">			
												<div class="col d-block">
													<div class="wr _self-col-xs-02-mb0">
														<input type="text" class="txt-dot txt-c" id="q4-opt2" name="q4-opt2" value="2" required>
													</div>
												</div>
												<div class="icheck-primary">
													<input type="radio" name="q4-chk" id="q4-chk2" disabled> 
													<label for="q4-chk2"></label>
												</div>
											  </div>
										</div>
										
										<div class="row-answer">
											<div class="txt-c middle-xs">			
												<div class="col d-block">
													<div class="wr _self-col-xs-02-mb0">
														<input type="text" class="txt-dot txt-c" id="q4-opt1" name="q4-opt1" value="3" readonly>
													</div>
												</div>
												<div class="icheck-primary">
													<input type="radio" name="q4-chk" id="q4-chk3" disabled> 
													<label for="q4-chk3"></label>
												</div>
											  </div>
										</div>
										
										<div class="row-answer">
											<div class="txt-c middle-xs">			
												<div class="col d-block">
													<div class="wr _self-col-xs-02-mb0">
														<input type="text" class="txt-dot txt-c" id="q4-opt4" name="q4-opt4" value="4" readonly>
													</div>
												</div>
												<div class="icheck-primary">
													<input type="radio" name="q4-chk" id="q4-chk4" disabled> 
													<label for="q4-chk4"></label>
												</div>
											  </div>
										</div>
										
										<div class="row-answer">
											<div class="txt-c middle-xs">			
												<div class="col d-block">
													<div class="wr _self-col-xs-02-mb0">
														<input type="text" class="txt-dot txt-c" id="q4-opt5" name="q4-opt5" value="5" readonly>
													</div>
												</div>
												<div class="icheck-primary">
													<input type="radio" name="q4-chk" id="q4-chk5" disabled> 
													<label for="q4-chk5"></label>
												</div>
											  </div>
										</div>
										
										

									</div>
									<div id="rating-smile" style="display: none" class="group-rating _chd-ph10 flex-nowrap q-answer">

										<div class="row-answer">
											<div class="txt-c middle-xs">			
												<div class="col d-block">
													<div class="wr _self-col-xs-02-mb0">
														<input type="text" class="txt-dot txt-c" id="q4-opt1" name="q4-opt1" value="1" required>
													</div>
												</div>
												<div class="icheck-primary">
													<input type="radio" name="q4-chk" id="q1-chk1" disabled> 
													<label for="q4-chk1"></label>
												</div>
											  </div>
										</div>
										<div class="row-answer">
											<div class="txt-c middle-xs">			
												<div class="col d-block">
													<div class="wr _self-col-xs-02-mb0">
														<input type="text" class="txt-dot txt-c" id="q4-opt2" name="q4-opt2" value="2" required>
													</div>
												</div>
												<div class="icheck-primary">
													<input type="radio" name="q4-chk" id="q4-chk2" disabled> 
													<label for="q4-chk2"></label>
												</div>
											  </div>
										</div>
										
										<div class="row-answer">
											<div class="txt-c middle-xs">			
												<div class="col d-block">
													<div class="wr _self-col-xs-02-mb0">
														<input type="text" class="txt-dot txt-c" id="q4-opt1" name="q4-opt1" value="3" readonly>
													</div>
												</div>
												<div class="icheck-primary">
													<input type="radio" name="q4-chk" id="q4-chk3" disabled> 
													<label for="q4-chk3"></label>
												</div>
											  </div>
										</div>
										
										<div class="row-answer">
											<div class="txt-c middle-xs">			
												<div class="col d-block">
													<div class="wr _self-col-xs-02-mb0">
														<input type="text" class="txt-dot txt-c" id="q4-opt4" name="q4-opt4" value="4" readonly>
													</div>
												</div>
												<div class="icheck-primary">
													<input type="radio" name="q4-chk" id="q4-chk4" disabled> 
													<label for="q4-chk4"></label>
												</div>
											  </div>
										</div>
										
										<div class="row-answer">
											<div class="txt-c middle-xs">			
												<div class="col d-block">
													<div class="wr _self-col-xs-02-mb0">
														<input type="text" class="txt-dot txt-c" id="q4-opt5" name="q4-opt5" value="5" readonly>
													</div>
												</div>
												<div class="icheck-primary">
													<input type="radio" name="q4-chk" id="q4-chk5" disabled> 
													<label for="q4-chk5"></label>
												</div>
											  </div>
										</div>
										
										

									</div>
									<!-- Add label -->

										<div class="row-answer pl10-xs pl20-md">
											<div class="input-group d-flex middle-xs">
												<div class="col d-flex middle-xs">
													<div class="text-option">
														<input type="text" class="txt-line" id="q1-optadd" name="q1-optadd" placeholder="Label (optional)" style="width: 150px">
													</div>
												</div>
											  </div>

										</div>
			
									<!-- /Add label -->
									</div>
									
													

								</div>
								<!-- /card -->
								
								<div class="add-qa d-flex end-sm center-xs">
									<a href="javascript:;" class="ui-btn-border-sq btn-lg" title="+ Add question"><i class="fas fa-plus"></i> Add question</a>
								</div>
								
								<div class="sticky-bottom card-footer bg-white">
										<div class="_chd-ph10 center-xs">
												<a href="broadcasts-survey.php" class="ui-btn-gray btn-md">Black</a>
												<button type="submit" class="ui-btn-yellow btn-md" onclick="$('.form-sending')[0].reset();">Save draff</button>
												<button type="submit" class="ui-btn-green btn-md" onclick="$('.form-sending')[0].reset();">Save</button>
										</div>
								  </div>
								
					
					</div>
					</form>

				</div>
			</div>
			
			
			
		</section>
    </div>
</div>

<!--<div id="skin-loading" class="bg-wh" onclick="$(this).fadeOut();">
	<div class="lds-hourglass"></div>
</div>-->
<script>
	window.setTimeout(function(){
		$('#skin-loading').fadeOut();
	}, 3000);
</script>

<!-- footer -->
<?php include("incs/footer-web.html") ?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js-web.html") ?>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<link href="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.css" rel="stylesheet"/>
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<script type="text/javascript">
$( document ).ready( function () {
	 
      $('#type-rating').on('change', function() {

		if ($(this)[0].selectedIndex == 0) { 
			$('#rating-normal').show();
			$('#rating-star').hide();
			$('#rating-smile').hide();
		} else if ($(this)[0].selectedIndex == 1)  {
			$('#rating-normal').hide();
			$('#rating-star').show();
			$('#rating-smile').hide();
		} else if ($(this)[0].selectedIndex == 2)  {
			$('#rating-normal').hide();
			$('#rating-star').hide();
			$('#rating-smile').show();
		}

	});

	//select2
	$(".select-num").select2({minimumResultsForSearch: -1});
	/*$('.keep-select-group').select2({
    	placeholder: "Please select",
    	//allowClear: true,
		dropdownAutoWidth : true,
		width: '100%'
	});*/
	//select2

function formatState (opt) {
    if (!opt.id) {
        return opt.text.toUpperCase();
    } 

    var optimage = $(opt.element).attr('data-image'); 
    console.log(optimage)
    if(!optimage){
       return opt.text.toUpperCase();
    } else {                    
        var $opt = $(
           '<span><img src="' + optimage + '" width="32px" /> ' + opt.text.toUpperCase() + '</span>'
        );
        return $opt;
    }
};

	$(".select-type-sv").select2({
		templateResult: formatState,
		templateSelection: formatState,
		minimumResultsForSearch: -1,
	});



/*function format(state) {
    if (!state.id) return state.text; // optgroup
    return "<img class='flag' src='https://select2.github.io/select2/images/flags/" + state.id.toLowerCase() + ".png'/>" + state.text;
}
$("#e4").select2({
    formatResult: format,
    formatSelection: format,
    escapeMarkup: function(m) { return m; }
});
*/
	

});
  </script>
  


<!-- /js -->

</body>
</html>
