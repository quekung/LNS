<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top-web.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden">
<!-- Headbar -->
<?php include("incs/header-v2.html") ?>
<script>
$(".main-menu .list-unstyled>li.active").removeClass('active');
$(".main-menu .list-unstyled>li:nth-child(4)").addClass('active');
</script>
<!-- /Headbar -->
<div class="page-checkout">

    
    <div id="toc">
		<section class="z-broadcast _self-pt0 mb0">
			<div class="bx-stepbar _self-pv20 cb-af container">
				<ul class="tabsbar">
						  <li><a href="broadcasts.php" title="Send Message"><i class="fas fa-bullhorn"></i> <span>Send Message</span></a></li>
						  <li><a href="broadcasts-create.php" title="Create Message"><i class="fas fa-layer-group"></i> <span>Create Template</span></a></li>
						  <li><a href="broadcasts-acc.php" title="User Detail"><i class="fas fa-users-cog"></i> <span>User Detail</span></a></li>
						  <li><a href="broadcasts-setting.php" title="Message Setting"><i class="fas fa-sliders-h"></i> <span>Message Setting</span></a></li>
						  <li><a href="broadcasts-report.php" title="Report"><i class="fas fa-file-medical-alt"></i> <span>Report</span></a></li>
						  <li><a href="broadcasts-survey.php" title="Survey" class="selected"><i class="fas fa-tasks"></i> <span>Survey</span></a></li>
				  </ul>
			</div>
					

			
			<div class="contentTabs main-sv">
				<div id="tbc-1" class="msg">

					<div class="head-bg">
					<div class="container">
						<h2>SURVEYS</h2>	
					</div>
					</div>


					<div class="main container">

						<div class="sort-bar d-flex between-xs middle-xs">
							<div class="sort">
								<select class="form-control select2" data-placeholder="Order by" style="width:auto">
								  <option></option>
								  <option>Last Update</option>
								  <option>Create Date</option>
								  <option>Processed</option>
								  <option>On Hold</option>
								</select>
								<div class="search-sm d-inline">
									<input class="txt-box" placeholder="ค้นหา...">
									<button type="submit" class="fas fa-search"></button>
								</div>
							</div>
							<div class="right">
								<a class="ui-btn-black btn-xs" href="survey-create.php" title="Add new">Add New</a>
							</div>
						</div>
						
						<div class="list-users list-survey">
							<div class="head card _flex middle-xs between-xs">
								<h2>Survey name</h2>
								<div class="detail d-flex flex-nowrap _chd-cl-xs middle-xs">
									<p class="col date">Start-End Date</p>
									<p class="col status">Status</p>
									<p class="col button">Send</p>
								</div>
							</div>
							
							<div class="card _flex middle-xs between-xs">
								<h2><i class="fas fa-clipboard-list"></i> <a href="survey-edit.php">แบบสำรวจความพึงพอใจในการปฏิบัติงานของพนักงาน ครึ่งปีแรก 2563</a></h2>
								<div class="detail d-flex flex-nowrap _chd-cl-xs middle-xs">
									<p class="col date">1 - 30 ก.ค. 63</p>
									<p class="col status"><span class="btn btn-outline-success btn-sm rounded2">Processed</span></p>
									<p class="col button"><a href="broadcasts-push-survey.php" class="ui-btn-gray2-min btn-xs"><i class="fas fa-paper-plane"></i> Push Survey</a></p>
								</div>
							</div>
							
							<div class="card _flex middle-xs between-xs">
								<h2><i class="fas fa-clipboard-list"></i> <a href="survey-edit.php">ความพึงพอใจของนักศึกษาต่อความสะอาดและความปลอดภัยของหอพักใน</a></h2>
								<div class="detail d-flex flex-nowrap _chd-cl-xs middle-xs">
									<p class="col date">1 - 30 ก.ค. 63</p>
									<p class="col status"><span class="btn btn-outline-success btn-sm rounded2">Processed</span></p>
									<p class="col button"><a href="broadcasts-push-survey.php" class="ui-btn-gray2-min btn-xs"><i class="fas fa-paper-plane"></i> Push Survey</a></p>
								</div>
							</div>
							
							<div class="card _flex middle-xs between-xs">
								<h2><i class="fas fa-clipboard-list"></i> <a href="survey-edit.php">แบบสำรวจความพึงพอใจต่องานอาคารและสถานที่หอใน</a></h2>
								<div class="detail d-flex flex-nowrap _chd-cl-xs middle-xs">
									<p class="col date">1 - 30 ส.ค. 63</p>
									<p class="col status"><span class="btn btn-outline-gray btn-sm rounded2">On Hold</span></p>
									<p class="col button"><a href="broadcasts-push-survey.php" class="ui-btn-gray2-min btn-xs"><i class="fas fa-paper-plane"></i> Push Survey</a></p>
								</div>
							</div>
							
							<div class="card _flex middle-xs between-xs">
								<h2><i class="fas fa-clipboard-list"></i> <a href="survey-edit.php">สำรวจความสะอาดของห้องน้ำภายในอาคารหอสมุดกลาง</a></h2>
								<div class="detail d-flex flex-nowrap _chd-cl-xs middle-xs">
									<p class="col date">1 - 7 ส.ค. 63</p>
									<p class="col status"><span class="btn btn-outline-gray btn-sm rounded2">Processed</span></p>
									<p class="col button"><a href="broadcasts-push-survey.php" class="ui-btn-gray2-min btn-xs"><i class="fas fa-paper-plane"></i> Push Survey</a></p>
								</div>
							</div>
							
							

						</div>
						

					</div>



			</div>
			</div>
			
			
			
		</section>
    </div>
</div>

<!--<div id="skin-loading" class="bg-wh" onclick="$(this).fadeOut();">
	<div class="lds-hourglass"></div>
</div>-->
<script>
	window.setTimeout(function(){
		$('#skin-loading').fadeOut();
	}, 3000);
</script>

<!-- footer -->
<?php include("incs/footer-web.html") ?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js-web.html") ?>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<link href="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.css" rel="stylesheet"/>
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<script type="text/javascript">
$( document ).ready( function () {
	 
      $('.carousel').flexslider({
        animation: "slide",
        animationLoop: false,
        itemWidth: 210,
        itemMargin: 5,
        minItems: 1,
        maxItems: 3,
		move: 1,
        /*start: function(slider){
          $('body').removeClass('loading');
        }*/
      });

	//select2
	$(".select2").select2({minimumResultsForSearch: -1});
	/*$('.keep-select-group').select2({
    	placeholder: "Please select",
    	//allowClear: true,
		dropdownAutoWidth : true,
		width: '100%'
	});*/
	//select2


/*function format(state) {
    if (!state.id) return state.text; // optgroup
    return "<img class='flag' src='https://select2.github.io/select2/images/flags/" + state.id.toLowerCase() + ".png'/>" + state.text;
}
$("#e4").select2({
    formatResult: format,
    formatSelection: format,
    escapeMarkup: function(m) { return m; }
});
*/
	

});
  </script>
  


<!-- /js -->

</body>
</html>
