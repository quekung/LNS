<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top-web.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-hidden">
<!-- Headbar -->
<?php include("incs/header-v2.html") ?>
<!-- /Headbar -->
<div class="page-checkout">

    
    <div id="toc">
		<section class="z-success txt-c _self-pt00 mb0">
			<div class="bx-stepbar _self-pv20 cb-af container">
				<ul class="progressbar">
						  <li class="active"><span>Choose your plan</span></li>
						  <li class="active"><span>Payment</span></li>
						  <li class="active"><span>Account setting</span></li>
						  <li><span>Get started</span></li>
				  </ul>
			</div>
			
			<!--<div class="progress-title mb20 txt-c">
				<h3 class="t-gray head">กรุณาตั้งค่าบัญชีใน Google account ของคุณ</h3>
			</div>-->
			
			<div class="bg-gray2 g-connect">
				<div class="container msg">
					
					<h2><i class="fas fa-check-circle _self-mr10"></i><b class="t-green2">ชำระเงินสำเร็จ</b></h2>	
					<p>ระบบได้ส่งใบเสร็จอิเล็กทรอนิกส์ไปยังอีเมลของท่านเรียบร้อยแล้ว</p>
					
				</div>

				<div class="ctrl-btn txt-c _self-mt30" style="padding: 30px;width: 800px;max-width:95%;margin: 0 auto;border-radius: 2em;background: #fff;">
					<h2><span style="display: inline-block;border-bottom: 2px solid #000;margin-bottom: 10px;">ขั้นตอนต่อไป!</span></h2>
				<p class="t-black"><big>กรุณาตั้งค่าบัญชีใน Google account ของคุณ<br>
				  เพื่อเปิดสิทธิ์การทำงานบน Google Drive , Google Contacts และ Google Calendar อย่างสมบูรณ์</big>
				</p>
								<a href="success-congrat.php" title="Get started on Line" class="ui-btn-green-sq t-white _self-mt30 btn-google">
									<i style="vertical-align: middle;" class="fas fa-angle-right fa-2x"></i> ไปยังตั้งค่าบัญชี 
									<i class="ic-google"><img alt="Google icon" src="https://img.icons8.com/color/344/google-logo.png" height="55"></i>
								</a>
							</div>
			</div>
			
			
			
			
		</section>
    </div>
</div>

<!--<div id="skin-loading" class="bg-wh" onclick="$(this).fadeOut();">
	<div class="lds-hourglass"></div>
</div>-->
<script>
	window.setTimeout(function(){
		$('#skin-loading').fadeOut();
	}, 3000);
</script>

<!-- footer -->
<?php include("incs/footer-web.html") ?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js-web.html") ?>
<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>
<!-- /js -->

</body>
</html>
