<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top-web.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden">
<!-- Headbar -->
<?php include("incs/header-v2.html") ?>
<script>
$(".main-menu .list-unstyled>li.active").removeClass('active');
$(".main-menu .list-unstyled>li:nth-child(6)").addClass('active');
</script>
<!-- /Headbar -->
<div class="page-checkout">

    
    <div id="toc">
		<section class="z-broadcast _self-pt0 mb0">
			<div class="bx-stepbar _self-pv20 cb-af container">
				<ul class="tabsbar">
						  <li><a href="company.php" title="Send Message" class="selected"><i class="fas fa-archway"></i> <span>Company</span></a></li>
						  <li><a href="department.php" title="Create Message"><i class="fas fa-boxes"></i> <span>Department</span></a></li>
						  <li><a href="member-account.php" title="User management "><i class="fas fa-user-shield"></i> <span>User management</span></a></li>
						  <li><a href="broadcasts-report.php" title="Report"><i class="fas fa-file-medical-alt"></i> <span>Report</span></a></li>
						  <!--<li><a href="broadcasts-acc.php" title="User Detail"><i class="fas fa-users-cog"></i> <span>User Detail</span></a></li>
						  <li><a href="lns-setting.php" title="Message Setting"><i class="fas fa-sliders-h"></i> <span>Message Setting</span></a></li>
						  <li><a href="broadcasts-report.php" title="Report"><i class="fas fa-file-medical-alt"></i> <span>Report</span></a></li>-->
				  </ul>
			</div>
					

			
			<div class="contentTabs main-sv">
				<div id="tbc-1" class="msg">

					<div class="head-bg">
					<div class="container">
						<h2>SETTING</h2>	
					</div>
					</div>


					<div class="main container">

						<div class="sort-bar d-flex between-xs middle-xs">
							<div class="sort">
								<select class="form-control select2" data-placeholder="Order by" style="width:auto">
								  <option></option>
								  <option>Last Update</option>
								  <option>Create Date</option>
								  <option>Active</option>
								  <option>On Hold</option>
								</select>
								<div class="search-sm d-inline">
									<input class="txt-box" placeholder="ค้นหา...">
									<button type="submit" class="fas fa-search"></button>
								</div>
							</div>
							<div class="right">
								<a class="ui-btn-black btn-xs" href="company-create.php" title="Add new">Add New</a>
							</div>
						</div>
						
						<div class="list-users list-survey projects">
							<div class="head card _flex middle-xs between-xs">
								<h2>Company name</h2>
								<div class="detail d-flex flex-nowrap _chd-cl-xs middle-xs">
									<p class="col member">Team Members</p>
									<p class="col depart">Department</p>
									<p class="col status">Status</p>
									<p class="col button">&nbsp;</p>
								</div>
							</div>
							
							<div class="card _flex middle-xs between-xs">
								<h2><i class="fas fa-archway"></i> <a href="company-detail.php">INNOHUB</a></h2>
								<div class="detail d-flex flex-nowrap _chd-cl-xs middle-xs">
									<div class="col member">
									<ul class="list-inline">
									  <li class="list-inline-item">
										  <img alt="Avatar" class="table-avatar" src="https://www.w3schools.com/w3images/avatar2.png">
										  <span>Username1</span>
									  </li>
									  <li class="list-inline-item">
										  <img alt="Avatar" class="table-avatar" src="https://www.w3schools.com/w3images/avatar3.png">
										  <span>Username2</span>
									  </li>
									  <li class="list-inline-item">
										  <img alt="Avatar" class="table-avatar" src="https://www.w3schools.com/w3images/avatar4.png">
										  <span>Username3</span>
									  </li>
									  <li class="list-inline-item">
										  <img alt="Avatar" class="table-avatar" src="https://www.w3schools.com/w3images/avatar6.png">
										  <span>Username4</span>
									  </li>
									  <li class="list-inline-item"><div class="more"><span>+8</span></div></li>
								    </ul>
									</div>
									<div class="col depart">
										<p>Platform</p>
										<p>ML</p>
									</div>
									<p class="col status"><span class="btn btn-outline-success btn-sm rounded2">Active</span></p>
									<p class="col button"><a href="company-detail.php" class="ui-btn-gray2-min btn-xs" title="view detail"><i class="fas fa-edit"></i> View</a> <a href="#" class="t-gray2 ml10-xs btn-xs" title="delete"><i class="fas fa-trash"></i></a></p>
								</div>
							</div>
							
							<div class="card _flex middle-xs between-xs">
								<h2><i class="fas fa-archway"></i> <a href="company-detail.php">Sprinkle</a></h2>
								<div class="detail d-flex flex-nowrap _chd-cl-xs middle-xs">
									<div class="col member">
									<ul class="list-inline">
									  <li class="list-inline-item">
										  <img alt="Avatar" class="table-avatar" src="https://www.w3schools.com/w3images/avatar2.png">
										  <span>Username1</span>
									  </li>
									  <li class="list-inline-item">
										  <img alt="Avatar" class="table-avatar" src="https://www.w3schools.com/w3images/avatar3.png">
										  <span>Username2</span>
									  </li>
									  <li class="list-inline-item">
										  <img alt="Avatar" class="table-avatar" src="https://www.w3schools.com/w3images/avatar4.png">
										  <span>Username3</span>
									  </li>
									  <li class="list-inline-item">
										  <img alt="Avatar" class="table-avatar" src="https://www.w3schools.com/w3images/avatar6.png">
										  <span>Username4</span>
									  </li>
								    </ul>
									</div>
									<div class="col depart">
										<p>Service</p>
										<p>Marketing</p>
									</div>
									<p class="col status"><span class="btn btn-outline-success btn-sm rounded2">Active</span></p>
									<p class="col button"><a href="company-detail.php" class="ui-btn-gray2-min btn-xs" title="view detail"><i class="fas fa-edit"></i> View</a> <a href="#" class="t-gray2 ml10-xs btn-xs" title="delete"><i class="fas fa-trash"></i></a></p>
								</div>
							</div>
							
							<div class="card _flex middle-xs between-xs">
								<h2><i class="fas fa-archway"></i> <a href="company-detail.php">segroup (อาคเนย์)</a></h2>
								<div class="detail d-flex flex-nowrap _chd-cl-xs middle-xs">
									<div class="col member">
									<ul class="list-inline">
									  <li class="list-inline-item">
										  <img alt="Avatar" class="table-avatar" src="https://www.w3schools.com/w3images/avatar4.png">
										  <span>Username3</span>
									  </li>
									  <li class="list-inline-item">
										  <img alt="Avatar" class="table-avatar" src="https://www.w3schools.com/w3images/avatar6.png">
										  <span>Username4</span>
									  </li>
								    </ul>
									</div>
									<div class="col depart">
										<p>Marketing</p>
									</div>
									<p class="col status"><span class="btn btn-outline-gray btn-sm rounded2">On Hold</span></p>
									<p class="col button"><a href="company-detail.php" class="ui-btn-gray2-min btn-xs" title="view detail"><i class="fas fa-edit"></i> View</a> <a href="#" class="t-gray2 ml10-xs btn-xs" title="delete"><i class="fas fa-trash"></i></a></p>
								</div>
							</div>
							
							<div class="card _flex middle-xs between-xs">
								<h2><i class="fas fa-archway"></i> <a href="company-detail.php">BNH Hospital</a></h2>
								<div class="detail d-flex flex-nowrap _chd-cl-xs middle-xs">
									<div class="col member">
									<ul class="list-inline">

									  <li class="list-inline-item">
										  <img alt="Avatar" class="table-avatar" src="https://www.w3schools.com/w3images/avatar3.png">
										  <span>Username2</span>
									  </li>
								    </ul>
									</div>
									<div class="col depart">
										<p>Sale</p>
										<p>Marketing</p>
									</div>
									<p class="col status"><span class="btn btn-outline-gray btn-sm rounded2">On Hold</span></p>
									<p class="col button"><a href="company-detail.php" class="ui-btn-gray2-min btn-xs" title="view detail"><i class="fas fa-edit"></i> View</a> <a href="#" class="t-gray2 ml10-xs btn-xs" title="delete"><i class="fas fa-trash"></i></a></p>
								</div>
							</div>
							
							

						</div>
						

					</div>



			</div>
			</div>
			
			
			
		</section>
    </div>
</div>

<!--<div id="skin-loading" class="bg-wh" onclick="$(this).fadeOut();">
	<div class="lds-hourglass"></div>
</div>-->
<script>
	window.setTimeout(function(){
		$('#skin-loading').fadeOut();
	}, 3000);
</script>

<!-- footer -->
<?php include("incs/footer-web.html") ?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js-web.html") ?>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<link href="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.css" rel="stylesheet"/>
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<script type="text/javascript">
$( document ).ready( function () {
	 
      $('.carousel').flexslider({
        animation: "slide",
        animationLoop: false,
        itemWidth: 210,
        itemMargin: 5,
        minItems: 1,
        maxItems: 3,
		move: 1,
        /*start: function(slider){
          $('body').removeClass('loading');
        }*/
      });

	//select2
	$(".select2").select2({minimumResultsForSearch: -1});
	/*$('.keep-select-group').select2({
    	placeholder: "Please select",
    	//allowClear: true,
		dropdownAutoWidth : true,
		width: '100%'
	});*/
	//select2


/*function format(state) {
    if (!state.id) return state.text; // optgroup
    return "<img class='flag' src='https://select2.github.io/select2/images/flags/" + state.id.toLowerCase() + ".png'/>" + state.text;
}
$("#e4").select2({
    formatResult: format,
    formatSelection: format,
    escapeMarkup: function(m) { return m; }
});
*/
	

});
  </script>
  


<!-- /js -->

</body>
</html>
