<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top-web.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden">
<!-- Headbar -->
<?php include("incs/header-v2.html") ?>
<script>
$(".main-menu .list-unstyled>li.active").removeClass('active');
$(".main-menu .list-unstyled>li:nth-child(3)").addClass('active');
</script>
<!-- /Headbar -->
<div class="page-checkout">

    
    <div id="toc">
		<section class="z-broadcast _self-pt0 mb0">
			<div class="bx-stepbar _self-pv20 cb-af container">
				<ul class="tabsbar">
						  <li><a href="call2line.php" title="Send Message"><i class="fas fa-bullhorn"></i> <span>Send Message</span></a></li>
						  <li><a href="call2line-create.php" title="Create Message"><i class="fas fa-layer-group"></i> <span>Create Template</span></a></li>
						  <li><a href="call2line-setting.php" title="Message Setting" class="selected"><i class="fas fa-sliders-h"></i> <span>Message Setting</span></a></li>
						  <li><a href="call2line-report.php" title="Report"><i class="fas fa-file-medical-alt"></i> <span>Report</span></a></li>
				  </ul>
			</div>
					

			
			<div class="bg-gray2 contentTabs">
				<div id="tbc-1" class="msg">
					<form method="post" class="form-checkout form-sending'">
					<div class="head-title container txt-c">
						<h2 class="t-black"><i class="fas fa-sliders-h _self-mr10 t-black"></i>Message Deliver Setting</h2>	
						<p>ระบบส่งข้อความผ่านทาง line</p>
					</div>
					<div class="wrap-full _chd-cl-xs-12 _chd-cl-sm">
						<div class="main">
							<div class="container">
								<!-- card -->
								<div id="msg-setting-tabContent" class="card">
									<ul class="idTabs tab-receiver tab-setting">
											<li><a href="#month1" class="selected"><i class="fas fa-circle nav-icon"></i> JUL</a></li>
											<li><a href="#month2"><i class="far fa-circle nav-icon"></i> AUG</a></li>
											<li><a href="#month3"><i class="far fa-circle nav-icon"></i> SEP</a></li>
									</ul>
									
									<div id="month1" class="bx-tab rounded1 bg-white">
									<div class="card-header border-0">
										<h3 class="card-title center-xs"><b>1. Monthly Limit setting</b></h3>
									</div>
									
									
									<div class="card-body d-flex msd-setting _self-pa20 top-xs">
										
										
										<div class="col-xs-12 col-sm-7 col-md-8">
										<ol class="setting-limit p-0">
						
						
										<li class="row d-flex between-xs middle-xs bg-light pa05-xs mb15-xs rounded">
																<div class="col">
											<div class="d-flex middle-xs">
											  <div class="input-name col-sm-4">
												<span class="d-block pr10-xs txt-r">PNP <small class="d-block" style="margin-top: -5px">Call to LINE</small></span>
											  </div>
											  <input type="text" id="pnp-noti-limit" class="txt-box rounded-2 col-sm-6 txt-c" value="200000" disabled="true">
											  <div class="input-edit">
												<span class="pl10-xs" onclick="$('#pnp-noti-limit').prop('disabled',false).focus(); $('#pnp-noti-unlimit').prop('checked', false);" style="cursor: pointer"><i class="far fa-edit"></i></span>
											  </div>
											</div>
										</div>

										<div class="col-sm-5 d-flex between-xs middle-xs">

												<div class="icheck-gray chk-unlimit mt-0 mb-0 pa10-xs"><input type="checkbox" name="pnp-noti-type" id="pnp-noti-unlimit" checked> <label for="pnp-noti-unlimit" style="font-weight: 400;">Unlimited</label></div>
												<div class="uid-lnm d-flex middle-xs">
													<small class="mr10-xs text-muted">UID Setting</small>
													<div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success float-right">
													  <input type="checkbox" class="custom-control-input" id="uidOffSwitch1-2" checked="">
													  <label class="custom-control-label" for="uidOffSwitch1-2" style="display: block;clear: both"></label>
													  <small class="text-label"></small>
													</div>
												</div>

										</div>
										</li>

										<li class="row d-flex between-xs middle-xs bg-light pa05-xs mb15-xs rounded">
										<!--<div class="col-sm-01">
											<div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
											  <input type="checkbox" class="custom-control-input" id="customSwitch3" checked>
											  <label class="custom-control-label" for="customSwitch3" style="display: block;clear: both"></label>
											</div>
											<small class="d-block" style="margin-top: -3px">on/off</small>
										</div>-->
																<div class="col">
											<div class="d-flex middle-xs">
											  <div class="input-name col-sm-4">
												<span class="d-block pr10-xs txt-r">&nbsp;UID</span>
											  </div>
											  <input type="text" id="uid-limit" class="txt-box rounded-2 col-sm-6 txt-c" value="35000" disabled="true">
											  <div class="input-edit">
												<span class="pl10-xs" onclick="$('#uid-limit').prop('disabled',false).focus(); $('#uid-unlimit').prop('checked', false); " style="cursor: pointer"><i class="far fa-edit"></i></span>
											  </div>
											</div>
										</div>


										<div class="col-sm-5">

												<div class="icheck-gray chk-unlimit mt-0 mb-0 pa10-xs"><input type="checkbox" name="uid-type" id="uid-unlimit" checked> <label for="uid-unlimit" style="font-weight: 400;">Unlimited</label></div>


										</div>
										</li>
									  </ol>
										
										</div>
										<div class="col-xs-12 col-sm-5 col-md-4">
											<div class="live-report">
											

												  
												  <div class="row _chd-cl-xs-06">
													 <div class="col form-group _self-mb0">

														<input type="text" id="inputEstimatedBudget" class="txt-box  is-warning bg-white rounded-2 txt-c" value="197250" readonly>
														<label for="inputEstimatedBudget" class="d-block txt-c">PNP <small class="text-muted">Message Delivered</small></label>
													  </div>
													  <div class="col form-group _self-mb0">

														<input type="text" id="inputSpentBudget" class="txt-box  rounded-2 txt-c" value="200000" readonly>
														<label for="inputSpentBudget" class="d-block txt-c">PNP <small class="text-muted">Monthly Credit</small></label>
													  </div>
												  </div>
												  <div class="row _chd-cl-xs-06">
													  <div class="col form-group _self-mb0">

														<input type="text" id="inputEstimatedDuration" class="txt-box  is-invalid bg-white rounded-2 txt-c" value="37000" readonly>
														<label for="inputEstimatedDuration" class="d-block txt-c">UID  <small class="text-muted">Message Delivered</small></label>
													  </div>
													  <div class="col form-group _self-mb0">

														<input type="text" id="inputEstimatedDuration" class="txt-box  rounded-2 txt-c" value="35000" readonly>
														<label for="inputEstimatedDuration" class="d-block txt-c">UID  <small class="text-muted">Monthly Credit</small></label>
													  </div>
												  </div>

										  </div>
										</div>
									</div>
									
									
									<div class="card-header border-0">
										<h3 class="card-title center-xs"><b>2. Priority setting / by Weekly</b></h3>
									</div>
									
									<div  id="type1" class="pa20-sm pa10-xs">
						
									 <div class="head-reserve d-flex middle-xs mb10-xs">
										<label class="text-md">Reserve PNP Call to LINE Message
										<span>From Monthly credit</span></label>
										<span class="col ml10-xs"><input type="text" class="form-control bg-white rounded-2 txt-c form-control-sm" value="200,000" readonly></span>
										<small class="mr10-xs text-muted">Period Setting</small>
										<div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success float-right">
																	<input type="checkbox" class="custom-control-input" id="periodSwitch2-m1" onClick="$('#offPeriod').slideToggle()">
																<label class="custom-control-label" for="periodSwitch2-m1" style="display: block;clear: both"></label>
											<small class="text-label"></small>
										</div>
									  </div>
									  <div id="offPeriod" style="display: none">
									  <div class="main-range  d-flex bg-gray2 pa20-xs rounded mb30-xs" data-select2-id="73">

											 <div class="col css-range pr20-xs">
												<input id="range_1-1" type="text" name="range_1-1" value="10">
											 </div>
											 <div class="fix-w">
												  <input type="text" id="res1-1_pnp" class="form-control bg-white rounded-2 txt-c" value="20,000">
											 </div>
										</div>

										<label class="text-md">PNP Setting</label>
										<!-- weekly -->
										<div class="row-date mb20-xs">

										  <div class="main-week d-flex pa10-xs rounded bg-white">
											<div class="col-md-3 d-flex bottom-xs">
												<h5 class="text-sm f-normal mr10-xs">Period 1</h5>
												<div class="txt-c col">
													<label class="text-muted">Minium </label>
													<input type="number" id="res1-1_week1" step="100" class="form-control rounded-2 txt-c" value="45000">
												</div>
											</div>
											<div class="col d-flex bottom-xs" style="border-left:2px solid #eaeaea">

												<div class="col pl10-xs">
													<label class="d-block txt-c"><span class="text-muted">Select </span>Period</label>
													<div class="input-group">
													<div class="input-group-prepend">
													  <span class="input-group-text rounded-2">
														<i class="far fa-calendar-alt"></i>
													  </span>
													</div>
													<input type="text" class="form-control float-right rounded-2 txt-c reservation1" id="reservation1-1" value="05/01/2020 - 05/07/2020">
												  </div>
												</div>
											</div>
											<div class="col-sm-5">
												<div class="d-flex bottom-xs _chd-ph10">
													<div class="col mb-0">
														<label for="PeriodPNP1" class="d-block txt-c">Period 1 PNP <small class="text-muted">Message Delivered</small></label>
														<input type="text" id="PeriodPNP1" class="form-control is-valid bg-white rounded-2 txt-c" value="18780" readonly>

													</div>
													<div class="col mb-0">
														<label for="PeriodUID1" class="d-block txt-c">Period 1 UID <small class="text-muted">Message Delivered</small></label>
														<input type="text" id="PeriodUID1" class="form-control is-warning bg-white rounded-2 txt-c" value="8780" readonly>

													</div>
												</div>
											</div>
										  </div>
										</div>
										<!-- /weekly -->

										<!-- weekly -->
										<div class="row-date mb20-xs">

										  <div class="main-week d-flex pa10-xs rounded bg-white">
											<div class="col-sm-3 d-flex bottom-xs">
												<h5 class="text-sm f-normal mr10-xs">Period 2</h5>
												<div class="txt-c col">
													<label class="text-muted">Minium </label>
													<input type="number" id="res1-1_week2" step="100" class="form-control rounded-2 txt-c" value="45000">
												</div>
											</div>
											<div class="col d-flex bottom-xs" style="border-left:2px solid #eaeaea">

												<div class="col pl10-xs">
													<label class="d-block txt-c"><span class="text-muted">Select </span>Period</label>
													<div class="input-group">
													<div class="input-group-prepend">
													  <span class="input-group-text rounded-2">
														<i class="far fa-calendar-alt"></i>
													  </span>
													</div>
													<input type="text" class="form-control float-right rounded-2 txt-c reservation2" id="reservation1-2" value="05/08/2020 - 05/14/2020">
												  </div>
												</div>
											</div>
											<div class="col-sm-5">
												<div class="d-flex bottom-xs _chd-ph10">
													<div class="col mb-0">
														<label for="PeriodPNP2" class="d-block txt-c">Period 2 PNP <small class="text-muted">Message Delivered</small></label>
														<input type="text" id="PeriodPNP2" class="form-control is-valid bg-white rounded-2 txt-c" value="18780" readonly>

													</div>
													<div class="col mb-0">
														<label for="PeriodUID2" class="d-block txt-c">Period 2 UID <small class="text-muted">Message Delivered</small></label>
														<input type="text" id="PeriodUID2" class="form-control is-valid bg-white rounded-2 txt-c" value="8780" readonly>

													</div>
												</div>
											</div>
										  </div>
										</div>
										<!-- /weekly -->

										<!-- weekly -->
										<div class="row-date mb20-xs">

										  <div class="main-week d-flex pa10-xs rounded bg-white">
											<div class="col-sm-3 d-flex bottom-xs">
												<h5 class="text-sm f-normal mr10-xs">Period 3</h5>
												<div class="txt-c col">
													<label class="text-muted">Minium </label>
													<input type="number" id="res1-1_week3" step="100" class="form-control rounded-2 txt-c" value="45000">
												</div>
											</div>
											<div class="col d-flex bottom-xs" style="border-left:2px solid #eaeaea">

												<div class="col pl10-xs">
													<label class="d-block txt-c"><span class="text-muted">Select </span>Period</label>
													<div class="input-group">
													<div class="input-group-prepend">
													  <span class="input-group-text rounded-2">
														<i class="far fa-calendar-alt"></i>
													  </span>
													</div>
													<input type="text" class="form-control float-right rounded-2 txt-c reservation3" id="reservation1-3" value="05/15/2020 - 05/21/2020">
												  </div>
												</div>
											</div>
											<div class="col-sm-5">
												<div class="d-flex bottom-xs _chd-ph10">
													<div class="col mb-0">
														<label for="PeriodPNP3" class="d-block txt-c">Period 3 PNP <small class="text-muted">Message Delivered</small></label>
														<input type="text" id="PeriodPNP3" class="form-control is-valid bg-white rounded-2 txt-c" value="0" readonly>

													</div>
													<div class="col mb-0">
														<label for="PeriodUID3" class="d-block txt-c">Period 3 UID <small class="text-muted">Message Delivered</small></label>
														<input type="text" id="PeriodUID3" class="form-control is-valid bg-white rounded-2 txt-c" value="0" readonly>

													</div>
												</div>
											</div>
										  </div>
										</div>
										<!-- /weekly -->

										<!-- weekly -->
										<div class="row-date mb20-xs">

										  <div class="main-week d-flex pa10-xs rounded bg-white">
											<div class="col-sm-3 d-flex bottom-xs">
												<h5 class="text-sm f-normal mr10-xs">Period 4</h5>
												<div class="txt-c col">
													<label class="text-muted">Minium </label>
													<input type="number" id="res1-1_week4" step="100" class="form-control rounded-2 txt-c" value="45000">
												</div>
											</div>
											<div class="col d-flex bottom-xs" style="border-left:2px solid #eaeaea">

												<div class="col pl10-xs">
													<label class="d-block txt-c"><span class="text-muted">Select </span>Period</label>
													<div class="input-group">
													<div class="input-group-prepend">
													  <span class="input-group-text rounded-2">
														<i class="far fa-calendar-alt"></i>
													  </span>
													</div>
													<input type="text" class="form-control float-right rounded-2 txt-c reservation4" id="reservation1-4" value="05/22/2020 - 05/31/2020">
												  </div>
												</div>
											</div>
											<div class="col-sm-5">
												<div class="d-flex bottom-xs _chd-ph10">
													<div class="col mb-0">
														<label for="PeriodPNP4" class="d-block txt-c">Period 4 PNP <small class="text-muted">Message Delivered</small></label>
														<input type="text" id="PeriodPNP4" class="form-control is-valid bg-white rounded-2 txt-c" value="0" readonly>

													</div>
													<div class="col mb-0">
														<label for="PeriodUID4" class="d-block txt-c">Period 4 UID <small class="text-muted">Message Delivered</small></label>
														<input type="text" id="PeriodUID4" class="form-control is-valid bg-white rounded-2 txt-c" value="0" readonly>

													</div>
												</div>
											</div>
										  </div>
										</div>
										<!-- /weekly -->
										
										</div>

									</div>
									
									</div>
									
									<div id="month2" class="bx-tab rounded1 bg-white">
										<div class="card-header border-0">
											<h3 class="card-title center-xs"><b>1. Monthly Limit setting</b></h3>
										</div>
										<div class="bg-gray2 d-flex center-xs middle-xs ml20-xs mr20-xs" style="height: 150px">
											<p class="text-md">Clone box tab1</p>
										</div>
										
										<div class="card-header border-0">
											<h3 class="card-title center-xs"><b>2. Priority setting / by Weekly</b></h3>
										</div>
										<div class="bg-gray2 d-flex center-xs middle-xs ml20-xs mr20-xs" style="height: 350px">
											<p class="text-md">Clone box tab1</p>
										</div>
										
									
									</div>
									<div id="month3" class="bx-tab rounded1 bg-white">
									
										<div class="card-header border-0">
											<h3 class="card-title center-xs"><b>1. Monthly Limit setting</b></h3>
										</div>
										<div class="bg-gray2 d-flex center-xs middle-xs ml20-xs mr20-xs" style="height: 150px">
											<p class="text-md">Clone box tab1</p>
										</div>
										
										<div class="card-header border-0">
											<h3 class="card-title center-xs"><b>2. Priority setting / by Weekly</b></h3>
										</div>
										<div class="bg-gray2 d-flex center-xs middle-xs ml20-xs mr20-xs" style="height: 350px">
											<p class="text-md">Clone box tab1</p>
										</div>
									</div>
									
									<div class="card-footer bg-white">
									<div class="_chd-ph10 center-xs">
											<button type="submit" class="ui-btn-gray btn-md" data-toggle="modal" data-target="#modal-close">Cancel</button>
											<button type="submit" class="ui-btn-green btn-md" onclick="$('.form-sending')[0].reset();">Save Changes</button>
									</div>
								  </div>
								</div>
								<!-- /card -->

							</div>

						</div>
					</div>
				</div>
					</form>

			</div>
			
			
			
			
		</section>
    </div>
</div>

<!--<div id="skin-loading" class="bg-wh" onclick="$(this).fadeOut();">
	<div class="lds-hourglass"></div>
</div>-->
<script>
	window.setTimeout(function(){
		$('#skin-loading').fadeOut();
	}, 3000);
</script>

<!-- footer -->
<?php include("incs/footer-web.html") ?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js-web.html") ?>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<link href="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.css" rel="stylesheet"/>
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<script type="text/javascript">
$( document ).ready( function () {
	 
      $('.carousel').flexslider({
        animation: "slide",
        animationLoop: false,
        itemWidth: 210,
        itemMargin: 5,
        minItems: 1,
        maxItems: 3,
		move: 1,
        /*start: function(slider){
          $('body').removeClass('loading');
        }*/
      });

	//select2
	$('.keep-select-group').select2({
    	placeholder: "Please select",
    	//allowClear: true,
		dropdownAutoWidth : true,
		width: '100%'
	});
	
	/*$('#uid-unlimit').change( function() {
			var isChecked = this.checked;
			if(isChecked) {
				$("#uid-limit").removeClass("bg-white");
				$("#uid-limit").prop("disabled",true); 
				$("#uid-limit").prop("required",false); 
			} else {
				$("#uid-limit").addClass("bg-white");
				$("#uid-limit").prop("disabled",false);
				$("#uid-limit").prop("required",true);
			}
		});*/
		
	
	/*toggle Unlimit demo*/
		$('#pnp-noti-unlimit').change( function() {
			var isChecked = this.checked;
			if(isChecked) {
				$("#pnp-noti-limit").removeClass("bg-white");
				$("#pnp-noti-limit").prop("disabled",true); 
				$("#pnp-noti-limit").prop("required",false); 
			} else {
				$("#pnp-noti-limit").addClass("bg-white");
				$("#pnp-noti-limit").prop("disabled",false);
				$("#pnp-noti-limit").prop("required",true);
			}
		});
		$('#uid-unlimit').change( function() {
			var isChecked = this.checked;
			if(isChecked) {
				$("#uid-limit").removeClass("bg-white");
				$("#uid-limit").prop("disabled",true); 
				$("#uid-limit").prop("required",false); 
			} else {
				$("#uid-limit").addClass("bg-white");
				$("#uid-limit").prop("disabled",false);
				$("#uid-limit").prop("required",true);
			}
		});
		$('#uidOffSwitch1, #uidOffSwitch1-2').change( function() {
			//var isChecked = this.checked;
			var isChecked = $('#uidOffSwitch1').is(':checked');
			var isChecked2 = $('#uidOffSwitch1-2').is(':checked');
			var isChecked3 = $('#uid-unlimit').is(':checked');
			if(isChecked || isChecked2) {
				$('#uid-unlimit').parents("li.row").removeClass("disabled");
				//$("#uid-limit").addClass("bg-white");
				$("#uid-limit").removeClass("bg-white");
				if(isChecked3) {
					$("#uid-unlimit").prop("disabled",false);
					} else {
					$("#uid-limit,#uid-unlimit").prop("disabled",false);
					}
				$("#uid-limit").prop("required",true);
			} else {
				$('#uid-unlimit').parents("li.row").addClass("disabled");
				$("#uid-limit").removeClass("bg-white");
				$("#uid-limit,#uid-unlimit").prop("disabled",true); 
				$("#uid-limit").prop("required",false);
				
			}
		});
	
/*	$('#range_1,#range_1-1,#range_2,#range_2-1,#range_3,#range_3-1').ionRangeSlider({
      min     : 5,
      max     : 50,
      type    : 'single',
      step    : 1,
      postfix : ' %',
      prettify: false,
      hasGrid : true
    });*/
	
	$('#range_1').change(function() {
		var valphp = $( "#range_1" ).val();
		//alert(200000 * valphp / 100);
		$( "#res_pnp" ).val( 200000*valphp/100 );
		var counter = 200000 - (200000 * valphp / 100);
		//alert(counter);
		$( "#res_week1" ).val(counter/4);
		$( "#res_week2" ).val(counter/4);
		$( "#res_week3" ).val(counter/4);
		$( "#res_week4" ).val(counter/4);
	});
	
	$('#range_1-1').change(function() {
		var valphp = $( "#range_1-1" ).val();
		//alert(200000 * valphp / 100);
		$( "#res1-1_pnp" ).val( 200000*valphp/100 );
		var counter = 200000 - (200000 * valphp / 100);
		//alert(counter);
		$( "#res1-1_week1" ).val(counter/4);
		$( "#res1-1_week2" ).val(counter/4);
		$( "#res1-1_week3" ).val(counter/4);
		$( "#res1-1_week4" ).val(counter/4);
	});
	//Date range picker
    $('.reservation1,.reservation2,.reservation3,.reservation4').daterangepicker({
    	singleDatePicker: false,
	})
	
	function addSwitch() {
		$(".sw-toggle-uid").each(function(){
		  $(this).bootstrapSwitch('state', $(this).prop('checked'));
		});
	};
	addSwitch();	
	

});
  </script>
  
  <!--Plugin CSS file with desired skin-->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/css/ion.rangeSlider.min.css"/>   
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<?php /*?><link rel="stylesheet" href="js/daterangepicker/daterangepicker.css"><?php */?>
<style type="text/css">/*fix calendar setting*/
.daterangepicker .drp-calendar.left{width: 100%;max-width: 100%}
.daterangepicker .drp-calendar.right .table-condensed>thead>tr+tr,
.daterangepicker .drp-calendar.right .table-condensed>thead>tr>th:not(.next),
.daterangepicker .drp-calendar.right .table-condensed tbody{display: none}
.daterangepicker .drp-calendar.right .table-condensed>thead>tr>th{ width: 37px;height: 26px;position: absolute;top:10px;right: 8px;z-index: 9}
</style>
<!--Plugin JavaScript file-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/js/ion.rangeSlider.min.js"></script>
<!-- daterangepicker -->
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

<script>



    $('#range_1,#range_1-1,#range_2,#range_2-1,#range_3,#range_3-1').ionRangeSlider({
        type: "single",
        min: 5,
        max: 50,
        from: 10,
		step    : 1,
	  postfix : ' %',
	  prettify: false,
	  hasGrid : true
        
    });
    


</script>
  


<!-- /js -->

</body>
</html>
