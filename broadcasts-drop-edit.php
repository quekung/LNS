<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top-web.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden">
<!-- Headbar -->
<?php include("incs/header-v2.html") ?>
<script>
$(".main-menu .list-unstyled>li.active").removeClass('active');
$(".main-menu .list-unstyled>li:nth-child(4)").addClass('active');
</script>
<!-- /Headbar -->
<div class="page-checkout">

    
    <div id="toc">
		<section class="z-broadcast _self-pt0 mb0">
			<div class="bx-stepbar _self-pv20 cb-af container">
				<ul class="tabsbar">
						  <li><a href="broadcasts.php" title="Send Message"><i class="fas fa-bullhorn"></i> <span>Send Message</span></a></li>
						  <li><a href="broadcasts-create.php" title="Create Message"><i class="fas fa-layer-group"></i> <span>Create Template</span></a></li>
						  <li><a href="broadcasts-acc.php" title="User Management" class="selected"><i class="fas fa-users-cog"></i> <span>User Management</span></a></li>
						  <li><a href="broadcasts-setting.php" title="Message Setting"><i class="fas fa-sliders-h"></i> <span>Message Setting</span></a></li>
						  <li><a href="broadcasts-report.php" title="Report"><i class="fas fa-file-medical-alt"></i> <span>Report</span></a></li>
						  <li><a href="broadcasts-survey.php" title="Survey"><i class="fas fa-tasks"></i> <span>Survey</span></a></li>
				  </ul>
			</div>
					

			
			<div class="bg-gray2 contentTabs">
				<div id="tbc-1" class="msg">
					<form method="post" class="form-checkout form-sending'">
					<div class="head-title container txt-c">
						<h2 class="t-black"><i class="fas fa-edit _self-mr10 t-black"></i> Edit Dropdown Options</h2>	
						<p>ระบบส่งข้อความผ่านทาง line</p>
					</div>
					<div class="wrap-full _chd-cl-xs-12 _chd-cl-sm">
						<div class="main">
							<div class="container">
								
								<!-- card -->
								<div class="card bg-white">
									<div class="card-header _flex center-xs between-xsh">
										<ul class="idTabs _self-pa0 tab-receiver">
											<li><a href="broadcasts-acc.php" ><i class="fas fa-users"></i> Line Groups</a></li>
											<li><a href="broadcasts-acc-user.php" class="selected"><i class="fas fa-user"></i> Users</a></li>
										</ul>
										<!--<div class="search-sm _self-mr20">
											<input class="txt-box" placeholder="ค้นหา...">
											<button type="submit" class="fas fa-search"></button>
										</div>-->
									</div>

									<!-- User -->
									<div id="users" class="card-body _self-pa30 middle-xs">
										<div class="tab-pane fade" id="vert-tabs-flex" style="display: block;">
											<p class="lead mb-0">Properties</p>
											<div class="tab-custom-content">
												 <div class="form-group">
													<label>Column Title</label>
													<input type="text" class="form-control" id="carouseltitle" placeholder="Title" value="POSITION">
												</div>
												<div class="row">
												  <div class="max-w-tab col-5 col-lg-4" style="max-width: 180px">

													<!--<div class="nav nav-tabs cs-flex-tab flex-column h-100" id="vert-tabs-tab" role="tablist" aria-orientation="vertical">-->
													<ul class="idTabs cs-flex-tab flex-column h-100" id="custom-carousel-tab" role="tablist" aria-orientation="vertical">


													  <li class="nav-item"><a class="nav-link selected" id="call-tab1" href="#crs-tab1">Option 1</a> <span> x </span></li>
													  <li class="nav-item"><a class="nav-link" id="call-tab2" href="#crs-tab2">Option 2</a> <span> x </span></li>
													  <li class="nav-item"><a class="nav-link" id="call-tab3" href="#crs-tab3">Option 3</a> <span> x </span></li>
													  <li class="nav-item"><a class="nav-link" id="call-tab4" href="#crs-tab4">Option 4</a> <span> x </span></li>
													  <li class="nav-item"><a class="nav-link" id="call-tab5" href="#crs-tab5">Option 5</a> <span> x </span></li>
													  <li class="nav-item"><a class="nav-link" id="call-tab6" href="#crs-tab6">Option 6</a> <span> x </span></li>
													  <li class="nav-item"><a class="nav-link" id="call-tab7" href="#crs-tab7">Option 7</a> <span> x </span></li>
													  <li class="nav-item"><a class="nav-link" id="call-tab8" href="#crs-tab8">Option 8</a> <span> x </span></li>
													  <li class="nav-item"><a class="nav-link" id="call-tab9" href="#crs-tab9">Option 9</a> <span> x </span></li>
													  <li class="_self-mt10 txt-c"><a href="javascript:;" class="add-tab ui-btn-mini btn-sm bg-maroon">+ Add Option</a></li>

												  </ul></div>
												  <div class="col-7 col-xl col-lg-8 pl10-xs pl20-sm">
													<div class="tab-content" id="carousel-tab">
													<!--
													<option selected="selected" data-select2-id="3">Straff</option>
													  <option data-select2-id="206">Senior</option>
													  <option data-select2-id="207">Assistant Manager</option>
													  <option data-select2-id="208">Manager</option>
													  <option data-select2-id="209">Senior Manager</option>
													  <option data-select2-id="210">AVP</option>
													  <option data-select2-id="211">VP</option>
													  <option data-select2-id="212">MD</option>
													  <option data-select2-id="213">CEO</option>
													-->
													  <div class="tab-pane text-left fade " id="crs-tab1" style="display: block;">
														<!-- card carousel -->
														<div id="main-card1" class="main-card wrap-upload">
															<div class="form-group p-0">
																<label>Select option title 1</label>
																<input type="text" class="form-control" placeholder="title" value="Straff">
															 </div>

														</div>
														<div class="form-group _self-pa10 bg-light rounded">
															<label>Option Value</label>
															<select class="custom-select">
															<? for($i=1; $i<=9; $i++){ ?>
															  <option value="<? echo $i; ?>" <? if($i==1){?>selected<?}?>><? echo $i; ?></option>
															<? } ?>
															</select>
														</div>
														<!-- /card carousel -->
													  </div>
													  
													  <div class="tab-pane fade" id="crs-tab2" style="display: none;">
														 <!-- card carousel -->
														<div id="main-card2" class="main-card wrap-upload">
															<div class="form-group p-0">
																<label>Select option title 2</label>
																<input type="text" class="form-control" placeholder="title" value="Senior">
															 </div>

														</div>
														<div class="form-group _self-pa10 bg-light rounded">
															<label>Option Value</label>
															<select class="custom-select">
															<? for($i=1; $i<=9; $i++){ ?>
															  <option value="<? echo $i; ?>" <? if($i==2){?>selected<?}?>><? echo $i; ?></option>
															<? } ?>
															</select>
														</div>
														<!-- /card carousel -->
													  </div>
													  
													  <div class="tab-pane fade" id="crs-tab3" style="display: none;">
														 <!-- card carousel -->
														<div id="main-card3" class="main-card wrap-upload">
															<div class="form-group p-0">
																<label>Select option title 3</label>
																<input type="text" class="form-control" placeholder="title" value="Assistant Manager">
															 </div>

														</div>
														<div class="form-group _self-pa10 bg-light rounded">
															<label>Option Value</label>
															<select class="custom-select">
															<? for($i=1; $i<=9; $i++){ ?>
															  <option value="<? echo $i; ?>" <? if($i==3){?>selected<?}?>><? echo $i; ?></option>
															<? } ?>
															</select>
														</div>
														<!-- /card carousel -->
													  </div>
													  
													  <div class="tab-pane fade" id="crs-tab4" style="display: none;">
														 <!-- card carousel -->
														<div id="main-card4" class="main-card wrap-upload">
															<div class="form-group p-0">
																<label>Select option title 4</label>
																<input type="text" class="form-control" placeholder="title" value="Manager">
															 </div>

														</div>
														<div class="form-group _self-pa10 bg-light rounded">
															<label>Option Value</label>
															<select class="custom-select">
															<? for($i=1; $i<=9; $i++){ ?>
															  <option value="<? echo $i; ?>" <? if($i==4){?>selected<?}?>><? echo $i; ?></option>
															<? } ?>
															</select>
														</div>
														<!-- /card carousel -->
													  </div>
													  
													  <div class="tab-pane fade" id="crs-tab5" style="display: none;">
														 <!-- card carousel -->
														<div id="main-card5" class="main-card wrap-upload">
															<div class="form-group p-0">
																<label>Select option title 5</label>
																<input type="text" class="form-control" placeholder="title" value="Senior Manager">
															 </div>

														</div>
														<div class="form-group _self-pa10 bg-light rounded">
															<label>Option Value</label>
															<select class="custom-select">
															<? for($i=1; $i<=9; $i++){ ?>
															  <option value="<? echo $i; ?>" <? if($i==5){?>selected<?}?>><? echo $i; ?></option>
															<? } ?>
															</select>
														</div>
														<!-- /card carousel -->
													  </div>
													  
													  <div class="tab-pane fade" id="crs-tab6" style="display: none;">
														 <!-- card carousel -->
														<div id="main-card6" class="main-card wrap-upload">
															<div class="form-group p-0">
																<label>Select option title 6</label>
																<input type="text" class="form-control" placeholder="title" value="AVP">
															 </div>

														</div>
														<div class="form-group _self-pa10 bg-light rounded">
															<label>Option Value</label>
															<select class="custom-select">
															<? for($i=1; $i<=9; $i++){ ?>
															  <option value="<? echo $i; ?>" <? if($i==6){?>selected<?}?>><? echo $i; ?></option>
															<? } ?>
															</select>
														</div>
														<!-- /card carousel -->
													  </div>
													  
													  <div class="tab-pane fade" id="crs-tab7" style="display: none;">
														 <!-- card carousel -->
														<div id="main-card7" class="main-card wrap-upload">
															<div class="form-group p-0">
																<label>Select option title 7</label>
																<input type="text" class="form-control" placeholder="title" value="VP">
															 </div>

														</div>
														<div class="form-group _self-pa10 bg-light rounded">
															<label>Option Value</label>
															<select class="custom-select">
															<? for($i=1; $i<=9; $i++){ ?>
															  <option value="<? echo $i; ?>" <? if($i==7){?>selected<?}?>><? echo $i; ?></option>
															<? } ?>
															</select>
														</div>
														<!-- /card carousel -->
													  </div>
													  
													  <div class="tab-pane fade" id="crs-tab8" style="display: none;">
														 <!-- card carousel -->
														<div id="main-card8" class="main-card wrap-upload">
															<div class="form-group p-0">
																<label>Select option title 8</label>
																<input type="text" class="form-control" placeholder="title" value="MD">
															 </div>

														</div>
														<div class="form-group _self-pa10 bg-light rounded">
															<label>Option Value</label>
															<select class="custom-select">
															<? for($i=1; $i<=9; $i++){ ?>
															  <option value="<? echo $i; ?>" <? if($i==8){?>selected<?}?>><? echo $i; ?></option>
															<? } ?>
															</select>
														</div>
														<!-- /card carousel -->
													  </div>
													  
													  <div class="tab-pane fade" id="crs-tab9" style="display: none;">
														 <!-- card carousel -->
														<div id="main-card9" class="main-card wrap-upload">
															<div class="form-group p-0">
																<label>Select option title 9</label>
																<input type="text" class="form-control" placeholder="title" value="CEO">
															 </div>

														</div>
														<div class="form-group _self-pa10 bg-light rounded">
															<label>Option Value</label>
															<select class="custom-select">
															<? for($i=1; $i<=9; $i++){ ?>
															  <option value="<? echo $i; ?>" <? if($i==9){?>selected<?}?>><? echo $i; ?></option>
															<? } ?>
															</select>
														</div>
														<!-- /card carousel -->
													  </div>
													

													</div>
												  </div>


												</div>



											</div>


										  </div>
									</div>
									
									<div class="sticky-bottom card-footer mf-bottom">
									<div class="__chd-ph10 center-xs">
											<button type="reset" class="ui-btn-gray btn-md" data-toggle="modal" data-target="#modal-close">Cancel</button>
											<button type="button" class="ui-btn-green btn-md" onclick="$(this).children('i').removeClass('hid'); $('.form-sending')[0].reset();"><i class="hid fas fa-circle-notch fa-spin"></i> Save Changes</button>
									</div>
								  </div>
								</div>
								<!-- /card -->
							</div>

						</div>
					</div>
				</div>
					</form>

			</div>
			
			
			
			
		</section>
    </div>
</div>

<!--<div id="skin-loading" class="bg-wh" onclick="$(this).fadeOut();">
	<div class="lds-hourglass"></div>
</div>-->
<script>
	window.setTimeout(function(){
		$('#skin-loading').fadeOut();
	}, 3000);
</script>

<!-- footer -->
<?php include("incs/footer-web.html") ?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js-web.html") ?>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<link href="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.css" rel="stylesheet"/>
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<script type="text/javascript">
$( document ).ready( function () {
	 
      $('.carousel').flexslider({
        animation: "slide",
        animationLoop: false,
        itemWidth: 210,
        itemMargin: 5,
        minItems: 1,
        maxItems: 3,
		move: 1,
        /*start: function(slider){
          $('body').removeClass('loading');
        }*/
      });

	//select2

	$(".js-example-tags").select2({
	   //dropdownAutoWidth : true,
	  width: '100%',
	  tags: true,
	  insertTag: function (data, tag) {
		// Insert the tag at the end of the results
		data.push(tag);
	  }
	  /*createTag: function (params) {
		var term = $.trim(params.term);

		if (term === '') {
		  return null;
		}

		return {
		  id: term,
		  text: term,
		  newTag: true // add additional parameters
		}
	  }*/
	});
	
	

});
  </script>
  
<script>
$( document ).ready( function () {

/*tab carousel*/
$(".cs-flex-tab").on("click", "a", function (e) {
        e.preventDefault();
        if (!$(this).hasClass('add-tab')) {
            $(this).tab('show');
        }
    })
    .on("click", "span", function () {
        var anchor = $(this).siblings('a');
        $(anchor.attr('href')).remove();
        $(this).parent().remove();
        $(".cs-flex-tab li").children('a').first().click();
    });
$('.add-tab').click(function (e) {
    e.preventDefault();
    var id = $(".cs-flex-tab").children().length; //think about it ;)
    var tabId = 'card_' + id;
	/*var startId = 'start: ' + id;
	var settings = { startId , change:false }; 
	alert ( startId );
	alert ( settings );*/
    $(this).closest('li').before('<li class="nav-item"><a class="nav-link" href="#card_' + id + '">Option ' + id + ' </a> <span> x </span></li>');
    //$('.tab-content').append('<div class="tab-pane" id="' + tabId + '">Contact Form: New Contact ' + id + '</div>');
	var clonecard= '<div id="main-card3" class="main-card wrap-upload">';
	clonecard+='<div class="form-group p-0">';
	clonecard+='<label>elect option title ' + id + '</label>';
	clonecard+='<input type="text" class="form-control" placeholder="title" value="">';
	clonecard+='</div>';


	clonecard+='<div class="form-group _self-pa10 bg-light rounded">';
	clonecard+='<label>Option Value ' + id + '</label>';
	clonecard+='<select class="custom-select">';															
	clonecard+='<option value="' + id + '" <? if($i==9){?>selected<?}?>>' + id + '</option>';
	clonecard+='</select>';
	clonecard+='</div>';
	//write
	$('#vert-tabs-flex .tab-content').append('<div class="tab-pane" id="' + tabId + '">' + clonecard + '</div>');
	$('#custom-carousel-tab').idTabs(); 
    $('#custom-carousel-tab li:nth-child(' + id + ') a').click();
});

});
</script>

<!-- /js -->

</body>
</html>
