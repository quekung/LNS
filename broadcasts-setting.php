<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top-web.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden">
<!-- Headbar -->
<?php include("incs/header-v2.html") ?>
<script>
$(".main-menu .list-unstyled>li.active").removeClass('active');
$(".main-menu .list-unstyled>li:nth-child(4)").addClass('active');
</script>
<!-- /Headbar -->
<div class="page-checkout">

    
    <div id="toc">
		<section class="z-broadcast _self-pt0 mb0">
			<div class="bx-stepbar _self-pv20 cb-af container">
				<ul class="tabsbar">
						  <li><a href="broadcasts.php" title="Send Message"><i class="fas fa-bullhorn"></i> <span>Send Message</span></a></li>
						  <li><a href="broadcasts-create.php" title="Create Message"><i class="fas fa-layer-group"></i> <span>Create Template</span></a></li>
						  <li><a href="broadcasts-acc.php" title="User Detail"><i class="fas fa-users-cog"></i> <span>User Detail</span></a></li>
						  <li><a href="broadcasts-setting.php" title="Message Setting" class="selected"><i class="fas fa-sliders-h"></i> <span>Message Setting</span></a></li>
						  <li><a href="broadcasts-report.php" title="Report"><i class="fas fa-file-medical-alt"></i> <span>Report</span></a></li>
						  <li><a href="broadcasts-survey.php" title="Survey"><i class="fas fa-tasks"></i> <span>Survey</span></a></li>
				  </ul>
			</div>
					

			
			<div class="bg-gray2 contentTabs">
				<div id="tbc-1" class="msg">
					<form method="post" class="form-checkout form-sending'">
					<div class="head-title container txt-c">
						<h2 class="t-black"><i class="fas fa-sliders-h _self-mr10 t-black"></i>Message Setting</h2>	
						<p>ระบบส่งข้อความผ่านทาง line</p>
					</div>
					<div class="wrap-full _chd-cl-xs-12 _chd-cl-sm">
						<div class="main">
							<div class="container">
								<!-- card -->
								<div class="card bg-white">
									<div class="card-header">
										<h3 class="card-title center-xs"><b>Monthly Limit setting</b></h3>
									</div>
									<div class="card-body d-flex msd-setting _self-pa20 middle-xs">
										<div class="col-xs-12 col-sm-7 col-md-8">
										<div class="d-flex between-xs bg-gray3 _self-pa10-mb20 rounded1">
											<!--<div class="col-sm-01">
												<div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
												  <input type="checkbox" class="custom-control-input" id="customSwitch3" checked>
												  <label class="custom-control-label" for="customSwitch3" style="display: block;clear: both"></label>
												</div>
												<small class="d-block" style="margin-top: -3px">on/off</small>
											</div>-->
											<div class="col">
												<div class="d-flex middle-xs">
												  <div class="input-name col-sm-4">
													<span class="d-block pr-2 text-right">&nbsp;UID</span>
												  </div>
												  <input type="text" id="uid-limit" class="form-control rounded-2 col-sm-6 txt-c" value="35000" disabled="true" style="max-width: 60%">
												  <div class="input-edit">
													<span class="_self-pl10" onclick="$('#uid-limit').prop('disabled',false).focus(); $('#uid-unlimit').prop('checked', false); " style="cursor: pointer"><i class="far fa-edit"></i></span>
												  </div>
												</div>
											</div>


											<div class="col-sm-5">

													<div class="icheck-gray chk-unlimit mt-0 mb-0 p-2"><input type="checkbox" name="uid-type" id="uid-unlimit" checked> <label for="uid-unlimit" style="font-weight: 400;">Unlimited</label></div>


											</div>
										</div>
										
										</div>
										<div class="col-xs-12 col-sm-5 col-md-4">
											<div class="live-report">
											
												  <div class="row _chd-cl-xs-06">
													  <div class="col form-group _self-mb0">

														<input type="text" id="UIDUSED" class="txt-box is-valid bg-white rounded-2 txt-c" value="7000" readonly="">
														<?php /*?><input type="text" id="UIDUSED" class="txt-box is-invalid bg-white rounded-2 txt-c" value="37000" readonly="">
														<input type="text" id="UIDUSED" class="txt-box is-warning bg-white rounded-2 txt-c" value="34000" readonly=""><?php */?>
														<label for="inputEstimatedDuration" class="d-block text-center">UID  <small class="text-muted">Message Delivered</small></label>
													  </div>
													  <div class="col form-group _self-mb0">

														<input type="text" id="inputEstimatedDuration" class="txt-box rounded-2 txt-c" value="35000" readonly="">
														<label for="inputEstimatedDuration" class="d-block text-center">UID  <small class="text-muted">Monthly Credit</small></label>
													  </div>
												  </div>

										  </div>
										</div>
									</div>
									
									<div class="card-footer bg-white">
									<div class="_chd-ph10 center-xs">
											<button type="submit" class="ui-btn-gray btn-md" data-toggle="modal" data-target="#modal-close">Cancel</button>
											<button type="submit" class="ui-btn-green btn-md" onclick="$('.form-sending')[0].reset();">Save Changes</button>
									</div>
								  </div>
								</div>
								<!-- /card -->
								
								<!-- card -->
								<div class="card bg-white mt20-xs">
									<div class="card-header">
										<h3 class="card-title center-xs">
											<a href="broadcasts-schedule.php" class="btn-calendar d-flex w-100 pl20-xs between-xs middle-xs ui-btn-trans-sq">
												<big class="t-black"><i class="far fa-calendar-alt _self-mr10 t-black"></i> Messaging calendar</big>
												<span class="ui-btn-green3">Schedule <i class="fas fa-angle-right ma0-xs"></i></span>
											</a>
										</h3>
										<h3 class="card-title center-xs">
											<a href="broadcasts-event.php" class="btn-calendar d-flex w-100 pl20-xs between-xs middle-xs ui-btn-trans-sq">
												<big class="t-black"><i class="fas fa-birthday-cake _self-mr10 t-black"></i> Template Birthday</big>
												<span class="ui-btn-green3">Schedule <i class="fas fa-angle-right ma0-xs"></i></span>
											</a>
										</h3>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
					</form>

			</div>
			
			
			
			
		</section>
    </div>
</div>

<!--<div id="skin-loading" class="bg-wh" onclick="$(this).fadeOut();">
	<div class="lds-hourglass"></div>
</div>-->
<script>
	window.setTimeout(function(){
		$('#skin-loading').fadeOut();
	}, 3000);
</script>

<!-- footer -->
<?php include("incs/footer-web.html") ?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js-web.html") ?>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<link href="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.css" rel="stylesheet"/>
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<script type="text/javascript">
$( document ).ready( function () {
	 
      $('.carousel').flexslider({
        animation: "slide",
        animationLoop: false,
        itemWidth: 210,
        itemMargin: 5,
        minItems: 1,
        maxItems: 3,
		move: 1,
        /*start: function(slider){
          $('body').removeClass('loading');
        }*/
      });

	//select2
	$('.keep-select-group').select2({
    	placeholder: "Please select",
    	//allowClear: true,
		dropdownAutoWidth : true,
		width: '100%'
	});
	
	$('#uid-unlimit').change( function() {
			var isChecked = this.checked;
			if(isChecked) {
				$("#uid-limit").removeClass("bg-white");
				$("#uid-limit").prop("disabled",true); 
				$("#uid-limit").prop("required",false); 
			} else {
				$("#uid-limit").addClass("bg-white");
				$("#uid-limit").prop("disabled",false);
				$("#uid-limit").prop("required",true);
			}
		});
	

});
  </script>
  


<!-- /js -->

</body>
</html>
