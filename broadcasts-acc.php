<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top-web.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden">
<!-- Headbar -->
<?php include("incs/header-v2.html") ?>
<script>
$(".main-menu .list-unstyled>li.active").removeClass('active');
$(".main-menu .list-unstyled>li:nth-child(4)").addClass('active');
</script>
<!-- /Headbar -->
<div class="page-checkout">

    
    <div id="toc">
		<section class="z-broadcast _self-pt0 mb0">
			<div class="bx-stepbar _self-pv20 cb-af container">
				<ul class="tabsbar">
						  <li><a href="broadcasts.php" title="Send Message"><i class="fas fa-bullhorn"></i> <span>Send Message</span></a></li>
						  <li><a href="broadcasts-create.php" title="Create Message"><i class="fas fa-layer-group"></i> <span>Create Template</span></a></li>
						  <li><a href="broadcasts-acc.php" title="User Detail" class="selected"><i class="fas fa-users-cog"></i> <span>User Detail</span></a></li>
						  <li><a href="broadcasts-setting.php" title="Message Setting"><i class="fas fa-sliders-h"></i> <span>Message Setting</span></a></li>
						  <li><a href="broadcasts-report.php" title="Report"><i class="fas fa-file-medical-alt"></i> <span>Report</span></a></li>
						  <li><a href="broadcasts-survey.php" title="Survey"><i class="fas fa-tasks"></i> <span>Survey</span></a></li>
				  </ul>
			</div>
					

			
			<div class="bg-gray2 contentTabs">
				<div id="tbc-1" class="msg">
					<form method="post" class="form-checkout form-sending'">
					<div class="head-title container txt-c">
						<h2 class="t-black"><i class="fas fa-users-cog _self-mr10 t-black"></i> User Detail</h2>	
						<p>ระบบส่งข้อความผ่านทาง line</p>
					</div>
					<div class="wrap-full _chd-cl-xs-12 _chd-cl-sm">
						<div class="main">
							<div class="container">
								
								<!-- card -->
								<div class="card bg-white">
									<div class="card-header _flex center-xs between-xsh">
										<ul class="idTabs _self-pa0 tab-receiver">
											<li><a href="broadcasts-acc.php" class="selected"><i class="fas fa-users"></i> Line Groups</a></li>
											<li><a href="broadcasts-acc-user.php"><i class="fas fa-user"></i> Users</a></li>
										</ul>
										<div class="search-sm _self-mr20">
											<input class="txt-box" placeholder="ค้นหา...">
											<button type="submit" class="fas fa-search"></button>
										</div>
									</div>
									<!-- Group -->
									<div id="linegroup" class="card-body _self-pa30 middle-xs">
										<div class="list-user">
											<ol id="myTable">
												<li class="head">
													<div class="c1">AVATAR</div>
													<div class="c4">DISPLAY GROUP NAME</div>
													<div class="c4">GROUP NAME DETAIL</div>
													<div class="c4">E-MAIL</div>
													<?php /*?><div class="c4 txt-c">ADMIN</div><?php */?>
													<div class="c2 small txt-c">STATUS</div>
													<!--<div class="c10 txt-c">APPROVED</div>-->

												</li>

												<?php function generateRandomString($length = 10) {
    $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
} ?>
												<?php for($i=4;$i<=20;$i++){ ?>
												<li>
													<div class="c1 pd0 txt-c">
														<?php if($i%3==0) {?><img class="rounded2" src="https://www.w3schools.com/w3images/avatar6.png" alt="Fat Rascal" width="40"><? } else { ?><img class="rounded2" src="https://www.w3schools.com/w3images/avatar5.png" alt="Fat Rascal" width="40"><? } ?>
													</div>
													<div class="c4"><?php echo generateRandomString(); ?></div>
													<div class="c4"><input type="text" class="txt-box" placeholder="Setting Name" required></div>
													<div class="c4"><input type="text" class="txt-box" placeholder="Add E-mail"></div>

													<?php /*?><div class="c4">
													<select class="form-control keep-select-group" multiple="multiple">
														<option>Select Admin</option>
													    <option>Alabama</option>
														<option>Alaska</option>
														<option>California</option>
														<option>Delaware</option>
														<option>Tennessee</option>
														<option>Texas</option>
														<option>Washington</option>
													</select>
													</div><?php */?>
													
													<div class="c2 small txt-c">
													<select class="form-control select2">
													  <option selected="selected">Active</option>
													  <option>Inactive</option>
													  <option>Disabled</option>
													</select>
													</div>

													<?php /*?><div class="c10 txt-c">
													  <div class="icheck-primary">
														<input type="checkbox" name="approve<? echo($i) ?>" id="approve-chk<? echo($i) ?>"> 
														<label for="approve-chk<? echo($i) ?>"><span class="visible-xs"> Approve</span></label>
													  </div>
													</div><?php */?>
												</li>
												<?php } ?>
											</ol>
										</div>
									</div>

									
									<div class="sticky-bottom card-footer mf-bottom">
									<div class="__chd-ph10 center-xs">
											<button type="reset" class="ui-btn-gray btn-md" data-toggle="modal" data-target="#modal-close">Cancel</button>
											<button type="button" class="ui-btn-green btn-md" onclick="$(this).children('i').removeClass('hid'); $('.form-sending')[0].reset();"><i class="hid fas fa-circle-notch fa-spin"></i> Save Changes</button>
									</div>
								  </div>
								</div>
								<!-- /card -->
							</div>

						</div>
					</div>
				</div>
					</form>

			</div>
			
			
			
			
		</section>
    </div>
</div>

<!--<div id="skin-loading" class="bg-wh" onclick="$(this).fadeOut();">
	<div class="lds-hourglass"></div>
</div>-->
<script>
	window.setTimeout(function(){
		$('#skin-loading').fadeOut();
	}, 3000);
</script>

<!-- footer -->
<?php include("incs/footer-web.html") ?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js-web.html") ?>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<link href="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.css" rel="stylesheet"/>
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<script type="text/javascript">
$( document ).ready( function () {
	 
      $('.carousel').flexslider({
        animation: "slide",
        animationLoop: false,
        itemWidth: 210,
        itemMargin: 5,
        minItems: 1,
        maxItems: 3,
		move: 1,
        /*start: function(slider){
          $('body').removeClass('loading');
        }*/
      });

	//select2
	$(".select2").select2();
	$('.keep-select-group').select2({
		maximumSelectionLength: 3,
    	placeholder: "Select Admin",
    	//allowClear: true,
		//tags: true,
		dropdownAutoWidth : true,
		width: '100%'
	});

	
	

});
  </script>
  


<!-- /js -->

</body>
</html>
