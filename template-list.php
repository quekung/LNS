<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top-web.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden">
<!-- Headbar -->
<?php include("incs/header-v2.html") ?>
<script>
$(".main-menu .list-unstyled>li.active").removeClass('active');
$(".main-menu .list-unstyled>li:nth-child(2)").addClass('active');
</script>
<!-- /Headbar -->
<div class="page-checkout">

    
    <div id="toc">
		<section class="z-broadcast _self-pt0 mb0">
			<div class="bx-stepbar _self-pv20 cb-af container">
				<ul class="tabsbar">
						  <li><a href="lns.php" title="Send Message"><i class="fas fa-bullhorn"></i> <span>Send Message</span></a></li>
						  <li><a href="lns-create.php" title="Create Message" class="selected"><i class="fas fa-layer-group"></i> <span>Create Template</span></a></li>
						  <li><a href="lns-setting.php" title="Message Setting"><i class="fas fa-sliders-h"></i> <span>Message Setting</span></a></li>
						  <li><a href="lns-report.php" title="Report"><i class="fas fa-file-medical-alt"></i> <span>Report</span></a></li>
				  </ul>
			</div>
					

			
			<div class="bg-gray2 contentTabs">
				<div id="tbc-1" class="msg">
					<form method="post" class="form-checkout form-sending'">
					<div class="head-title container txt-c">
						<h2 class="t-black"><i class="fas fa-fa-layer-group _self-mr10 t-black"></i>My Template</h2>	
						<p>ระบบส่งข้อความผ่านทาง line</p>
					</div>
					<div class="wrap-full _chd-cl-xs-12 _chd-cl-sm">
						<div class="main row center-xs">
							<div class="container main-list-tpl">
								<div class="my-template row _chd-cl-xs-06-sm-04">
									
									 	<div class="card" id="Template1">
											<div class="hd-ac" id="headingOne">
											  <h2>

												  Template #1 พนักงานใหม่

											  </h2>
											  <div class="tools">
												  <a href="broadcasts-edit.php"><i class="fas fa-edit text-white"></i></a>
												  <a href="javascript:;" onclick="$(this).parents('.hd-ac').next().remove(); $(this).parents('.hd-ac').remove(); "><i class="fas fa-trash text-white"></i></a>
												</div>
											</div>

											<div id="collapseOne" class="pane">
											  <ul class="idTabs tab-mini">
													<li><a href="#tmp1" class="selected"><i class="fas fa-list nav-icon"></i> Title</a></li>
													<li><a href="#tmp1-view"><i class="fas fa-eye nav-icon"></i> live</a></li>
											  </ul>
											  <div class="card-body ps-rlt p-0 list-bubble">
													<ul id="tmp1" class="todo-list show ui-sortable" data-widget="todo-list">
													  <li id="msg1" class="done">
														<!-- drag handle -->
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<!-- checkbox -->
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo1" id="todoCheck1" checked="">
														  <label for="todoCheck1"></label>
														</div>
														<!-- todo text -->
														<span class="text">Design a nice theme</span>
														<!-- Emphasis label -->
														<small class="badge badge-info"><i class="fas fa-text-height"></i> Text</small>
														<!--<div class="tools">
														  <i class="fas fa-edit text-muted"></i>
														  <i class="fas fa-trash"></i>
														</div>-->
													  </li>
													  <li id="msg2" class="done">
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo2" id="todoCheck2" checked="">
														  <label for="todoCheck2"></label>
														</div>
														<span class="text">Make the theme responsive</span>
														<small class="badge badge bg-teal"><i class="far fa-image"></i> Image</small>

													  </li>
													  <li id="msg3" class="done">
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo3" id="todoCheck3" checked="">
														  <label for="todoCheck3"></label>
														</div>
														<span class="text">Let theme shine like a star</span>
														<small class="badge badge bg-teal"><i class="far fa-image"></i> Image</small>

													  </li>
													  <li id="msg4" class="done">
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo4" id="todoCheck4" checked="">
														  <label for="todoCheck4"></label>
														</div>
														<span class="text">Let theme shine like a star</span>
														<small class="badge badge-info"><i class="fas fa-text-height"></i> Text</small>

													  </li>
													  <li id="msg5" class="done">
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo5" id="todoCheck5" checked="">
														  <label for="todoCheck5"></label>
														</div>
														<span class="text">Check your messages and notifications</span>
														<small class="badge badge bg-teal"><i class="far fa-image"></i> Image</small>

													  </li>

													</ul>
													
													<div id="tmp1-view" class="live tab-pane bg-light pl10-xs pr10-xs">
														<ul class="list-sr-chat show">

															<li class="done messageScreen1"><input type="hidden" class="done messageScreen1" id="valScreen1" value="txt|text"><div class="direct-chat-msg"><div class="direct-chat-img"></div><div class="direct-chat-text">text</div></div></li><li class="done messageScreen2"><input type="hidden" class="done messageScreen2" id="valScreen2" value="img|https://dummyimage.com/500x500/17a3b8/ffffff.png&amp;text=Meeting"><div class="direct-chat-msg"><div class="direct-chat-img"></div><div class="direct-chat-text direct-chat-image"><img class="img-fluid pad" src="https://dummyimage.com/500x500/17a3b8/ffffff.png&amp;text=Meeting"></div></div></li><li class="result-imgmap messageScreen3"><input type="hidden" class="done messageScreen3" id="valScreen3" value="img|https://dummyimage.com/500x500/17a3b8/ffffff.png&amp;text=Meeting"><div class="frame-image"><img class="img-fluid pad" src="https://dummyimage.com/500x500/17a3b8/ffffff.png&amp;text=Meeting" alt="Photo"><div class="mark-link v2"><a href="#test1" class="link-p-1" title="Action1" style="background: #ccc; opacity:.5"></a><a href="#test2" class="link-p-2" title="Action2" style="background: #aaa; opacity:.5"></a></div></div></li><li class="result-carousel messageScreen4"><div class="direct-chat-msg"><img class="direct-chat-img" src="http://ui.beurguide.net/LNS/dist/img/avatar-admin.png" alt="Message User Image"></div>
																<div class="flexslider carousel">
																 <ul class="slides">
																	<li>
																	  <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_cheesecake_brownie.jpg" /></a>
																	</li>
																	<li>
																	 <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_lemon.jpg" /></a>
																	</li>
																	<li>
																	  <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_donut.jpg" /></a>
																	</li>
																	<li>
																	 <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_caramel.jpg" /></a>
																	</li>
																  </ul>
																</div>
															<!--<div class="flexslider carousel"><div class="flex-viewport" style="overflow: hidden; position: relative;"><ul class="slides" style="width: 800%; transition-duration: 0s; transform: translate3d(0px, 0px, 0px);"><li style="width: 210px; margin-right: 5px; float: left; display: block;" data-thumb-alt=""><a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_cheesecake_brownie.jpg" draggable="false"></a></li><li data-thumb-alt="" style="width: 210px; margin-right: 5px; float: left; display: block;"> <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_lemon.jpg" draggable="false"></a></li><li data-thumb-alt="" style="width: 210px; margin-right: 5px; float: left; display: block;">  <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_donut.jpg" draggable="false"></a></li><li data-thumb-alt="" style="width: 210px; margin-right: 5px; float: left; display: block;">  <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_caramel.jpg" draggable="false"></a></li></ul></div><ol class="flex-control-nav flex-control-paging"><li><a href="#" class="flex-active">1</a></li><li><a href="#">2</a></li><li><a href="#">3</a></li><li><a href="#">4</a></li></ol><ul class="flex-direction-nav"><li class="flex-nav-prev"><a class="flex-prev flex-disabled" href="#" tabindex="-1">Previous</a></li><li class="flex-nav-next"><a class="flex-next" href="#">Next</a></li></ul></div>-->
															</li><li class="done messageScreen5"><input type="hidden" class="done messageScreen5" id="valScreen5" value="txt|test"><div class="direct-chat-msg"><div class="direct-chat-img"></div><div class="direct-chat-text">Custom Message!</div></div></li></ul>
													</div>
											  </div>
											</div>
										  </div>
										  
										  <div class="card" id="Template2">
											<div class="hd-ac" id="headingTwo">
											  <h2>

												  Template #2 อบรมหลักสูตร

											  </h2>
											  <div class="tools">
												  <a href="broadcasts-edit.php"><i class="fas fa-edit text-white"></i></a>
												  <a href="javascript:;" onclick="$(this).parents('.hd-ac').next().remove(); $(this).parents('.hd-ac').remove(); "><i class="fas fa-trash text-white"></i></a>
												</div>
											</div>
											<div id="collapseTwo" class="pane">
											  <ul class="idTabs tab-mini">
													<li><a href="#tmp2" class="selected"><i class="fas fa-list nav-icon"></i> Title</a></li>
													<li><a href="#tmp2-view"><i class="fas fa-eye nav-icon"></i> live</a></li>
											  </ul>
											  <div class="card-body p-0 list-bubble">
												<ul id="tmp2" class="todo-list show ui-sortable" data-widget="todo-list">
													  <li id="msg2-1" class="done">
														<!-- drag handle -->
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<!-- checkbox -->
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo2-1" id="todoCheck2-1" checked="">
														  <label for="todoCheck2-1"></label>
														</div>
														<!-- todo text -->
														<span class="text">Design a nice theme</span>
														<!-- Emphasis label -->
														<small class="badge badge-info"><i class="fas fa-text-height"></i> Text</small>

													  </li>
													  <li id="msg2-2" class="done">
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo2-2" id="todoCheck2-2" checked="">
														  <label for="todoCheck2-2"></label>
														</div>
														<span class="text">Make the theme responsive</span>
														<small class="badge badge bg-teal"><i class="far fa-image"></i> Image</small>

													  </li>
													  <li id="msg2-3" class="done">
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo2-3" id="todoCheck2-3" checked="">
														  <label for="todoCheck2-3"></label>
														</div>
														<span class="text">Let theme shine like a star</span>
														<small class="badge badge bg-teal"><i class="far fa-image"></i> Image</small>

													  </li>
													  <li id="msg2-4" class="done">
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo2-4" id="todoCheck2-4" checked="">
														  <label for="todoCheck2-4"></label>
														</div>
														<span class="text">Let theme shine like a star</span>
														<small class="badge badge-info"><i class="fas fa-text-height"></i> Text</small>

													  </li>
													  <li id="msg2-5" class="done">
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo2-5" id="todoCheck2-5" checked="">
														  <label for="todoCheck2-5"></label>
														</div>
														<span class="text">Check your messages and notifications</span>
														<small class="badge badge bg-teal"><i class="far fa-image"></i> Image</small>

													  </li>

													</ul>
													
												<div id="tmp2-view" class="live tab-pane bg-light pl10-xs pr10-xs">
														<ul class="list-sr-chat show">

															<li class="done messageScreen1"><input type="hidden" class="done messageScreen1" id="valScreen1" value="txt|text"><div class="direct-chat-msg"><div class="direct-chat-img"></div><div class="direct-chat-text">text</div></div></li><li class="done messageScreen2"><input type="hidden" class="done messageScreen2" id="valScreen2" value="img|https://dummyimage.com/500x500/17a3b8/ffffff.png&amp;text=Meeting"><div class="direct-chat-msg"><div class="direct-chat-img"></div><div class="direct-chat-text direct-chat-image"><img class="img-fluid pad" src="https://dummyimage.com/500x500/17a3b8/ffffff.png&amp;text=Meeting"></div></div></li><li class="result-imgmap messageScreen3"><input type="hidden" class="done messageScreen3" id="valScreen3" value="img|https://dummyimage.com/500x500/17a3b8/ffffff.png&amp;text=Meeting"><div class="frame-image"><img class="img-fluid pad" src="https://dummyimage.com/500x500/17a3b8/ffffff.png&amp;text=Meeting" alt="Photo"><div class="mark-link v2"><a href="#test1" class="link-p-1" title="Action1" style="background: #ccc; opacity:.5"></a><a href="#test2" class="link-p-2" title="Action2" style="background: #aaa; opacity:.5"></a></div></div></li><li class="result-carousel messageScreen4"><div class="direct-chat-msg"><img class="direct-chat-img" src="http://ui.beurguide.net/LNS/dist/img/avatar-admin.png" alt="Message User Image"></div>
																<div class="flexslider carousel">
																 <ul class="slides">
																	<li>
																	  <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_cheesecake_brownie.jpg" /></a>
																	</li>
																	<li>
																	 <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_lemon.jpg" /></a>
																	</li>
																	<li>
																	  <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_donut.jpg" /></a>
																	</li>
																	<li>
																	 <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_caramel.jpg" /></a>
																	</li>
																  </ul>
																</div>
															<!--<div class="flexslider carousel"><div class="flex-viewport" style="overflow: hidden; position: relative;"><ul class="slides" style="width: 800%; transition-duration: 0s; transform: translate3d(0px, 0px, 0px);"><li style="width: 210px; margin-right: 5px; float: left; display: block;" data-thumb-alt=""><a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_cheesecake_brownie.jpg" draggable="false"></a></li><li data-thumb-alt="" style="width: 210px; margin-right: 5px; float: left; display: block;"> <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_lemon.jpg" draggable="false"></a></li><li data-thumb-alt="" style="width: 210px; margin-right: 5px; float: left; display: block;">  <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_donut.jpg" draggable="false"></a></li><li data-thumb-alt="" style="width: 210px; margin-right: 5px; float: left; display: block;">  <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_caramel.jpg" draggable="false"></a></li></ul></div><ol class="flex-control-nav flex-control-paging"><li><a href="#" class="flex-active">1</a></li><li><a href="#">2</a></li><li><a href="#">3</a></li><li><a href="#">4</a></li></ol><ul class="flex-direction-nav"><li class="flex-nav-prev"><a class="flex-prev flex-disabled" href="#" tabindex="-1">Previous</a></li><li class="flex-nav-next"><a class="flex-next" href="#">Next</a></li></ul></div>-->
															</li><li class="done messageScreen5"><input type="hidden" class="done messageScreen5" id="valScreen5" value="txt|test"><div class="direct-chat-msg"><div class="direct-chat-img"></div><div class="direct-chat-text">Custom Message!</div></div></li></ul>
													</div>
											  </div>
											</div>
										  </div>
										  
										  <div class="card" id="Template3">
											<div class="hd-ac" id="headingThree">
											  <h2>

												  Template #3 สิทธิประโยชน์พนักงาน 

											  </h2>
											  <div class="tools">
												  <a href="broadcasts-edit.php"><i class="fas fa-edit text-white"></i></a>
												  <a href="javascript:;" onclick="$(this).parents('.hd-ac').next().remove(); $(this).parents('.hd-ac').remove(); "><i class="fas fa-trash text-white"></i></a>
												</div>
											</div>
											<div id="collapseThree" class="pane">
											  <ul class="idTabs tab-mini">
													<li><a href="#tmp3" class="selected"><i class="fas fa-list nav-icon"></i> Title</a></li>
													<li><a href="#tmp3-view"><i class="fas fa-eye nav-icon"></i> live</a></li>
											  </ul>
											   <div class="card-body p-0 list-bubble">
												<ul id="tmp3" class="todo-list show ui-sortable" data-widget="todo-list">
													  <li id="msg3-1" class="done">
														<!-- drag handle -->
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<!-- checkbox -->
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo3-1" id="todoCheck3-1" checked="">
														  <label for="todoCheck3-1"></label>
														</div>
														<!-- todo text -->
														<span class="text">Design a nice theme</span>
														<!-- Emphasis label -->
														<small class="badge badge-info"><i class="fas fa-text-height"></i> Text</small>

													  </li>
													  <li id="msg2-2" class="done">
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo3-2" id="todoCheck3-2" checked="">
														  <label for="todoCheck3-2"></label>
														</div>
														<span class="text">Make the theme responsive</span>
														<small class="badge badge bg-teal"><i class="far fa-image"></i> Image</small>

													  </li>
													  <li id="msg2-3" class="done">
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo3-3" id="todoCheck3-3" checked="">
														  <label for="todoCheck3-3"></label>
														</div>
														<span class="text">Let theme shine like a star</span>
														<small class="badge badge bg-teal"><i class="far fa-image"></i> Image</small>

													  </li>
													  <li id="msg2-4" class="done">
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo3-4" id="todoCheck3-4" checked="">
														  <label for="todoCheck3-4"></label>
														</div>
														<span class="text">Let theme shine like a star</span>
														<small class="badge badge-info"><i class="fas fa-text-height"></i> Text</small>

													  </li>
													  <li id="msg2-5" class="done">
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo3-5" id="todoCheck3-5" checked="">
														  <label for="todoCheck3-5"></label>
														</div>
														<span class="text">Check your messages and notifications</span>
														<small class="badge badge bg-teal"><i class="far fa-image"></i> Image</small>

													  </li>

													</ul>
													
													<div id="tmp3-view" class="live tab-pane bg-light pl10-xs pr10-xs">
														<ul class="list-sr-chat show">

															<li class="done messageScreen1"><input type="hidden" class="done messageScreen1" id="valScreen1" value="txt|text"><div class="direct-chat-msg"><div class="direct-chat-img"></div><div class="direct-chat-text">text</div></div></li><li class="done messageScreen2"><input type="hidden" class="done messageScreen2" id="valScreen2" value="img|https://dummyimage.com/500x500/17a3b8/ffffff.png&amp;text=Meeting"><div class="direct-chat-msg"><div class="direct-chat-img"></div><div class="direct-chat-text direct-chat-image"><img class="img-fluid pad" src="https://dummyimage.com/500x500/17a3b8/ffffff.png&amp;text=Meeting"></div></div></li><li class="result-imgmap messageScreen3"><input type="hidden" class="done messageScreen3" id="valScreen3" value="img|https://dummyimage.com/500x500/17a3b8/ffffff.png&amp;text=Meeting"><div class="frame-image"><img class="img-fluid pad" src="https://dummyimage.com/500x500/17a3b8/ffffff.png&amp;text=Meeting" alt="Photo"><div class="mark-link v2"><a href="#test1" class="link-p-1" title="Action1" style="background: #ccc; opacity:.5"></a><a href="#test2" class="link-p-2" title="Action2" style="background: #aaa; opacity:.5"></a></div></div></li><li class="result-carousel messageScreen4"><div class="direct-chat-msg"><img class="direct-chat-img" src="http://ui.beurguide.net/LNS/dist/img/avatar-admin.png" alt="Message User Image"></div>
																<div class="flexslider carousel">
																 <ul class="slides">
																	<li>
																	  <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_cheesecake_brownie.jpg" /></a>
																	</li>
																	<li>
																	 <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_lemon.jpg" /></a>
																	</li>
																	<li>
																	  <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_donut.jpg" /></a>
																	</li>
																	<li>
																	 <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_caramel.jpg" /></a>
																	</li>
																  </ul>
																</div>
															<!--<div class="flexslider carousel"><div class="flex-viewport" style="overflow: hidden; position: relative;"><ul class="slides" style="width: 800%; transition-duration: 0s; transform: translate3d(0px, 0px, 0px);"><li style="width: 210px; margin-right: 5px; float: left; display: block;" data-thumb-alt=""><a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_cheesecake_brownie.jpg" draggable="false"></a></li><li data-thumb-alt="" style="width: 210px; margin-right: 5px; float: left; display: block;"> <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_lemon.jpg" draggable="false"></a></li><li data-thumb-alt="" style="width: 210px; margin-right: 5px; float: left; display: block;">  <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_donut.jpg" draggable="false"></a></li><li data-thumb-alt="" style="width: 210px; margin-right: 5px; float: left; display: block;">  <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_caramel.jpg" draggable="false"></a></li></ul></div><ol class="flex-control-nav flex-control-paging"><li><a href="#" class="flex-active">1</a></li><li><a href="#">2</a></li><li><a href="#">3</a></li><li><a href="#">4</a></li></ol><ul class="flex-direction-nav"><li class="flex-nav-prev"><a class="flex-prev flex-disabled" href="#" tabindex="-1">Previous</a></li><li class="flex-nav-next"><a class="flex-next" href="#">Next</a></li></ul></div>-->
															</li><li class="done messageScreen5"><input type="hidden" class="done messageScreen5" id="valScreen5" value="txt|test"><div class="direct-chat-msg"><div class="direct-chat-img"></div><div class="direct-chat-text">Custom Message!</div></div></li></ul>
													</div>
											  </div>
											</div>
										  </div>
										  
										  <div class="card" id="Template4">
											<div class="hd-ac" id="headingFour">
											  <h2>

												  Template #4

											  </h2>
											  <div class="tools">
												  <a href="broadcasts-edit.php"><i class="fas fa-edit text-white"></i></a>
												  <a href="javascript:;" onclick="$(this).parents('.hd-ac').next().remove(); $(this).parents('.hd-ac').remove(); "><i class="fas fa-trash text-white"></i></a>
												</div>
											</div>

											<div id="collapseFour" class="pane">
											  <ul class="idTabs tab-mini">
													<li><a href="#tmp4" class="selected"><i class="fas fa-list nav-icon"></i> Title</a></li>
													<li><a href="#tmp4-view"><i class="fas fa-eye nav-icon"></i> live</a></li>
											  </ul>
											  <div class="card-body ps-rlt p-0 list-bubble">
													<ul id="tmp4" class="todo-list show ui-sortable" data-widget="todo-list">
													  <li id="msg1" class="done">
														<!-- drag handle -->
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<!-- checkbox -->
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo1" id="todoCheck1" checked="">
														  <label for="todoCheck1"></label>
														</div>
														<!-- todo text -->
														<span class="text">Design a nice theme</span>
														<!-- Emphasis label -->
														<small class="badge badge-info"><i class="fas fa-text-height"></i> Text</small>
														<!--<div class="tools">
														  <i class="fas fa-edit text-muted"></i>
														  <i class="fas fa-trash"></i>
														</div>-->
													  </li>
													  <li id="msg2" class="done">
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo2" id="todoCheck2" checked="">
														  <label for="todoCheck2"></label>
														</div>
														<span class="text">Make the theme responsive</span>
														<small class="badge badge bg-teal"><i class="far fa-image"></i> Image</small>

													  </li>
													  <li id="msg3" class="done">
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo3" id="todoCheck3" checked="">
														  <label for="todoCheck3"></label>
														</div>
														<span class="text">Let theme shine like a star</span>
														<small class="badge badge bg-teal"><i class="far fa-image"></i> Image</small>

													  </li>
													  <li id="msg4" class="done">
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo4" id="todoCheck4" checked="">
														  <label for="todoCheck4"></label>
														</div>
														<span class="text">Let theme shine like a star</span>
														<small class="badge badge-info"><i class="fas fa-text-height"></i> Text</small>

													  </li>
													  <li id="msg5" class="done">
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo5" id="todoCheck5" checked="">
														  <label for="todoCheck5"></label>
														</div>
														<span class="text">Check your messages and notifications</span>
														<small class="badge badge bg-teal"><i class="far fa-image"></i> Image</small>

													  </li>

													</ul>
													
													<div id="tmp4-view" class="live tab-pane bg-light pl10-xs pr10-xs">
														<ul class="list-sr-chat show">

															<li class="done messageScreen1"><input type="hidden" class="done messageScreen1" id="valScreen1" value="txt|text"><div class="direct-chat-msg"><div class="direct-chat-img"></div><div class="direct-chat-text">text</div></div></li><li class="done messageScreen2"><input type="hidden" class="done messageScreen2" id="valScreen2" value="img|https://dummyimage.com/500x500/17a3b8/ffffff.png&amp;text=Meeting"><div class="direct-chat-msg"><div class="direct-chat-img"></div><div class="direct-chat-text direct-chat-image"><img class="img-fluid pad" src="https://dummyimage.com/500x500/17a3b8/ffffff.png&amp;text=Meeting"></div></div></li><li class="result-imgmap messageScreen3"><input type="hidden" class="done messageScreen3" id="valScreen3" value="img|https://dummyimage.com/500x500/17a3b8/ffffff.png&amp;text=Meeting"><div class="frame-image"><img class="img-fluid pad" src="https://dummyimage.com/500x500/17a3b8/ffffff.png&amp;text=Meeting" alt="Photo"><div class="mark-link v2"><a href="#test1" class="link-p-1" title="Action1" style="background: #ccc; opacity:.5"></a><a href="#test2" class="link-p-2" title="Action2" style="background: #aaa; opacity:.5"></a></div></div></li><li class="result-carousel messageScreen4"><div class="direct-chat-msg"><img class="direct-chat-img" src="http://ui.beurguide.net/LNS/dist/img/avatar-admin.png" alt="Message User Image"></div>
																<div class="flexslider carousel">
																 <ul class="slides">
																	<li>
																	  <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_cheesecake_brownie.jpg" /></a>
																	</li>
																	<li>
																	 <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_lemon.jpg" /></a>
																	</li>
																	<li>
																	  <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_donut.jpg" /></a>
																	</li>
																	<li>
																	 <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_caramel.jpg" /></a>
																	</li>
																  </ul>
																</div>
															<!--<div class="flexslider carousel"><div class="flex-viewport" style="overflow: hidden; position: relative;"><ul class="slides" style="width: 800%; transition-duration: 0s; transform: translate3d(0px, 0px, 0px);"><li style="width: 210px; margin-right: 5px; float: left; display: block;" data-thumb-alt=""><a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_cheesecake_brownie.jpg" draggable="false"></a></li><li data-thumb-alt="" style="width: 210px; margin-right: 5px; float: left; display: block;"> <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_lemon.jpg" draggable="false"></a></li><li data-thumb-alt="" style="width: 210px; margin-right: 5px; float: left; display: block;">  <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_donut.jpg" draggable="false"></a></li><li data-thumb-alt="" style="width: 210px; margin-right: 5px; float: left; display: block;">  <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_caramel.jpg" draggable="false"></a></li></ul></div><ol class="flex-control-nav flex-control-paging"><li><a href="#" class="flex-active">1</a></li><li><a href="#">2</a></li><li><a href="#">3</a></li><li><a href="#">4</a></li></ol><ul class="flex-direction-nav"><li class="flex-nav-prev"><a class="flex-prev flex-disabled" href="#" tabindex="-1">Previous</a></li><li class="flex-nav-next"><a class="flex-next" href="#">Next</a></li></ul></div>-->
															</li><li class="done messageScreen5"><input type="hidden" class="done messageScreen5" id="valScreen5" value="txt|test"><div class="direct-chat-msg"><div class="direct-chat-img"></div><div class="direct-chat-text">Custom Message!</div></div></li></ul>
													</div>
											  </div>
											</div>
										  </div>
										  
										  <div class="card" id="Template5">
											<div class="hd-ac" id="headingFive">
											  <h2>

												  Template #5

											  </h2>
											  <div class="tools">
												  <a href="broadcasts-edit.php"><i class="fas fa-edit text-white"></i></a>
												  <a href="javascript:;" onclick="$(this).parents('.hd-ac').next().remove(); $(this).parents('.hd-ac').remove(); "><i class="fas fa-trash text-white"></i></a>
												</div>
											</div>
											<div id="collapseFive" class="pane">
											  <ul class="idTabs tab-mini">
													<li><a href="#tmp5" class="selected"><i class="fas fa-list nav-icon"></i> Title</a></li>
													<li><a href="#tmp5-view"><i class="fas fa-eye nav-icon"></i> live</a></li>
											  </ul>
											  <div class="card-body p-0 list-bubble">
												<ul id="tmp5" class="todo-list show ui-sortable" data-widget="todo-list">
													  <li id="msg2-1" class="done">
														<!-- drag handle -->
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<!-- checkbox -->
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo2-1" id="todoCheck2-1" checked="">
														  <label for="todoCheck2-1"></label>
														</div>
														<!-- todo text -->
														<span class="text">Design a nice theme</span>
														<!-- Emphasis label -->
														<small class="badge badge-info"><i class="fas fa-text-height"></i> Text</small>

													  </li>
													  <li id="msg2-2" class="done">
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo2-2" id="todoCheck2-2" checked="">
														  <label for="todoCheck2-2"></label>
														</div>
														<span class="text">Make the theme responsive</span>
														<small class="badge badge bg-teal"><i class="far fa-image"></i> Image</small>

													  </li>
													  <li id="msg2-3" class="done">
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo2-3" id="todoCheck2-3" checked="">
														  <label for="todoCheck2-3"></label>
														</div>
														<span class="text">Let theme shine like a star</span>
														<small class="badge badge bg-teal"><i class="far fa-image"></i> Image</small>

													  </li>
													  <li id="msg2-4" class="done">
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo2-4" id="todoCheck2-4" checked="">
														  <label for="todoCheck2-4"></label>
														</div>
														<span class="text">Let theme shine like a star</span>
														<small class="badge badge-info"><i class="fas fa-text-height"></i> Text</small>

													  </li>
													  <li id="msg2-5" class="done">
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo2-5" id="todoCheck2-5" checked="">
														  <label for="todoCheck2-5"></label>
														</div>
														<span class="text">Check your messages and notifications</span>
														<small class="badge badge bg-teal"><i class="far fa-image"></i> Image</small>

													  </li>

													</ul>
													
												<div id="tmp5-view" class="live tab-pane bg-light pl10-xs pr10-xs">
														<ul class="list-sr-chat show">

															<li class="done messageScreen1"><input type="hidden" class="done messageScreen1" id="valScreen1" value="txt|text"><div class="direct-chat-msg"><div class="direct-chat-img"></div><div class="direct-chat-text">text</div></div></li><li class="done messageScreen2"><input type="hidden" class="done messageScreen2" id="valScreen2" value="img|https://dummyimage.com/500x500/17a3b8/ffffff.png&amp;text=Meeting"><div class="direct-chat-msg"><div class="direct-chat-img"></div><div class="direct-chat-text direct-chat-image"><img class="img-fluid pad" src="https://dummyimage.com/500x500/17a3b8/ffffff.png&amp;text=Meeting"></div></div></li><li class="result-imgmap messageScreen3"><input type="hidden" class="done messageScreen3" id="valScreen3" value="img|https://dummyimage.com/500x500/17a3b8/ffffff.png&amp;text=Meeting"><div class="frame-image"><img class="img-fluid pad" src="https://dummyimage.com/500x500/17a3b8/ffffff.png&amp;text=Meeting" alt="Photo"><div class="mark-link v2"><a href="#test1" class="link-p-1" title="Action1" style="background: #ccc; opacity:.5"></a><a href="#test2" class="link-p-2" title="Action2" style="background: #aaa; opacity:.5"></a></div></div></li><li class="result-carousel messageScreen4"><div class="direct-chat-msg"><img class="direct-chat-img" src="http://ui.beurguide.net/LNS/dist/img/avatar-admin.png" alt="Message User Image"></div>
																<div class="flexslider carousel">
																 <ul class="slides">
																	<li>
																	  <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_cheesecake_brownie.jpg" /></a>
																	</li>
																	<li>
																	 <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_lemon.jpg" /></a>
																	</li>
																	<li>
																	  <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_donut.jpg" /></a>
																	</li>
																	<li>
																	 <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_caramel.jpg" /></a>
																	</li>
																  </ul>
																</div>
															<!--<div class="flexslider carousel"><div class="flex-viewport" style="overflow: hidden; position: relative;"><ul class="slides" style="width: 800%; transition-duration: 0s; transform: translate3d(0px, 0px, 0px);"><li style="width: 210px; margin-right: 5px; float: left; display: block;" data-thumb-alt=""><a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_cheesecake_brownie.jpg" draggable="false"></a></li><li data-thumb-alt="" style="width: 210px; margin-right: 5px; float: left; display: block;"> <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_lemon.jpg" draggable="false"></a></li><li data-thumb-alt="" style="width: 210px; margin-right: 5px; float: left; display: block;">  <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_donut.jpg" draggable="false"></a></li><li data-thumb-alt="" style="width: 210px; margin-right: 5px; float: left; display: block;">  <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_caramel.jpg" draggable="false"></a></li></ul></div><ol class="flex-control-nav flex-control-paging"><li><a href="#" class="flex-active">1</a></li><li><a href="#">2</a></li><li><a href="#">3</a></li><li><a href="#">4</a></li></ol><ul class="flex-direction-nav"><li class="flex-nav-prev"><a class="flex-prev flex-disabled" href="#" tabindex="-1">Previous</a></li><li class="flex-nav-next"><a class="flex-next" href="#">Next</a></li></ul></div>-->
															</li><li class="done messageScreen5"><input type="hidden" class="done messageScreen5" id="valScreen5" value="txt|test"><div class="direct-chat-msg"><div class="direct-chat-img"></div><div class="direct-chat-text">Custom Message!</div></div></li></ul>
													</div>
											  </div>
											</div>
										  </div>
										  
										  <div class="card" id="Template6">
											<div class="hd-ac" id="headingSix">
											  <h2>

												  Template #6 

											  </h2>
											  <div class="tools">
												  <a href="broadcasts-edit.php"><i class="fas fa-edit text-white"></i></a>
												  <a href="javascript:;" onclick="$(this).parents('.hd-ac').next().remove(); $(this).parents('.hd-ac').remove(); "><i class="fas fa-trash text-white"></i></a>
												</div>
											</div>
											<div id="collapseSix" class="pane">
											  <ul class="idTabs tab-mini">
													<li><a href="#tmp6" class="selected"><i class="fas fa-list nav-icon"></i> Title</a></li>
													<li><a href="#tmp6-view"><i class="fas fa-eye nav-icon"></i> live</a></li>
											  </ul>
											   <div class="card-body p-0 list-bubble">
												<ul id="tmp6" class="todo-list show ui-sortable" data-widget="todo-list">
													  <li id="msg6-1" class="done">
														<!-- drag handle -->
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<!-- checkbox -->
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo6-1" id="todoCheck6-1" checked="">
														  <label for="todoCheck6-1"></label>
														</div>
														<!-- todo text -->
														<span class="text">Design a nice theme</span>
														<!-- Emphasis label -->
														<small class="badge badge-info"><i class="fas fa-text-height"></i> Text</small>

													  </li>
													  <li id="msg2-2" class="done">
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo3-2" id="todoCheck3-2" checked="">
														  <label for="todoCheck3-2"></label>
														</div>
														<span class="text">Make the theme responsive</span>
														<small class="badge badge bg-teal"><i class="far fa-image"></i> Image</small>

													  </li>
													  <li id="msg2-3" class="done">
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo3-3" id="todoCheck3-3" checked="">
														  <label for="todoCheck3-3"></label>
														</div>
														<span class="text">Let theme shine like a star</span>
														<small class="badge badge bg-teal"><i class="far fa-image"></i> Image</small>

													  </li>
													  <li id="msg2-4" class="done">
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo3-4" id="todoCheck3-4" checked="">
														  <label for="todoCheck3-4"></label>
														</div>
														<span class="text">Let theme shine like a star</span>
														<small class="badge badge-info"><i class="fas fa-text-height"></i> Text</small>

													  </li>
													  <li id="msg2-5" class="done">
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo3-5" id="todoCheck3-5" checked="">
														  <label for="todoCheck3-5"></label>
														</div>
														<span class="text">Check your messages and notifications</span>
														<small class="badge badge bg-teal"><i class="far fa-image"></i> Image</small>

													  </li>

													</ul>
													
													<div id="tmp6-view" class="live tab-pane bg-light pl10-xs pr10-xs">
														<ul class="list-sr-chat show">

															<li class="done messageScreen1"><input type="hidden" class="done messageScreen1" id="valScreen1" value="txt|text"><div class="direct-chat-msg"><div class="direct-chat-img"></div><div class="direct-chat-text">text</div></div></li><li class="done messageScreen2"><input type="hidden" class="done messageScreen2" id="valScreen2" value="img|https://dummyimage.com/500x500/17a3b8/ffffff.png&amp;text=Meeting"><div class="direct-chat-msg"><div class="direct-chat-img"></div><div class="direct-chat-text direct-chat-image"><img class="img-fluid pad" src="https://dummyimage.com/500x500/17a3b8/ffffff.png&amp;text=Meeting"></div></div></li><li class="result-imgmap messageScreen3"><input type="hidden" class="done messageScreen3" id="valScreen3" value="img|https://dummyimage.com/500x500/17a3b8/ffffff.png&amp;text=Meeting"><div class="frame-image"><img class="img-fluid pad" src="https://dummyimage.com/500x500/17a3b8/ffffff.png&amp;text=Meeting" alt="Photo"><div class="mark-link v2"><a href="#test1" class="link-p-1" title="Action1" style="background: #ccc; opacity:.5"></a><a href="#test2" class="link-p-2" title="Action2" style="background: #aaa; opacity:.5"></a></div></div></li><li class="result-carousel messageScreen4"><div class="direct-chat-msg"><img class="direct-chat-img" src="http://ui.beurguide.net/LNS/dist/img/avatar-admin.png" alt="Message User Image"></div>
																<div class="flexslider carousel">
																 <ul class="slides">
																	<li>
																	  <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_cheesecake_brownie.jpg" /></a>
																	</li>
																	<li>
																	 <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_lemon.jpg" /></a>
																	</li>
																	<li>
																	  <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_donut.jpg" /></a>
																	</li>
																	<li>
																	 <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_caramel.jpg" /></a>
																	</li>
																  </ul>
																</div>
															<!--<div class="flexslider carousel"><div class="flex-viewport" style="overflow: hidden; position: relative;"><ul class="slides" style="width: 800%; transition-duration: 0s; transform: translate3d(0px, 0px, 0px);"><li style="width: 210px; margin-right: 5px; float: left; display: block;" data-thumb-alt=""><a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_cheesecake_brownie.jpg" draggable="false"></a></li><li data-thumb-alt="" style="width: 210px; margin-right: 5px; float: left; display: block;"> <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_lemon.jpg" draggable="false"></a></li><li data-thumb-alt="" style="width: 210px; margin-right: 5px; float: left; display: block;">  <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_donut.jpg" draggable="false"></a></li><li data-thumb-alt="" style="width: 210px; margin-right: 5px; float: left; display: block;">  <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_caramel.jpg" draggable="false"></a></li></ul></div><ol class="flex-control-nav flex-control-paging"><li><a href="#" class="flex-active">1</a></li><li><a href="#">2</a></li><li><a href="#">3</a></li><li><a href="#">4</a></li></ol><ul class="flex-direction-nav"><li class="flex-nav-prev"><a class="flex-prev flex-disabled" href="#" tabindex="-1">Previous</a></li><li class="flex-nav-next"><a class="flex-next" href="#">Next</a></li></ul></div>-->
															</li><li class="done messageScreen5"><input type="hidden" class="done messageScreen5" id="valScreen5" value="txt|test"><div class="direct-chat-msg"><div class="direct-chat-img"></div><div class="direct-chat-text">Custom Message!</div></div></li></ul>
													</div>
											  </div>
											</div>
										  </div>
								
								
								</div>

							</div>

						</div>
					</div>
				</div>
					</form>

			</div>
			
			
			
			
		</section>
    </div>
</div>

<!--<div id="skin-loading" class="bg-wh" onclick="$(this).fadeOut();">
	<div class="lds-hourglass"></div>
</div>-->
<script>
	window.setTimeout(function(){
		$('#skin-loading').fadeOut();
	}, 3000);
</script>

<!-- footer -->
<?php include("incs/footer-web.html") ?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js-web.html") ?>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<link href="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.css" rel="stylesheet"/>
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<script type="text/javascript">
$( document ).ready( function () {
	 
      $('.carousel').flexslider({
        animation: "slide",
        animationLoop: false,
        itemWidth: 210,
        itemMargin: 5,
        minItems: 1,
        maxItems: 3,
		move: 1,
        /*start: function(slider){
          $('body').removeClass('loading');
        }*/
      });

	//select2
	$('.keep-select-group').select2({
    	placeholder: "Please select",
    	//allowClear: true,
		dropdownAutoWidth : true,
		width: '100%'
	});
	
	$('#uid-unlimit').change( function() {
			var isChecked = this.checked;
			if(isChecked) {
				$("#uid-limit").removeClass("bg-white");
				$("#uid-limit").prop("disabled",true); 
				$("#uid-limit").prop("required",false); 
			} else {
				$("#uid-limit").addClass("bg-white");
				$("#uid-limit").prop("disabled",false);
				$("#uid-limit").prop("required",true);
			}
		});
	

});
  </script>
  


<!-- /js -->

</body>
</html>
