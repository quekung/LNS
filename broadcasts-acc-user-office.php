<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top-web.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden">
<!-- Headbar -->
<?php include("incs/header-v2.html") ?>
<script>
$(".main-menu .list-unstyled>li.active").removeClass('active');
$(".main-menu .list-unstyled>li:nth-child(4)").addClass('active');
</script>
<!-- /Headbar -->
<div class="page-checkout">

    
    <div id="toc">
		<section class="z-broadcast _self-pt0 mb0">
			<div class="bx-stepbar _self-pv20 cb-af container">
				<ul class="tabsbar">
						  <li><a href="broadcasts.php" title="Send Message"><i class="fas fa-bullhorn"></i> <span>Send Message</span></a></li>
						  <li><a href="broadcasts-create.php" title="Create Message"><i class="fas fa-layer-group"></i> <span>Create Template</span></a></li>
						  <li><a href="broadcasts-acc.php" title="User Detail" class="selected"><i class="fas fa-users-cog"></i> <span>User Detail</span></a></li>
						  <li><a href="broadcasts-setting.php" title="Message Setting"><i class="fas fa-sliders-h"></i> <span>Message Setting</span></a></li>
						  <li><a href="broadcasts-report.php" title="Report"><i class="fas fa-file-medical-alt"></i> <span>Report</span></a></li>
						  <li><a href="broadcasts-survey.php" title="Survey"><i class="fas fa-tasks"></i> <span>Survey</span></a></li>
				  </ul>
			</div>
					

			
			<div class="bg-gray2 contentTabs">
				<div id="tbc-1" class="msg">
					<form method="post" class="form-checkout form-sending'">
					<div class="head-title container txt-c">
						<h2 class="t-black"><i class="fas fa-users-cog _self-mr10 t-black"></i> User Detail</h2>	
						<p>ระบบส่งข้อความผ่านทาง line</p>
					</div>
					<div class="wrap-full _chd-cl-xs-12 _chd-cl-sm">
						<div class="main">
							<div class="wrap-full">
								
								<!-- card -->
								<div class="card bg-white">
									<div class="card-header _flex center-xs between-xsh">
										<ul class="idTabs _self-pa0 tab-receiver">
											<li><a href="broadcasts-acc.php" ><i class="fas fa-users"></i> Line Groups</a></li>
											<li><a href="broadcasts-acc-user.php" class="selected"><i class="fas fa-user"></i> Users</a></li>
										</ul>
										<div class="search-sm _self-mr20">
											<input class="txt-box" placeholder="ค้นหา...">
											<button type="submit" class="fas fa-search"></button>
										</div>
									</div>

									<!-- User -->
									<div id="users" class="card-body _self-pa30 middle-xs">
										<div class="list-user">
											<ol id="myTable">
												<li class="head">
													<div class="c1">AVATAR</div>
													<div class="c2">DISPLAY</div>
													<div class="c3">NAME</div>
													<div class="c4">SURNAME</div>
													<div class="c5">POSITION <a href="broadcasts-drop-edit.php" title="Edit Dropdown option"><i class="fas fa-edit"></i></a></div>
													<div class="c6">DEPARTMENT <a href="broadcasts-drop-edit.php" title="Edit Dropdown option"><i class="fas fa-edit"></i></a></div>
													<div class="c6">BRANCH <a href="broadcasts-drop-edit.php" title="Edit Dropdown option"><i class="fas fa-edit"></i></a></div>
													<div class="c8">EMPLOYEE ID</div>
													<div class="c7">E-MAIL</div>
													<div class="c8">TEL</div>
													<div class="c8">BIRTHDATE</div>
													<div class="c9 small">GENDER</div>
													<div class="c10 txt-c">APPROVED</div>

												</li>
													
												<?php /*?><li>
													<div class="c1 pd0">
														<img class="rounded2" src="https://www.w3schools.com/w3images/avatar3.png" alt="Fat Rascal" width="40">
													</div>
													<div class="c2">TVTIFYBPCE</div>
													<div class="c3"><input type="text" class="txt-box" placeholder="Name" value="Fatory" required=""></div>
													<div class="c4"><input type="text" class="txt-box" placeholder="Surname" value="Rascal" required=""></div>
													<div class="c5">
													<select class="form-control js-example-tags">
													  <option>Straff</option>
													  <option>Senior</option>
													  <option>Assistant Manager</option>
													  <option selected="selected">Manager</option>
													  <option>Senior Manager</option>
													  <option>AVP</option>
													  <option>VP</option>
													  <option>MD</option>
													  <option>CEO</option>
													</select>
													</div>
													<div class="c6">
														<select class="form-control js-example-tags">
														  <option>IT</option>
														  <option selected="selected">Accounting</option>
														  <option>Finance</option>
														  <option>Sale</option>
														  <option>Maketing</option>
														  <option>Call Center</option>
														  <option>HR</option>
														  <option>Reseption</option>
														</select>					
													</div>
													<div class="c7"><input type="text" class="txt-box" placeholder="Add email" value="fatory.r@oto.samartcorp.com"></div>
													<div class="c8"><input type="text" class="txt-box" placeholder="Add Telelphone" value="0812345678"></div>
													<div class="c9 male">
													  <div class="icheck-primary">
														<input type="radio" name="ugender1" id="male-chk1" checked=""> 
														<label for="male-chk1">Male</label>
													  </div>
													</div>
													<div class="c9 female">
														<div class="icheck-primary">
															<input type="radio" name="ugender1" id="female-chk1">
															<label for="female-chk1"> Female</label>
														</div>
													</div>
													<div class="c9 lgbt">
														<div class="icheck-primary">
															<input type="radio" name="ugender1" id="lgbt-chk1">
															<label for="lgbt-chk1"> LGBT</label>
														</div>
													</div>
												</li>	
												<li>
													<div class="c1 pd0">
														<img class="rounded2" src="https://www.w3schools.com/w3images/avatar3.png" alt="Fat Rascal" width="40">
													</div>
													<div class="c2">TVTIFYBPCE</div>
													<div class="c3"><input type="text" class="txt-box" placeholder="Name" value="Roony" required=""></div>
													<div class="c4"><input type="text" class="txt-box" placeholder="Surname" value="Maroon" required=""></div>
													<div class="c5">
													<select class="form-control js-example-tags">
													  <option>Straff</option>
													  <option selected="selected">Senior</option>
													  <option>Assistant Manager</option>
													  <option>Manager</option>
													  <option>Senior Manager</option>
													  <option>AVP</option>
													  <option>VP</option>
													  <option>MD</option>
													  <option>CEO</option>
													</select>
													</div>
													<div class="c6">
														<select class="form-control js-example-tags">
														  <option>IT</option>
														  <option>Accounting</option>
														  <option>Finance</option>
														  <option selected="selected">Sale</option>
														  <option>Maketing</option>
														  <option>Call Center</option>
														  <option>HR</option>
														  <option>Reseption</option>
														</select>					
													</div>
													<div class="c7"><input type="text" class="txt-box" placeholder="Add email" value="roony.m@oto.samartcorp.com"></div>
													<div class="c8"><input type="text" class="txt-box" placeholder="Add Telelphone" value="0898764532"></div>
													<div class="c9 male">
													  <div class="icheck-primary">
														<input type="radio" name="ugender2" id="male-chk2" checked=""> 
														<label for="male-chk2">Male</label>
													  </div>
													</div>
													<div class="c9 female">
														<div class="icheck-primary">
															<input type="radio" name="ugender2" id="female-chk2">
															<label for="female-chk2"> Female</label>
														</div>
													</div>
													<div class="c9 lgbt">
														<div class="icheck-primary">
															<input type="radio" name="ugender2" id="lgbt-chk2">
															<label for="lgbt-chk2"> LGBT</label>
														</div>
													</div>
												</li>	
												
												<li>
													<div class="c1 pd0">
														<img class="rounded2" src="https://www.w3schools.com/w3images/avatar4.png" alt="Fat Rascal" width="40">
													</div>
													<div class="c2">TVTIFYBPCE</div>
													<div class="c3"><input type="text" class="txt-box" placeholder="Name" value="Tims" required=""></div>
													<div class="c4"><input type="text" class="txt-box" placeholder="Surname" value="Antony" required=""></div>
													<div class="c5">
													<select class="form-control js-example-tags">
													  <option>Straff</option>
													  <option>Senior</option>
													  <option selected="selected">Assistant Manager</option>
													  <option>Manager</option>
													  <option>Senior Manager</option>
													  <option>AVP</option>
													  <option>VP</option>
													  <option>MD</option>
													  <option>CEO</option>
													</select>
													</div>
													<div class="c6">
														<select class="form-control js-example-tags">
														  <option>IT</option>
														  <option>Accounting</option>
														  <option>Finance</option>
														  <option>Sale</option>
														  <option selected="selected">Maketing</option>
														  <option>Call Center</option>
														  <option>HR</option>
														  <option>Reseption</option>
														</select>					
													</div>
													<div class="c7"><input type="text" class="txt-box" placeholder="Add email" value="tims.a@oto.samartcorp.com"></div>
													<div class="c8"><input type="text" class="txt-box" placeholder="Add Telelphone" value="0831564784"></div>
													<div class="c9 male">
													  <div class="icheck-primary">
														<input type="radio" name="ugender3" id="male-chk3"> 
														<label for="male-chk3">Male</label>
													  </div>
													</div>
													<div class="c9 female">
														<div class="icheck-primary">
															<input type="radio" name="ugender3" id="female-chk3" checked="">
															<label for="female-chk3"> Female</label>
														</div>
													</div>
													<div class="c9 lgbt">
														<div class="icheck-primary">
															<input type="radio" name="ugender3" id="lgbt-chk3">
															<label for="lgbt-chk3"> LGBT</label>
														</div>
													</div>
													
												</li>	<?php */?>
												<?php function generateRandomString($length = 10) {
    $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
} ?>
												<?php for($i=4;$i<=20;$i++){ ?>
												<li>
													<div class="c1 pd0 txt-c">
														<?php if($i%3==0) {?><img class="rounded2" src="https://www.w3schools.com/w3images/avatar4.png" alt="Fat Rascal" width="40"><? } else { ?><img class="rounded2" src="https://www.w3schools.com/w3images/avatar3.png" alt="Fat Rascal" width="40"><? } ?>
													</div>
													<div class="c2"><?php echo generateRandomString(); ?></div>
													<div class="c3"><input type="text" class="txt-box" placeholder="Name" required></div>
													<div class="c4"><input type="text" class="txt-box" placeholder="Surname" required></div>
													<div class="c5">
													<select class="form-control js-example-tags">
													  <option selected="selected">Straff</option>
													  <option <?php if($i%3==0) {?>selected="selected"<? } ?>>Senior</option>
													  <option>Assistant Manager</option>
													  <option <?php if($i%5==0) {?>selected="selected"<? } ?>>Manager</option>
													  <option>Senior Manager</option>
													  <option <?php if($i%7==0) {?>selected="selected"<? } ?>>AVP</option>
													  <option>VP</option>
													  <option>MD</option>
													  <option>CEO</option>
													</select>
													</div>
													<div class="c6">
														<select class="form-control js-example-tags">
														  <option selected="selected">IT</option>
														  <option <?php if($i%3==0) {?>selected="selected"<? } ?>>Accounting</option>
														  <option>Finance</option>
														  <option <?php if($i%5==0) {?>selected="selected"<? } ?>>Sale</option>
														  <option>Maketing</option>
														  <option <?php if($i%7==0) {?>selected="selected"<? } ?>>Call Center</option>
														  <option>HR</option>
														  <option>Reseption</option>
														</select>
					
													</div>
													<div class="c6">
														<select class="form-control js-example-tags">
														  <option selected="selected">branch</option>
														  <option <?php if($i%3==0) {?>selected="selected"<? } ?>>R&amp;D</option>
														  <option>Developer</option>
														  <option <?php if($i%5==0) {?>selected="selected"<? } ?>>Network</option>
														  <option>Application</option>
														  <option <?php if($i%7==0) {?>selected="selected"<? } ?>>Sale</option>
														  <option>Admin</option>
														  <option>Tester</option>
														</select>
					
													</div>
													<div class="c7"><input type="text" class="txt-box" placeholder="Add ID" required></div>
													<div class="c7"><input type="text" class="txt-box" placeholder="Add E-mail"></div>
													<div class="c8"><input type="text" class="txt-box" placeholder="Add Telelphone"></div>
													<div class="c7"><input type="date" class="txt-box" placeholder=""></div>
													<div class="c9 small">
													 <select class="form-control js-example-tags">
													 	<option>Male</option>
														<option>Female</option>
														<option>LGBT</option>
													 </select>
													</div>
													<!--<div class="c9 male">
													  <div class="icheck-primary">
														<input type="radio" name="ugender<? echo($i) ?>" id="male-chk<? echo($i) ?>" <?php /* if($i%3!=0) {?>checked<? } */ ?>> 
														<label for="male-chk<? echo($i) ?>">Male</label>
													  </div>
													</div>
													<div class="c9 female">
														<div class="icheck-primary">
															<input type="radio" name="ugender<? echo($i) ?>" id="female-chk<? echo($i) ?>" <?php /* if($i%3==0) {?>checked<? } */?>>
															<label for="female-chk<? echo($i) ?>"> Female</label>
														</div>
													</div>
													<div class="c9 lgbt">
														<div class="icheck-primary">
															<input type="radio" name="ugender<? echo($i) ?>" id="lgbt-chk<? echo($i) ?>" <?php /* if($i%3==0) {?>checked<? } */?>>
															<label for="lgbt-chk<? echo($i) ?>"> LGBT</label>
														</div>
													</div>-->
													<div class="c10 txt-c">
													  <div class="icheck-primary">
														<input type="checkbox" name="approve<? echo($i) ?>" id="approve-chk<? echo($i) ?>"> 
														<label for="approve-chk<? echo($i) ?>"><span class="visible-xs"> Approve</span></label>
													  </div>
													</div>
												</li>
												<?php } ?>
											</ol>
										</div>
									</div>
									
									<div class="sticky-bottom card-footer mf-bottom">
									<div class="__chd-ph10 center-xs">
											<button type="reset" class="ui-btn-gray btn-md" data-toggle="modal" data-target="#modal-close">Cancel</button>
											<button type="button" class="ui-btn-green btn-md" onclick="$(this).children('i').removeClass('hid'); $('.form-sending')[0].reset();"><i class="hid fas fa-circle-notch fa-spin"></i> Save Changes</button>
									</div>
								  </div>
								</div>
								<!-- /card -->
							</div>

						</div>
					</div>
				</div>
					</form>

			</div>
			
			
			
			
		</section>
    </div>
</div>

<!--<div id="skin-loading" class="bg-wh" onclick="$(this).fadeOut();">
	<div class="lds-hourglass"></div>
</div>-->
<script>
	window.setTimeout(function(){
		$('#skin-loading').fadeOut();
	}, 3000);
</script>

<!-- footer -->
<?php include("incs/footer-web.html") ?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js-web.html") ?>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<link href="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.css" rel="stylesheet"/>
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<script type="text/javascript">
$( document ).ready( function () {
	 
      $('.carousel').flexslider({
        animation: "slide",
        animationLoop: false,
        itemWidth: 210,
        itemMargin: 5,
        minItems: 1,
        maxItems: 3,
		move: 1,
        /*start: function(slider){
          $('body').removeClass('loading');
        }*/
      });

	//select2

	$(".js-example-tags").select2({
	   //dropdownAutoWidth : true,
	  width: '100%',
	  tags: true,
	  insertTag: function (data, tag) {
		// Insert the tag at the end of the results
		data.push(tag);
	  }
	  /*createTag: function (params) {
		var term = $.trim(params.term);

		if (term === '') {
		  return null;
		}

		return {
		  id: term,
		  text: term,
		  newTag: true // add additional parameters
		}
	  }*/
	});
	
	

});
  </script>
  


<!-- /js -->

</body>
</html>
