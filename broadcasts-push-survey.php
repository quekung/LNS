<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top-web.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden">
<!-- Headbar -->
<?php include("incs/header-v2.html") ?>
<script>
$(".main-menu .list-unstyled>li.active").removeClass('active');
$(".main-menu .list-unstyled>li:nth-child(4)").addClass('active');
</script>
<!-- /Headbar -->
<div class="page-checkout">

    
    <div id="toc">
		<section class="z-broadcast _self-pt0 mb0">
			<div class="bx-stepbar _self-pv20 cb-af container">
				<ul class="tabsbar">
						  <li><a href="broadcasts.php" title="Send Message"><i class="fas fa-bullhorn"></i> <span>Send Message</span></a></li>
						  <li><a href="broadcasts-create.php" title="Create Message"><i class="fas fa-layer-group"></i> <span>Create Template</span></a></li>
						  <li><a href="broadcasts-acc.php" title="User Detail"><i class="fas fa-users-cog"></i> <span>User Detail</span></a></li>
						  <li><a href="broadcasts-setting.php" title="Message Setting"><i class="fas fa-sliders-h"></i> <span>Message Setting</span></a></li>
						  <li><a href="broadcasts-report.php" title="Report"><i class="fas fa-file-medical-alt"></i> <span>Report</span></a></li>
						  <li><a href="broadcasts-survey.php" title="Survey" class="selected"><i class="fas fa-tasks"></i> <span>Survey</span></a></li>
				  </ul>
			</div>
					

			
			<div class="bg-gray2 contentTabs">
				<div id="tbc-1" class="msg">
					<form method="post" class="form-checkout form-sending'">
					<div class="head-title container txt-c">
						<h2 class="t-black"><i class="fas fa-bullhorn _self-mr10 t-black"></i>Push Survey</h2>	
						<p>ระบบส่งข้อความผ่านทาง line</p>
					</div>
					<div class="wrap-full _chd-cl-xs-12 _chd-cl-sm">
						<div class="main row _chd-cl-xs-12 _chd-cl-sm">
							<div class="col-l _self-cl-sm-05-md-05-lg-04">
								
								<!-- card -->
								<div class="card">
									<div class="card-header">
										<h3 class="card-title m-0"><span class="step ui-btn-green2 _self-ph10-mr10">ขั้นตอนที่ 1</span> <i class="nav-icon fas fa-comment-alt"></i> Edit Titile</h3>
									</div>
									<div class="card-body">
									
									<ul class="idTabs tab-receiver">
											<li><a href="#newpush" class="selected"><i class="fas fa-comment-medical nav-icon"></i> Survey Detail</a></li>
										</ul>

									<div id="newpush" class="bg-white ps-rlt _self-pa10 cb-af rounded1" style="display: block">
										
										<div class="tab-content" id="vert-tabs-tabContent">


										 <div class="tab-pane fade" id="display-push-sv">

											<p class="lead mb-0">Properties</p>
											 <div class="tab-custom-content">

												<div id="main-apm" class="main-imagemap wrap-upload mb-4">
													<div class="form-group p-0 mb-1">
														<label>Select Type Image</label>
														<select class="custom-select" id="select-apmimage-type">
														  <option value="0">URL</option>
														  <option value="1">Local file</option>
														</select>
													 </div>

													<div id="f-url_apm" class="f-url form-group">  
														<label for="apm_urlimg2">From image URL</label>
														<div class="input-group mb-3">
														  <div class="input-group-prepend">
															<span class="input-group-text"><i class="fas fa-link"></i></span>
														  </div>
														  <input type="text" class="form-control" placeholder="URL Image" id="apm_urlimg2" value="https://dummyimage.com/500x300/17a3b8/ffffff.png&text=Survey%20demo">
														</div>
													</div>

													<div id="f-upload_apm" class="f-upload form-group" style="display: none">
														<label for="apm_img2">Upload image</label>
														<div class="input-group">
																<div class="custom-file">
																	<input type="file" class="custom-file-input2" id="apm_img2" accept=".jpg,.jpeg,.png">
																	<img id="temp_apmimage_src" hidden="">
																	<label class="custom-file-label" id="apm_labelimg2" for="apm_img2"><p>Choose file</p></label>
																</div>
																<div class="input-group-append">
																	<button class="input-group-text" id="apm_uploadimg2">Upload</button>
																</div>

														</div>
													 </div>
												</div>

												 <div class="form-group">
													<input type="text" class="form-control" id="titleApm" placeholder="Title" value="แบบสำรวจความพึงพอใจในการปฏิบัติงานของพนักงาน ครึ่งปีแรก 2563">
												</div>

												<div class="form-group">
													<textarea id="textApm" class="form-control" rows="3" placeholder="Description">ขอความกรุณาพนักงานทุกท่านร่วมประเมิณความพอใจในการปฏิบัติงานของพนักงานค่ะ</textarea>
													<!--<input type="text" class="form-control" id="textApm" placeholder="Text">-->
												</div>

												<div class="form-group">
													<label>Number of Actions</label>
													<select class="form-control select-action-chd" id="select-apmNum-action">
													  <option value="0">1</option>
													  <option value="1" selected>2</option>
													  <option value="2">3</option>
													</select>
												</div>

												<div class="main-apm-action bg-light p_self-pa10">
													<div id="group-a1" class="g-action1">
														<div class="form-group">
															<label>Action1</label>
															<div class="_flex middle-xs _self-cl-xs-12-mb10">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Type</span>
																<div class="col">
																	<select class="form-control" id="select-apmNum-action1">
																	  <option value="0">Message Action</option>
																	  <option value="1" selected>URI Action</option>
																	  <option value="2">Postback Action</option>
																	  <option value="2">Web App Action</option>
																	  <option value="2">Datetime Picker Action</option>
																	</select>
																</div>
															</div>

															<div class="_flex middle-xs _self-cl-xs-12-mb10">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Label</span>
																<div class="col">
																	<input type="text" class="form-control" id="actionApm1" placeholder="Action 1" value="เริ่มทำแบบสอบถาม">
																</div>
															</div>

															<div class="_flex middle-xs _self-cl-xs-12-mb10">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">URI</span>
																<div class="col">
																	<input type="text" class="form-control" id="actionLinkApm1" placeholder="URI" value="http://ui.beurguide.net/keeppro/survey-view.php">
																</div>
															</div>
														</div>
													</div>

													<div id="group-a2" class="g-action2" style="display: block">
														<div class="form-group">
															<label>Action2</label>
															<div class="_flex middle-xs _self-cl-xs-12-mb10">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Type</span>
																<div class="col">
																	<select class="form-control" id="select-apmNum-action2">
																	  <option value="0">Message Action</option>
																	  <option value="1">URI Action</option>
																	  <option value="2" selected>Postback Action</option>
																	  <option value="2">Web App Action</option>
																	  <option value="2">Datetime Picker Action</option>
																	</select>
																</div>
															</div>

															<div class="_flex middle-xs _self-cl-xs-12-mb10">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Label</span>
																<div class="col">
																	<input type="text" class="form-control" id="actionApm2" placeholder="Action 2" value="ยังก่อน">
																</div>
															</div>


															<div class="_flex middle-xs _self-cl-xs-12-mb10">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Text</span>
																<div class="col">
																	<input type="text" class="form-control" id="actionLinkApm2" placeholder="Action" value="Cancel">
																</div>
															</div>
														</div>
													</div>

													<div id="group-a3" class="g-action3">
														<div class="form-group">
															<label>Action3</label>
															<div class="_flex middle-xs _self-cl-xs-12-mb10">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Type</span>
																<div class="col">
																	<select class="form-control" id="select-apmNum-action3">
																	  <option value="0">Message Action</option>
																	  <option value="1" selected>URI Action</option>
																	  <option value="2">Postback Action</option>
																	  <option value="2">Web App Action</option>
																	  <option value="2">Datetime Picker Action</option>
																	</select>
																</div>
															</div>

															<div class="_flex middle-xs _self-cl-xs-12-mb10">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Label</span>
																<div class="col">
																	<input type="text" class="form-control" id="actionApm3" placeholder="Action 3">
																</div>
															</div>


															<div class="_flex middle-xs _self-cl-xs-12-mb10">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">URI</span>
																<div class="col">
																	<input type="text" class="form-control" id="actionLinkApm3" placeholder="URI">
																</div>
															</div>
														</div>
													</div>

												</div>

											 </div>

											 <div class="tab-footer clearfix _self-mt20">
												<button type="button" id="displayCard" class="ui-btn  btn-info btn-lg float-right btn-add-appointment">Display</button>
											</div>

										  </div>
					
										</div>
										
										
					
									</div>
									</div>
								</div>
								<!-- /card -->
							</div>
							<div class="col-c  _self-cl-sm-07-md-07-lg-08">
								<!-- card -->
								<div class="card">
									<div class="card-header">
										<h3 class="card-title"><span class="step ui-btn-green2 _self-ph10-mr10">ขั้นตอนที่ 2</span> <i class="nav-icon fas fa-user-check"></i> Add Receiver</h3>
									</div>
									<div class="card-body">
										<ul class="idTabs tab-receiver">
											<li><a href="#linegroup"><i class="fas fa-users"></i> Line Groups</a></li>
											<li><a href="#users"><i class="fas fa-user"></i> Users</a></li>
										</ul>
										<div class="contentTabs">
											<div class="remain">
												Remaining Messages <input class="showRmUID" type="text" id="countRmainUID" value="34920" readonly>/month
											</div>
											<div id="linegroup" class="form-group _self-pa30 bg-white rounded1">

												<label for="datepicker">เลือกกลุ่ม LINE</label>

												<div class="icheck-primary _self_ps-rlt"><input type="checkbox" id="checkAll"> <label for="checkAll">Select All</label></div>
												<div class="js-select wr">
													<select id="e1" class="keep-select-group" name="states[]" multiple="multiple">
													  <!--<option value="0">Select All Group</option>-->
													  <option value="A">Group name A</option>
													  <option value="B">Group name B</option>
													  <option value="C">Group name C</option>
													  <option value="D">Group name D</option>
													  <option value="E">Group name E</option>
													  <option value="F">Group name F</option>
													</select>
													
													<div class="info-container">     <small class="info text-muted">*รายการกลุ่ม line ที่มี bot Keepaline <span class="t-red">"Activated"</span> แล้วเท่านั้น</small> </div>
												</div>
											</div>
											
											<div id="users" class="form-group _self-pa30 bg-white rounded1">
												<div class="top-bar _flex between-xs middle-xs">
													<label for="byUser">เลือก User ในระบบ</label>
													<div class="sort-bar d-flex flex-nowrap">
														<select class="form-control select-box" data-placeholder="Type" style="width:auto">
														  <option></option>
														  <option>Name</option>
														  <option>Employee ID</option>
														  <option>Telephone</option>
														  <option>Department</option>
														  <option>Gender</option>
														</select>
														<div class="search-sm ml10-xs">
															<input class="txt-box" placeholder="ค้นหา..." style="width: 140px">
															<button type="submit" class="fas fa-search"></button>
														</div>
													</div>
												</div>
												<div class="icheck-primary _self_ps-rlt"><input type="checkbox" id="checkUserAll" class="master-chk"> <label for="checkUserAll">Select All</label></div>
												<div class="list-users">
													<?php for($i=1;$i<=10;$i++){ ?>
													<!-- User -->
													<div class="card _flex top-xs">
														<a class="_flex" href="javascript:;">
															<img src="https://www.w3schools.com/w3images/avatar3.png" alt="Fat Rascal" width="40">
														</a>
														<div class="pl-2 _flex between-xs _self-cl-xs">
															<div class="card-body _flex middle-xs">
																<a href="javascript:;" class="col-l">
																	<p class="list-item-heading">Fatory Rascal</p>
																</a>
																<p class="col-s">Manager</p>
																<p class="col-s txt-c">IT</p>
																<p class="col-s txt-c">
																<?php if($i%3==0) {?>R&amp;D
																<?php } elseif($i%5==0) {?>Network
																<?php } else { ?>Application <?php } ?>
																</p>
																<div class="col-s txt-c">
																	<span class="badge badge-primary rounded2">Male</span>
																</div>
																<div class="col-auto txt-c">Age 42</div>
															</div>
															<div class="custom-control icheck-primary">
															  <input class="chd-chk bubble-chk" type="checkbox" value="" name="sentMSG" id="userCheck1-<?php echo $i; ?>">
															  <label for="userCheck1-<?php echo $i; ?>"></label>
															</div>
															
														</div>
													</div>
													<!-- /User -->
													<!-- User -->
													<div class="card _flex top-xs">
														<a class="_flex" href="javascript:;">
															<img src="https://www.w3schools.com/w3images/avatar2.png" alt="Fat Rascal" width="40">
														</a>
														<div class="pl-2 _flex between-xs _self-cl-xs">
															<div class="card-body _flex middle-xs">
																<a href="javascript:;" class="col-l">
																	<p class="list-item-heading">Roony Maroon</p>
																</a>
																<p class="col-s">Senior</p>
																<p class="col-s txt-c">Sale</p>
																<p class="col-s txt-c">
																<?php if($i%3==0) {?>R&amp;D
																<?php } elseif($i%5==0) {?>Network
																<?php } else { ?>Supervisor <?php } ?>
																</p>
																<div class="col-s txt-c">
																	<?php  if($i%3==0) {?><span class="badge bg-purple rounded2">LGBT</span><? } else {?><span class="badge badge-primary rounded2">Male</span><? } ?>
																</div>
																<div class="col-auto txt-c">Age 28</div>
															</div>
															<div class="custom-control icheck-primary">
															  <input class="chd-chk bubble-chk" type="checkbox" value="" name="sentMSG" id="userCheck2-<?php echo $i; ?>">
															  <label for="userCheck2-<?php echo $i; ?>"></label>
															</div>
															
														</div>
													</div>
													<!-- /User -->
													<!-- User -->
													<div class="card _flex top-xs">
														<a class="_flex" href="javascript:;">
															<img src="https://www.w3schools.com/w3images/avatar4.png" alt="Tims Antony" width="40">
														</a>
														<div class="pl-2 _flex between-xs _self-cl-xs">
															<div class="card-body _flex middle-xs">
																<a href="javascript:;" class="col-l">
																	<p class="list-item-heading mb-0 truncate">Tims Antony</p>
																</a>
																<p class="col-s">Staff</p>
																<p class="col-s txt-c">Accounting</p>
																<p class="col-s txt-c">
																<?php if($i%3==0) {?>R&amp;D
																<?php } elseif($i%5==0) {?>Network
																<?php } else { ?>Admin <?php } ?>
																</p>
																<div class="col-s txt-c">
																	<span class="badge bg-maroon rounded2">Female</span>
																</div>
																<div class="col-auto txt-c">Age 24</div>
															</div>
															<div class="custom-control icheck-primary">
															  <input class="chd-chk bubble-chk" type="checkbox" value="" name="sentMSG" id="userCheck3-<?php echo $i; ?>">
															  <label for="userCheck3-<?php echo $i; ?>"></label>
															</div>
															
														</div>
													</div>
													<!-- /User -->
													<?php } ?>
												</div>
											</div>
										
										</div>
										
									</div>
								</div>
								<!-- /card -->
								<!-- card -->
								<div class="card _self-mt30">
									<div class="card-header">
										<h3 class="card-title"><span class="step ui-btn-green2 _self-ph10-mr10">ขั้นตอนที่ 3</span> <i class="nav-icon fas fa-calendar-alt"></i> Schedule</h3>
									</div>
									<div class="card-body">
									
									<div class="form-group p-2">
								<div class="bg-light p-3 rounded">
								  <div class="form-group clearfix">
								  <div class="_self-mb10">
									  <div class="icheck-primary">
										<input type="radio" id="radioPrimary1" name="chk-send" value="now" checked="">
										<label for="radioPrimary1">
											ส่งตอนนี้
										</label>
									  </div>
								  </div>
								  <div class="_self-mb10">
									  <div class="icheck-primary">
										<input type="radio" id="radioPrimary2" name="chk-send" value="set">
										<label for="radioPrimary2">
											ตั้งค่าวันที่/เวลาส่ง
										</label>
									  </div>
								  </div>
								  

								</div>

								<div id="show-schedule" class="row" style="display: none">
								<div class="col-sm-6">
									<div class="form-group">
										<label for="datepicker">ตั้งค่าวันที่</label>
										<div class="_flex nowrap middle-xs date" id="timepicker" data-target-input="nearest">
											<input class="txt-box" type="text" id="datepicker">
											<i class="_self-ml10 far fa-calendar" onClick="$('#datepicker').focus();"></i>
										</div>
									 </div>
									 <!--<div class="txt-c" style="margin-top: -15px">
										<div id="calendarshow" class="w-100"></div>
									 </div>-->
								</div>

								<div class="col-sm-6 d-flex flex-column justify-content-between">

									<!-- time Picker -->
									<div class="bootstrap-timepicker">
									  <div class="form-group">
										<label>ตั้งค่าเวลา</label>

										<div class="_flex nowrap middle-xs date" id="timepicker" data-target-input="nearest">
										  <!--<input class="txt-box" type="time" id="appt" name="appt">-->
										  <input class="txt-box" type="text" id="time" placeholder="Time">
										  <i class="_self-ml10 far fa-clock" onClick="$('#time').focus();"></i>
										</div>
										<!-- /.input group -->
									  </div>
									  <!-- /.form group -->
									</div>





								</div>
								</div>

								<div class="mt20 _chd-ph10 center-xs">
									<button type="button" class="ui-btn-gray btn-lg" data-toggle="modal" data-target="#modal-close">Cancel</button>
									<button type="submit" class="ui-btn-green btn-lg swalDefaultSuccess" onclick="$(this).children('i').removeClass('hid'); $('.form-sending')[0].reset();"><i class="hid fas fa-circle-notch fa-spin"></i> Send Message</button>
									<!-- <button type="submit" class="btn btn-danger btn-lg toastrDefaultError mr-2">Send Unsuccessful</button> -->
								</div>

								</div>
									
									</div>
								</div>
								<!-- /card -->
								
							</div>
						</div>
						
						<div class="sidebar-right _self-cl-sm-12-md-03 _flex center-xs">
							<!-- live Mobile -->
							<div id="main-log" class="bx-log active">
<div id="live-screen">
<ul class="list-sr-chat show">

<li class="result-carousel messageScreen4">
	<div class="direct-chat-msg"><div class="direct-chat-img"></div></div>
	<div class="card-button"><figure class="image"><a href="#listaction"><img src="https://dummyimage.com/500x300/17a3b8/ffffff.png&amp;text=Survey%20demo"></a></figure>
		<div class="detail"> <h2>แบบสำรวจความพึงพอใจในการปฏิบัติงานของพนักงาน ครึ่งปีแรก 2563</h2> 
			<p>ขอความกรุณาพนักงานทุกท่านร่วมประเมิณความพอใจในการปฏิบัติงานของพนักงานค่ะ</p>
		</div>
		<ul>
		<li><a href="#Action1">เริ่มทำแบบสอบถาม</a></li>
		<li><a href="#Action2">ยังก่อน</a></li></ul>
		</div>
		</li>

</ul>
</div>
</div>
							<?php /*?><div id="main-log" class="bx-log active">
							<div id="live-screen">
								<ul class="list-sr-chat show">
									<li id="result-msg1">
										<div class="direct-chat-msg">
											<!--<div class="direct-chat-infos clearfix">
											  <span class="direct-chat-name float-left">Alexander Pierce</span>
											  <span class="direct-chat-timestamp float-right">23 Jan 2:00 pm</span>
											</div>-->
											<!-- /.direct-chat-infos -->
											<img class="direct-chat-img" src="https://ui.beurguide.net/LNS/dist/img/avatar-admin.png" alt="Message User Image">
											<!-- /.direct-chat-img -->
											<div class="direct-chat-text">
											  Is this template really for free? That's unbelievable!
											</div>
											<!-- /.direct-chat-text -->
										  </div>
									</li>

									<li id="result-msg2">
										<div class="direct-chat-msg">
											<!-- /.direct-chat-infos -->
											<img class="direct-chat-img" src="https://ui.beurguide.net/LNS/dist/img/avatar-admin.png" alt="Message User Image">
											<!-- /.direct-chat-img -->
											<div class="direct-chat-text direct-chat-image">
											  <img class="img-fluid pad" src="https://ui.beurguide.net/LNS/dist/img/photo3.jpg" alt="Photo">
											</div>
											<!-- /.direct-chat-text -->
										  </div>
									</li>

									
								</ul>
							</div>
						</div><?php */?>
							<!-- /live Mobile -->
						</div>
					</div>
				</div>
					</form>

			</div>
			
			
			
			
		</section>
    </div>
</div>

<!--<div id="skin-loading" class="bg-wh" onclick="$(this).fadeOut();">
	<div class="lds-hourglass"></div>
</div>-->
<script>
	window.setTimeout(function(){
		$('#skin-loading').fadeOut();
	}, 3000);
</script>

<!-- footer -->
<?php include("incs/footer-web.html") ?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js-web.html") ?>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<link href="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.css" rel="stylesheet"/>
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>

<!--<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>-->
<script>
  $( function() {
     $( ".todo-list" ).sortable({
      placeholder: "ui-state-highlight",
	  items: "li:not(.ui-state-disabled)",
	  //cancel: ".ui-state-disabled"
    });
    $( ".todo-list li" ).disableSelection();
  } );
</script>

<script>
/*upload*/
$( document ).ready( function () {
	var count_message = 0;
	
	$('.custom-select').on('change', function() {

		if ($(this)[0].selectedIndex == 0) { 
			$(this).parents('.wrap-upload').find('.f-url').show();
			$(this).parents('.wrap-upload').find('.f-upload').hide();
			
		} else if ($(this)[0].selectedIndex == 1)  {
			$(this).parents('.wrap-upload').find('.f-upload').show();
			$(this).parents('.wrap-upload').find('.f-url').hide();
		}

	});
	
	$('input[type=radio][name=chk-send]').change(function() {
		if (this.value == 'now') {
			$("#show-schedule").hide();
		}
		else if (this.value == 'set') {
			$("#show-schedule").show();
		}
	});
	
	
	// jQuery UI sortable for the todo list
	  /*$('.todo-list').sortable({
		placeholder         : 'sort-highlight',
		handle              : '.handle',
		forcePlaceholderSize: true,
		zIndex              : 999999
	  })*/
	  
	  // Add Text
	$('.btn-add-msg').click(function() {
		var message_text = $("#message_text").val();
		var count_items = 0;
		$("ul.list-sr-chat>li").each(function () {
			count_items++;
		});
		if ( count_items < 3 && message_text != '' ) {
			count_message++;
			if ( $('#newpush .todo-list').hasClass('show') ) {
				
			} else {
				$('.todo-list,.list-sr-chat,.save-template').addClass('show');
			}
			$('.todo-list').append('<li class="done messageList'+count_message+'">'+
				'<span class="handle ui-sortable-handle">'+
					' <i class="fas fa-ellipsis-v"></i>'+
					' <i class="fas fa-ellipsis-v"></i>'+
				'</span>'+
				'<div class="icheck-primary d-inline ml-2">'+
					' <input type="checkbox" id="chklist'+count_message+'" onclick="checkboxShowHide('+count_message+')" checked>'+
					' <label for="chklist'+count_message+'"></label>'+
				'</div>'+
				'<span class="text">'+message_text+'</span>'+
				'<small class="badge badge-info"><i class="fas fa-text-height"></i> Text</small>'+
				'<div class="tools">'+
					'<i class="fas fa-edit text-gray" onclick="editMessage('+count_message+')"></i>'+
					'<i class="fas fa-trash" onclick="removeMessage('+count_message+')"></i>'+
				'</div>'+
			'</li>');
			$('.list-sr-chat').append('<li class="messageScreen'+count_message+'">'+
				'<input type="hidden" class="messageScreen'+count_message+'" id="valScreen'+count_message+'" value="txt|'+message_text+'"/>'+
				'<div class="direct-chat-msg">'+
					'<div class="direct-chat-img"></div>'+
					'<div class="direct-chat-text">'+message_text+'</div>'+
				'</div>'+
			'</li>');
			$("#message_text").val('');
		} else {
			if ( count_items >= 3 ) {
				alert('Maximum 3 Message');
			} else {
				alert('Please input text');
			}
		}
	});
	//addd image
	$('.btn-add-image').click(function() {
		var src_img = '';
		if ($("#select-image-type")[0].selectedIndex == 0)  {
			src_img = $('#message_urlimg').val();
		} else {
			src_img = $('#temp_image_src')[0].src; 
		}
		var count_items = 0;
		$("ul.list-sr-chat>li").each(function () {
			count_items++;
		});
		if ( count_items < 3 && src_img != '' ) {
			count_message++;
			if ($('#newpush .todo-list').hasClass('show')) {

			} else {
				$('.todo-list,.list-sr-chat,.save-template').addClass('show');
			}
			$('.todo-list').append('<li class="done messageList'+count_message+'">'+
				'<span class="handle ui-sortable-handle">'+
					' <i class="fas fa-ellipsis-v"></i>'+
					' <i class="fas fa-ellipsis-v"></i>'+
				'</span>'+
				'<div class="icheck-primary d-inline ml-2">'+
					' <input type="checkbox" id="chklist'+count_message+'" onclick="checkboxShowHide('+count_message+')" checked>'+
					' <label for="chklist'+count_message+'"></label>'+
				'</div>'+
				'<span class="text">'+src_img+'</span>'+
				'<small class="badge bg-teal"><i class="far fa-image"></i> Image</small>'+
				'<div class="tools">'+
					'<i class="fas fa-edit text-gray" onclick="editMessage('+count_message+')"></i>'+
					'<i class="fas fa-trash" onclick="removeMessage('+count_message+')"></i>'+
				'</div>'+
			'</li>');
			$('.list-sr-chat').append('<li class="messageScreen'+count_message+'">'+
				'<input type="hidden" class="messageScreen'+count_message+'" id="valScreen'+count_message+'" value="img|'+src_img+'"/>'+
				'<div class="direct-chat-msg">'+
					'<div class="direct-chat-img"></div>'+
					'<div class="direct-chat-text direct-chat-image"><img class="img-fluid pad" src="'+src_img+'"></div>'+
				'</div>'+
			'</li>');
			$('#message_urlimg').val('');
			$('#message_img').val('');
			$('#temp_image_src')[0].src = '';
			$('#displayimg').prop('disabled', true);
		} else {
			if ( count_items >= 3 ) {
				alert('Maximum 3 Message');
			} else {
				alert('Please input image');
			}
		}
	});
	
   /*imagemap*/
$("[name=option_imgmap]").on("click", function() {
		var isCase = this.value;
		if(isCase == 1) {
			//alert("Video");
			$('#for-vdo').show();
			$('#for-image').hide();
		} else {
			//alert("Image");
			$('#for-vdo').hide();
			$('#for-image').show();
		}

	})
	$("[name=option_layoutmap]").on("click", function() {
		var isCase = this.value;
		if(isCase == 0) {
			$('.lay-sw').hide();
			$('.imt-1').show();	
		} else if(isCase == 1) {
			$('.lay-sw').hide();
			$('.imt-2').show();	
		} else if(isCase == 2) {
			$('.lay-sw').hide();
			$('.imt-3').show();	
		} else {
			$('.lay-sw').hide();
			$('.imt-4').show();	
		}
		
		if ($(this).parents('.btn').hasClass( 'active' )) {
		} else {
		$(".tab-layout .btn").removeClass('active');
		$(this).parents('.btn').addClass('active');
		}
		

	})
	$("[name=option_imgmap]:checked").click();
   
	$('.btn-add-imagemap').click(function() {
		var src_img = '';
		if ($("#select-mainimage-type")[0].selectedIndex == 0)  {
			src_img = $('#map_urlimg2').val();
		} else {
			src_img = $('#map_mapimage_src')[0].src; 
		}
		
		var radios = document.getElementsByName('option_layoutmap');
		var isCase = '';
		
		for (var i = 0, length = radios.length; i < length; i++) {
		  if (radios[i].checked) {
			// do whatever you want with the checked radio
			//alert(radios[i].value);
			isCase = radios[i].value;

			// only one radio can be logically checked, don't check the rest
			break;
		  }
		}

		//alert(isCase);

		
		
		var count_items = 0;
		$("ul.list-sr-chat>li").each(function () {
			count_items++;
		});
		if ( count_items < 3 && src_img != '' ) {
			count_message++;
			if ($('#newpush .todo-list').hasClass('show')) {

			} else {
				$('.todo-list,.list-sr-chat,.save-template').addClass('show');
			}
			$('.todo-list').append('<li class="done messageList'+count_message+'">'+
				'<span class="handle ui-sortable-handle">'+
					' <i class="fas fa-ellipsis-v"></i>'+
					' <i class="fas fa-ellipsis-v"></i>'+
				'</span>'+
				'<div class="icheck-primary d-inline ml-2">'+
					' <input type="checkbox" id="chklist'+count_message+'" onclick="checkboxShowHide('+count_message+')" checked>'+
					' <label for="chklist'+count_message+'"></label>'+
				'</div>'+
				'<span class="text">'+src_img+'</span>'+
				'<small class="badge bg-purple"><i class="fas fa-images"></i> Imagemap</small>'+
				'<div class="tools">'+
					'<i class="fas fa-edit text-gray" onclick="editMessage('+count_message+')"></i>'+
					'<i class="fas fa-trash" onclick="removeMessage('+count_message+')"></i>'+
				'</div>'+
			'</li>');
			
			/*$('#message_urlimg').val('');
			$('#message_img').val('');
			$('#temp_image_src')[0].src = '';
			$('#displayimg').prop('disabled', true);*/
			if(isCase == 0) {
				//$('.mark-link').addClass('full-w');
				$('.list-sr-chat').append('<li class="result-imgmap messageScreen'+count_message+'">'+
					'<input type="hidden" class="messageScreen'+count_message+'" id="valScreen'+count_message+'" value="img|'+src_img+'"/>'+
					'<div class="frame-image">'+
					'<img class="img-fluid pad"src="'+src_img+'" alt="Photo">'+
					'<div class="mark-link"><!--<div class="mark-link v2"><div class="mark-link p4">-->'+
					// 'if(isCase == 1) {'h2'} else if(isCase == 2) {'v2'} else if(isCase == 3) {'p4 '}'
					'<a href="#test1" class="link-p-1" title="Action1" style="background: #ccc; opacity:.5"></a>'+
					'</div>'+
					'</div>'+
				'</li>');
			} else if(isCase == 1) {
				//$('.mark-link').addClass('h2');
				$('.list-sr-chat').append('<li class="result-imgmap messageScreen'+count_message+'">'+
				'<input type="hidden" class="messageScreen'+count_message+'" id="valScreen'+count_message+'" value="img|'+src_img+'"/>'+
					'<div class="frame-image">'+
					'<img class="img-fluid pad"src="'+src_img+'" alt="Photo">'+
					'<div class="mark-link h2">'+
					'<a href="#test1" class="link-p-1" title="Action1" style="background: #ccc; opacity:.5"></a>'+
					'<a href="#test2" class="link-p-2" title="Action2" style="background: #aaa; opacity:.5"></a>'+
					'</div>'+
					'</div>'+
				'</li>');
			} else if(isCase == 2) {
				//$('.mark-link').addClass('v2');	
				$('.list-sr-chat').append('<li class="result-imgmap messageScreen'+count_message+'">'+
				'<input type="hidden" class="messageScreen'+count_message+'" id="valScreen'+count_message+'" value="img|'+src_img+'"/>'+
					'<div class="frame-image">'+
					'<img class="img-fluid pad"src="'+src_img+'" alt="Photo">'+
					'<div class="mark-link v2">'+
					'<a href="#test1" class="link-p-1" title="Action1" style="background: #ccc; opacity:.5"></a>'+
					'<a href="#test2" class="link-p-2" title="Action2" style="background: #aaa; opacity:.5"></a>'+
					'</div>'+
					'</div>'+
				'</li>');
			} else {
				//$('.mark-link').addClass('p4');
				$('.list-sr-chat').append('<li class="result-imgmap messageScreen'+count_message+'">'+
				'<input type="hidden" class="messageScreen'+count_message+'" id="valScreen'+count_message+'" value="img|'+src_img+'"/>'+
					'<div class="frame-image">'+
					'<img class="img-fluid pad"src="'+src_img+'" alt="Photo">'+
					'<div class="mark-link p4">'+
					'<a href="#test1" class="link-p-1" title="Action1" style="background: #ccc; opacity:.5"></a>'+
					'<a href="#test2" class="link-p-2" title="Action2" style="background: #aaa; opacity:.5"></a>'+
					'<a href="#test3" class="link-p-3" title="Action3" style="background: #888; opacity:.5"></a>'+
					'<a href="#test4" class="link-p-4" title="Action4" style="background: #666; opacity:.5"></a>'+
					'</div>'+
					'</div>'+
				'</li>');
			}
		} else {
			if ( count_items >= 3 ) {
				alert('Maximum 3 Message');
			} else {
				alert('Please input image');
			}
		}
	});
	//add carousel
	$('.btn-add-carousel').click(function() {
		function addSlider() {
			  $('.flexslider').flexslider({
				animation: "slide",
				animationLoop: false,
				itemWidth: 210,
				itemMargin: 5,
				minItems: 1,
				maxItems: 3,
				move: 1,
				slideshow: false, 
				start: function(slider){
				  $('body').removeClass('loading');
				}
			  });

			  }
		var myCustomSlide = $("#carouseltitle").val();
		var count_items = 0;
		$("ul.list-sr-chat>li").each(function () {
			count_items++;
		});
		if ( count_items < 3 && myCustomSlide != '' ) {
			count_message++;
			if ( $('#newpush .todo-list').hasClass('show') ) {
				
			} else {
				$('.todo-list,.list-sr-chat,.save-template').addClass('show');
			}
			$('.todo-list').append('<li class="done messageList'+count_message+'">'+
				'<span class="handle ui-sortable-handle">'+
					' <i class="fas fa-ellipsis-v"></i>'+
					' <i class="fas fa-ellipsis-v"></i>'+
				'</span>'+
				'<div class="icheck-primary d-inline ml-2">'+
					' <input type="checkbox" id="chklist'+count_message+'" onclick="checkboxShowHide('+count_message+')" checked>'+
					' <label for="chklist'+count_message+'"></label>'+
				'</div>'+
				'<span class="text">'+myCustomSlide+'</span>'+
				'<small class="badge bg-maroon"><i class="fas fa-columns"></i> Carousel</small>'+
				'<div class="tools">'+
					'<i class="fas fa-edit text-gray" onclick="editMessage('+count_message+')"></i>'+
					'<i class="fas fa-trash" onclick="removeMessage('+count_message+')"></i>'+
				'</div>'+
			'</li>');
			$('.list-sr-chat').append('<li class="result-carousel messageScreen'+count_message+'">'+
										'<div class="direct-chat-msg">'+
											'<div class="direct-chat-img"></div>'+
										 '</div>'+
										'<div class="flexslider carousel">'+
										  '<ul class="slides">'+
											'<li>'+
											  '<a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_cheesecake_brownie.jpg" /></a>'+
											'</li>'+
											'<li>'+
											 ' <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_lemon.jpg" /></a>'+
											'</li>'+
											'<li>'+
											'  <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_donut.jpg" /></a>'+
											'</li>'+
											'<li>'+
											'  <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_caramel.jpg" /></a>'+
											'</li>'+
										  '</ul>'+
										'</div>'+

			'</li>');
			addSlider();
			$("#carouseltitle").val('');
			
			
		} else {
			if ( count_items >= 3 ) {
				alert('Maximum 3 Message');
			} else {
				alert('Please input text');
			}
		}
	});
	//add appointment
	<?php /*?>$('#select-apmNum-action').on('change', function() {

		if ($(this)[0].selectedIndex == 0) { 
			$('#group-a1').show();
			$('#group-a2').hide();
			$('#group-a3').hide();
			
		} else if ($(this)[0].selectedIndex == 1)  {
			$('#group-a1').show();
			$('#group-a2').show();
			$('#group-a3').hide();
		} else if ($(this)[0].selectedIndex == 2)  {
			$('#group-a1').show();
			$('#group-a2').show();
			$('#group-a3').show();
		}
	});
<?php */?>
	
	$('.btn-add-appointment').click(function() {
		var isAction = $('#select-apmNum-action')[0].selectedIndex;
		var myApmTitle = $("#titleApm").val();
		var myApmText = $("#textApm").val();
		var myApmAction1 = $("#actionApm1").val();
		var myApmAction2 = $("#actionApm2").val();
		var myApmAction3 = $("#actionApm3").val();
		
		var count_items = 0;
		$("ul.list-sr-chat>li").each(function () {
			count_items++;
		});
		if ( count_items < 3 && myApmTitle != '' ) {
			count_message++;
			if ( $('#newpush .todo-list').hasClass('show') ) {
				
			} else {
				$('.todo-list,.list-sr-chat,.save-template').addClass('show');
			}
			$('.todo-list').append('<li class="done messageList'+count_message+'">'+
				'<span class="handle ui-sortable-handle">'+
					' <i class="fas fa-ellipsis-v"></i>'+
					' <i class="fas fa-ellipsis-v"></i>'+
				'</span>'+
				'<div class="icheck-primary d-inline ml-2">'+
					' <input type="checkbox" id="chklist'+count_message+'" onclick="checkboxShowHide('+count_message+')" checked>'+
					' <label for="chklist'+count_message+'"></label>'+
				'</div>'+
				'<span class="text">'+myApmTitle+'</span>'+
				'<small class="badge bg-primary"><i class="fas fa-user-clock"></i> Appointment</small>'+
				'<div class="tools">'+
					'<i class="fas fa-edit text-gray" onclick="editMessage('+count_message+')"></i>'+
					'<i class="fas fa-trash" onclick="removeMessage('+count_message+')"></i>'+
				'</div>'+
			'</li>');
			if(isAction == 0) {
			$('.list-sr-chat').append('<li class="result-carousel messageScreen'+count_message+'">'+
				'<div class="direct-chat-msg">'+
					'<div class="direct-chat-img"></div>'+
				 '</div>'+
				'<div class="card-button">'+
				  '<figure class="image">'+
					  '<a href="#listaction"><img src="https://dummyimage.com/500x500/fea3b8/ffffff.png&text=demo" /></a>'+
				  '</figure>'+
					'<div class="detail">'+
					 ' <h2>'+myApmTitle+'</h2>'+
					 ' <p>'+myApmText+'</p>'+
					'</div>'+
					'<ul>'+
					'<li>'+
					'<a href="#Action1">'+myApmAction1+'</a>'+
					'</li>'+
				  '</ul>'+
				'</div>'+
			'</li>');
			} else if(isAction == 1) {
			$('.list-sr-chat').append('<li class="result-carousel messageScreen'+count_message+'">'+
				'<div class="direct-chat-msg">'+
					'<div class="direct-chat-img"></div>'+
				 '</div>'+
				'<div class="card-button">'+
				  '<figure class="image">'+
					  '<a href="#listaction"><img src="https://dummyimage.com/500x500/fea3b8/ffffff.png&text=demo" /></a>'+
				  '</figure>'+
					'<div class="detail">'+
					 ' <h2>'+myApmTitle+'</h2>'+
					 ' <p>'+myApmText+'</p>'+
					'</div>'+
					'<ul>'+
					'<li>'+
					'<a href="#Action1">'+myApmAction1+'</a>'+
					'</li>'+
					'<li>'+
					'<a href="#Action2">'+myApmAction2+'</a>'+
					'</li>'+
				  '</ul>'+
				'</div>'+
			'</li>');
			} else if(isAction == 2) {
			$('.list-sr-chat').append('<li class="result-carousel messageScreen'+count_message+'">'+
				'<div class="direct-chat-msg">'+
					'<div class="direct-chat-img"></div>'+
				 '</div>'+
				'<div class="card-button">'+
				  '<figure class="image">'+
					  '<a href="#listaction"><img src="https://dummyimage.com/500x500/fea3b8/ffffff.png&text=demo" /></a>'+
				  '</figure>'+
					'<div class="detail">'+
					 ' <h2>'+myApmTitle+'</h2>'+
					 ' <p>'+myApmText+'</p>'+
					'</div>'+
					'<ul>'+
					'<li>'+
					'<a href="#Action1">'+myApmAction1+'</a>'+
					'</li>'+
					'<li>'+
					'<a href="#Action2">'+myApmAction2+'</a>'+
					'</li>'+
					'<li>'+
					'<a href="#Action3">'+myApmAction3+'</a>'+
					'</li>'+
				  '</ul>'+
				'</div>'+
			'</li>');
			}

			$("#titleApm").val('');
			$("#textApm").val('');
			
		} else {
			if ( count_items >= 3 ) {
				alert('Maximum 3 Message');
			} else {
				alert('Please input text');
			}
		}
	});
	//add payload
	$('.btn-add-payload').click(function() {
		
		var myCustomText = $("#myCustomText").val();
		var count_items = 0;
		$("ul.list-sr-chat>li").each(function () {
			count_items++;
		});
		if ( count_items < 3 && myCustomText != '' ) {
			count_message++;
			if ( $('#newpush .todo-list').hasClass('show') ) {
				
			} else {
				$('.todo-list,.list-sr-chat,.save-template').addClass('show');
			}
			$('.todo-list').append('<li class="done messageList'+count_message+'">'+
				'<span class="handle ui-sortable-handle">'+
					' <i class="fas fa-ellipsis-v"></i>'+
					' <i class="fas fa-ellipsis-v"></i>'+
				'</span>'+
				'<div class="icheck-primary d-inline ml-2">'+
					' <input type="checkbox" id="chklist'+count_message+'" onclick="checkboxShowHide('+count_message+')" checked>'+
					' <label for="chklist'+count_message+'"></label>'+
				'</div>'+
				'<span class="text">'+myCustomText+'</span>'+
				'<small class="badge badge-info"><i class="fas fa-comment-dots"></i> Payload</small>'+
				'<div class="tools">'+
					'<i class="fas fa-edit text-gray" onclick="editMessage('+count_message+')"></i>'+
					'<i class="fas fa-trash" onclick="removeMessage('+count_message+')"></i>'+
				'</div>'+
			'</li>');
			$('.list-sr-chat').append('<li class="messageScreen'+count_message+'">'+
				'<input type="hidden" class="messageScreen'+count_message+'" id="valScreen'+count_message+'" value="txt|'+myCustomText+'"/>'+
				'<div class="direct-chat-msg">'+
					'<div class="direct-chat-img"></div>'+
					'<div class="direct-chat-text">Custom Message!</div>'+
				'</div>'+
			'</li>');
			$("#myCustomText").val('');
			
		} else {
			if ( count_items >= 3 ) {
				alert('Maximum 3 Message');
			} else {
				alert('Please input text');
			}
		}
	});
	
	  
	  
	$('#message_urlimg').change(function () {
		$('#displayimg').prop('disabled', false);
	});
	// Upload image
	$("#message_img").change(function () {
		var file = $('#message_img')[0].files[0];
		$('#labelimg').find('p').remove();
		$('#labelimg').append('<p>'+file.name+'</p>');
	});
	$('#displayimg').click(function () {
		$('#labelimg').find('p').remove();
		$('#labelimg').append('<p>Choose file</p>');
	});
	$('#uploadimg').click(function () {
		var error_upload = [];
		error_upload[1] = 'Only file type JPG or PNG';
		error_upload[2] = 'File size Maximum 3 MB';
		var url = "https://api.innohub.me/pnp/upload";
		var chk_error = 0;
		var file = $('#message_img')[0].files[0];
		if (file.type != 'image/jpeg' && file.type != 'image/png') {
			chk_error = 1;
		} else if (file.size > 5*1024000) {
			chk_error = 2;
		}
		if (chk_error != 0) {
			alert(error_upload[chk_error]);
			return;
		}
		var reader = new FileReader();
		reader.onload = function(e) {
			$('#temp_image_src').attr('src', e.target.result);
		}
		reader.readAsDataURL(file);
		var formdata = new FormData();
		formdata.append("upload", file);
		$('#displayimg').append(' <i class="fa fa-spinner fa-spin"></i>');
		$.ajax({
			type: 'POST',
			url: url,
			data: formdata,
			success: function(data){
				if ( data.statusdesc == 'SUCCESS' ) {
					$('#displayimg').prop('disabled', false);
					$('#displayimg').find('i').remove();
					$('#temp_image_src').prop('src', data.url);
				}
			},
			cache: false,
			contentType: false,
			processData: false,
			dataType: 'json'
		});
	});
	
	
	
	//live 
	/*$('.call-log').click(function() {
		if ($('#dxy').hasClass( 'open' )) {
			$('#main-log').removeClass('active');
			$('#dxy').removeClass('open');
			$('#dxy').addClass('close');
			$('.call-log').removeClass('active');
		} else {
		   $('#main-log').addClass('active');
		   $('#dxy').toggleClass('open');
		   $('#dxy').removeClass('close');
		   $(this).toggleClass('active');
		}
    });*/
	

		/*toggle save template*/
		$('.save-as').change( function() {
			var isChecked = this.checked;
			if(isChecked) {
				$(".set-name-tp").fadeIn(100);
				$("#tpl-name").prop("disabled",false); 
				$("#tpl-name").prop("required",true); 
			} else {
				$(".set-name-tp").fadeOut(100);
				$("#tpl-name").prop("disabled",true);
				$("#tpl-name").prop("required",false);
			}
		});
		
		//toggle action number
	
		function callToggleNum() {
			$('.select-action-chd').on('change', function() {

				if ($(this)[0].selectedIndex == 0) { 
					$(this).parent().next('.main-apm-action').children('.g-action1').show();
					$(this).parent().next('.main-apm-action').children('.g-action2').hide();
					$(this).parent().next('.main-apm-action').children('.g-action3').hide();

				} else if ($(this)[0].selectedIndex == 1)  {
					$(this).parent().next('.main-apm-action').children('.g-action1').show();
					$(this).parent().next('.main-apm-action').children('.g-action2').show();
					$(this).parent().next('.main-apm-action').children('.g-action3').hide();
				} else if ($(this)[0].selectedIndex == 2)  {
					$(this).parent().next('.main-apm-action').children('.g-action1').show();
					$(this).parent().next('.main-apm-action').children('.g-action2').show();
					$(this).parent().next('.main-apm-action').children('.g-action3').show();
				}
			});
		}
		callToggleNum();

	  
});

</script>


<script type="text/javascript">
$( document ).ready( function () {
	 
      $('.carousel').flexslider({
        animation: "slide",
        animationLoop: false,
        itemWidth: 210,
        itemMargin: 5,
        minItems: 1,
        maxItems: 3,
		move: 1,
        /*start: function(slider){
          $('body').removeClass('loading');
        }*/
      });
	  /*send check type*/
	  $('input[type=radio][name=chk-send]').change(function() {
		if (this.value == 'now') {
			$("#show-schedule").hide();
		}
		else if (this.value == 'set') {
			$("#show-schedule").show();
		}
	});
	//select2
	$(".select2").select2();
	$(".select-box").select2({dropdownAutoWidth : '70',minimumResultsForSearch: -1});
	$('.keep-select-group').select2({
    	placeholder: "Please select",
    	//allowClear: true,
		//tags: true,
		dropdownAutoWidth : true,
		width: '100%'
	});
	/*Groups all*/
	$("#checkAll").click(function(){
		if($("#checkAll").is(':checked') ){
			$("#e1 > option").prop("selected","selected");
			$("#e1").trigger("change");
		}else{
			$("#e1 > option").removeAttr("selected");
			$("#e1").trigger("change");
		 }
	});
	/*users all*/
	$('.master-chk').click(function() {
		if($(this).is(':checked')) {
			$("input[class*='chd-chk']").prop("checked", true);
			$(this).prop("checked", true);
		} else {
			$("input[class*='chd-chk']").prop("checked", false);
			$(this).prop("checked", false);
		}
	});
	$("input[class*='chd-chk']").change( function() {
		$('.master-chk').prop("checked", false);
	});
	/*count remain*/
	//var credit = 3;
	//var count = 1;
	var checkboxes = $('.bubble-chk');
	var remain = $('#countRmainUID').val();
	checkboxes.change(function(){
        var current = checkboxes.filter(':checked').length;
		//alert(current);
		$('#countRmainUID').val(remain - current);
		if(current >= remain) {
		   this.checked = false;
		   checkboxes.filter(':not(:checked)').prop("disabled", true);
		   alert("ขออภัยค่ะเครดิตคุณเหลือ 0 ");
	   }
        //checkboxes.filter(':not(:checked)').prop('disabled', current >= max);
    });
	
	/*$('.bubble-chk').on('change', function(evt) {
	   if($(this).siblings(':checked').length >= remain) {
		   this.checked = false;
		   alert("ขออภัยค่ะเครดิตคุณเหลือ 0 ");
	   }
	   count = $(this).siblings(':checked').length + 1;
	   alert(count);
	   $('#countRmainUID').val(remain - count);
	});*/

});
  </script>
  
  <script>
$( document ).ready( function () {

/*tab carousel*/
$(".cs-flex-tab").on("click", "a", function (e) {
        e.preventDefault();
        if (!$(this).hasClass('add-tab')) {
            $(this).tab('show');
        }
    })
    .on("click", "span", function () {
        var anchor = $(this).siblings('a');
        $(anchor.attr('href')).remove();
        $(this).parent().remove();
        $(".cs-flex-tab li").children('a').first().click();
    });

$('.add-tab').click(function (e) {
    e.preventDefault();
    var id = $(".cs-flex-tab").children().length; //think about it ;)
    var tabId = 'card_' + id;
	/*var startId = 'start: ' + id;
	var settings = { startId , change:false }; 
	alert ( startId );
	alert ( settings );*/
    $(this).closest('li').before('<li class="nav-item"><a class="nav-link" href="#card_' + id + '">Card ' + id + ' </a> <span> x </span></li>');
    //$('.tab-content').append('<div class="tab-pane" id="' + tabId + '">Contact Form: New Contact ' + id + '</div>');
	var clonecard= '<div id="main-card3" class="main-card wrap-upload">';
	clonecard+='<div class="form-group p-0">';
	clonecard+='<label>Select Type Image ' + id + '</label>';
	clonecard+='<select class="custom-select" id="select-card' + id + '-type">';
	clonecard+='<option value="0">URL</option>';
	clonecard+='<option value="1">Local file</option>';
	clonecard+='</select>';
	clonecard+='</div>';

	clonecard+='<div id="f-url_card' + id + '" class="f-url form-group">  ';
	clonecard+='<label for="card_urlimg' + id + '">From image URL</label>';
	clonecard+='<div class="input-group mb-3">';
	clonecard+='<div class="input-group-prepend">';
	clonecard+='<span class="input-group-text"><i class="fas fa-link"></i></span>';
	clonecard+='</div>';
	clonecard+='<input type="text" class="form-control" placeholder="URL Image" id="card_urlimg' + id + '">';
	clonecard+='</div>';
	clonecard+='</div>';

	clonecard+='<div id="f-upload_card' + id + '" class="f-upload form-group" style="display: none">';
	clonecard+='<label for="card_img' + id + '-1">Upload image</label>';
	clonecard+='<div class="input-group">';
	clonecard+='<div class="custom-file">';
	clonecard+='<input type="file" class="card-file-input' + id + '" id="card_img' + id + '" accept=".jpg,.jpeg,.png">';
	clonecard+='<img id="temp_card' + id + '_src" hidden/>';
	clonecard+='<label class="custom-file-label" id="card_labelimg' + id + '" for="card_img' + id + '"><p>Choose file</p></label>';
	clonecard+='</div>';
	clonecard+='<div class="input-group-append">';
	clonecard+='<button class="input-group-text" id="card_uploadimg' + id + '">Upload</button>';
	clonecard+='</div>';

	clonecard+='</div>';
	clonecard+='</div>';
	clonecard+='</div>';
	clonecard+='<div class="form-group p-2 bg-light rounded">';
	clonecard+='<label>Link URL</label>';
	clonecard+='<div class="input-group">';
	clonecard+='<input type="text" class="form-control" placeholder="Action URL">';
	clonecard+='<div class="input-group-append">';
	clonecard+='<span class="input-group-text"><i class="fas fa-link"></i></span>';
	clonecard+='</div>';
	clonecard+='</div>';
	clonecard+='</div>';
	//write
	$('#vert-tabs-flex .tab-content').append('<div class="tab-pane" id="' + tabId + '">' + clonecard + '</div>');
	$('#custom-carousel-tab').idTabs(); 
    $('#custom-carousel-tab li:nth-child(' + id + ') a').click();
});

});
</script>
  

<script>
$( function() {
	
    //$( "#calendarshow" ).datepicker();

		$( "#datepicker" ).datepicker({
		  showOtherMonths: true,
		  selectOtherMonths: true
		});
	//set time	
	var timepicker = new TimePicker('time', {
  lang: 'en',
  theme: 'dark'
});
timepicker.on('change', function(evt) {
  
  var value = (evt.hour || '00') + ':' + (evt.minute || '00');
  evt.element.value = value;

});

  } );
</script>
<!-- /js -->

</body>
</html>
