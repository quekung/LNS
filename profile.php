<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top-web.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden">
<!-- Headbar -->
<?php include("incs/header-v2.html") ?>

<!-- /Headbar -->
<div id="app" class="container view-atn">
    <div class="close-page"><a href="javascript:;" onclick="clkExit()" title="close">ปิด</a></div>
    
    <div id="app-main">
			
		
        	<form class="form-keep bx-keep form-checkout" method="post" action="#">
				<fieldset>
					
					<!-- box-->
					<div class="box-san">
							<!--<div class="head-title">
							<h2 class="hd">My Profile <a href="#" title="edit"><i class="ic-edit"></i></a></h2>
							</div>-->
							<div class="cover pd0 d-flex between-xs midle-xs">
								<h3 class="head txt-l">My Profile </h3>
								<span class="d-flex middle-xs pr10-xs"><a href="javascript:;"  data-fancybox="" data-modal ="modal" data-src="#modal-edit-user" class="t-white"><i class="fas fa-edit"></i> Edit</a></span>
							</div>
							<div class="body">
					
					<div class="user full">
						<img src="di/avatar-new.png" alt="Avatar">
						<div>
							<p><b>Name : </b> Penny</p>
							<p><b>Surname : </b> Lane</p>
							<p><b>E-mail : </b> Username@mail.com</p>
							<p><b>Role : </b> Admin</p>
						</div>
					</div>
					<?php /*?><div class="wr">
						<input type="text" class="txt-box" id="group_name" name="group_name" required="">
						<label for="group_name">Group Name</label>
						<span class="line"></span>
					</div><?php */?>
					
					<div id="tabs">
						<ul class="idTabs">
							<li><a href="#tabs-1" class="selected">Groups</a></li>
							<li><a href="#tabs-2">History</a></li>

						</ul>
						<div id="tabs-1" class="bx-tab">
							<div class="box-result list-group">
								<ul>
											<li class="_flex between-xs middle-xs">
												<!--<a href="javascript:;" data-fancybox="" data-src="#onOffBot" title="Remove bots from the group">-->
													<p>Department : <span class="f-normal t-black">Platform</span></p>
													<label class="toggle">
													  <span class="hidden-xs hidden-sm toggle-label">Active ON</span>
													  <input class="toggle-checkbox" type="checkbox" name="status" checked="">
													  <div class="toggle-switch"></div>
												 	</label>
												<!--</a>-->
											</li>
											<li class="_flex between-xs middle-xs">
													<p>Department : <span class="f-normal t-black">ML</span></p>
													<label class="toggle">
													  <span class="hidden-xs hidden-sm toggle-label">Active ON</span>
													  <input class="toggle-checkbox" type="checkbox" name="status">
													  <div class="toggle-switch"></div>
												 	</label>
											</li>
											<!--<li class="_flex between-xs middle-xs">
													group C
													<label class="toggle">
													  <span class="hidden-xs hidden-sm toggle-label">BOT ON</span>
													  <input class="toggle-checkbox" type="checkbox" name="status" checked="">
													  <div class="toggle-switch"></div>
												 	</label>
											</li>-->

										</ul>
								<?php /*?><ul>
									<li>
										<a href="javascript:;" data-fancybox="" data-src="#leaveGroup" title="Remove bots from the group">
											group A
											<i class="ic-del"></i>
										</a>
									</li>
									<li>
										<a href="javascript:;" data-fancybox="" data-src="#leaveGroup" title="Remove bots from the group">
											group B
											<i class="ic-del"></i>
										</a>
									</li>
									<li>
										<a href="javascript:;" data-fancybox="" data-src="#leaveGroup" title="Remove bots from the group">
											group C
											<i class="ic-del"></i>
										</a>
									</li>

								</ul><?php */?>
							</div>
						</div>
						<div id="tabs-2" class="bx-tab" style="display: none">
							<div class="box-result list-group-leave">
								<ul>
									<li>
										<a href="javascript:;">
											<span>group X</span>
											<small>Left - 21/08/2019  12:08</small>
										</a>
									</li>
									<li>
										<a href="javascript:;">
											<span>group Y</span>
											<small>Left - 11/08/2019  16:53</small>
										</a>
									</li>
									<li>
										<a href="javascript:;">
											<span>group Z</span>
											<small>Left - 31/07/2019  24:00</small>
										</a>
									</li>

								</ul>
							</div>
						</div>
					</div>
					
					
					
					
					</div>
					</div>
					<!-- /box -->
				</fieldset>
			</form>
	
    </div>
</div>


<div class="popup thm-lite" id="modal-edit-user">
				<div class="box-middle">
				  <div class="modal-content">
					<div class="modal-header">
					  <h2 class="modal-title t-black mb20-xs">Edit Member</h2>
					</div>
					<div class="modal-body txt-l">
					  
					  <div class="form-group p-2 bg-light rounded" data-select2-id="72">

							<div class="row">
								<div class="col-sm-6">
									  <div class="form-group">
										<label for="u-Email1">Email address</label>
										<input type="email" class="form-control" id="u-Email1" value="Username@mail.com">
									  </div>
									   <div class="form-group">
										<label for="u-user1">Username</label>
										<input type="text" class="form-control" id="u-user1" value="innohub1">
									  </div>

									  <div class="form-group">
										<label for="u-pass">Password</label>

										<div class="input-group">
										  <input type="password" class="form-control" id="u-pass" value="123456">
										  <span class="input-group-append">
											<button type="button" class="btn bg-black pl5-xs pr5-xs border-0">Reset</button>
										  </span>
										</div>
									  </div>
								</div>

								<div class="col-sm-6">
									  <div class="form-group">
										<label for="u-name">Display Name</label>
										<input type="text" class="form-control" id="exampleInputEmail1" value="Alexander Pierce">
									  </div>

									  <div class="form-group">
										<label for="exampleInputEmail1">Role</label>
										<div class="btn-group btn-group-toggle w-100" data-toggle="buttons">
										  <label class="btn bg-teal active">
											<input type="radio" name="options" id="option1" autocomplete="off" checked> Admin
										  </label>
										  <label class="btn bg-teal">
											<input type="radio" name="options" id="option2" autocomplete="off"> Agent
										  </label>
										  <label class="btn bg-teal">
											<input type="radio" name="options" id="option3" autocomplete="off"> Viewer
										  </label>
										</div>
									  </div>
									  
									  <div id="f-upload_avatar" class="f-upload form-group" style="">
										<label for="apm_img2">Upload Avatar</label>
										<div class="input-group">
												<div class="custom-file">
													<input type="file" class="custom-file-input2" id="apm_img2" accept=".jpg,.jpeg,.png">
													<img id="temp_apmimage_src" hidden="">
													<label class="custom-file-label" id="apm_labelimg2" for="apm_img2"><p>Choose file</p></label>
												</div>
												<div class="input-group-append">
													<button class="input-group-text" id="apm_uploadimg2">Upload</button>
												</div>

										</div>
									 </div>
									  
									  <div class="form-group">
										<label for="gender">Gender</label>
										<div class="btn-group btn-group-toggle w-100" data-toggle="buttons">
										  <label class="btn bg-teal active">
											<input type="radio" name="gender" id="male" autocomplete="off" checked> Male
										  </label>
										  <label class="btn bg-teal">
											<input type="radio" name="gender" id="female" autocomplete="off"> Female
										  </label>
										</div>
									  </div>
								</div>
							</div>
						</div>
					  
					</div>
					<div class="modal-footer justify-content-between">
					  <button type="button" data-fancybox-close="" class="ui-btn-gray btn-sm" title="Close" href="javascript:;" onclick="parent.jQuery.fancybox.close();">Cancel</button>
					  <button type="button" data-fancybox-close="" class="ui-btn-green2 btn-sm" title="Close" href="javascript:;" onclick="parent.jQuery.fancybox.close();">Save</button>
					</div>
				  </div>
				  <!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			  </div>
			  <!-- /.modal -->



<div id="skin-loading" class="bg-wh" onclick="$(this).fadeOut();">
	<div class="lds-hourglass"></div>
</div>
<script>
	window.setTimeout(function(){
		$('#skin-loading').fadeOut();
	}, 3000);
</script>
<!-- footer -->
<?php include("incs/footer-web.html") ?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js-web.html") ?>
<script src="js/jquery.idTabs.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
<script>
//Dropdown plugin data
$(document).ready(function() {
     $('.select2').select2({
    	placeholder: "Please select",
    	//allowClear: true,
		minimumResultsForSearch: -1,
		dropdownAutoWidth : true,
		width: '100%'
	});

	/*toggle radio*/
	$(".btn-group-toggle input[type=radio]").on("click", function() {
		
		if ($(this).parents('.btn').hasClass( 'active' )) {
		} else {
			$(this).parents(".btn-group-toggle").find('.btn').removeClass('active');
			$(this).parents('.btn').addClass('active');
		}
		

	})
	
});
</script>
<!-- /js -->

</body>
</html>
