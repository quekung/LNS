<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top-web.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden">
<!-- Headbar -->
<?php include("incs/header-v2.html") ?>
<script>
$(".main-menu .list-unstyled>li.active").removeClass('active');
$(".main-menu .list-unstyled>li:nth-child(4)").addClass('active');
</script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<!-- /Headbar -->
<div class="page-checkout">

    
    <div id="toc">
		<section class="z-broadcast _self-pt0 mb0">
			<div class="bx-stepbar _self-pv20 cb-af container">
				<ul class="tabsbar">
						  <li><a href="e-hr-dashboard.php" title="Send Message" class="selected"><i class="fas fa-bullhorn"></i> <span>Dashboard</span></a></li>
						  <li><a href="e-hr.php" title="Create Message"><i class="fas fa-users"></i> <span>Employee</span></a></li>
						  <li><a href="e-hr-employee.php" title="User Detail"><i class="fas fa-users-cog"></i> <span>Import User</span></a></li>
						  <li><a href="e-hr-setting.php" title="Message Setting"><i class="fas fa-sliders-h"></i> <span>Message Setting</span></a></li>
						  <li><a href="e-hr-report.php" title="Report"><i class="fas fa-file-medical-alt"></i> <span>Report</span></a></li>
				  </ul>
			</div>
					

			
			<div class="bg-gray2 contentTabs">
				<div id="tbc-1" class="msg">
					<div class="head-bg">
					<div class="container">
						<h2>E-HR</h2>	
					</div>
					</div>

					<form method="post" class="form-checkout form-sending'">
					
					<div class="wrap-full _chd-cl-xs-12 _chd-cl-sm">
						<div class="main row center-xs">
							<div class="container">
							
								<!--<div class="sort-bar d-flex between-xs middle-xs">
									<div class="sort">
										<select class="form-control select2" data-placeholder="Search by" style="width:auto">
										  <option></option>
										  <option>ชื่อ-นามสกุล</option>
										  <option>ฝ่าย</option>
										  <option>แผนก</option>
										  <option>ตำแหน่ง</option>
										</select>
										<div class="search-sm d-inline">
											<input class="txt-box" placeholder="ค้นหา...">
											<button type="submit" class="fas fa-search" aria-hidden="true"></button>
										</div>
									</div>
									<div class="right">
										<a class="ui-btn-black btn-xs" data-fancybox="" data-modal="modal" data-src="#modal-add-user" title="Add new">Add New</a>
									</div>

								</div>-->
							
								<!-- row -->
								<div class="row">
									<div class="col-xs-12 mb20-xs">
										<div class="card bg-white rounded mt20-xs pa10-xs border-0">
										  <div class="card-header border-0">
											<div class="d-flex between-xs">
											  <h3 class="card-title f-normal">ภาพรวม ขาด, ลา, มาสาย งาน แต่ละเดือน</h3>
											  <a class="text-sm t-blue" href="javascript:void(0);">View Report</a>
											</div>
										  </div>
										  <div class="card-body">
	
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawVisualization);

      function drawVisualization() {
        // Some raw data (not necessarily accurate)
        var data = google.visualization.arrayToDataTable([
          ['Month', 'Absences', 'Leave', 'Late'],
          ['เม.ย.',  165,      938,         522],
          ['พ.ค.',  135,      1120,        599],
          ['มิ.ย.',  157,      1167,        587],
          ['ก.ค.',  139,      1110,        615],
          ['ส.ค.',  136,      691,         629]
        ]);

        var options = {
          //title : 'ภาพรวม ขาด, ลา, มาสาย งาน แต่ละเดือน',
          vAxis: {title: 'Employee'},
          hAxis: {title: 'Month'},
          seriesType: 'bars',
          series: {5: {type: 'line'}}        };

        var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>

										  
										  	<div id="chart_div" style="width: 1150px; height: 500px;"></div>

										  </div>
										 </div>
									</div>

									
									<div class="col-lg-6">
										<div class="card bg-white rounded mt20-xs pa10-xs border-0">
										  <div class="card-header border-0">
											<div class="d-flex between-xs">
											  <h3 class="card-title">INNOHUB Company</h3>
											  <a class="text-sm t-blue" href="javascript:void(0);">View Report</a>
											</div>
										  </div>
										  <div class="card-body">
											<div class="d-flex pt20-xs pb20-xs">
											  	<div class="col-xs-4">
													<div>
													<input type="text" class="knob" data-readonly="true" value="30" data-fgColor="#0088bd" data-width="120" data-height="120" data-thickness="0.1" data-linecap="round">
													<h2 class="text-sm f-bold">Absences</h2>
													</div>
													<div class="score">
														<i class="ion ion-ios-man" size="large"></i>
														<span>120 ช.ม.</span>
													</div>
												</div>
												
												<div class="col-xs-4">
													<div>
													<input type="text" class="knob" data-readonly="true" value="7" data-fgColor="#ff7a6b" data-width="120" data-height="120" data-thickness="0.1" data-linecap="round">
													<h2 class="text-sm f-bold">Leave</h2>
													</div>
													<div class="score">
														<i class="ion ion-ios-woman" size="large"></i>
														<span>7 วัน</span>
													</div>
												</div>
												
												<div class="col-xs-4">
													<div>
													<input type="text" class="knob" data-readonly="true" value="5" data-fgColor="#bf6bff" data-width="120" data-height="120" data-thickness="0.1" data-linecap="round">
													<h2 class="text-sm f-bold">Late</h2>
													</div>
													<div class="score">
														<i class="ion ion-ios-transgender" size="large"></i>
														<span>5 ช.ม.</span>
													</div>
												</div>
												
												
											</div>
											<!-- /.d-flex -->

										

											
										  </div>
										</div>
										
										<div class="card bg-white rounded mt20-xs pa10-xs border-0">
										  <div class="card-header border-0">
											<div class="d-flex between-xs">
											  <h3 class="card-title">OTO Company</h3>
											  <a class="text-sm t-blue" href="javascript:void(0);">View Report</a>
											</div>
										  </div>
										  <div class="card-body">
											<div class="d-flex pt20-xs pb20-xs">
											  	<div class="col-xs-4">
													<div>
													<input type="text" class="knob" data-readonly="true" value="50" data-fgColor="#0088bd" data-width="120" data-height="120" data-thickness="0.1" data-linecap="round">
													<h2 class="text-sm f-bold">Absences</h2>
													</div>
													<div class="score">
														<i class="ion ion-ios-man" size="large"></i>
														<span>160 ช.ม.</span>
													</div>
												</div>
												
												<div class="col-xs-4">
													<div>
													<input type="text" class="knob" data-readonly="true" value="24" data-fgColor="#ff7a6b" data-width="120" data-height="120" data-thickness="0.1" data-linecap="round">
													<h2 class="text-sm f-bold">Leave</h2>
													</div>
													<div class="score">
														<i class="ion ion-ios-woman" size="large"></i>
														<span>24 วัน</span>
													</div>
												</div>
												
												<div class="col-xs-4">
													<div>
													<input type="text" class="knob" data-readonly="true" value="31" data-fgColor="#bf6bff" data-width="120" data-height="120" data-thickness="0.1" data-linecap="round">
													<h2 class="text-sm f-bold">Late</h2>
													</div>
													<div class="score">
														<i class="ion ion-ios-transgender" size="large"></i>
														<span>31 ช.ม.</span>
													</div>
												</div>
												
												
											</div>
											<!-- /.d-flex -->

										

											
										  </div>
										</div>
										<!-- /.card -->
										</div>
										
										<div class="col-lg-6">
										<div class="card bg-white rounded mt20-xs pa10-xs border-0">
										  <div class="card-header border-0">
											<h3 class="card-title">Employee performance summary</h3>
										  </div>
										  <div class="card-body">
											<div class="d-flex between-xs middle-xs pt10-xs pb10-xs border-bottom mb-3">
											  <p class="text-success text-xl mb0-xs">
												<i class="fas fa-user-slash fa-2x"></i>
											  </p>
											  <p class="d-flex flex-column txt-r mb0-xs">
												<span class="f-bold">
												  <i class="fas fa-arrow-alt-circle-up text-success"></i> 12%
												</span>
												<span class="text-muted">Absences</span>
											  </p>
											</div>
											<!-- /.d-flex -->
											<div class="d-flex between-xs middle-xs pt10-xs pb10-xs border-bottom mb-3">
											  <p class="t-orange text-xl mb0-xs">
												<i class="fas fa-hospital-user fa-2x"></i>
											  </p>
											  <p class="d-flex flex-column txt-r mb0-xs">
												<span class="font-weight-bold">
												  <i class="fas fa-arrow-alt-circle-up t-orange"></i> 0.8%
												</span>
												<span class="text-muted">Sick leave</span>
											  </p>
											</div>
											<!-- /.d-flex -->
											<div class="d-flex between-xs middle-xs pt10-xs pb10-xs mb-0">
											  <p class="text-danger text-xl mb0-xs">
												<i class="fas fa-user-clock fa-2x"></i>
											  </p>
											  <p class="d-flex flex-column txt-r mb0-xs">
												<span class="font-weight-bold">
												  <i class="fas fa-arrow-alt-circle-down text-danger"></i> 1%
												</span>
												<span class="text-muted">LATE</span>
											  </p>
											</div>
											<!-- /.d-flex -->
										  </div>
										</div>
										
										<div class="card bg-white rounded mt20-xs pa10-xs border-0">
										  <div class="card-header border-0">
											<h3 class="card-title">Employee performance summary</h3>
										  </div>
										  <div class="card-body">
	<script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Department', 'Late'],
          ['ไอที',  20],
          ['ภาษี',  2],
          ['บุคคล',  8],
          ['การเงิน', 5],
		  ['เซลล์',  30],
		  ['บัญชี',  10],
        ]);

        var options = {
		  vAxis: {title: 'Department'},
          hAxis: {title: 'Late'},
          title: 'Company Performance',
          curveType: 'function',
          legend: { position: 'bottom' }
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

        chart.draw(data, options);
      }
    </script>

											<div id="curve_chart" style="width:100%; height: 220px"></div>

										  </div>
										</div>
									  </div>
								</div>
								<!-- /row -->
							</div>

						</div>
					</div>
				</div>
					</form>

			</div>
			
			
			
			
		</section>
    </div>
</div>
	

<!--<div id="skin-loading" class="bg-wh" onclick="$(this).fadeOut();">
	<div class="lds-hourglass"></div>
</div>-->
<script>
	window.setTimeout(function(){
		$('#skin-loading').fadeOut();
	}, 3000);
</script>

<!-- footer -->
<?php include("incs/footer-web.html") ?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js-web.html") ?>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<link href="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.css" rel="stylesheet"/>
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<script src="https://www.beurguide.net/assets/AdminLTE/bower_components/jquery-knob/dist/jquery.knob.min.js"></script>

<script type="text/javascript">
$( document ).ready( function () {
	 

	//select2
	$(".select2").select2({minimumResultsForSearch: -1});
	
	$(".js-example-tags").select2({
	   //dropdownAutoWidth : true,
	   placeholder: "Select",
	  width: '100%',
	  tags: true,
	  insertTag: function (data, tag) {
		// Insert the tag at the end of the results
		data.push(tag);
	  }
	});
	

});
  </script>
  
  <script language="JavaScript">
<!-- //
$(function(){
  /* jQueryKnob */
  $('.knob').knob({
    'format' : function (value) { return value + '%'; }
  });


  var total = 200;
    // Donut Chart
  Morris.Donut({
    element  : 'donut-chart',
    resize   : true,
    colors   : ['#3c8dbc', '#f56954', '#00a65a', '#FE9901', '#BF6BFF'],
    data     : [
      { label: 'Age 18-24', value: 27 },
      { label: 'Age 25-34', value: 95 },
      { label: 'Age 35-44', value: 45 },
      { label: 'Age 45-54', value: 22 },
      { label: 'Age 55+', value: 11 }
    ],
    hideHover: 'auto',
    formatter: function (value, data) { return value+" ("+(value/total*100) + '%)'; }
  });

  // jvectormap data
  var visitorsData = {"US":398,"DE":500,"FR":760,"CN":1000,"AU":700,"GB":320,"TH":3000};
  
});
// -->
</script>
  


<!-- /js -->

</body>
</html>
