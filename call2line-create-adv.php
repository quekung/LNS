<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top-web.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden">
<!-- Headbar -->
<?php include("incs/header-v2.html") ?>
<script>
$(".main-menu .list-unstyled>li.active").removeClass('active');
$(".main-menu .list-unstyled>li:nth-child(3)").addClass('active');
</script>
<!-- /Headbar -->
<div class="page-checkout">

    
    <div id="toc">
		<section class="z-broadcast _self-pt0 mb0">
			<div class="bx-stepbar _self-pv20 cb-af container">
				<ul class="tabsbar">
						  <li><a href="call2line.php" title="Send Message"><i class="fas fa-bullhorn"></i> <span>Send Message</span></a></li>
						  <li><a href="call2line-create.php" title="Create Message" class="selected"><i class="fas fa-layer-group"></i> <span>Create Template</span></a></li>
						  <li><a href="call2line-setting.php" title="Message Setting"><i class="fas fa-sliders-h"></i> <span>Message Setting</span></a></li>
						  <li><a href="call2line-report.php" title="Report"><i class="fas fa-file-medical-alt"></i> <span>Report</span></a></li>
				  </ul>
			</div>
					

			
			<div class="bg-gray2 contentTabs">
				<div id="tbc-1" class="msg">
					<form method="post" class="form-checkout form-sending'" action="call2line.php">
					<div class="head-title container txt-c">
						<h2 class="t-black"><i class="fas fa-layer-group _self-mr10 t-black"></i>Create Template</h2>	
						<p>ระบบส่งข้อความผ่านทาง line</p>
					</div>
					
					<div class="set-name-tpl bg-gray3 pa20-sm pa10-xs">
						  <div class="input-group mb0-xs d-flex middle-xs">
							<h5 class="card-title m-0 input-group-prepend middle-xs f-normal"><span class="step ui-btn-green2 _self-ph10-mr10">Edit</span> <i class="nav-icon fas fa-layer-group _self-mr10"></i>  Template : </h5>
							<div class="col pl10-xs"><input type="text" class="w-100 txt-box form-control bg-white" id="template-title" placeholder="Template Name" autofocus="" value="Quick Menu" readonly></div>
							<div class="input-group-append">
								<span class="input-group-text" style="cursor: pointer"><i class="fas fa-lock"></i></span>
							  </div>
							  
							  <h5 class="card-title m-0 input-group-prepend middle-xs f-normal pl10-xs pr10-xs"><i class="nav-icon fas fa-globe-asia pr5-xs"></i>  Language : </h5>
								<div class="col-2">
								<select class="custom-select custom-select-lg" id="select-lang-template">
									  <option value="0" selected="">TH</option>
									  <option value="1">EN</option>
								  </select>
								</div>
						  </div>
					</div>
						<!-- card -->
						<div class="card border-top bg-white">
			
							<div class="card-body _self-ph20">

							<div class="wrap-full _chd-cl-xs-12 _chd-cl-sm">
								<div class="main row _chd-cl-xs-12 _chd-cl-sm">
									<div class="col-l ps-rlt _self-cl-sm-06-md-07">
										<!-- Left -->
										<div id="newpush" class="bg-white ps-rlt _self-pa10 cb-af rounded1">
										<!-- Tab -->
										<ul class="msg-tab idTabs bg-light">
											<li class="nav-item">
											<a class="selected" id="vert-tabs-text-tab" href="#vert-tabs-text">
												<span class="btn bg-info mb-0 mr-1"><i class="fas fa-text-height"></i></span>
												<div>Text</div></a>
											</li>


										  <li class="nav-item">
											  <a class="nav-link" id="vert-tabs-qreply-tab" href="#vert-tabs-qreply">
												<span class="btn bg-purple mb-0 mr-1"><i class="fas fa-reply"></i></span>
												<div>Reply</div></a>
										  </li>

										  <li class="nav-item">
											  <a class="nav-link" id="vert-tabs-button-tab" href="#vert-tabs-button">
												<span class="btn bg-primary mb-0 mr-1"><i class="fas fa-user-clock"></i></span>
												<div>Consent</div></a>
										  </li>

										  <li class="nav-item m-adv">
											<button type="button" class="bg-gray btn-sm dropdown-toggle">
												  <i class="fas fa-ellipsis-v"></i></button>
												<a class="nav-link" id="vert-tabs-payload-tab" href="#vert-tabs-payload" aria-controls="vert-tabs-payload" aria-selected="false">
														<span class="btn bg-olive mb-0 mr-1"><i class="fas fa-comment-dots"></i></span>
														Payload</a>
										  </li>
										</ul>


										<div class="tab-content" id="vert-tabs-tabContent">
										  <div class="tab-pane show" id="vert-tabs-text" >

											<p class="lead mb-0">Properties</p>
											 <div class="tab-custom-content">
																			  <!-- textarea -->
												  <div class="form-group">
													<label for="message_text">Your Message</label>
													<textarea class="form-control" id="message_text" rows="3" placeholder="Enter ..."></textarea>
													<textarea class="form-control" id="message_edittext" rows="3" placeholder="Enter ..." style="display: none;"></textarea>
												  </div>
												  <!-- Link -->

											</div>

											<div class="tab-footer clearfix">
												<button type="button" id="displaytxt" class="ui-btn  btn-info btn-lg float-right btn-add-msg">Display</button>
											</div>
										  </div>

										  <!-- Quick reply -->
										  <div class="tab-pane fade" id="vert-tabs-qreply" role="tabpanel" aria-labelledby="vert-tabs-qreply-tab">

											<p class="lead mb-0">Properties</p>
											 <div class="tab-custom-content">
												  <!-- textarea -->
												  <div class="form-group">
													<label for="message_text">Your Message</label>
													<textarea class="form-control" id="message_replay_text" rows="3" placeholder="Enter ..."></textarea>
													<!--<textarea class="form-control" id="message_replay_edittext" rows="3" placeholder="Enter ..." style="display: none;"></textarea>-->
												  </div>
											</div>

											<div class="form-group">
													<label>Number of Actions</label>
													<select class="form-control" id="select-replay-action">
													  <option value="0">1</option>
													  <option value="1">2</option>
													  <option value="2">3</option>
													  <option value="3">4</option>
													  <option value="4">5</option>
													  <option value="5">6</option>
													</select>
												</div>
												<div class="main-reply-action bg-light pa10-xs mb10-xs">
																					<div id="replay-action1">
														<div class="form-group">
															<label>Reply Action1</label>
															<div class="row middle-xs mb10-xs">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Type</span>
																<div class="col">
																	<select class="form-control" id="select-tpye-action1">
																	  <option value="0">Select Action</option>
																	  <option value="1" selected>Text</option>
																	  <option value="2">Location</option>
																	  <option value="3">Camera</option>
																	  <!--<option value="4">Video</option>
																	  <option value="5">Audio</option>
																	  <option value="6">Location</option>
																	  <!--<option value="7">Imagemap</option>
																	  <option value="8">Template</option>-->
																	</select>
																</div>
															</div>

															<div class="row middle-xs mb10-xs">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Label</span>
																<div class="col">
																	<input type="text" class="form-control" id="labelQreply1" placeholder="Action 1">
																</div>
															</div>


															<div class="row middle-xs mb10-xs">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Action</span>
																<div class="col">
																	<input type="text" class="form-control" id="actionQreply1" placeholder="Action 1">
																</div>
															</div>
														</div>
													</div>
																					<div id="replay-action2">
														<div class="form-group">
															<label>Reply Action2</label>
															<div class="row middle-xs mb10-xs">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Type</span>
																<div class="col">
																	<select class="form-control" id="select-tpye-action2">
																	  <option value="0">Select Action</option>
																	  <option value="1" selected>Text</option>
																	  <option value="2">Location</option>
																	  <option value="3">Camera</option>
																	  <!--<option value="4">Video</option>
																	  <option value="5">Audio</option>
																	  <option value="6">Location</option>
																	  <!--<option value="7">Imagemap</option>
																	  <option value="8">Template</option>-->
																	</select>
																</div>
															</div>

															<div class="row middle-xs mb10-xs">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Label</span>
																<div class="col">
																	<input type="text" class="form-control" id="labelQreply2" placeholder="Action 2">
																</div>
															</div>


															<div class="row middle-xs mb10-xs">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Action</span>
																<div class="col">
																	<input type="text" class="form-control" id="actionQreply2" placeholder="Action 2">
																</div>
															</div>
														</div>
													</div>
																					<div id="replay-action3">
														<div class="form-group">
															<label>Reply Action3</label>
															<div class="row middle-xs mb10-xs">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Type</span>
																<div class="col">
																	<select class="form-control" id="select-tpye-action3">
																	  <option value="0">Select Action</option>
																	  <option value="1" selected>Text</option>
																	  <option value="2">Location</option>
																	  <option value="3">Camera</option>
																	  <!--<option value="4">Video</option>
																	  <option value="5">Audio</option>
																	  <option value="6">Location</option>
																	  <!--<option value="7">Imagemap</option>
																	  <option value="8">Template</option>-->
																	</select>
																</div>
															</div>

															<div class="row middle-xs mb10-xs">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Label</span>
																<div class="col">
																	<input type="text" class="form-control" id="labelQreply3" placeholder="Action 3">
																</div>
															</div>


															<div class="row middle-xs mb10-xs">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Action</span>
																<div class="col">
																	<input type="text" class="form-control" id="actionQreply3" placeholder="Action 3">
																</div>
															</div>
														</div>
													</div>
																					<div id="replay-action4">
														<div class="form-group">
															<label>Reply Action4</label>
															<div class="row middle-xs mb10-xs">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Type</span>
																<div class="col">
																	<select class="form-control" id="select-tpye-action4">
																	  <option value="0">Select Action</option>
																	  <option value="1" selected>Text</option>
																	  <option value="2">Location</option>
																	  <option value="3">Camera</option>
																	  <!--<option value="4">Video</option>
																	  <option value="5">Audio</option>
																	  <option value="6">Location</option>
																	  <!--<option value="7">Imagemap</option>
																	  <option value="8">Template</option>-->
																	</select>
																</div>
															</div>

															<div class="row middle-xs mb10-xs">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Label</span>
																<div class="col">
																	<input type="text" class="form-control" id="labelQreply4" placeholder="Action 4">
																</div>
															</div>


															<div class="row middle-xs mb10-xs">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Action</span>
																<div class="col">
																	<input type="text" class="form-control" id="actionQreply4" placeholder="Action 4">
																</div>
															</div>
														</div>
													</div>
																					<div id="replay-action5">
														<div class="form-group">
															<label>Reply Action5</label>
															<div class="row middle-xs mb10-xs">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Type</span>
																<div class="col">
																	<select class="form-control" id="select-tpye-action5">
																	  <option value="0">Select Action</option>
																	  <option value="1" selected>Text</option>
																	  <option value="2">Location</option>
																	  <option value="3">Camera</option>
																	  <!--<option value="4">Video</option>
																	  <option value="5">Audio</option>
																	  <option value="6">Location</option>
																	  <!--<option value="7">Imagemap</option>
																	  <option value="8">Template</option>-->
																	</select>
																</div>
															</div>

															<div class="row middle-xs mb10-xs">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Label</span>
																<div class="col">
																	<input type="text" class="form-control" id="labelQreply5" placeholder="Action 5">
																</div>
															</div>


															<div class="row middle-xs mb10-xs">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Action</span>
																<div class="col">
																	<input type="text" class="form-control" id="actionQreply5" placeholder="Action 5">
																</div>
															</div>
														</div>
													</div>
																					<div id="replay-action6">
														<div class="form-group">
															<label>Reply Action6</label>
															<div class="row middle-xs mb10-xs">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Type</span>
																<div class="col">
																	<select class="form-control" id="select-tpye-action6">
																	  <option value="0">Select Action</option>
																	  <option value="1" selected>Text</option>
																	  <option value="2">Location</option>
																	  <option value="3">Camera</option>
																	  <!--<option value="4">Video</option>
																	  <option value="5">Audio</option>
																	  <option value="6">Location</option>
																	  <!--<option value="7">Imagemap</option>
																	  <option value="8">Template</option>-->
																	</select>
																</div>
															</div>

															<div class="row middle-xs mb10-xs">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Label</span>
																<div class="col">
																	<input type="text" class="form-control" id="labelQreply6" placeholder="Action 6">
																</div>
															</div>


															<div class="row middle-xs mb10-xs">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Action</span>
																<div class="col">
																	<input type="text" class="form-control" id="actionQreply6" placeholder="Action 6">
																</div>
															</div>
														</div>
													</div>
																			</div>

											<div class="tab-footer clearfix">
												<button type="button" id="display-Qreply" class="ui-btn  btn-info btn-lg float-right btn-add-qreply">Display</button>
											</div>
										  </div>

										  <div class="tab-pane fade" id="vert-tabs-button">

											<p class="lead mb-0">Properties</p>
						 <div class="tab-custom-content">
						 
						 	<div id="main-apm" class="main-imagemap wrap-upload mb-4">
								<div class="form-group p-0 mb10-xs">
									<label>Select Type Image</label>
									<select class="custom-select" id="select-apmimage-type">
									  <option value="0">URL</option>
									  <option value="1">Local file</option>
									</select>
								 </div>

								<div id="f-url_apm" class="f-url form-group">  
									<label for="apm_urlimg2">From image URL</label>
									<div class="input-group mb-3">
									  <div class="input-group-prepend">
										<span class="input-group-text"><i class="fas fa-link"></i></span>
									  </div>
									  <input type="text" class="form-control" placeholder="URL Image" id="apm_urlimg2">
									</div>
								</div>

								<div id="f-upload_apm" class="f-upload form-group" style="display: none">
									<label for="apm_img2">Upload image</label>
									<div class="input-group">
											<div class="custom-file">
												<input type="file" class="custom-file-input2" id="apm_img2" accept=".jpg,.jpeg,.png">
												<img id="temp_apmimage_src" hidden="">
												<label class="custom-file-label" id="apm_labelimg2" for="apm_img2"><p>Choose file</p></label>
											</div>
											<div class="input-group-append">
												<button class="input-group-text" id="apm_uploadimg2">Upload</button>
											</div>

									</div>
								 </div>
							</div>
							
						 	 <div class="form-group">
								<input type="text" class="form-control" id="titleApm" placeholder="Title">
							</div>
							
							<div class="form-group  mb-1">
								<textarea id="textApm" class="form-control mb10-xs" rows="2" placeholder="Description"></textarea>
								<textarea id="textApm2" class="form-control" rows="2" placeholder="Description 2"></textarea>
								<!--<input type="text" class="form-control" id="textApm" placeholder="Text">-->
							</div>
							
							<!--<div class="check-mirosite">
								<div class="icheck-primary chk-tpl text-right mb-0"><input type="checkbox" id="has-link" class="has-link"> <label for="has-link">Create Link Microsite</label></div>
							</div>
							 <div id="create-msite" class="form-group mb10-xs" style="display: none">
								<label>Consent Form</label>
								<textarea id="textConsent" class="form-control" rows="6" placeholder="การขอความยินยอม" onKeyPress="$('#btn-view-micro').removeClass('disabled').removeClass('btn-outline-secondary').addClass('bg-gray')"></textarea>
								
								<div class="mt-1 d-flex justify-content-start">
									<a id="btn-view-micro" href="javascript:;" data-toggle="modal" data-target="#modal-consent" title="Link to microsite" class="btn btn-xs disabled btn-outline-secondary"><i class="far fa-sticky-note"></i> View Microsite</a>
								</div>
							</div>-->
							
							<div class="form-group">
								<label>Number of Actions</label>
								<select class="form-control select-action-chd" id="select-apmNum-action">
								  <option value="0">1</option>
								  <option value="1">2</option>
								  <option value="2">3</option>
								</select>
							</div>
							
							<div class="main-apm-action bg-light pa10-xs mb10-xs">
								<div id="group-a1" class="g-action1">
									<div class="form-group">
										<label>Action1</label>
										<div class="row align-items-center mb10-xs">
											<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Type</span>
											<div class="col">
												<select class="form-control" id="select-apmNum-action1">
												  <option value="0">Message Action</option>
												  <option value="5" selected>Consent Form</option>
												  <option value="1">URI Action</option>
												  <option value="2">Postback Action</option>
												  <option value="2">Web App Action</option>
												  <option value="2">Datetime Picker Action</option>
												</select>
											</div>
										</div>

										<div class="row align-items-center mb10-xs">
											<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Label</span>
											<div class="col">
												<input type="text" class="form-control" id="actionApm1" placeholder="ชื่อปุ่มเงื่อนไข" value="เงื่อนไขและรายละเอียด">
											</div>
										</div>

										<!--<div class="row align-items-center mb10-xs">
											<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Text</span>
											<div class="col">
												<input type="text" class="form-control" id="actionLinkApm1" placeholder="Action 1">
											</div>
										</div>-->
										
										<div class="row align-items-starts mb10-xs">
											<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0 pt-1">Detail</span>
											<div class="col">
												<textarea id="textConsent" class="form-control w-100" rows="6" placeholder="การขอความยินยอม" onKeyPress="$('#btn-view-micro').removeClass('disabled').removeClass('ui-btn-border').addClass('ui-btn-blue')" onClick="$('#btn-view-micro').removeClass('disabled').removeClass('ui-btn-border').addClass('ui-btn-blue')"></textarea>

												<div class="mt10-xs d-flex justify-content-start">
													<a id="btn-view-micro" href="javascript:;" data-fancybox="consent" data-modal="modal" data-src="#modal-consent" title="Link to microsite" class="ui-btn-border btn-xs disabled"><i class="far fa-sticky-note"></i> View Microsite</a>
												</div>
											</div>
										</div>
										
										
									</div>
								</div>
								
								<div id="group-a2" class="g-action2">
									<div class="form-group">
										<label>Action2</label>
										<div class="row align-items-center mb10-xs">
											<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Type</span>
											<div class="col">
												<select class="form-control" id="select-apmNum-action1">
												  <option value="0">Message Action</option>
												  <option value="1" selected>URI Action</option>
												  <option value="2">Postback Action</option>
												  <option value="2">Web App Action</option>
												  <option value="2">Datetime Picker Action</option>
												</select>
											</div>
										</div>

										<div class="row align-items-center mb10-xs">
											<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Label</span>
											<div class="col">
												<input type="text" class="form-control" id="actionApm2" placeholder="Action 2">
											</div>
										</div>


										<div class="row align-items-center mb10-xs">
											<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">URI</span>
											<div class="col">
												<input type="text" class="form-control" id="actionLinkApm2" placeholder="URI">
											</div>
										</div>
									</div>
								</div>
								
								<div id="group-a3" class="g-action3">
									<div class="form-group">
										<label>Action3</label>
										<div class="row align-items-center mb10-xs">
											<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Type</span>
											<div class="col">
												<select class="form-control" id="select-apmNum-action1">
												  <option value="0">Message Action</option>
												  <option value="1" selected>URI Action</option>
												  <option value="2">Postback Action</option>
												  <option value="2">Web App Action</option>
												  <option value="2">Datetime Picker Action</option>
												</select>
											</div>
										</div>

										<div class="row align-items-center mb10-xs">
											<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Label</span>
											<div class="col">
												<input type="text" class="form-control" id="actionApm3" placeholder="Action 3">
											</div>
										</div>


										<div class="row align-items-center mb10-xs">
											<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">URI</span>
											<div class="col">
												<input type="text" class="form-control" id="actionLinkApm3" placeholder="URI">
											</div>
										</div>
									</div>
								</div>
								
							</div>
							
							
							
					     </div>

											 <div class="tab-footer clearfix _self-mt20">
												<button type="button" id="displayCard" class="ui-btn  btn-info btn-lg float-right btn-add-appointment">Display</button>
											</div>

										  </div>

										  <div class="tab-pane fade" id="vert-tabs-payload">

											<p class="lead mb-0">Custom text</p>
											 <div class="tab-custom-content">
												 <div class="form-group">
													<label>Title</label>
													<input type="text" class="form-control" id="payloadtitle" placeholder="Title">
												</div>
												  <!-- textarea -->
												  <div class="form-group">
													<label>JSON Code</label>
													<textarea class="form-control" id="myCustomText" rows="10" placeholder="Paste code here ..."></textarea>
												  </div>
												  <!-- Link -->

											</div>

											<div class="tab-footer clearfix">
												<button type="button" id="displaypayload" class="ui-btn  btn-info btn-lg float-right btn-add-payload">Display</button>
											</div>
										  </div>
										</div>
										
										<div class="list-bubble">
											<p class="lead mb-0 pt-3">Sortable <small class="text-muted">(Maximun 3 messages)</small></p>
											<div class="tab-custom-content">
											<ul class="todo-list" data-widget="todo-list">


											</ul>
											
											<div class="save-template">
												<div class="icheck-primary chk-tpl text-right mb-0"><input type="checkbox" id="save-tp" class="save-as"> <label for="save-tp">save as template</label></div>
												<div class="set-name-tp" style="display: none">
														<p class="lead mb-0">Properties</p>
														<div class="tab-custom-content">
															 <div class="form-group">
																<label>Template Name</label>
																<div class="input-group mb-3">
																  <input type="text" class="form-control" id="tpl-name" placeholder="กรุณาตั้งชื่อ Template ค่ะ" onKeyPress="$('#btn-save-tpl').prop('disabled',false);">
																  <div class="input-group-append">
																	<button class="ui-btn-sq btn-sm btn bg-info t-white" type="button" id="btn-save-tpl" disabled onClick="$(this).children('i').removeClass('hid'); $('#btn-save-tpl').prop('disabled',true); $('#tpl-name').prop('readonly',true);"><i class="hid fas fa-check-circle"></i> Save</button>
																  </div>
																</div>
															</div>
														</div>
												</div>
											</div>
										</div>
									  </div>
					
									</div>

					
										<!-- /Left -->
									</div>
									
									
									
									<div class="col-c  _self-cl-sm-06-md-05">
										<!-- center -->
										<div class="list-bubble">
											<p class="lead mb-0 pt-3">Sortable <small class="text-muted">(Maximun 3 messages)</small></p>
											<div class="tab-custom-content">
											<ul class="todo-list" data-widget="todo-list">


											</ul>
											
											<div class="save-template">
												<div class="icheck-primary chk-tpl text-right mb-0"><input type="checkbox" id="save-tp" class="save-as"> <label for="save-tp">save as template</label></div>
												<div class="set-name-tp" style="display: none">
														<p class="lead mb-0">Properties</p>
														<div class="tab-custom-content">
															 <div class="form-group">
																<label>Template Name</label>
																<div class="input-group mb-3">
																  <input type="text" class="form-control" id="tpl-name" placeholder="กรุณาตั้งชื่อ Template ค่ะ" onKeyPress="$('#btn-save-tpl').prop('disabled',false);">
																  <div class="input-group-append">
																	<button class="ui-btn-sq btn-sm btn bg-info t-white" type="button" id="btn-save-tpl" disabled onClick="$(this).children('i').removeClass('hid'); $('#btn-save-tpl').prop('disabled',true); $('#tpl-name').prop('readonly',true);"><i class="hid fas fa-check-circle"></i> Save</button>
																  </div>
																</div>
															</div>
														</div>
												</div>
											</div>
										</div>
									  </div>
									  <!-- /center -->
									  </div>


	
								</div>

								<div class="sidebar-right _self-cl-sm-12-md-03 _flex center-xs">
									<!-- live Mobile -->
									<div id="main-log" class="bx-log active">
		<div id="live-screen">
		<ul class="list-sr-chat show">

									<li class="done messageScreen1"><input type="hidden" class="done messageScreen1" id="valScreen1" value="txt|text">
										<div class="direct-chat-msg">
											<div class="direct-chat-img"></div>
											<div class="direct-chat-text">กรุณาแชร์โลเคชั่นคุณลูกค้ามาให้ทางเราด้วยค่ะ</div>
										</div>
									</li>
									<li class="done messageScreen2"><input type="hidden" class="done messageScreen2" id="valScreen2" value="txt|text">
										<div class="direct-chat-msg">
											<div class="direct-chat-img"></div>
											<div class="direct-chat-text">กรุณาส่ง Location ที่เกิดเหตุด้วยค่ะ</div>
										</div>
										<div class="button-quick-reply d-flex justify-content-start mt-3">
											<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> กด Location ปัจุบัน</a>
											<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> กดระบุ Location ใหม่</a>
											<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> กดระบุบริเวณใกล้เคียง (จุดสังเกตุ)</a>
										</div>
									</li>
									<li class="done result-carousel messageScreen3">
										<div class="direct-chat-msg">
											<div class="direct-chat-img"></div>
										</div>
										<div class="card-button">
											<figure class="image"><a href="#listaction"><img src="https://dummyimage.com/500x500/fea3b8/ffffff.png&amp;text=Consent form"></a>
											</figure>
											<div class="detail">
												<h2>ทางบ.xxx ขออนุญาตเรียนแจ้งให้ท่านทราบว่า</h2>
												<p class="max">สำหรับการใช้บริการ....คุณลูกค้ากรุณาแสดงความยินยอม หรือ consent สำหรับการขอใช้บริการ ดูรายละเอียด consent form ได้ที่นี่ค่ะ [เงื่อนไขการยินยอม]
												</p>
											</div>
											<ul>
												<li class="p-1"><a href="https://ui.beurguide.net/LNS/microsite/consent1.html" target="_blank">เงื่อนไขและรายละเอียด</a>
												</li>
												<li class="p-1"><a class="btn btn-block bg-gray2 p-1" href="#Action1">ยินยอม</a>
												</li>
												<li class="p-1"><a class="btn btn-block bg-gray2 p-1" href="#Action2">ไม่ยินยอม</a>
												</li>
											</ul>
										</div>
									</li>
																		<li class="done messageScreen5"><input type="hidden" class="done messageScreen5" id="valScreen5" value="txt|text">
										<div class="direct-chat-msg">
											<div class="direct-chat-img"></div>
											<div class="direct-chat-text">ขอบคุณค่ะ</div>
										</div>
									</li>
								</ul>
		</div>
		</div>
									<!-- /live Mobile -->
								</div>

						</div>

						</div>
						<div class="sticky-bottom card-footer">
							<div class="__chd-ph10 center-xs">
									<button type="button" class="ui-btn-gray btn-lg" data-toggle="modal" data-target="#modal-close">Cancel</button>
									<button type="submit" class="ui-btn-green btn-lg" onclick="$(this).children('i').removeClass('hid'); $('.form-sending')[0].reset();"><i class="hid fas fa-circle-notch fa-spin"></i> Save Template</button>
							</div>
						  </div>
						
					</div>
					<!-- /card -->
					</form>

			</div>
			
			
			
			
		</section>
    </div>
</div>

<div class="popup thm-lite" id="modal-consent">
				<div class="box-middle">
				  <div class="modal-content">
					<div class="modal-header">
					  <h2 class="t-black">Consent Form</h2>
					</div>
					<div class="modal-body">
					  
					  <div class="text-body txt-l">
					  	

							<div class="detail">
									<h3 class="text-md t-black">การขอความยินยอม (Consent Form)</h3>
									<div class="max">
									<p>วัตถุประสงค์</p>
									<p>1. เพื่อให้ประชาชน ผู้รับบริการ ทุกพื้นที่สามารถเข้าถึงบริการด้านการให้ค าแนะน าด้านสุขภาพ
									วินิจฉัยโรคเบื้องต้น ดูแลในกรณีฉุกเฉิน และดูแลในกรณีพิเศษ (ส่งต่อ)</p>
									<p>2. เพื่อให้ประชาชนผู้รับบริการสามารถเข้าถึงและบริหารจัดการข้อมูลสุขภาพทางอิเล็กทรอนิกส์
									ของตนได้</p>
									<p>3. เพื่อให้บุคลากรที่ได้รับ</p>
									<p>4. เพื่อให้ประชาชน ผู้รับบริการ ทุกพื้นที่สามารถเข้าถึงบริการด้านการให้ค าแนะน าด้านสุขภาพ
									วินิจฉัยโรคเบื้องต้น ดูแลในกรณีฉุกเฉิน และดูแลในกรณีพิเศษ (ส่งต่อ)</p>
									<p>5. เพื่อให้ประชาชนผู้รับบริการสามารถเข้าถึงและบริหารจัดการข้อมูลสุขภาพทางอิเล็กทรอนิกส์
									ของตนได้</p>
									<p>6. เพื่อให้บุคลากรที่ได้รับ</p>

									</div>
								</div>

					  </div>
	
					</div>
					<div class="modal-footer p-0 center-xs">
					  <a data-fancybox-close="" class="ui-btn-green2 btn-sm" title="Close" href="javascript:;" onclick="parent.jQuery.fancybox.close();">Close</a>
					</div>
				  </div>
				  <!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			  </div>
			  <!-- /.modal -->
			  

<!--<div id="skin-loading" class="bg-wh" onclick="$(this).fadeOut();">
	<div class="lds-hourglass"></div>
</div>-->
<script>
	window.setTimeout(function(){
		$('#skin-loading').fadeOut();
	}, 3000);
</script>

<!-- footer -->
<?php include("incs/footer-web.html") ?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js-web.html") ?>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<link href="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.css" rel="stylesheet"/>
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<script type="text/javascript">
$( document ).ready( function () {
	 
      $('.carousel').flexslider({
        animation: "slide",
        animationLoop: false,
        itemWidth: 210,
        itemMargin: 5,
        minItems: 1,
        maxItems: 3,
		move: 1,
        /*start: function(slider){
          $('body').removeClass('loading');
        }*/
      });
	  
	//select2
	$('.keep-select-group').select2({
    	placeholder: "Please select",
    	//allowClear: true,
		dropdownAutoWidth : true,
		width: '100%'
	});
	
});
  </script>
  
<script>
  $( function() {
     $( ".todo-list" ).sortable({
      placeholder: "ui-state-highlight",
	  items: "li:not(.ui-state-disabled)",
	  //cancel: ".ui-state-disabled"
    });
    $( ".todo-list li" ).disableSelection();
  } );
</script>

<script>
/*upload*/
$( document ).ready( function () {
	var count_message = 0;
	
	$('.custom-select').on('change', function() {

		if ($(this)[0].selectedIndex == 0) { 
			$(this).parents('.wrap-upload').find('.f-url').show();
			$(this).parents('.wrap-upload').find('.f-upload').hide();
			
		} else if ($(this)[0].selectedIndex == 1)  {
			$(this).parents('.wrap-upload').find('.f-upload').show();
			$(this).parents('.wrap-upload').find('.f-url').hide();
		}

	});
	
	$('input[type=radio][name=chk-send]').change(function() {
		if (this.value == 'now') {
			$("#show-schedule").hide();
		}
		else if (this.value == 'set') {
			$("#show-schedule").show();
		}
	});
	
	
	// jQuery UI sortable for the todo list
	  /*$('.todo-list').sortable({
		placeholder         : 'sort-highlight',
		handle              : '.handle',
		forcePlaceholderSize: true,
		zIndex              : 999999
	  })*/
	  
	  // Add Text
	$('.btn-add-msg').click(function() {
		var message_text = $("#message_text").val();
		var count_items = 0;
		$("ul.list-sr-chat>li").each(function () {
			count_items++;
		});
		if ( count_items < 5 && message_text != '' ) {
			count_message++;
			if ( $('#newpush .todo-list').hasClass('show') ) {
				
			} else {
				$('.todo-list,.list-sr-chat,.save-template').addClass('show');
			}
			$('.todo-list').append('<li class="done messageList'+count_message+'">'+
				'<span class="handle ui-sortable-handle">'+
					' <i class="fas fa-ellipsis-v"></i>'+
					' <i class="fas fa-ellipsis-v"></i>'+
				'</span>'+
				'<div class="icheck-primary d-inline ml-2">'+
					' <input type="checkbox" id="chklist'+count_message+'" onclick="checkboxShowHide('+count_message+')" checked>'+
					' <label for="chklist'+count_message+'"></label>'+
				'</div>'+
				'<span class="text">'+message_text+'</span>'+
				'<small class="badge badge-info"><i class="fas fa-text-height"></i> Text</small>'+
				'<div class="tools">'+
					'<i class="fas fa-edit text-gray" onclick="editMessage('+count_message+')"></i>'+
					'<i class="fas fa-trash" onclick="removeMessage('+count_message+')"></i>'+
				'</div>'+
			'</li>');
			$('.list-sr-chat').append('<li class="messageScreen'+count_message+'">'+
				'<input type="hidden" class="messageScreen'+count_message+'" id="valScreen'+count_message+'" value="txt|'+message_text+'"/>'+
				'<div class="direct-chat-msg">'+
					'<div class="direct-chat-img"></div>'+
					'<div class="direct-chat-text">'+message_text+'</div>'+
				'</div>'+
			'</li>');
			$("#message_text").val('');
		} else {
			if ( count_items >= 5 ) {
				alert('Maximum 5 Message');
			} else {
				alert('Please input text');
			}
		}
	});
	
	//add Quick reply 
	$('#select-replay-action').on('change', function() {

		if ($(this)[0].selectedIndex == 0) { 
			$('#replay-action1').show();
			$('#replay-action2').hide();
			$('#replay-action3').hide();
			$('#replay-action4').hide();
			$('#replay-action5').hide();
			$('#replay-action6').hide();
			
		} else if ($(this)[0].selectedIndex == 1)  {
			$('#replay-action1').show();
			$('#replay-action2').show();
			$('#replay-action3').hide();
			$('#replay-action4').hide();
			$('#replay-action5').hide();
			$('#replay-action6').hide();
		} else if ($(this)[0].selectedIndex == 2)  {
			$('#replay-action1').show();
			$('#replay-action2').show();
			$('#replay-action3').show();
			$('#replay-action4').hide();
			$('#replay-action5').hide();
			$('#replay-action6').hide();
		} else if ($(this)[0].selectedIndex == 3)  {
			$('#replay-action1').show();
			$('#replay-action2').show();
			$('#replay-action3').show();
			$('#replay-action4').show();
			$('#replay-action5').hide();
			$('#replay-action6').hide();
		} else if ($(this)[0].selectedIndex == 4)  {
			$('#replay-action1').show();
			$('#replay-action2').show();
			$('#replay-action3').show();
			$('#replay-action4').show();
			$('#replay-action5').show();
			$('#replay-action6').hide();
		} else if ($(this)[0].selectedIndex == 5)  {
			$('#replay-action1').show();
			$('#replay-action2').show();
			$('#replay-action3').show();
			$('#replay-action4').show();
			$('#replay-action5').show();
			$('#replay-action6').show();
		}
	});
	$('.btn-add-qreply').click(function() {
		var message_replay_text = $("#message_replay_text").val();
		var isAction = $('#select-replay-action')[0].selectedIndex;
		var myLabelAction1 = $("#labelQreply1").val();
		var myLabelAction2 = $("#labelQreply2").val();
		var myLabelAction3 = $("#labelQreply3").val();
		var myLabelAction4 = $("#labelQreply4").val();
		var myLabelAction5 = $("#labelQreply5").val();
		var myLabelAction6 = $("#labelQreply6").val();
		
		var count_items = 0;
		$("ul.list-sr-chat>li").each(function () {
			count_items++;
		});
		if ( count_items < 5 && message_text != '' ) {
			count_message++;
			if ( $('#newpush .todo-list').hasClass('show') ) {
				
			} else {
				$('.todo-list,.list-sr-chat,.save-template').addClass('show');
			}
			$('.todo-list').append('<li class="done messageList'+count_message+'">'+
				'<span class="handle ui-sortable-handle">'+
					' <i class="fas fa-ellipsis-v"></i>'+
					' <i class="fas fa-ellipsis-v"></i>'+
				'</span>'+
				'<div class="icheck-primary d-inline ml-2">'+
					' <input type="checkbox" id="chklist'+count_message+'" onclick="checkboxShowHide('+count_message+')" checked>'+
					' <label for="chklist'+count_message+'"></label>'+
				'</div>'+
				'<span class="text">'+message_replay_text+'</span>'+
				'<small class="badge bg-purple"><i class="fas fa-reply"></i> Reply</small>'+
				'<div class="tools">'+
					'<i class="fas fa-edit text-gray" onclick="editMessage('+count_message+')"></i>'+
					'<i class="fas fa-trash" onclick="removeMessage('+count_message+')"></i>'+
				'</div>'+
			'</li>');
			if(isAction == 0) {
			$('.list-sr-chat').append('<li class="messageScreen'+count_message+'">'+
				'<input type="hidden" class="messageScreen'+count_message+'" id="valScreen'+count_message+'" value="txt|'+message_replay_text+'"/>'+
				'<div class="direct-chat-msg">'+
					'<div class="direct-chat-img"></div>'+
					'<div class="direct-chat-text">'+message_replay_text+'</div>'+
				'</div>'+
				'<div class="button-quick-reply d-flex justify-content-start mt-3">'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction1+'</a>'+
				'</div>'+
			'</li>');
			} else if(isAction == 1) {
			$('.list-sr-chat').append('<li class="messageScreen'+count_message+'">'+
				'<input type="hidden" class="messageScreen'+count_message+'" id="valScreen'+count_message+'" value="txt|'+message_replay_text+'"/>'+
				'<div class="direct-chat-msg">'+
					'<div class="direct-chat-img"></div>'+
					'<div class="direct-chat-text">'+message_replay_text+'</div>'+
				'</div>'+
				'<div class="button-quick-reply d-flex justify-content-start mt-3">'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction1+'</a>'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction2+'</a>'+
				'</div>'+
			'</li>');
			} else if(isAction == 2) {
			$('.list-sr-chat').append('<li class="messageScreen'+count_message+'">'+
				'<input type="hidden" class="messageScreen'+count_message+'" id="valScreen'+count_message+'" value="txt|'+message_replay_text+'"/>'+
				'<div class="direct-chat-msg">'+
					'<div class="direct-chat-img"></div>'+
					'<div class="direct-chat-text">'+message_replay_text+'</div>'+
				'</div>'+
				'<div class="button-quick-reply d-flex justify-content-start mt-3">'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction1+'</a>'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction2+'</a>'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction3+'</a>'+
				'</div>'+
			'</li>');
			} else if(isAction == 3) {
			$('.list-sr-chat').append('<li class="messageScreen'+count_message+'">'+
				'<input type="hidden" class="messageScreen'+count_message+'" id="valScreen'+count_message+'" value="txt|'+message_replay_text+'"/>'+
				'<div class="direct-chat-msg">'+
					'<div class="direct-chat-img"></div>'+
					'<div class="direct-chat-text">'+message_replay_text+'</div>'+
				'</div>'+
				'<div class="button-quick-reply d-flex justify-content-start mt-3">'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction1+'</a>'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction2+'</a>'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction3+'</a>'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction4+'</a>'+
				'</div>'+
			'</li>');
			} else if(isAction == 4) {
			$('.list-sr-chat').append('<li class="messageScreen'+count_message+'">'+
				'<input type="hidden" class="messageScreen'+count_message+'" id="valScreen'+count_message+'" value="txt|'+message_replay_text+'"/>'+
				'<div class="direct-chat-msg">'+
					'<div class="direct-chat-img"></div>'+
					'<div class="direct-chat-text">'+message_replay_text+'</div>'+
				'</div>'+
				'<div class="button-quick-reply d-flex justify-content-start mt-3">'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction1+'</a>'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction2+'</a>'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction3+'</a>'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction4+'</a>'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction5+'</a>'+
				'</div>'+
			'</li>');
			} else if(isAction == 5) {
			$('.list-sr-chat').append('<li class="messageScreen'+count_message+'">'+
				'<input type="hidden" class="messageScreen'+count_message+'" id="valScreen'+count_message+'" value="txt|'+message_replay_text+'"/>'+
				'<div class="direct-chat-msg">'+
					'<div class="direct-chat-img"></div>'+
					'<div class="direct-chat-text">'+message_replay_text+'</div>'+
				'</div>'+
				'<div class="button-quick-reply d-flex justify-content-start mt-3">'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction1+'</a>'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction2+'</a>'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction3+'</a>'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction4+'</a>'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction5+'</a>'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction6+'</a>'+
				'</div>'+
			'</li>');
			}
			
			$("#message_text").val('');
		} else {
			if ( count_items >= 5 ) {
				alert('Maximum 5 Message');
			} else {
				alert('Please input text');
			}
		}
	});
	

	//toggle action number
	
	function callToggleNum() {
		$('.select-action-chd').on('change', function() {

			if ($(this)[0].selectedIndex == 0) { 
				$(this).parent().next('.main-apm-action').children('.g-action1').show();
				$(this).parent().next('.main-apm-action').children('.g-action2').hide();
				$(this).parent().next('.main-apm-action').children('.g-action3').hide();

			} else if ($(this)[0].selectedIndex == 1)  {
				$(this).parent().next('.main-apm-action').children('.g-action1').show();
				$(this).parent().next('.main-apm-action').children('.g-action2').show();
				$(this).parent().next('.main-apm-action').children('.g-action3').hide();
			} else if ($(this)[0].selectedIndex == 2)  {
				$(this).parent().next('.main-apm-action').children('.g-action1').show();
				$(this).parent().next('.main-apm-action').children('.g-action2').show();
				$(this).parent().next('.main-apm-action').children('.g-action3').show();
			}
		});
	}
	callToggleNum();
	
	//add appointment
	$('#select-apmNum-action').on('change', function() {

		if ($(this)[0].selectedIndex == 0) { 
			$('#group-a1').show();
			$('#group-a2').hide();
			$('#group-a3').hide();
			
		} else if ($(this)[0].selectedIndex == 1)  {
			$('#group-a1').show();
			$('#group-a2').show();
			$('#group-a3').hide();
		} else if ($(this)[0].selectedIndex == 2)  {
			$('#group-a1').show();
			$('#group-a2').show();
			$('#group-a3').show();
		}
	});

	
	$('.btn-add-appointment').click(function() {
		var isAction = $('#select-apmNum-action')[0].selectedIndex;
		var myApmTitle = $("#titleApm").val();
		var myApmText = $("#textApm").val();
		var myApmText2 = $("#textApm2").val();
		var myApmAction1 = $("#actionApm1").val();
		var myApmAction2 = $("#actionApm2").val();
		var myApmAction3 = $("#actionApm3").val();
		
		var count_items = 0;
		$("ul.list-sr-chat>li").each(function () {
			count_items++;
		});
		if ( count_items < 5 && myApmTitle != '' ) {
			count_message++;
			if ( $('#newpush .todo-list').hasClass('show') ) {
				
			} else {
				$('.todo-list,.list-sr-chat,.save-template').addClass('show');
			}
			$('.todo-list').append('<li class="done messageList'+count_message+'">'+
				'<span class="handle ui-sortable-handle">'+
					' <i class="fas fa-ellipsis-v"></i>'+
					' <i class="fas fa-ellipsis-v"></i>'+
				'</span>'+
				'<div class="icheck-primary d-inline ml-2">'+
					' <input type="checkbox" id="chklist'+count_message+'" onclick="checkboxShowHide('+count_message+')" checked>'+
					' <label for="chklist'+count_message+'"></label>'+
				'</div>'+
				'<span class="text">'+myApmTitle+'</span>'+
				'<small class="badge bg-primary"><i class="fas fa-user-clock"></i> Appointment</small>'+
				'<div class="tools">'+
					'<i class="fas fa-edit text-gray" onclick="editMessage('+count_message+')"></i>'+
					'<i class="fas fa-trash" onclick="removeMessage('+count_message+')"></i>'+
				'</div>'+
			'</li>');
			if(isAction == 0) {
			$('.list-sr-chat').append('<li class="result-carousel messageScreen'+count_message+'">'+
				'<div class="direct-chat-msg">'+
					'<div class="direct-chat-img"></div>'+
				 '</div>'+
				'<div class="card-button">'+
				  '<figure class="image">'+
					  '<a href="#listaction"><img src="https://dummyimage.com/500x500/fea3b8/ffffff.png&text=demo" /></a>'+
				  '</figure>'+
					'<div class="detail">'+
					 ' <h2>'+myApmTitle+'</h2>'+
					 ' <p>'+myApmText+'</p>'+
					 ' <p>'+myApmText2+'</p>'+
					'</div>'+
					'<ul>'+
					'<li class="p-1">'+
					'<a href="https://ui.beurguide.net/LNS/microsite/consent1.html" target="_blank">'+myApmAction1+'</a>'+
					'</li>'+
				  '</ul>'+
				'</div>'+
			'</li>');
			} else if(isAction == 1) {
			$('.list-sr-chat').append('<li class="result-carousel messageScreen'+count_message+'">'+
				'<div class="direct-chat-msg">'+
					'<div class="direct-chat-img"></div>'+
				 '</div>'+
				'<div class="card-button">'+
				  '<figure class="image">'+
					  '<a href="#listaction"><img src="https://dummyimage.com/500x500/fea3b8/ffffff.png&text=demo" /></a>'+
				  '</figure>'+
					'<div class="detail">'+
					 ' <h2>'+myApmTitle+'</h2>'+
					 ' <p>'+myApmText+'</p>'+
					 ' <p>'+myApmText2+'</p>'+
					'</div>'+
					'<ul>'+
					'<li class="p-1">'+
					'<a href="https://ui.beurguide.net/LNS/microsite/consent1.html" target="_blank">'+myApmAction1+'</a>'+
					'</li>'+
					'<li class="p-1">'+
					'<a class="btn btn-block bg-gray2 p-1" href="#Action2">'+myApmAction2+'</a>'+
					'</li>'+
				  '</ul>'+
				'</div>'+
			'</li>');
			} else if(isAction == 2) {
			$('.list-sr-chat').append('<li class="result-carousel messageScreen'+count_message+'">'+
				'<div class="direct-chat-msg">'+
					'<div class="direct-chat-img"></div>'+
				 '</div>'+
				'<div class="card-button">'+
				  '<figure class="image">'+
					  '<a href="#listaction"><img src="https://dummyimage.com/500x500/fea3b8/ffffff.png&text=demo" /></a>'+
				  '</figure>'+
					'<div class="detail">'+
					 ' <h2>'+myApmTitle+'</h2>'+
					 ' <p>'+myApmText+'</p>'+
					 ' <p>'+myApmText2+'</p>'+
					'</div>'+
					'<ul>'+
					'<li>'+
					'<a href="https://ui.beurguide.net/LNS/microsite/consent1.html" target="_blank">'+myApmAction1+'</a>'+
					'</li>'+
					'<li class="p-1">'+
					'<a class="btn btn-block bg-gray2 p-1" href="#Action2">'+myApmAction2+'</a>'+
					'</li>'+
					'<li class="p-1">'+
					'<a class="btn btn-block bg-gray2 p-1" href="#Action3">'+myApmAction3+'</a>'+
					'</li>'+
				  '</ul>'+
				'</div>'+
			'</li>');
			}

			$("#titleApm").val('');
			$("#textApm").val('');
			
		} else {
			if ( count_items >= 5 ) {
				alert('Maximum 5 Message');
			} else {
				alert('Please input text');
			}
		}
	});
	//add payload
	$('.btn-add-payload').click(function() {
		
		var myCustomText = $("#myCustomText").val();
		var count_items = 0;
		$("ul.list-sr-chat>li").each(function () {
			count_items++;
		});
		if ( count_items < 5 && myCustomText != '' ) {
			count_message++;
			if ( $('#newpush .todo-list').hasClass('show') ) {
				
			} else {
				$('.todo-list,.list-sr-chat,.save-template').addClass('show');
			}
			$('.todo-list').append('<li class="done messageList'+count_message+'">'+
				'<span class="handle ui-sortable-handle">'+
					' <i class="fas fa-ellipsis-v"></i>'+
					' <i class="fas fa-ellipsis-v"></i>'+
				'</span>'+
				'<div class="icheck-primary d-inline ml-2">'+
					' <input type="checkbox" id="chklist'+count_message+'" onclick="checkboxShowHide('+count_message+')" checked>'+
					' <label for="chklist'+count_message+'"></label>'+
				'</div>'+
				'<span class="text">'+myCustomText+'</span>'+
				'<small class="badge badge-info"><i class="fas fa-comment-dots"></i> Payload</small>'+
				'<div class="tools">'+
					'<i class="fas fa-edit text-gray" onclick="editMessage('+count_message+')"></i>'+
					'<i class="fas fa-trash" onclick="removeMessage('+count_message+')"></i>'+
				'</div>'+
			'</li>');
			$('.list-sr-chat').append('<li class="messageScreen'+count_message+'">'+
				'<input type="hidden" class="messageScreen'+count_message+'" id="valScreen'+count_message+'" value="txt|'+myCustomText+'"/>'+
				'<div class="direct-chat-msg">'+
					'<div class="direct-chat-img"></div>'+
					'<div class="direct-chat-text">Custom Message!</div>'+
				'</div>'+
			'</li>');
			$("#myCustomText").val('');
			
		} else {
			if ( count_items >= 3 ) {
				alert('Maximum 3 Message');
			} else {
				alert('Please input text');
			}
		}
	});
	
	  
	  
	$('#message_urlimg').change(function () {
		$('#displayimg').prop('disabled', false);
	});
	// Upload image
	$("#message_img").change(function () {
		var file = $('#message_img')[0].files[0];
		$('#labelimg').find('p').remove();
		$('#labelimg').append('<p>'+file.name+'</p>');
	});
	$('#displayimg').click(function () {
		$('#labelimg').find('p').remove();
		$('#labelimg').append('<p>Choose file</p>');
	});
	$('#uploadimg').click(function () {
		var error_upload = [];
		error_upload[1] = 'Only file type JPG or PNG';
		error_upload[2] = 'File size Maximum 3 MB';
		var url = "https://api.innohub.me/pnp/upload";
		var chk_error = 0;
		var file = $('#message_img')[0].files[0];
		if (file.type != 'image/jpeg' && file.type != 'image/png') {
			chk_error = 1;
		} else if (file.size > 5*1024000) {
			chk_error = 2;
		}
		if (chk_error != 0) {
			alert(error_upload[chk_error]);
			return;
		}
		var reader = new FileReader();
		reader.onload = function(e) {
			$('#temp_image_src').attr('src', e.target.result);
		}
		reader.readAsDataURL(file);
		var formdata = new FormData();
		formdata.append("upload", file);
		$('#displayimg').append(' <i class="fa fa-spinner fa-spin"></i>');
		$.ajax({
			type: 'POST',
			url: url,
			data: formdata,
			success: function(data){
				if ( data.statusdesc == 'SUCCESS' ) {
					$('#displayimg').prop('disabled', false);
					$('#displayimg').find('i').remove();
					$('#temp_image_src').prop('src', data.url);
				}
			},
			cache: false,
			contentType: false,
			processData: false,
			dataType: 'json'
		});
	});
	
	
	
	//live 
	/*$('.call-log').click(function() {
		if ($('#dxy').hasClass( 'open' )) {
			$('#main-log').removeClass('active');
			$('#dxy').removeClass('open');
			$('#dxy').addClass('close');
			$('.call-log').removeClass('active');
		} else {
		   $('#main-log').addClass('active');
		   $('#dxy').toggleClass('open');
		   $('#dxy').removeClass('close');
		   $(this).toggleClass('active');
		}
    });*/
	

		/*toggle save template*/
		$('.save-as').change( function() {
			var isChecked = this.checked;
			if(isChecked) {
				$(".set-name-tp").fadeIn(100);
				$("#tpl-name").prop("disabled",false); 
				$("#tpl-name").prop("required",true); 
			} else {
				$(".set-name-tp").fadeOut(100);
				$("#tpl-name").prop("disabled",true);
				$("#tpl-name").prop("required",false);
			}
		});
		/*toggle Receiver*/
		$('.rcv-addfriend').change( function() {
			var isChecked = this.checked;
			if(isChecked) {
				$("#phone-chk").addClass('bg-light');
				$("#number-check").removeClass('hid');
			} else {
				$("#phone-chk").removeClass('bg-light');
				$("#number-check").addClass('hid');
			}
		});
		function removeBtnCheck() {
		  $('#number-check').find('i').removeClass('fa-spinner').removeClass('fa-spin');
		  $('#showPhone').fadeIn(100);
		}
		$('#number-check').click( function() {
			$(this).find('i').addClass('fa-spinner').addClass('fa-spin');
			setTimeout(function () {
				removeBtnCheck();
			}, 1800);
			
		});

	  
});

</script>


<script type="text/javascript">
$( document ).ready( function () {
	 
      $('.carousel').flexslider({
        animation: "slide",
        animationLoop: false,
        itemWidth: 210,
        itemMargin: 5,
        minItems: 1,
        maxItems: 3,
		move: 1,
        /*start: function(slider){
          $('body').removeClass('loading');
        }*/
      });
	  /*send check type*/
	  $('input[type=radio][name=chk-send]').change(function() {
		if (this.value == 'now') {
			$("#show-schedule").hide();
		}
		else if (this.value == 'set') {
			$("#show-schedule").show();
		}
	});
	//select2
	$(".select2").select2();
	$(".select-box").select2({dropdownAutoWidth : '70',minimumResultsForSearch: -1});
	$('.keep-select-group').select2({
    	placeholder: "Please select",
    	//allowClear: true,
		//tags: true,
		dropdownAutoWidth : true,
		width: '100%'
	});
	$('.js-select-multi').select2({
		//maximumSelectionLength: 3,
    	placeholder: "Select",
    	//allowClear: true,
		//tags: true,
		dropdownAutoWidth : true,
		width: '100%'
	});
	$(".js-adv-select").select2({
	   //dropdownAutoWidth : true,
	  width: '100%',
	  tags: true,
	  insertTag: function (data, tag) {
		// Insert the tag at the end of the results
		data.push(tag);
	  }
	  });

	/*Groups all*/
	$("#checkAll").click(function(){
		if($("#checkAll").is(':checked') ){
			$("#e1 > option").prop("selected","selected");
			$("#e1").trigger("change");
		}else{
			$("#e1 > option").removeAttr("selected");
			$("#e1").trigger("change");
		 }
	});
	/*users all*/
	$('.master-chk').click(function() {
		if($(this).is(':checked')) {
			$("input[class*='chd-chk']").prop("checked", true);
			$(this).prop("checked", true);
		} else {
			$("input[class*='chd-chk']").prop("checked", false);
			$(this).prop("checked", false);
		}
	});
	$("input[class*='chd-chk']").change( function() {
		$('.master-chk').prop("checked", false);
	});
	/*count remain*/
	//var credit = 3;
	//var count = 1;
	var checkboxes = $('.bubble-chk');
	var remain = $('#countRmainUID').val();
	checkboxes.change(function(){
        var current = checkboxes.filter(':checked').length;
		//alert(current);
		$('#countRmainUID').val(remain - current);
		if(current >= remain) {
		   this.checked = false;
		   checkboxes.filter(':not(:checked)').prop("disabled", true);
		   alert("ขออภัยค่ะเครดิตคุณเหลือ 0 ");
	   }
        //checkboxes.filter(':not(:checked)').prop('disabled', current >= max);
    });
	
	/*$('.bubble-chk').on('change', function(evt) {
	   if($(this).siblings(':checked').length >= remain) {
		   this.checked = false;
		   alert("ขออภัยค่ะเครดิตคุณเหลือ 0 ");
	   }
	   count = $(this).siblings(':checked').length + 1;
	   alert(count);
	   $('#countRmainUID').val(remain - count);
	});*/

});
  </script>
<!-- /js -->

</body>
</html>
