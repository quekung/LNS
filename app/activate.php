<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("../incs/head-top.html") ?>
<!-- /Top Head -->

<body class="app">
<!-- Headbar -->
<?php include("../incs/header.html") ?>

<!-- /Headbar -->
<div class="container view-atn">
    <div class="close-page"><a href="javascript:;" onclick="clkExit()" title="close">ปิด</a></div>
    
    <div id="toc">
			<h2 class="h-text">Key Activation Code</h2>
		
        	<form class="form-keep" method="post" action="index.php">
				<fieldset>
					<legend class="hid">Activation Code</legend>
					<div class="user">
						<img src="di/avatar-new.png" alt="Avatar">
						<div>
							<b>Penny Lane</b>
							<p class="credet">Use <span class="c-red">1</span> Credit <small>(from your 5 Credits)</small></p>
						</div>
					</div>
					<div class="wr">
						<input type="text" class="txt-box" id="group_name" name="group_name" required="">
						<label for="group_name">Group Name</label>
						<span class="line"></span>
					</div>
					
					<div class="mt10">
						<em>Don't have a Code. Click buy here!</em>
						<a href="#" title="buy" class="ui-btn-green-mini-cr">BUY</a>
					</div>
					
					<div class="ctrl-btn fix-bottom">
						<input type="submit" class="ui-btn-green2 btn-block" value="OK">
					</div>
				</fieldset>
			</form>
	
    </div>
</div>

<div id="skin-loading" class="bg-wh" onclick="$(this).fadeOut();">
	<div class="lds-hourglass"></div>
</div>
<script>
	window.setTimeout(function(){
		$('#skin-loading').fadeOut();
	}, 3000);
</script>

<!-- js -->
<?php include("../incs/js.html") ?>
<!-- /js -->

</body>
</html>
