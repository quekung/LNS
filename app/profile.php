<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("../incs/head-top.html") ?>
<!-- /Top Head -->

<body class="app">
<!-- Headbar -->
<?php include("../incs/header.html") ?>

<!-- /Headbar -->
<div id="app" class="container view-atn">
    <div class="close-page"><a href="javascript:;" onclick="clkExit()" title="close">ปิด</a></div>
    
    <div id="app-main">
			<h2 class="title-bar">My Profile <a href="#" title="edit"><i class="ic-edit"></i></a></h2>
		
        	<form class="form-keep" method="post" action="#">
				<fieldset>
					<legend class="hid">Profile</legend>
					<div class="user full">
						<img src="../di/avatar.png" alt="Avatar">
						<div>
							<p><b>Name : </b> Penny</p>
							<p><b>Surname : </b> Lane</p>
							<p><b>Employee ID : </b> 12345</p>
							<p><b>Credit : </b> 2/10 credits</p>
						</div>
					</div>
					<?php /*?><div class="wr">
						<input type="text" class="txt-box" id="group_name" name="group_name" required="">
						<label for="group_name">Group Name</label>
						<span class="line"></span>
					</div><?php */?>
					
					<div id="tabs">
						<ul class="idTabs">
							<li><a href="#tabs-1" class="selected">Groups</a></li>
							<li><a href="#tabs-2">History</a></li>

						</ul>
						<div id="tabs-1" class="bx-tab">
							<div class="box-result list-group">
								<ul>
									<li>
										<a href="javascript:;" data-fancybox="" data-src="#leaveGroup" title="Remove bots from the group">
											group A
											<i class="ic-del"></i>
										</a>
									</li>
									<li>
										<a href="javascript:;" data-fancybox="" data-src="#leaveGroup" title="Remove bots from the group">
											group B
											<i class="ic-del"></i>
										</a>
									</li>
									<li>
										<a href="javascript:;" data-fancybox="" data-src="#leaveGroup" title="Remove bots from the group">
											group C
											<i class="ic-del"></i>
										</a>
									</li>

								</ul>
							</div>
						</div>
						<div id="tabs-2" class="bx-tab" style="display: none">
							<div class="box-result list-group-leave">
								<ul>
									<li>
										<a href="javascript:;">
											<span>group X</span>
											<small>Leave - 21/08/2019  12:08</small>
										</a>
									</li>
									<li>
										<a href="javascript:;">
											<span>group Y</span>
											<small>Leave - 11/08/2019  16:53</small>
										</a>
									</li>
									<li>
										<a href="javascript:;">
											<span>group Z</span>
											<small>Leave - 31/07/2019  24:00</small>
										</a>
									</li>

								</ul>
							</div>
						</div>
					</div>
					
					
					
					

				</fieldset>
			</form>
	
    </div>
</div>

<div class="popup thm-trans" id="leaveGroup">
	<div class="box-middle">
	<h2>Do you want to remove bots from the group?</h2>
	<p>
		<a data-fancybox-close class="ui-btn-gray" title="Cancel" href="javascript:;" onClick="parent.jQuery.fancybox.close();">Cancel</a>
		<a data-fancybox-close class="ui-btn-green" title="Confirm" href="javascript:;" onClick="parent.jQuery.fancybox.close();"> Confirm</a>
	</p>
	</div>
</div>


<div id="skin-loading" class="bg-wh" onclick="$(this).fadeOut();">
	<div class="lds-hourglass"></div>
</div>
<script>
	window.setTimeout(function(){
		$('#skin-loading').fadeOut();
	}, 3000);
</script>

<!-- js -->
<?php include("../incs/js.html") ?>
<script src="js/jquery.idTabs.min.js"></script>
<!-- /js -->

</body>
</html>
