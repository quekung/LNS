<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("../incs/head-top.html") ?>
<!-- /Top Head -->

<body class="app">
<!-- Headbar -->
<?php include("../incs/header.html") ?>

<!-- /Headbar -->
<div class="container view-atn">
    <div class="close-page"><a href="javascript:;" onclick="clkExit()" title="close">ปิด</a></div>
    
    <div id="toc">
			<h2 class="h-text">Recall by Text</h2>
		
        	<form class="form-keep" method="post" action="#">
				<fieldset>
					<legend class="hid">Search Keyword</legend>

					<div class="wr mb15">
						<input type="text" class="txt-box" id="text_q" name="text_q" required="">
						<label for="text_q">Keyword</label>
						<span class="line"></span>
					</div>
					
					<div class="row js-select">
						<select class="select2" id="select-group" name="select-group" data-placeholder="Select Group">
							<option></option>
							<option value="1">All Group</option>
							<option value="2">Group A</option>
							<option value="3">Group B</option>
							<option value="4">Group C</option>
						</select>
					</div>
					
					<div class="row js-select">
					<select class="select2" required name="select-type" id="select-type" data-placeholder="Select File type">   
						<option></option>
						<option value="1">File type all</option>
						<option value="2">.PDF</option>
						<option value="3">.XLS</option>
						<option value="4">.DOC</option>
						<option value="5">.PPT</option>
					</select>
					</div>
					
					<!--<div class="ctrl-btn fix-bottom">
						<input type="submit" class="ui-btn-blue btn-block" value="Search">
					</div>-->
					
					<div class="box-result">
						<ul>
							<li>
								<a href="#download" title="name">
									Mileage.pdf
									<i class="ic-dl"></i>
								</a>
							</li>
							<li>
								<a href="#download" title="name">
									Mileage.xls
									<i class="ic-dl"></i>
								</a>
							</li>
							<li>
								<a href="#download" title="name">
									Mileage2.xls
									<i class="ic-dl"></i>
								</a>
							</li>

						</ul>
					</div>
				</fieldset>
			</form>
	
    </div>
</div>

<div id="skin-loading" class="bg-wh" onclick="$(this).fadeOut();">
	<div class="lds-hourglass"></div>
</div>
<script>
	window.setTimeout(function(){
		$('#skin-loading').fadeOut();
	}, 3000);
</script>


<!-- js -->
<?php include("../incs/js.html") ?>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>



<script>
//Dropdown plugin data
$(document).ready(function() {
    $('.select2').select2({
    	placeholder: "Please select",
    	//allowClear: true,
		dropdownAutoWidth : true,
		width: '100%'
	});
	$('#select-text').select2({
		minimumResultsForSearch: -1,
		width: '100%'
	});
});
</script>
<!-- /js -->

</body>
</html>
