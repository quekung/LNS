<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("../incs/head-top.html") ?>
<!-- /Top Head -->

<body class="app">
<!-- Headbar -->
<?php include("../incs/header.html") ?>

<!-- /Headbar -->
<div class="container view-atn">
    <div class="close-page"><a href="javascript:;" onclick="clkExit()" title="close">ปิด</a></div>
    
    <div id="toc">
			<h2 class="h-text">Digital Library</h2>
			
			<h2 class="title-bar mt10">
				<a href="#"><i class="ic-add"></i> New </a>
				<a href="#" title="edit"><i class="ic-edit"></i></a>
			</h2>
			
        	<form class="form-keep" method="post" action="#">
				<fieldset>
					<legend class="hid">Digital Library</legend>
					<div class="wr">
						<input type="text" class="txt-box" id="topic_name" name="topic_name" required="">
						<label for="topic_name">Topic :</label>
						<span class="line"></span>
					</div>
										
					<div class="wr">
						<input id="activate_tagator2" type="text" class="txt-box tagator" value="Mileage,document" data-tagator-show-all-options-on-focus="true" data-tagator-autocomplete="['Mileage', 'ค่าเดินทาง', 'เอกสารเบิกค่าเดินทาง', 'เบิกเงิน', 'ฟอร์มเบิกค่าเดินทาง', 'เอกสารการเงิน', 'document']">
						<label for="topic_name">Key words :</label>
						<span class="line"></span>
					</div>
					
					<div class="wr">
						<div class="file-upload-ar" data-text="Select File">
							<input type="file" class="file-upload-input" id="train_image-01" name="train_image-01" accept="*" required=""><!--  open camera = capture-->
							<label class="hid" for="train_image-01">เลือกอัพโหลดไฟล์</label>                                
						</div>

						<script>
						/*input file*/
						$("form").on("change", ".file-upload-input", function(){ 
							$(this).parent(".file-upload-ar").attr("data-text", $(this).val().replace(/.*(\/|\\)/, '') );
						});
						</script>													
					</div>
					
					<!--<div class="wr">
						<input type="text" class="txt-box" id="group_name" name="group_name" required="">
						<label for="group_name"><i class="ic-search"></i> Search Group</label>
						<span class="line"></span>
					</div>-->
					
					<div class="row mt20 js-select">
						<select class="select2" id="select-group" name="select-group" data-placeholder="Select Group">
							<option></option>
							<option value="2">Group A</option>
							<option value="3">Group B</option>
							<option value="4">Group C</option>
						</select>
					</div>
					
					
					<div class="ctrl-btn fix-bottom">
						<input type="submit" class="ui-btn-green2 btn-block" value="Submit">
					</div>
				</fieldset>
			</form>
	
    </div>
</div>

<div id="skin-loading" class="bg-wh" onclick="$(this).fadeOut();">
	<div class="lds-hourglass"></div>
</div>
<script>
	window.setTimeout(function(){
		$('#skin-loading').fadeOut();
	}, 3000);
</script>

<!-- js -->
<?php include("../incs/js.html") ?>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
<link href="cs/fm.tagator.jquery.css" rel="stylesheet" />


<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<script src="js/fm.tagator.jquery.js"></script>

<script>
//Dropdown plugin data
$(document).ready(function() {
    $('.select2').select2({
    	placeholder: "Please select",
    	//allowClear: true,
		dropdownAutoWidth : true,
		width: '100%'
	});
});
</script>

<!-- /js -->

</body>
</html>
