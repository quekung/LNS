<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("../incs/head-top.html") ?>
<!-- /Top Head -->

<body class="app">
<!-- Headbar -->
<?php include("../incs/header.html") ?>

<!-- /Headbar -->
<div class="container view-atn">
    <div class="close-page"><a href="javascript:;" onclick="clkExit()" title="close">ปิด</a></div>
    
    <div id="toc">
			<h2 class="h-text txt-c">Line Login</h2>
		
        	<form class="form-keep" method="post" action="index.php" style="width: 400px; margin: 0 auto; <?php /*?>border: 1px solid #f0f0f0; padding:0 10px 20px;border-radius: 1em<?php */?>">
				<fieldset>
					<legend class="hid">Login Santakeep</legend>
					<div class="wr">
						<input type="text" class="txt-box" id="group_name" name="group_name" required="">
						<label for="group_name">Username</label>
						<span class="line"></span>
					</div>
					<div class="wr">
						<input type="password" class="txt-box" id="group_name" name="group_name" required="">
						<label for="group_name">Password</label>
						<span class="line"></span>
					</div>
					
					<div class="mt10 txt-c">
						<em>Don't  a  Member. Click Register here!</em>
						<a href="#" title="buy" class="ui-btn-green-mini-cr">Register</a>
					</div>
					
					<div class="ctrl-btn fix-bottom">
						<input type="submit" class="ui-btn-green2 btn-block" value="Sign In">
					</div>
				</fieldset>
			</form>
	
    </div>
</div>

<div id="skin-loading" class="bg-wh" onclick="$(this).fadeOut();">
	<div class="lds-hourglass"></div>
</div>
<script>
	window.setTimeout(function(){
		$('#skin-loading').fadeOut();
	}, 3000);
</script>

<!-- js -->
<?php include("../incs/js.html") ?>
<!-- /js -->

</body>
</html>
