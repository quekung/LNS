<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("../incs/head-top.html") ?>
<!-- /Top Head -->

<body class="app">
<!-- Headbar -->
<?php include("../incs/header.html") ?>

<!-- /Headbar -->
<div class="container view-atn">
    <div class="close-page"><a href="javascript:;" onclick="clkExit()" title="close">ปิด</a></div>
    
    <div id="toc">
			<h2 class="h-text">How can I help you?</h2>
		
        	<nav class="main-nav">
				<ul>
					<li class="m-calendar"><a class="btn" href="calendar.php" title="Calendar"><i class="ic-main-calendar"></i> <span>Calendar</span></a></li>
					<li class="m-switch"><a class="btn" href="javascript:;" data-fancybox="" data-src="#onOffBot" title="BOT ON"><i class="ic-main-switch"></i> <span>BOT ON</span></a></li>
					<li class="m-about">
					<a href="#" title="About Keep a line">
						<i class="icon-about"><img src="../di/nav-main-about.png"></i>
					 	<span>About Keep a line</span>
					</a>
					</li>
				</ul>
			</nav>
	
    </div>
</div>

<div class="popup thm-trans" id="onOffBot">
	<div class="box-middle">
	<h2>You are closing the BOT?</h2>
	<p>
		<a data-fancybox-close class="ui-btn-gray" title="Cancel" href="javascript:;" onClick="parent.jQuery.fancybox.close();">Cancel</a>
		<a data-fancybox-close class="ui-btn-green" title="OK" href="javascript:;" onClick="$('.m-switch a').toggleClass('off'); parent.jQuery.fancybox.close();"> OK</a>
	</p>
	</div>
</div>


<div id="skin-loading" class="bg-wh" onclick="$(this).fadeOut();">
	<div class="lds-hourglass"></div>
</div>
<script>
	window.setTimeout(function(){
		$('#skin-loading').fadeOut();
	}, 3000);
</script>

<!-- js -->
<?php include("../incs/js.html") ?>
<!-- /js -->

</body>
</html>
