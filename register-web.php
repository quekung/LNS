<!DOCTYPE html>
<!-- Top Head -->
<?php include("incs/head-top-web.html") ?>
<link href="//chatbot.innohub.me:8020/static/css/bootstrap.css" rel="stylesheet">
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />

<!-- /Top Head -->

<body id="app-container" class="menu-hidden">
<!-- Headbar -->
<?php include("incs/header-v2.html") ?>
<!-- /Headbar -->
<style>
body{background:#00b2b2;font:16px "Mitr";color:#fff}
@media (min-width:1240px){.container { width: 1220px}}
#gb-header>.container:before,#gb-header>.container:after{display: none}
body>.container{position:relative}
.container .jumbotron{background:0 0;padding:5vh 0;margin:0;border:0}
.footer{border:0!important;position:absolute;bottom:0;left:0;right:0;text-align:center}
.footer p{color:#aaa;font-size:13px;text-align:center;text-transform:uppercase}
.btn-lg[id=btnSignUp]{border-radius:5em;-webkit-border-radius:5em}
/*edit*/
.c-red{color:#dd1741}
@media (max-width:767px){
#gb-footer .row{margin-left: 0;margin-right: 0}
}
</style>


<body>
    <div class="container">        
        <!-- menu header : end -->
        <div class="jumbotron">
            
            <form id="save_guest" class="form-signin" method="POST" action="javascript:;" enctype="multipart/form-data">
            <fieldset id="add-register" class="fix-label">
            <legend><h3 class="head"><span>ข้อมูลเพื่อลงทะเบียน</span></h3></legend>
            <ul class="form-list">   
                
                <li class="group">
                    
                    <div class="row">
                        <div class="left col-xs-12 col-sm-6">
                            <div class="wr">
                                <input type="text" class="txt-box" id="first_name" name="first_name" required>
                                <label for="first_name">ชื่อจริง</label>
                                <span class="line"></span>
                            </div>
                        </div>
                        <div class="right col-xs-12 col-sm-6">
                            <div class="wr">
                                <input type="text" class="txt-box" id="last_name" name="last_name" required>
                                <label for="last_name">นามสกุล</label>
                                <span class="line"></span>
                            </div>
                        </div>
                    </div>
					<div class="row">
                        <div class="left col-xs-12 col-sm-6">
                            <div class="wr">
                                <input type="text" class="txt-box" id="gender" name="gender" required>
                                <label for="gender">เพศ</label>
                                <span class="line"></span>
                            </div>
                        </div>
                        <div class="right col-xs-12 col-sm-6">
						<!--<?php 
								$date_finish = date_create(date('Y-m-d'));
								date_add($date_finish,date_interval_create_from_date_string("-15 years"));
								$date_finish = date_format($date_finish,"Y-m-d");	
							?>	-->			
                            <div class="wr">
                                <input type="date" class="txt-box" id="birthdate" name="birthdate" max="<?php echo $date_finish; ?>"  required="">
                                <label class="bg-wh" for="birthdate">วันเดือนปีเกิด</label>
                                <span class="line"></span>
                            </div>
                        </div>
                    </div>
					<div class="row">
						<div class="left col-xs-12 col-sm-6">
							<div class="wr">
								<input type="tel" class="txt-box" id="mobile" name="mobile" required maxlength="10">
								<label for="staff_mobile">เบอร์โทรศัพท์มือถือ</label>
								<span class="line"></span>
							</div>
						</div>
						<div class="right col-xs-12 col-sm-6">
							<div class="wr">
                                <input type="email" class="txt-box" id="email" name="email" autocomplete="off" required>
                                <label for="email">e-mail</label>
                                <span class="line"></span>
                            </div>
						</div>
					</div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12">
                            <div class="wr js-select">
                                <!--<input type="text" class="txt-box" id="division" name="division" autocomplete="off" required>-->
								<select class="select2" id="select-group" name="select-group" data-placeholder="Select Group">
									<option></option>
									<option value="1">พนักงานบริษัทเอกชน</option>
									<option value="2">ภาครัฐ / รัฐวิสาหกิจ</option>
									<option value="3">เจ้าของธุรกิจ SME</option>
									<option value="4">เจ้าของธุรกิจขนาดใหญ่</option>
									<option value="4">อาชีพอิสระ/Freelance</option>
								</select>
                                <label for="division">อาชีพ</label>
                                <span class="line"></span>
                            </div>
                        </div>   
                    </div>                    
										
					<div class="corporate">
						<!--<div id="check-staff" class="col-xs-12"><input type="checkbox" id="iam-staff" name="iam-staff" class="Blocked"> <label for="iam-staff"> กรณีเป็นลูกค้าองค์กร</label></div>-->
						<div class="row add-detail">
							   
								<div class="right col-xs-12 col-sm-6">
									<div class="wr">
										<input type="text" class="txt-box" id="staff_company" name="staff_company">
										<label for="staff_company">ชื่อบริษัท/หน่วยงาน</label>
										<span class="line"></span>
									</div>
								</div>
								<div class="left col-xs-12 col-sm-6">
									<div class="wr js-select">
										<select class="select2" id="sumStaff" name="sumStaff" data-placeholder="ระบุจำนวน">
											<option></option>
											<option>คุณคนเดียว</option>
											<option>2-9 คน</option>
											<option>10-99 คน</option>
											<option>100-299 คน</option>
											<option>300 คนขึ้นไป</option>
										</select>
										<label for="sumStaff">จำนวนรวมพนักงานทั้งหมด</label>
										<span class="line"></span>
									</div>
									<!--<div class="wr select-ar">
										<select class="txt-box" id="sumStaff" name="sumStaff" onchange="$(this).parent().addClass('valid')">
											<option class="hid"></option>
											<option>คุณคนเดียว</option>
											<option>2-9 คน</option>
											<option>10-99 คน</option>
											<option>100-299 คน</option>
											<option>300 คนขึ้นไป</option>
										</select>
										<label for="sumStaff">จำนวนรวมพนักงานทั้งหมด</label>
										<span class="line"></span>
									</div>-->
								</div>
<!--								<div class="col-xs-12">
			
										<label for="Product">เลือกผลิตภัณฑ์ที่คุณสนใจ</label>
										<div class="row inputGroup">
											<div class="col-xs-12 col-sm-6">
												<input type="checkbox" id="pd-01" name="pd-01"> <label for="pd-01"> LINE <span class="c-red">SANTA</span> keep</label>
											</div>
											<div class="col-xs-12 col-sm-6">
												<input type="checkbox" id="pd-02" name="pd-02"> <label for="pd-02"> LINE Customer <span class="c-red">Connect</span></label>
											</div>
										</div>
		
								</div>-->
						</div>
						
					</div>
					
                </li>
				
				<?php /*?><li class="quest">
					<div class="row">
                        <div class="col-xs-12 col-sm-12">
                            <label for="linefor">คุณใช้ Line ในเรื่องใดมากที่สุด?</label>
							<div class="wrap-range">
							
									<div class="label jobs">
										<span>งาน</span>
										<output name="preJudge_rangeOutput" id="preJudge_rangeOutput">50%</output>
									</div>
									<div class="bar">
										<input type="range" name="judge_range" id="judge_range" min="0" max="100" value="50" step="5">
									</div>
									<div class="label personally">
										<span>ส่วนตัว</span>
										<output name="judge_rangeOutput" id="judge_rangeOutput">50%</output>
									</div>
									<input type="range" name="prejudges_range" id="prejudges_range" min="0" max="100" value="50" step="5" style="opacity: 0;visibility: hidden">
							
							</div>
                        </div>
                    </div>     
				</li><?php */?>

                
                <li class="ctrl-btn txt-c">
                    <button id="btnNext" class="ui-btn-green" style="width: 180px;" type="button" onClick="$('#add-card').fadeIn(500); $('#add-register').fadeOut(100);">ถัดไป <i class="fas fa-angle-right"></i> </button>
                </li>
            </ul>
                

            </fieldset>
			
			<fieldset id="add-card" class="fix-label" style="display: none">
            	<legend><h3 class="head"><span>ข้อมูล Credit / Debit Card</span></h3></legend>
				<ul class="form-list">   
                
                <li class="group lg-visa">
                    	<div class="row">
							<div class="col-xs-12 col-sm-12">
							<div class="wr">
								<input type="tel" class="txt-box" id="card-num" name="card-num" required maxlength="16">
								<label for="card-num">Card number</label>
							</div>
							</div>
						</div>
						
						<div class="row">
							<div class="col-xs-12 col-sm-12">
							<div class="wr">
								<input type="text" class="txt-box" id="card-name" name="card-name" required>
								<label for="card-name">Name on card</label>
							</div>
							</div>
						</div>
						
						<div class="row">
								<div class="left col-xs-12 col-sm-6">
									<div class="wr js-select">
										<select class="select2" id="expire-month" name="expire-month" data-placeholder="MM">
											<option></option>
											<?php for($i=1;$i<=9;$i++) { ?>
											<option value="<?php echo $i; ?>">0<?php echo $i; ?></option>
											<?php } ?>
											<option value="10">10</option>
											<option value="11">11</option>
											<option value="12">12</option>
										</select>
										<label for="expire-month">Expiry date</label>
										
									</div>
								</div>
								<div class="right col-xs-12 col-sm-6">
									<div class="wr js-select">
										<select class="select2" id="expire-year" name="expire-year" data-placeholder="YYYY">
											<option></option>
											<?php for($i=2025;$i>=1950;$i--) { ?>
											<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
	
											<?php } ?>
										</select>
										<label for="expire-month">Expiry Year</label>
									</div>
								</div>
						</div>
						
						<div class="row">
							<div class="left col-xs-12 col-sm-6">
								<div class="wr mb0">
									<input type="tel" class="txt-box" id="card-cvv" name="card-cvv" required maxlength="3">
									<label for="staff_mobile">CVV</label>
								</div>
							</div>
							<div class="cvv right col-xs-12 col-sm-6">
								<a class="_self-pt15 _flex middle-xs t-gray" href="javascript:;" data-fancybox="CVV" data-src="#popupCard"><i class="far fa-question-circle"></i></a>
							</div>
						</div>

						<!-- Popup -->
						<div class="popup thm-trans" id="popupCard">
							<div class="box-middle">
							<div class="head">เลข CVV</div>
							<figure><img src="di/cvv-demo.png" alt="CVV"></figure>
							<p>เลข CVV คือ เลขรหัสหลังบัตรเครดิตหรือบัตรเดบิต</p>
							<p>	
								<a data-fancybox-close class="ui-btn-gray2-mini" title="Close" href="javascript:;" onClick="parent.jQuery.fancybox.close();"> Close</a>
							</p>
							</div>
						</div>
						<!-- /Popup -->
				</li>
				
				<li class="ctrl-btn txt-c">
                    <a class="t-white _self-pa10" href="javascript:;" onClick="$('#add-card').fadeOut(1); $('#add-register').fadeIn(1);" title="แก้ไข"><i class="fas fa-angle-left"></i> แก้ไข</a><button id="btnSignUp" class="ui-btn-green btn-lg" style="width: 180px;" type="submit" data-fancybox="success" data-src="#popupCredit"data-modal="true">ยืนยัน</button>
                </li>
				</ul>
			</fieldset>
        </form>

        </div>
 

 
    </div>


<!-- Popup -->
<div class="popup thm-trans" id="popupCredit">
	<div class="box-middle">
	<div class="head">Register Successful</div>
	<h2>คุณได้สิทธิ์การทดลองใช้ฟรี 30 วัน <u><big class="c-red">1</big> Credits</u></h2>	
	<p><small>หมายเหตุ : ระบบจะส่ง Notification แจ้งเตือน เมื่อวันทดลองใกล้ถึงกำหนดค่ะ</small></p>
	<p>	
		<a data-fancybox-close class="ui-btn-green" title="Confirm" href="javascript:;" onClick="parent.jQuery.fancybox.close();"> Confirm</a>
	</p>
	</div>
</div>
<!-- /Popup -->


<!-- footer -->
<?php include("incs/footer-web.html") ?>
<!-- /footer -->

<!-- js -->
<?php include("incs/js-web.html") ?>
<!-- /js -->

<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<script>
//Dropdown plugin data
$(document).ready(function() {
    $('.select2').select2({
    	placeholder: "Please select",
    	//allowClear: true,
		dropdownAutoWidth : true,
		width: '100%'
	});
	/*range*/
	$("#judge_range").on("input", function() {
	  $("#judge_rangeOutput").val(this.value).append('%');
	  $("#prejudges_range").val(100 - this.value);
	  $("#preJudge_rangeOutput").val(100 - this.value).append('%');
	});
	$("#prejudges_range").on("input", function() {
	  $("#preJudge_rangeOutput").val(this.value).append('%');
	  $("#judge_range").val(100 - this.value);
	  $("#judge_rangeOutput").val(100 - this.value).append('%');
	});
	/*toggle boarding pass*/
	$('.Blocked').change( function() {
		var isChecked = this.checked;
		if(isChecked) {
			$(".add-detail").fadeIn(300);
			$(".add-detail .txt-box").prop("disabled",false);
			$(".add-detail .txt-box").prop("required",true);
		} else {
			$(".add-detail").fadeOut(300);
			$(".add-detail .txt-box").prop("disabled",true); 
			$(".add-detail .txt-box").prop("required",false); 
		}
	});
});
</script>
	
<script>
$(document).ready(function() {
	$('.select2').click( function() {
		$(this).parent().addClass('valid');
	});
});
</script>


<script>
$(document).ready(function() {$("[data-fancybox='image']").fancybox({
		protect : true,
        touch: true,
	});
    $("[data-fancybox='modal']").fancybox({
		toolbar  : false,
		infobar : false,
		protect : false,
		arrows : false,
        touch: false,
	});
	
});
</script>
</body>
 
</html>