<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top-web.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden">
<!-- Headbar -->
<?php include("incs/header-v2.html") ?>
<script>
$(".main-menu .list-unstyled>li.active").removeClass('active');
$(".main-menu .list-unstyled>li:nth-child(3)").addClass('active');
</script>
<!-- /Headbar -->
<div class="page-checkout">

    
    <div id="toc">
		<section class="z-broadcast _self-pt0 mb0">
			<!--<div class="bx-stepbar _self-pv20 cb-af container">
				<ul class="tabsbar">
						  <li><a href="call2line.php" title="Send Message" class="selected"><i class="fas fa-bullhorn"></i> <span>Send Message</span></a></li>
						  <li><a href="call2line-create.php" title="Create Message"><i class="fas fa-layer-group"></i> <span>Create Template</span></a></li>
						  <li><a href="template-list.php" title="My Templates"><i class="fas fa-layer-group"></i> <span>My Templates</span></a></li>
						  <li><a href="call2line-setting.php" title="Message Setting"><i class="fas fa-sliders-h"></i> <span>Message Setting</span></a></li>
						  <li><a href="call2line-report.php" title="Report"><i class="fas fa-file-medical-alt"></i> <span>Report</span></a></li>
				  </ul>
			</div>-->
					

			
			<div class="bg-gray2 contentTabs">
				<div id="tbc-1" class="msg">
					<form method="post" class="form-checkout form-sending'">
					<div class="head-title container txt-c">
						<h2 class="t-black"><i class="fas fa-bullhorn _self-mr10 t-black"></i>Send Message</h2>	
						<p>ระบบส่งข้อความผ่านทาง line</p>
					</div>
					<div class="wrap-full _chd-cl-xs-12 _chd-cl-sm">
						<div class="main row _chd-cl-xs-12 _chd-cl-sm">
							<div id="UserCaller" class="">
								<div class="addCall bg-white pa20-xs rounded">
										<label>Please Add Mobile Number:</label>


											<div class="js-select wr">
												<div class="input-group js-tagator">
											<div class="input-group-prepend">
											  <span class="input-group-text"><i class="fas fa-phone fa-rotate-90"></i></span>
											</div>
											<input type="text" id="activate_tagator2" class="form-control tagator" value="" data-tagator-show-all-options-on-focus="true" data-tagator-autocomplete="['0899599955', '0844445566']" placeholder="Please input mobile numbers">

											  <div class="input-group-append">
												<button type="button" class="btn border-0 bg-green t-white f-normal pl10-xs pr10-xs border-0" onclick="$('.main>.col-l').removeClass('hid'); $('.main>.col-c').removeClass('hid'); $('#UserCaller').addClass('hid');">Confirm</button>
											  </div>
										  </div>



												<div class="info-container">     <small class="info text-muted">Demo Number : 081-234-4567 and <span class="text-danger">"Enter"</span></small> </div>
											</div>
								</div>
							</div>
							<div class="col-l hid _self-cl-sm-05-md-05-lg-04 main-list-tpl sr-call2line">
								
								<!-- card -->
								<div class="card">
									<div class="card-header">
										<h3 class="card-title m-0"><!--<span class="step ui-btn-green2 _self-ph10-mr10">ขั้นตอนที่ 1</span> --><i class="nav-icon fas fa-comment-alt"></i> Select Template</h3>
									</div>
									<div class="card-body">
									
									<ul class="idTabs tab-receiver">
											<li><a href="#template" class="selected"><i class="fas fa-layer-group nav-icon"></i> Template</a></li>
											<li><a href="#newpush" onClick="$('#live-screen .list-sr-chat').empty()"><i class="fas fa-comment-medical nav-icon"></i> New Push</a></li>
										</ul>
								
									<div id="template" class="accordion select-tpl" id="accordionExample">
										  <div class="card">
											<div class="hd-ac active" id="headingOne">
											  <h2>

												  Template #1 พนักงานใหม่

											  </h2>
											  <div class="tools">
												  <a href="broadcasts-edit.php"><i class="fas fa-edit text-white" onclick="$('#edit-template').show();"></i></a>
												  <a href="javascript:;" onclick="$(this).parents('.hd-ac').next().remove(); $(this).parents('.hd-ac').remove(); "><i class="fas fa-trash text-white"></i></a>
												</div>
											</div>

											<div id="collapseOne" class="pane active">
											  <ul class="idTabs tab-mini">
													<li><a href="#tmp1" class="selected"><i class="fas fa-list nav-icon"></i> Title</a></li>
													<li><a href="#tmp1-view"><i class="fas fa-eye nav-icon"></i> live</a></li>
											  </ul>
											  <div class="card-body ps-rlt p-0 list-bubble">
													<ul id="tmp1" class="todo-list show ui-sortable" data-widget="todo-list">
													  <li id="msg1" class="done">
														<!-- drag handle -->
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<!-- checkbox -->
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo1" id="todoCheck1" checked="">
														  <label for="todoCheck1"></label>
														</div>
														<!-- todo text -->
														<span class="text">Design a nice theme</span>
														<!-- Emphasis label -->
														<small class="badge badge-info"><i class="fas fa-text-height"></i> Text</small>
														<!--<div class="tools">
														  <i class="fas fa-edit text-muted" onClick="$('#edit-template').show();"></i>
														  <i class="fas fa-trash"></i>
														</div>-->
													  </li>
													  <li id="msg2" class="done">
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo2" id="todoCheck2" checked="">
														  <label for="todoCheck2"></label>
														</div>
														<span class="text">Make the theme responsive</span>
														<small class="badge badge bg-teal"><i class="far fa-image"></i> Image</small>

													  </li>
													  <li id="msg3" class="done">
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo3" id="todoCheck3" checked="">
														  <label for="todoCheck3"></label>
														</div>
														<span class="text">Let theme shine like a star</span>
														<small class="badge badge bg-teal"><i class="far fa-image"></i> Image</small>

													  </li>
													  <li id="msg4" class="done">
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo4" id="todoCheck4" checked="">
														  <label for="todoCheck4"></label>
														</div>
														<span class="text">Let theme shine like a star</span>
														<small class="badge badge-info"><i class="fas fa-text-height"></i> Text</small>

													  </li>
													  <li id="msg5" class="done">
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo5" id="todoCheck5" checked="">
														  <label for="todoCheck5"></label>
														</div>
														<span class="text">Check your messages and notifications</span>
														<small class="badge badge bg-teal"><i class="far fa-image"></i> Image</small>

													  </li>

													</ul>
													
													<div id="tmp1-view" class="live tab-pane bg-light pl10-xs pr10-xs">
														<ul class="list-sr-chat show">

															<li class="done messageScreen1"><input type="hidden" class="done messageScreen1" id="valScreen1" value="txt|text"><div class="direct-chat-msg"><div class="direct-chat-img"></div><div class="direct-chat-text">text</div></div></li><li class="done messageScreen2"><input type="hidden" class="done messageScreen2" id="valScreen2" value="img|https://dummyimage.com/500x500/17a3b8/ffffff.png&amp;text=Meeting"><div class="direct-chat-msg"><div class="direct-chat-img"></div><div class="direct-chat-text direct-chat-image"><img class="img-fluid pad" src="https://dummyimage.com/500x500/17a3b8/ffffff.png&amp;text=Meeting"></div></div></li><li class="result-imgmap messageScreen3"><input type="hidden" class="done messageScreen3" id="valScreen3" value="img|https://dummyimage.com/500x500/17a3b8/ffffff.png&amp;text=Meeting"><div class="frame-image"><img class="img-fluid pad" src="https://dummyimage.com/500x500/17a3b8/ffffff.png&amp;text=Meeting" alt="Photo"><div class="mark-link v2"><a href="#test1" class="link-p-1" title="Action1" style="background: #ccc; opacity:.5"></a><a href="#test2" class="link-p-2" title="Action2" style="background: #aaa; opacity:.5"></a></div></div></li><li class="result-carousel messageScreen4"><div class="direct-chat-msg"><img class="direct-chat-img" src="http://ui.beurguide.net/LNS/dist/img/avatar-admin.png" alt="Message User Image"></div>
																<div class="flexslider carousel">
																 <ul class="slides">
																	<li>
																	  <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_cheesecake_brownie.jpg" /></a>
																	</li>
																	<li>
																	 <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_lemon.jpg" /></a>
																	</li>
																	<li>
																	  <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_donut.jpg" /></a>
																	</li>
																	<li>
																	 <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_caramel.jpg" /></a>
																	</li>
																  </ul>
																</div>
															<!--<div class="flexslider carousel"><div class="flex-viewport" style="overflow: hidden; position: relative;"><ul class="slides" style="width: 800%; transition-duration: 0s; transform: translate3d(0px, 0px, 0px);"><li style="width: 210px; margin-right: 5px; float: left; display: block;" data-thumb-alt=""><a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_cheesecake_brownie.jpg" draggable="false"></a></li><li data-thumb-alt="" style="width: 210px; margin-right: 5px; float: left; display: block;"> <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_lemon.jpg" draggable="false"></a></li><li data-thumb-alt="" style="width: 210px; margin-right: 5px; float: left; display: block;">  <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_donut.jpg" draggable="false"></a></li><li data-thumb-alt="" style="width: 210px; margin-right: 5px; float: left; display: block;">  <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_caramel.jpg" draggable="false"></a></li></ul></div><ol class="flex-control-nav flex-control-paging"><li><a href="#" class="flex-active">1</a></li><li><a href="#">2</a></li><li><a href="#">3</a></li><li><a href="#">4</a></li></ol><ul class="flex-direction-nav"><li class="flex-nav-prev"><a class="flex-prev flex-disabled" href="#" tabindex="-1">Previous</a></li><li class="flex-nav-next"><a class="flex-next" href="#">Next</a></li></ul></div>-->
															</li><li class="done messageScreen5"><input type="hidden" class="done messageScreen5" id="valScreen5" value="txt|test"><div class="direct-chat-msg"><div class="direct-chat-img"></div><div class="direct-chat-text">Custom Message!</div></div></li></ul>
													</div>
											  </div>
											</div>
										  </div>
										  
										  <div class="card">
											<div class="hd-ac" id="headingTwo">
											  <h2>

												  Template #2 อบรมหลักสูตร

											  </h2>
											  <div class="tools">
												  <a href="broadcasts-edit.php"><i class="fas fa-edit text-white" onclick="$('#edit-template').show();"></i></a>
												  <a href="javascript:;" onclick="$(this).parents('.hd-ac').next().remove(); $(this).parents('.hd-ac').remove(); "><i class="fas fa-trash text-white"></i></a>
												</div>
											</div>
											<div id="collapseTwo" class="pane" style="display: none">
											  <ul class="idTabs tab-mini">
													<li><a href="#tmp2" class="selected"><i class="fas fa-list nav-icon"></i> Title</a></li>
													<li><a href="#tmp2-view"><i class="fas fa-eye nav-icon"></i> live</a></li>
											  </ul>
											  <div class="card-body p-0 list-bubble">
												<ul id="tmp2" class="todo-list show ui-sortable" data-widget="todo-list">
													  <li id="msg2-1" class="done">
														<!-- drag handle -->
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<!-- checkbox -->
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo2-1" id="todoCheck2-1" checked="">
														  <label for="todoCheck2-1"></label>
														</div>
														<!-- todo text -->
														<span class="text">Design a nice theme</span>
														<!-- Emphasis label -->
														<small class="badge badge-info"><i class="fas fa-text-height"></i> Text</small>

													  </li>
													  <li id="msg2-2" class="done">
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo2-2" id="todoCheck2-2" checked="">
														  <label for="todoCheck2-2"></label>
														</div>
														<span class="text">Make the theme responsive</span>
														<small class="badge badge bg-teal"><i class="far fa-image"></i> Image</small>

													  </li>
													  <li id="msg2-3" class="done">
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo2-3" id="todoCheck2-3" checked="">
														  <label for="todoCheck2-3"></label>
														</div>
														<span class="text">Let theme shine like a star</span>
														<small class="badge badge bg-teal"><i class="far fa-image"></i> Image</small>

													  </li>
													  <li id="msg2-4" class="done">
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo2-4" id="todoCheck2-4" checked="">
														  <label for="todoCheck2-4"></label>
														</div>
														<span class="text">Let theme shine like a star</span>
														<small class="badge badge-info"><i class="fas fa-text-height"></i> Text</small>

													  </li>
													  <li id="msg2-5" class="done">
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo2-5" id="todoCheck2-5" checked="">
														  <label for="todoCheck2-5"></label>
														</div>
														<span class="text">Check your messages and notifications</span>
														<small class="badge badge bg-teal"><i class="far fa-image"></i> Image</small>

													  </li>

													</ul>
													
												<div id="tmp2-view" class="live tab-pane bg-light pl10-xs pr10-xs">
														<ul class="list-sr-chat show">

															<li class="done messageScreen1"><input type="hidden" class="done messageScreen1" id="valScreen1" value="txt|text"><div class="direct-chat-msg"><div class="direct-chat-img"></div><div class="direct-chat-text">text</div></div></li><li class="done messageScreen2"><input type="hidden" class="done messageScreen2" id="valScreen2" value="img|https://dummyimage.com/500x500/17a3b8/ffffff.png&amp;text=Meeting"><div class="direct-chat-msg"><div class="direct-chat-img"></div><div class="direct-chat-text direct-chat-image"><img class="img-fluid pad" src="https://dummyimage.com/500x500/17a3b8/ffffff.png&amp;text=Meeting"></div></div></li><li class="result-imgmap messageScreen3"><input type="hidden" class="done messageScreen3" id="valScreen3" value="img|https://dummyimage.com/500x500/17a3b8/ffffff.png&amp;text=Meeting"><div class="frame-image"><img class="img-fluid pad" src="https://dummyimage.com/500x500/17a3b8/ffffff.png&amp;text=Meeting" alt="Photo"><div class="mark-link v2"><a href="#test1" class="link-p-1" title="Action1" style="background: #ccc; opacity:.5"></a><a href="#test2" class="link-p-2" title="Action2" style="background: #aaa; opacity:.5"></a></div></div></li><li class="result-carousel messageScreen4"><div class="direct-chat-msg"><img class="direct-chat-img" src="http://ui.beurguide.net/LNS/dist/img/avatar-admin.png" alt="Message User Image"></div>
																<div class="flexslider carousel">
																 <ul class="slides">
																	<li>
																	  <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_cheesecake_brownie.jpg" /></a>
																	</li>
																	<li>
																	 <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_lemon.jpg" /></a>
																	</li>
																	<li>
																	  <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_donut.jpg" /></a>
																	</li>
																	<li>
																	 <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_caramel.jpg" /></a>
																	</li>
																  </ul>
																</div>
															<!--<div class="flexslider carousel"><div class="flex-viewport" style="overflow: hidden; position: relative;"><ul class="slides" style="width: 800%; transition-duration: 0s; transform: translate3d(0px, 0px, 0px);"><li style="width: 210px; margin-right: 5px; float: left; display: block;" data-thumb-alt=""><a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_cheesecake_brownie.jpg" draggable="false"></a></li><li data-thumb-alt="" style="width: 210px; margin-right: 5px; float: left; display: block;"> <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_lemon.jpg" draggable="false"></a></li><li data-thumb-alt="" style="width: 210px; margin-right: 5px; float: left; display: block;">  <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_donut.jpg" draggable="false"></a></li><li data-thumb-alt="" style="width: 210px; margin-right: 5px; float: left; display: block;">  <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_caramel.jpg" draggable="false"></a></li></ul></div><ol class="flex-control-nav flex-control-paging"><li><a href="#" class="flex-active">1</a></li><li><a href="#">2</a></li><li><a href="#">3</a></li><li><a href="#">4</a></li></ol><ul class="flex-direction-nav"><li class="flex-nav-prev"><a class="flex-prev flex-disabled" href="#" tabindex="-1">Previous</a></li><li class="flex-nav-next"><a class="flex-next" href="#">Next</a></li></ul></div>-->
															</li><li class="done messageScreen5"><input type="hidden" class="done messageScreen5" id="valScreen5" value="txt|test"><div class="direct-chat-msg"><div class="direct-chat-img"></div><div class="direct-chat-text">Custom Message!</div></div></li></ul>
													</div>
											  </div>
											</div>
										  </div>
										  
										  <div class="card">
											<div class="hd-ac" id="headingThree">
											  <h2>

												  Template #3 สิทธิประโยชน์พนักงาน 

											  </h2>
											  <div class="tools">
												  <a href="broadcasts-edit.php"><i class="fas fa-edit text-white" onclick="$('#edit-template').show();"></i></a>
												  <a href="javascript:;" onclick="$(this).parents('.hd-ac').next().remove(); $(this).parents('.hd-ac').remove(); "><i class="fas fa-trash text-white"></i></a>
												</div>
											</div>
											<div id="collapseThree" class="pane" style="display: none">
											  <ul class="idTabs tab-mini">
													<li><a href="#tmp3" class="selected"><i class="fas fa-list nav-icon"></i> Title</a></li>
													<li><a href="#tmp3-view"><i class="fas fa-eye nav-icon"></i> live</a></li>
											  </ul>
											   <div class="card-body p-0 list-bubble">
												<ul id="tmp3" class="todo-list show ui-sortable" data-widget="todo-list">
													  <li id="msg3-1" class="done">
														<!-- drag handle -->
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<!-- checkbox -->
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo3-1" id="todoCheck3-1" checked="">
														  <label for="todoCheck3-1"></label>
														</div>
														<!-- todo text -->
														<span class="text">Design a nice theme</span>
														<!-- Emphasis label -->
														<small class="badge badge-info"><i class="fas fa-text-height"></i> Text</small>

													  </li>
													  <li id="msg2-2" class="done">
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo3-2" id="todoCheck3-2" checked="">
														  <label for="todoCheck3-2"></label>
														</div>
														<span class="text">Make the theme responsive</span>
														<small class="badge badge bg-teal"><i class="far fa-image"></i> Image</small>

													  </li>
													  <li id="msg2-3" class="done">
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo3-3" id="todoCheck3-3" checked="">
														  <label for="todoCheck3-3"></label>
														</div>
														<span class="text">Let theme shine like a star</span>
														<small class="badge badge bg-teal"><i class="far fa-image"></i> Image</small>

													  </li>
													  <li id="msg2-4" class="done">
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo3-4" id="todoCheck3-4" checked="">
														  <label for="todoCheck3-4"></label>
														</div>
														<span class="text">Let theme shine like a star</span>
														<small class="badge badge-info"><i class="fas fa-text-height"></i> Text</small>

													  </li>
													  <li id="msg2-5" class="done">
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo3-5" id="todoCheck3-5" checked="">
														  <label for="todoCheck3-5"></label>
														</div>
														<span class="text">Check your messages and notifications</span>
														<small class="badge badge bg-teal"><i class="far fa-image"></i> Image</small>

													  </li>

													</ul>
													
													<div id="tmp3-view" class="live tab-pane bg-light pl10-xs pr10-xs">
														<ul class="list-sr-chat show">

															<li class="done messageScreen1"><input type="hidden" class="done messageScreen1" id="valScreen1" value="txt|text"><div class="direct-chat-msg"><div class="direct-chat-img"></div><div class="direct-chat-text">text</div></div></li><li class="done messageScreen2"><input type="hidden" class="done messageScreen2" id="valScreen2" value="img|https://dummyimage.com/500x500/17a3b8/ffffff.png&amp;text=Meeting"><div class="direct-chat-msg"><div class="direct-chat-img"></div><div class="direct-chat-text direct-chat-image"><img class="img-fluid pad" src="https://dummyimage.com/500x500/17a3b8/ffffff.png&amp;text=Meeting"></div></div></li><li class="result-imgmap messageScreen3"><input type="hidden" class="done messageScreen3" id="valScreen3" value="img|https://dummyimage.com/500x500/17a3b8/ffffff.png&amp;text=Meeting"><div class="frame-image"><img class="img-fluid pad" src="https://dummyimage.com/500x500/17a3b8/ffffff.png&amp;text=Meeting" alt="Photo"><div class="mark-link v2"><a href="#test1" class="link-p-1" title="Action1" style="background: #ccc; opacity:.5"></a><a href="#test2" class="link-p-2" title="Action2" style="background: #aaa; opacity:.5"></a></div></div></li><li class="result-carousel messageScreen4"><div class="direct-chat-msg"><img class="direct-chat-img" src="http://ui.beurguide.net/LNS/dist/img/avatar-admin.png" alt="Message User Image"></div>
																<div class="flexslider carousel">
																 <ul class="slides">
																	<li>
																	  <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_cheesecake_brownie.jpg" /></a>
																	</li>
																	<li>
																	 <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_lemon.jpg" /></a>
																	</li>
																	<li>
																	  <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_donut.jpg" /></a>
																	</li>
																	<li>
																	 <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_caramel.jpg" /></a>
																	</li>
																  </ul>
																</div>
															<!--<div class="flexslider carousel"><div class="flex-viewport" style="overflow: hidden; position: relative;"><ul class="slides" style="width: 800%; transition-duration: 0s; transform: translate3d(0px, 0px, 0px);"><li style="width: 210px; margin-right: 5px; float: left; display: block;" data-thumb-alt=""><a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_cheesecake_brownie.jpg" draggable="false"></a></li><li data-thumb-alt="" style="width: 210px; margin-right: 5px; float: left; display: block;"> <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_lemon.jpg" draggable="false"></a></li><li data-thumb-alt="" style="width: 210px; margin-right: 5px; float: left; display: block;">  <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_donut.jpg" draggable="false"></a></li><li data-thumb-alt="" style="width: 210px; margin-right: 5px; float: left; display: block;">  <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_caramel.jpg" draggable="false"></a></li></ul></div><ol class="flex-control-nav flex-control-paging"><li><a href="#" class="flex-active">1</a></li><li><a href="#">2</a></li><li><a href="#">3</a></li><li><a href="#">4</a></li></ol><ul class="flex-direction-nav"><li class="flex-nav-prev"><a class="flex-prev flex-disabled" href="#" tabindex="-1">Previous</a></li><li class="flex-nav-next"><a class="flex-next" href="#">Next</a></li></ul></div>-->
															</li><li class="done messageScreen5"><input type="hidden" class="done messageScreen5" id="valScreen5" value="txt|test"><div class="direct-chat-msg"><div class="direct-chat-img"></div><div class="direct-chat-text">Custom Message!</div></div></li></ul>
													</div>
											  </div>
											</div>
										  </div>
										</div>
									<div id="newpush" class="bg-white ps-rlt _self-pa10 cb-af rounded1" style="display: none">
										<!-- Tab -->
										<ul class="msg-tab idTabs bg-light">
											<li class="nav-item">
											<a class="selected" id="vert-tabs-text-tab" href="#vert-tabs-text">
												<span class="btn bg-info mb-0 mr-1"><i class="fas fa-text-height"></i></span>
												<div>Text</div></a>
											</li>


										  <li class="nav-item">
											  <a class="nav-link" id="vert-tabs-qreply-tab" href="#vert-tabs-qreply">
												<span class="btn bg-purple mb-0 mr-1"><i class="fas fa-reply"></i></span>
												<div>Reply</div></a>
										  </li>

										  <li class="nav-item">
											  <a class="nav-link" id="vert-tabs-button-tab" href="#vert-tabs-button">
												<span class="btn bg-primary mb-0 mr-1"><i class="fas fa-user-clock"></i></span>
												<div>Button</div></a>
										  </li>

										  <li class="nav-item m-adv">
											<button type="button" class="bg-gray btn-sm dropdown-toggle">
												  <i class="fas fa-ellipsis-v"></i></button>
												<a class="nav-link" id="vert-tabs-payload-tab" href="#vert-tabs-payload" aria-controls="vert-tabs-payload" aria-selected="false">
														<span class="btn bg-olive mb-0 mr-1"><i class="fas fa-comment-dots"></i></span>
														Payload</a>
										  </li>
										</ul>


										<div class="tab-content" id="vert-tabs-tabContent">
										  <div class="tab-pane show" id="vert-tabs-text" >

											<p class="lead mb-0">Properties</p>
											 <div class="tab-custom-content">
																			  <!-- textarea -->
												  <div class="form-group">
													<label for="message_text">Your Message</label>
													<textarea class="form-control" id="message_text" rows="3" placeholder="Enter ..."></textarea>
													<textarea class="form-control" id="message_edittext" rows="3" placeholder="Enter ..." style="display: none;"></textarea>
												  </div>
												  <!-- Link -->

											</div>

											<div class="tab-footer clearfix">
												<button type="button" id="displaytxt" class="ui-btn  btn-info btn-lg float-right btn-add-msg">Display</button>
											</div>
										  </div>

										  <!-- Quick reply -->
										  <div class="tab-pane fade" id="vert-tabs-qreply" role="tabpanel" aria-labelledby="vert-tabs-qreply-tab">

											<p class="lead mb-0">Properties</p>
											 <div class="tab-custom-content">
												  <!-- textarea -->
												  <div class="form-group">
													<label for="message_text">Your Message</label>
													<textarea class="form-control" id="message_replay_text" rows="3" placeholder="Enter ..."></textarea>
													<!--<textarea class="form-control" id="message_replay_edittext" rows="3" placeholder="Enter ..." style="display: none;"></textarea>-->
												  </div>
											</div>

											<div class="form-group">
													<label>Number of Actions</label>
													<select class="form-control" id="select-replay-action">
													  <option value="0">1</option>
													  <option value="1">2</option>
													  <option value="2">3</option>
													  <option value="3">4</option>
													  <option value="4">5</option>
													  <option value="5">6</option>
													</select>
												</div>
												<div class="main-reply-action bg-light pa10-xs mb10-xs">
																					<div id="replay-action1">
														<div class="form-group">
															<label>Reply Action1</label>
															<div class="row middle-xs mb10-xs">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Type</span>
																<div class="col">
																	<select class="form-control" id="select-tpye-action1">
																	  <option value="0">Select Action</option>
																	  <option value="1" selected>Text</option>
																	  <option value="2">Location</option>
																	  <option value="3">Camera</option>
																	  <!--<option value="4">Video</option>
																	  <option value="5">Audio</option>
																	  <option value="6">Location</option>
																	  <!--<option value="7">Imagemap</option>
																	  <option value="8">Template</option>-->
																	</select>
																</div>
															</div>

															<div class="row middle-xs mb10-xs">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Label</span>
																<div class="col">
																	<input type="text" class="form-control" id="labelQreply1" placeholder="Action 1">
																</div>
															</div>


															<div class="row middle-xs mb10-xs">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Action</span>
																<div class="col">
																	<input type="text" class="form-control" id="actionQreply1" placeholder="Action 1">
																</div>
															</div>
														</div>
													</div>
																					<div id="replay-action2">
														<div class="form-group">
															<label>Reply Action2</label>
															<div class="row middle-xs mb10-xs">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Type</span>
																<div class="col">
																	<select class="form-control" id="select-tpye-action2">
																	  <option value="0">Select Action</option>
																	  <option value="1" selected>Text</option>
																	  <option value="2">Location</option>
																	  <option value="3">Camera</option>
																	  <!--<option value="4">Video</option>
																	  <option value="5">Audio</option>
																	  <option value="6">Location</option>
																	  <!--<option value="7">Imagemap</option>
																	  <option value="8">Template</option>-->
																	</select>
																</div>
															</div>

															<div class="row middle-xs mb10-xs">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Label</span>
																<div class="col">
																	<input type="text" class="form-control" id="labelQreply2" placeholder="Action 2">
																</div>
															</div>


															<div class="row middle-xs mb10-xs">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Action</span>
																<div class="col">
																	<input type="text" class="form-control" id="actionQreply2" placeholder="Action 2">
																</div>
															</div>
														</div>
													</div>
																					<div id="replay-action3">
														<div class="form-group">
															<label>Reply Action3</label>
															<div class="row middle-xs mb10-xs">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Type</span>
																<div class="col">
																	<select class="form-control" id="select-tpye-action3">
																	  <option value="0">Select Action</option>
																	  <option value="1" selected>Text</option>
																	  <option value="2">Location</option>
																	  <option value="3">Camera</option>
																	  <!--<option value="4">Video</option>
																	  <option value="5">Audio</option>
																	  <option value="6">Location</option>
																	  <!--<option value="7">Imagemap</option>
																	  <option value="8">Template</option>-->
																	</select>
																</div>
															</div>

															<div class="row middle-xs mb10-xs">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Label</span>
																<div class="col">
																	<input type="text" class="form-control" id="labelQreply3" placeholder="Action 3">
																</div>
															</div>


															<div class="row middle-xs mb10-xs">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Action</span>
																<div class="col">
																	<input type="text" class="form-control" id="actionQreply3" placeholder="Action 3">
																</div>
															</div>
														</div>
													</div>
																					<div id="replay-action4">
														<div class="form-group">
															<label>Reply Action4</label>
															<div class="row middle-xs mb10-xs">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Type</span>
																<div class="col">
																	<select class="form-control" id="select-tpye-action4">
																	  <option value="0">Select Action</option>
																	  <option value="1" selected>Text</option>
																	  <option value="2">Location</option>
																	  <option value="3">Camera</option>
																	  <!--<option value="4">Video</option>
																	  <option value="5">Audio</option>
																	  <option value="6">Location</option>
																	  <!--<option value="7">Imagemap</option>
																	  <option value="8">Template</option>-->
																	</select>
																</div>
															</div>

															<div class="row middle-xs mb10-xs">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Label</span>
																<div class="col">
																	<input type="text" class="form-control" id="labelQreply4" placeholder="Action 4">
																</div>
															</div>


															<div class="row middle-xs mb10-xs">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Action</span>
																<div class="col">
																	<input type="text" class="form-control" id="actionQreply4" placeholder="Action 4">
																</div>
															</div>
														</div>
													</div>
																					<div id="replay-action5">
														<div class="form-group">
															<label>Reply Action5</label>
															<div class="row middle-xs mb10-xs">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Type</span>
																<div class="col">
																	<select class="form-control" id="select-tpye-action5">
																	  <option value="0">Select Action</option>
																	  <option value="1" selected>Text</option>
																	  <option value="2">Location</option>
																	  <option value="3">Camera</option>
																	  <!--<option value="4">Video</option>
																	  <option value="5">Audio</option>
																	  <option value="6">Location</option>
																	  <!--<option value="7">Imagemap</option>
																	  <option value="8">Template</option>-->
																	</select>
																</div>
															</div>

															<div class="row middle-xs mb10-xs">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Label</span>
																<div class="col">
																	<input type="text" class="form-control" id="labelQreply5" placeholder="Action 5">
																</div>
															</div>


															<div class="row middle-xs mb10-xs">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Action</span>
																<div class="col">
																	<input type="text" class="form-control" id="actionQreply5" placeholder="Action 5">
																</div>
															</div>
														</div>
													</div>
																					<div id="replay-action6">
														<div class="form-group">
															<label>Reply Action6</label>
															<div class="row middle-xs mb10-xs">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Type</span>
																<div class="col">
																	<select class="form-control" id="select-tpye-action6">
																	  <option value="0">Select Action</option>
																	  <option value="1" selected>Text</option>
																	  <option value="2">Location</option>
																	  <option value="3">Camera</option>
																	  <!--<option value="4">Video</option>
																	  <option value="5">Audio</option>
																	  <option value="6">Location</option>
																	  <!--<option value="7">Imagemap</option>
																	  <option value="8">Template</option>-->
																	</select>
																</div>
															</div>

															<div class="row middle-xs mb10-xs">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Label</span>
																<div class="col">
																	<input type="text" class="form-control" id="labelQreply6" placeholder="Action 6">
																</div>
															</div>


															<div class="row middle-xs mb10-xs">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Action</span>
																<div class="col">
																	<input type="text" class="form-control" id="actionQreply6" placeholder="Action 6">
																</div>
															</div>
														</div>
													</div>
																			</div>

											<div class="tab-footer clearfix">
												<button type="button" id="display-Qreply" class="ui-btn  btn-info btn-lg float-right btn-add-qreply">Display</button>
											</div>
										  </div>

										  <div class="tab-pane fade" id="vert-tabs-button">

											<p class="lead mb-0">Properties</p>
						 <div class="tab-custom-content">
						 
						 	<div id="main-apm" class="main-imagemap wrap-upload mb-4">
								<div class="form-group p-0 mb10-xs">
									<label>Select Type Image</label>
									<select class="custom-select" id="select-apmimage-type">
									  <option value="0">URL</option>
									  <option value="1">Local file</option>
									</select>
								 </div>

								<div id="f-url_apm" class="f-url form-group">  
									<label for="apm_urlimg2">From image URL</label>
									<div class="input-group mb-3">
									  <div class="input-group-prepend">
										<span class="input-group-text"><i class="fas fa-link"></i></span>
									  </div>
									  <input type="text" class="form-control" placeholder="URL Image" id="apm_urlimg2">
									</div>
								</div>

								<div id="f-upload_apm" class="f-upload form-group" style="display: none">
									<label for="apm_img2">Upload image</label>
									<div class="input-group">
											<div class="custom-file">
												<input type="file" class="custom-file-input2" id="apm_img2" accept=".jpg,.jpeg,.png">
												<img id="temp_apmimage_src" hidden="">
												<label class="custom-file-label" id="apm_labelimg2" for="apm_img2"><p>Choose file</p></label>
											</div>
											<div class="input-group-append">
												<button class="input-group-text" id="apm_uploadimg2">Upload</button>
											</div>

									</div>
								 </div>
							</div>
							
						 	 <div class="form-group">
								<input type="text" class="form-control" id="titleApm" placeholder="Title">
							</div>
							
							<div class="form-group  mb-1">
								<textarea id="textApm" class="form-control mb10-xs" rows="2" placeholder="Description"></textarea>
								<textarea id="textApm2" class="form-control" rows="2" placeholder="Description 2"></textarea>
								<!--<input type="text" class="form-control" id="textApm" placeholder="Text">-->
							</div>
							
							<!--<div class="check-mirosite">
								<div class="icheck-primary chk-tpl text-right mb-0"><input type="checkbox" id="has-link" class="has-link"> <label for="has-link">Create Link Microsite</label></div>
							</div>
							 <div id="create-msite" class="form-group mb10-xs" style="display: none">
								<label>Consent Form</label>
								<textarea id="textConsent" class="form-control" rows="6" placeholder="การขอความยินยอม" onKeyPress="$('#btn-view-micro').removeClass('disabled').removeClass('btn-outline-secondary').addClass('bg-gray')"></textarea>
								
								<div class="mt-1 d-flex justify-content-start">
									<a id="btn-view-micro" href="javascript:;" data-toggle="modal" data-target="#modal-consent" title="Link to microsite" class="btn btn-xs disabled btn-outline-secondary"><i class="far fa-sticky-note"></i> View Microsite</a>
								</div>
							</div>-->
							
							<div class="form-group">
								<label>Number of Actions</label>
								<select class="form-control select-action-chd" id="select-apmNum-action">
								  <option value="0">1</option>
								  <option value="1">2</option>
								  <option value="2">3</option>
								</select>
							</div>
							
							<div class="main-apm-action bg-light pa10-xs mb10-xs">
								<div id="group-a1" class="g-action1">
									<div class="form-group">
										<label>Action1</label>
										<div class="row align-items-center mb10-xs">
											<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Type</span>
											<div class="col">
												<select class="form-control" id="select-apmNum-action1">
												  <option value="0">Message Action</option>
												  <option value="5" selected>Consent Form</option>
												  <option value="1">URI Action</option>
												  <option value="2">Postback Action</option>
												  <option value="2">Web App Action</option>
												  <option value="2">Datetime Picker Action</option>
												</select>
											</div>
										</div>

										<div class="row align-items-center mb10-xs">
											<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Label</span>
											<div class="col">
												<input type="text" class="form-control" id="actionApm1" placeholder="ชื่อปุ่มเงื่อนไข" value="เงื่อนไขและรายละเอียด">
											</div>
										</div>

										<!--<div class="row align-items-center mb10-xs">
											<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Text</span>
											<div class="col">
												<input type="text" class="form-control" id="actionLinkApm1" placeholder="Action 1">
											</div>
										</div>-->
										
										<div class="row align-items-starts mb10-xs">
											<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0 pt-1">Detail</span>
											<div class="col">
												<textarea id="textConsent" class="form-control w-100" rows="6" placeholder="การขอความยินยอม" onKeyPress="$('#btn-view-micro').removeClass('disabled').removeClass('btn-outline-secondary').addClass('bg-gray')" onClick="$('#btn-view-micro').removeClass('disabled').removeClass('btn-outline-secondary').addClass('bg-gray')"></textarea>

												<div class="mt10-xs d-flex justify-content-start">
													<a id="btn-view-micro" href="javascript:;" data-fancybox="consent" data-modal="modal" data-src="#modal-consent" title="Link to microsite" class="ui-btn-border btn-xs disabled"><i class="far fa-sticky-note"></i> View Microsite</a>
												</div>
											</div>
										</div>
										
										
									</div>
								</div>
								
								<div id="group-a2" class="g-action2">
									<div class="form-group">
										<label>Action2</label>
										<div class="row align-items-center mb10-xs">
											<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Type</span>
											<div class="col">
												<select class="form-control" id="select-apmNum-action1">
												  <option value="0">Message Action</option>
												  <option value="1" selected>URI Action</option>
												  <option value="2">Postback Action</option>
												  <option value="2">Web App Action</option>
												  <option value="2">Datetime Picker Action</option>
												</select>
											</div>
										</div>

										<div class="row align-items-center mb10-xs">
											<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Label</span>
											<div class="col">
												<input type="text" class="form-control" id="actionApm2" placeholder="Action 2">
											</div>
										</div>


										<div class="row align-items-center mb10-xs">
											<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">URI</span>
											<div class="col">
												<input type="text" class="form-control" id="actionLinkApm2" placeholder="URI">
											</div>
										</div>
									</div>
								</div>
								
								<div id="group-a3" class="g-action3">
									<div class="form-group">
										<label>Action3</label>
										<div class="row align-items-center mb10-xs">
											<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Type</span>
											<div class="col">
												<select class="form-control" id="select-apmNum-action1">
												  <option value="0">Message Action</option>
												  <option value="1" selected>URI Action</option>
												  <option value="2">Postback Action</option>
												  <option value="2">Web App Action</option>
												  <option value="2">Datetime Picker Action</option>
												</select>
											</div>
										</div>

										<div class="row align-items-center mb10-xs">
											<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Label</span>
											<div class="col">
												<input type="text" class="form-control" id="actionApm3" placeholder="Action 3">
											</div>
										</div>


										<div class="row align-items-center mb10-xs">
											<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">URI</span>
											<div class="col">
												<input type="text" class="form-control" id="actionLinkApm3" placeholder="URI">
											</div>
										</div>
									</div>
								</div>
								
							</div>
							
							
							
					     </div>

											 <div class="tab-footer clearfix _self-mt20">
												<button type="button" id="displayCard" class="ui-btn  btn-info btn-lg float-right btn-add-appointment">Display</button>
											</div>

										  </div>

										  <div class="tab-pane fade" id="vert-tabs-payload">

											<p class="lead mb-0">Custom text</p>
											 <div class="tab-custom-content">
												 <div class="form-group">
													<label>Title</label>
													<input type="text" class="form-control" id="payloadtitle" placeholder="Title">
												</div>
												  <!-- textarea -->
												  <div class="form-group">
													<label>JSON Code</label>
													<textarea class="form-control" id="myCustomText" rows="10" placeholder="Paste code here ..."></textarea>
												  </div>
												  <!-- Link -->

											</div>

											<div class="tab-footer clearfix">
												<button type="button" id="displaypayload" class="ui-btn  btn-info btn-lg float-right btn-add-payload">Display</button>
											</div>
										  </div>
										</div>
										
										<div class="list-bubble">
											<p class="lead mb-0 pt-3">Sortable <small class="text-muted">(Maximun 3 messages)</small></p>
											<div class="tab-custom-content">
											<ul class="todo-list" data-widget="todo-list">


											</ul>
											
											<div class="save-template">
												<div class="icheck-primary chk-tpl text-right mb-0"><input type="checkbox" id="save-tp" class="save-as"> <label for="save-tp">save as template</label></div>
												<div class="set-name-tp" style="display: none">
														<p class="lead mb-0">Properties</p>
														<div class="tab-custom-content">
															 <div class="form-group">
																<label>Template Name</label>
																<div class="input-group mb-3">
																  <input type="text" class="form-control" id="tpl-name" placeholder="กรุณาตั้งชื่อ Template ค่ะ" onKeyPress="$('#btn-save-tpl').prop('disabled',false);">
																  <div class="input-group-append">
																	<button class="ui-btn-sq btn-sm btn bg-info t-white" type="button" id="btn-save-tpl" disabled onClick="$(this).children('i').removeClass('hid'); $('#btn-save-tpl').prop('disabled',true); $('#tpl-name').prop('readonly',true);"><i class="hid fas fa-check-circle"></i> Save</button>
																  </div>
																</div>
															</div>
														</div>
												</div>
											</div>
										</div>
									  </div>
					
									</div>
									</div>
								</div>
								<!-- /card -->
								
								<!-- card -->
								<div class="card _self-mt30">
									<div class="card-header">
										<h3 class="card-title"><!--<span class="step ui-btn-green2 _self-ph10-mr10">ขั้นตอนที่ 3</span>--> <i class="nav-icon fas fa-calendar-alt"></i> Schedule</h3>
									</div>
									<div class="card-body bg-white  pa10-xs">
									
									<div class="form-group">
								<div class="bg-light pa10-xs rounded">
								  <div class="form-group clearfix">
								  <div class="_self-mb10">
									  <div class="icheck-primary">
										<input type="radio" id="radioPrimary1" name="chk-send" value="now" checked="">
										<label for="radioPrimary1">
											ส่งตอนนี้
										</label>
									  </div>
								  </div>
								  <div class="_self-mb10">
									  <div class="icheck-primary">
										<input type="radio" id="radioPrimary2" name="chk-send" value="set">
										<label for="radioPrimary2">
											ตั้งค่าวันที่/เวลาส่ง
										</label>
									  </div>
								  </div>
								  

								</div>

								<div id="show-schedule" class="row" style="display: none">
								<div class="col-sm-6">
									<div class="form-group">
										<label for="datepicker">ตั้งค่าวันที่</label>
										<div class="_flex nowrap middle-xs date" id="timepicker" data-target-input="nearest">
											<input class="txt-box" type="text" id="datepicker">
											<i class="_self-ml10 far fa-calendar" onClick="$('#datepicker').focus();"></i>
										</div>
									 </div>
									 <!--<div class="txt-c" style="margin-top: -15px">
										<div id="calendarshow" class="w-100"></div>
									 </div>-->
								</div>

								<div class="col-sm-6 d-flex flex-column between-xs">

									<!-- time Picker -->
									<div class="bootstrap-timepicker">
									  <div class="form-group">
										<label>ตั้งค่าเวลา</label>

										<div class="_flex nowrap middle-xs date" id="timepicker" data-target-input="nearest">
										  <!--<input class="txt-box" type="time" id="appt" name="appt">-->
										  <input class="txt-box" type="text" id="time" placeholder="Time">
										  <i class="_self-ml10 far fa-clock" onClick="$('#time').focus();"></i>
										</div>
										<!-- /.input group -->
									  </div>
									  <!-- /.form group -->
									</div>





								</div>
								</div>

								<div class="mt20 _chd-ph10 center-xs">
									<button type="button" class="ui-btn-gray btn-lg" data-toggle="modal" data-target="#modal-close">Cancel</button>
									<button type="submit" class="ui-btn-green btn-lg swalDefaultSuccess" onclick="$(this).children('i').removeClass('hid'); $('.form-sending')[0].reset();"><i class="hid fas fa-circle-notch fa-spin"></i> Send Message</button>
									<!-- <button type="submit" class="btn btn-danger btn-lg toastrDefaultError mr-2">Send Unsuccessful</button> -->
								</div>

								</div>
									
									</div>
								</div>
								<!-- /card -->
								
							</div>
								<!-- /card -->
							</div>
							<div class="col-c hid _self-cl-sm-07-md-07-lg-08">
								<!-- card -->
								<div class="card">
									<div class="card-header d-flex between-xs">
										<h3 class="card-title"><!--<span class="step ui-btn-green2 _self-ph10-mr10">ขั้นตอนที่ 2</span>--> <i class="nav-icon fas fa-user-check"></i> User Detail</h3>
										<div class="end-call txt-r">
											<button class="ui-btn-red btn-md rounded" onclick="$('.main>.col-l').addClass('hid'); $('.main>.col-c').addClass('hid'); $('#UserCaller').removeClass('hid');"><i class="fas fa-phone-slash fa-rotate-90"></i> End Call</button>
											<div class="remain">
													Remaining Messages <input class="showRmUID" type="text" id="countRmainUID" value="34920" readonly>/month
												</div>
										</div>
									</div>
									<div class="card-body">
										<!--<ul class="idTabs tab-receiver">
											<li><a href="#linegroup"><i class="fas fa-users"></i> Line Groups</a></li>
											<li><a href="#users"><i class="fas fa-user"></i> Users</a></li>
										</ul>-->
										<div class="contentTabs">
											
											<div id="UserCaller" class="card card-widget widget-user-2">
											  <!-- Add the bg color to the header using any of the bg-* classes -->
											  <div class="widget-user-header bg-green d-flex between-xs">
											   <div class="main-user">
													<div class="widget-user-image">
													  <img class="img-circle elevation-2" src="https://www.w3schools.com/w3images/avatar4.png" alt="User Avatar">
													</div>
													<!-- /.widget-user-image -->
													<h3 class="widget-user-username">Nadia Carmichael <a href="javascript:;" data-fancybox="" data-modal="modal" data-src="#modal-edit-name" title="edit"><i class="fas fa-edit text-sm t-white"></i></a></h3>
													<h5 class="widget-user-desc">LINE ID : Nadia99</h5>
												</div>
												<div class="call-in txt-r">
													<big class="f-normal">025026888</big> 
													<small class="d-block text-black-50 text-right"><i class="fas fa-phone fa-rotate-90"></i> Call in</small>
													<!--<div class="end-call mt-2"><button class="btn-danger border-dark"><i class="fas fa-phone-slash fa-rotate-90"></i> End Call</button></div>-->
												</div>

											  </div>
											  <div class="card-footer border-bottom info-call2line _self-pb0">
												<div class="row">
												  <div class="col-sm-4 border-right">
													<div class="description-block">
													  <h5 class="description-header">Tel <a href="javascript:;" data-fancybox="" data-modal="modal" data-src="#modal-edit-user" title="edit"><i class="far fa-edit text-sm text-muted"></i></a></h5>
													  <big class="description-text">0899599955</big>
													</div>
													<!-- /.description-block -->
												  </div>
												  <!-- /.col -->
												  <div class="col-sm-4 border-right">
													<div class="description-block">
													  <h5 class="description-header">User ID <a href="javascript:;" data-fancybox="" data-modal="modal" data-src="#modal-edit-user" title="edit"><i class="far fa-edit text-sm text-muted"></i></a></h5>
													  <big class="description-text"><span class="d-block">Ub8c6ef4a7880161aa490bd7bd11d0133</span></big>
													</div>
													<!-- /.description-block -->
												  </div>
												  <!-- /.col -->
												  <div class="col-sm-4">
													<div class="description-block">
													  <h5 class="description-header">Last Call <a href="javascript:;" data-fancybox="" data-modal="modal" data-src="#modal-history" title="History"><i class="fas fa-history text-muted"></i></a></h5>
													  <span class="description-text">20/07/2020<small class="d-block" style="margin-top: -5px">12.00 น.</small></span>
													</div>
													<!-- /.description-block -->
												  </div>
												  <!-- /.col -->
												</div>
												<!-- /.row -->
											  </div>
											<div class="card-footer card-footer _self-pt0">
												<ul class="nav flex-column">
												  <li class="nav-item">
													<a href="javascript:;" onclick="$('.list-sr-chat').addClass('show'); /*$(this).children('.status-send').addClass('hid')*/;$(this).children('.status-success').removeClass('hid');" class="nav-link text-dark">
													  Request Location 
													  <span class="status-send float-right badge bg-primary ml5-xs"><i class="fas fa-paper-plane"></i> Send</span>
													  <span class="status-success float-right badge text-success ml5-xs hid"><i class="fas fa-check-circle"></i> Success</span>
													</a>
												  </li>
												  <li class="nav-item">
													<a href="javascript:;" data-fancybox="" data-modal="modal" data-src="#modal-map" onclick="$('.list-sr-chat').addClass('show'); /*$(this).children('.status-send').addClass('hid')*/;$(this).children('.status-success').removeClass('hid');" class="nav-link text-dark">
													  Request Accident Location 
													  <span class="status-send float-right badge bg-primary ml5-xs"><i class="fas fa-paper-plane"></i> Send</span>
													  <span class="status-success float-right badge text-success ml5-xs hid"><i class="fas fa-check-circle"></i> Success</span>
													  <span class="float-right badge bg-gray ml5-xs"><i class="fas fa-map-marker-alt"></i> Current location</span> 
													</a>
												  </li>
												  <li class="nav-item">
													<a href="javascript:;" onclick="$('.list-sr-chat').addClass('show'); $(this).children('.status-send').addClass('hid');$(this).children('.status-success,.status-accept').removeClass('hid');" class="nav-link text-dark">
													  Consent Message
													  <span class="status-send float-right badge bg-primary ml5-xs"><i class="fas fa-paper-plane"></i> Send</span>
													  <span class="status-success float-right badge bg-success ml5-xs hid"><i class="fas fa-check-circle"></i> Success</span>
													  <span class="status-accept float-right badge bg-gray2 ml5-xs hid"><i class="fas fa-check"></i> Accepted</span> 
													</a>
												  </li>
												  <li class="nav-item">
													<a href="#" class="nav-link text-dark">
													  Link Microsite Consent 
													  <span class="float-right badge bg-success ml5-xs"><i class="fas fa-check-circle"></i> Success</span>
													  <span class="status-accept float-right badge bg-gray2 ml5-xs"><i class="fas fa-check"></i> Accepted</span> 
													</a>
												  </li>
												  <!--<li class="nav-item">
													<a href="#" class="nav-link">
													  Followers <span class="float-right badge bg-danger">842</span>
													</a>
												  </li>-->
												</ul>
												<div class="bg-gray2 rounded-2 pa5-xs mt10-xs txt-c">
													<a href="call2line-create-adv.php" class="ui-btn-border btn-xs rounded-2"><i class="fas fa-edit"></i> Edit Quick menu</a>
												</div>
											  </div>
											</div>
											
											<?php /*?>
											<div id="phoneNum" class="form-group _self-pa30 bg-white rounded1">
												<div class="icheck-primary chk-mkt mb10-xs"><input type="checkbox" id="type-mkt" class="rcv-addfriend"> <label for="type-mkt">Type Marketing <small class="ml-2">(Send messages to numbers have added friends only.)</small></label></div>
	
												<label>Add Mobile Number:</label>


												<div class="js-select wr">
													<div class="input-group">
													  <div class="col-md-7 col-lg-8 pa0-xs 0">
													  <div class="input-group js-tagator">
														<div class="input-group-prepend">
														  <span class="input-group-text"><i class="fas fa-phone"></i></span>
														</div>
														<input type="text" id="activate_tagator2" class="form-control tagator" value="0899599955" data-tagator-show-all-options-on-focus="true" data-tagator-autocomplete="['0899599955', '0844445566']" placeholder="Please input mobile numbers">
													  </div>
													  </div>
													  <div class="col-md-5 col-lg-4 input-group-append">
														<div class="custom-file upload-xls">
														  <input type="file" class="custom-file-input" id="customFile">
														  <label class="custom-file-label text-xs" for="customFile">or Upload Excell</label>
														</div>
													  </div>
													</div>

													
													
													<div class="info-container">     <small class="info text-muted">Mulitiple Number : 081-234-4567, 089-123-4567 <span class="text-danger">"Comma"</span> or <span class="text-danger">"Enter"</span></small> </div>
												</div>
												
												
												<div class="mt10-ปห text-center">
													<button id="number-check" type="button" class="ui-btn-green btn-block btn-md hid"><i class="fas fa-check-double"></i> Check add friends</button>
												</div>
												
												<div id="showPhone" class="status-freinds between-xs" style="display: none">
													<div class="added col">
														<h3 class="bg-green pa10-xs rounded"><i class="fas fa-check-circle"></i> Can send messages <small class="t-white">(60%)</small></h3>
														<ul>
																							<li><i class="fas fa-user-plus text-success"></i> 0839599955</li>
															<li><i class="fas fa-user-plus text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-plus text-success"></i> 0811414170</li>
																							<li><i class="fas fa-user-plus text-success"></i> 0879599955</li>
															<li><i class="fas fa-user-plus text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-plus text-success"></i> 0811414170</li>
																							<li><i class="fas fa-user-plus text-success"></i> 0859599955</li>
															<li><i class="fas fa-user-plus text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-plus text-success"></i> 0811414170</li>
																							<li><i class="fas fa-user-plus text-success"></i> 0879599955</li>
															<li><i class="fas fa-user-plus text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-plus text-success"></i> 0811414170</li>
																							<li><i class="fas fa-user-plus text-success"></i> 0859599955</li>
															<li><i class="fas fa-user-plus text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-plus text-success"></i> 0811414170</li>
																							<li><i class="fas fa-user-plus text-success"></i> 0849599955</li>
															<li><i class="fas fa-user-plus text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-plus text-success"></i> 0811414170</li>
																							<li><i class="fas fa-user-plus text-success"></i> 0899599955</li>
															<li><i class="fas fa-user-plus text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-plus text-success"></i> 0811414170</li>
																							<li><i class="fas fa-user-plus text-success"></i> 0839599955</li>
															<li><i class="fas fa-user-plus text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-plus text-success"></i> 0811414170</li>
																							<li><i class="fas fa-user-plus text-success"></i> 0879599955</li>
															<li><i class="fas fa-user-plus text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-plus text-success"></i> 0811414170</li>
																							<li><i class="fas fa-user-plus text-success"></i> 0899599955</li>
															<li><i class="fas fa-user-plus text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-plus text-success"></i> 0811414170</li>
																							<li><i class="fas fa-user-plus text-success"></i> 0899599955</li>
															<li><i class="fas fa-user-plus text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-plus text-success"></i> 0811414170</li>
																							<li><i class="fas fa-user-plus text-success"></i> 0879599955</li>
															<li><i class="fas fa-user-plus text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-plus text-success"></i> 0811414170</li>
																							<li><i class="fas fa-user-plus text-success"></i> 0869599955</li>
															<li><i class="fas fa-user-plus text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-plus text-success"></i> 0811414170</li>
																							<li><i class="fas fa-user-plus text-success"></i> 0839599955</li>
															<li><i class="fas fa-user-plus text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-plus text-success"></i> 0811414170</li>
																							<li><i class="fas fa-user-plus text-success"></i> 0859599955</li>
															<li><i class="fas fa-user-plus text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-plus text-success"></i> 0811414170</li>
																							<li><i class="fas fa-user-plus text-success"></i> 0879599955</li>
															<li><i class="fas fa-user-plus text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-plus text-success"></i> 0811414170</li>
																							<li><i class="fas fa-user-plus text-success"></i> 0869599955</li>
															<li><i class="fas fa-user-plus text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-plus text-success"></i> 0811414170</li>
																							<li><i class="fas fa-user-plus text-success"></i> 0859599955</li>
															<li><i class="fas fa-user-plus text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-plus text-success"></i> 0811414170</li>
																							<li><i class="fas fa-user-plus text-success"></i> 0879599955</li>
															<li><i class="fas fa-user-plus text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-plus text-success"></i> 0811414170</li>
																							<li><i class="fas fa-user-plus text-success"></i> 0859599955</li>
															<li><i class="fas fa-user-plus text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-plus text-success"></i> 0811414170</li>
																							<li><i class="fas fa-user-plus text-success"></i> 0879599955</li>
															<li><i class="fas fa-user-plus text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-plus text-success"></i> 0811414170</li>
																							<li><i class="fas fa-user-plus text-success"></i> 0859599955</li>
															<li><i class="fas fa-user-plus text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-plus text-success"></i> 0811414170</li>
																							<li><i class="fas fa-user-plus text-success"></i> 0889599955</li>
															<li><i class="fas fa-user-plus text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-plus text-success"></i> 0811414170</li>
																							<li><i class="fas fa-user-plus text-success"></i> 0899599955</li>
															<li><i class="fas fa-user-plus text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-plus text-success"></i> 0811414170</li>
																							<li><i class="fas fa-user-plus text-success"></i> 0889599955</li>
															<li><i class="fas fa-user-plus text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-plus text-success"></i> 0811414170</li>
																							<li><i class="fas fa-user-plus text-success"></i> 0849599955</li>
															<li><i class="fas fa-user-plus text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-plus text-success"></i> 0811414170</li>
																							<li><i class="fas fa-user-plus text-success"></i> 0899599955</li>
															<li><i class="fas fa-user-plus text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-plus text-success"></i> 0811414170</li>
																							<li><i class="fas fa-user-plus text-success"></i> 0899599955</li>
															<li><i class="fas fa-user-plus text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-plus text-success"></i> 0811414170</li>
																							<li><i class="fas fa-user-plus text-success"></i> 0859599955</li>
															<li><i class="fas fa-user-plus text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-plus text-success"></i> 0811414170</li>
																							<li><i class="fas fa-user-plus text-success"></i> 0859599955</li>
															<li><i class="fas fa-user-plus text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-plus text-success"></i> 0811414170</li>
																							<li><i class="fas fa-user-plus text-success"></i> 0869599955</li>
															<li><i class="fas fa-user-plus text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-plus text-success"></i> 0811414170</li>
																							<li><i class="fas fa-user-plus text-success"></i> 0899599955</li>
															<li><i class="fas fa-user-plus text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-plus text-success"></i> 0811414170</li>
																							<li><i class="fas fa-user-plus text-success"></i> 0899599955</li>
															<li><i class="fas fa-user-plus text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-plus text-success"></i> 0811414170</li>
																							<li><i class="fas fa-user-plus text-success"></i> 0839599955</li>
															<li><i class="fas fa-user-plus text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-plus text-success"></i> 0811414170</li>
																							<li><i class="fas fa-user-plus text-success"></i> 0899599955</li>
															<li><i class="fas fa-user-plus text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-plus text-success"></i> 0811414170</li>
																							<li><i class="fas fa-user-plus text-success"></i> 0839599955</li>
															<li><i class="fas fa-user-plus text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-plus text-success"></i> 0811414170</li>
																							<li><i class="fas fa-user-plus text-success"></i> 0899599955</li>
															<li><i class="fas fa-user-plus text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-plus text-success"></i> 0811414170</li>
																							<li><i class="fas fa-user-plus text-success"></i> 0859599955</li>
															<li><i class="fas fa-user-plus text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-plus text-success"></i> 0811414170</li>
																							<li><i class="fas fa-user-plus text-success"></i> 0899599955</li>
															<li><i class="fas fa-user-plus text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-plus text-success"></i> 0811414170</li>
																							<li><i class="fas fa-user-plus text-success"></i> 0899599955</li>
															<li><i class="fas fa-user-plus text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-plus text-success"></i> 0811414170</li>
																							<li><i class="fas fa-user-plus text-success"></i> 0839599955</li>
															<li><i class="fas fa-user-plus text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-plus text-success"></i> 0811414170</li>
																							<li><i class="fas fa-user-plus text-success"></i> 0869599955</li>
															<li><i class="fas fa-user-plus text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-plus text-success"></i> 0811414170</li>
																							<li><i class="fas fa-user-plus text-success"></i> 0849599955</li>
															<li><i class="fas fa-user-plus text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-plus text-success"></i> 0811414170</li>
																							<li><i class="fas fa-user-plus text-success"></i> 0849599955</li>
															<li><i class="fas fa-user-plus text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-plus text-success"></i> 0811414170</li>
																							<li><i class="fas fa-user-plus text-success"></i> 0889599955</li>
															<li><i class="fas fa-user-plus text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-plus text-success"></i> 0811414170</li>
																							<li><i class="fas fa-user-plus text-success"></i> 0859599955</li>
															<li><i class="fas fa-user-plus text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-plus text-success"></i> 0811414170</li>
																							<li><i class="fas fa-user-plus text-success"></i> 0859599955</li>
															<li><i class="fas fa-user-plus text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-plus text-success"></i> 0811414170</li>
																							<li><i class="fas fa-user-plus text-success"></i> 0899599955</li>
															<li><i class="fas fa-user-plus text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-plus text-success"></i> 0811414170</li>
																							<li><i class="fas fa-user-plus text-success"></i> 0849599955</li>
															<li><i class="fas fa-user-plus text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-plus text-success"></i> 0811414170</li>
																							<li><i class="fas fa-user-plus text-success"></i> 0889599955</li>
															<li><i class="fas fa-user-plus text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-plus text-success"></i> 0811414170</li>
																						</ul>

													</div>
													<div class="unfriend col-sm-5 col-md-4">
														<h3 class="bg-danger pa10-xs rounded"><i class="fas fa-times-circle"></i> Unable to send <small class="t-white">(40%)</small></h3>
														<ul>
																							<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
															<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
																							<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
															<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
																							<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
															<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
																							<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
															<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
																							<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
															<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
																							<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
															<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
																							<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
															<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
																							<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
															<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
																							<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
															<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
																							<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
															<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
																							<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
															<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
																							<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
															<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
																							<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
															<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
																							<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
															<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
																							<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
															<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
																							<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
															<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
																							<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
															<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
																							<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
															<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
																							<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
															<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
																							<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
															<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
																						</ul>
													</div>
												  </div>
												
											</div>
											<?php */?>

										
										</div>
										
									</div>
								</div>
								<!-- /card -->
								
						</div>
						
						<div class="sidebar-right _self-cl-sm-12-md-03 _flex center-xs">
							<!-- live Mobile -->
							<div id="main-log" class="bx-log active">
<div id="live-screen">
<ul class="list-sr-chat">

									<li class="messageScreen1"><input type="hidden" class="done messageScreen1" id="valScreen1" value="txt|text">
										<div class="direct-chat-msg">
											<div class="direct-chat-img"></div>
											<div class="direct-chat-text">กรุณาแชร์โลเคชั่นคุณลูกค้ามาให้ทางเราด้วยค่ะ</div>
										</div>
									</li>
									<li class="messageScreen1-reply">
										<div class="direct-chat-msg right">
											<img class="direct-chat-img" alt="message user image" src="http://ui.beurguide.net/LNS/dist/img/user7-128x128.jpg">
											<div class="direct-chat-text display-map">
											<figure class="map">
											<img src="http://ui.beurguide.net/LNS/dist/img/googlemap-demo.png">
											</figure>
											<div class="address">
											ถนน แจ้งวัฒนะ ตำบลคลองเกลือ อำเภอปากเกร็ด นนทบุรี 11120 ประ
											</div>
											</div>
											<div class="direct-chat-infos clearfix">
											  <span class="direct-chat-timestamp float-right">23 May 5:24 pm</span>
											</div>
										</div>
										
									</li>
									<li class="messageScreen2"><input type="hidden" class="done messageScreen2" id="valScreen2" value="txt|text">
										<div class="direct-chat-msg">
											<div class="direct-chat-img"></div>
											<div class="direct-chat-text">กรุณาส่ง Location ที่เกิดเหตุด้วยค่ะ</div>
										</div>
										<div class="button-quick-reply d-flex justify-content-center mt-3">
											<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> กด แจ้ง Location</a>
										</div>
									</li>
									<li class="messageScreen2-reply">
										<div class="direct-chat-msg right">
											<img class="direct-chat-img" alt="message user image" src="http://ui.beurguide.net/LNS/dist/img/user7-128x128.jpg">
											<div class="direct-chat-text display-map">
											<figure class="map">
											<img src="http://ui.beurguide.net/LNS/dist/img/googlemap-demo.png">
											</figure>
											<div class="address">
											ถนน รังสิต-นครนายก คลอง6 ตำบลรังสิต อำเภอธัญบุรี ปทุมธานี 12110 ประเทศไทย
											</div>
											</div>
											<div class="direct-chat-infos clearfix">
											  <span class="direct-chat-timestamp float-right">23 May 5:27 pm</span>
											</div>
										</div>
										
									</li>
									
									<li class="result-carousel messageScreen3">
										<div class="direct-chat-msg">
											<div class="direct-chat-img"></div>
										</div>
										<div class="card-button">
											<figure class="image"><a href="#listaction"><img src="https://dummyimage.com/500x500/fea3b8/ffffff.png&amp;text=Consent form"></a>
											</figure>
											<div class="detail">
												<h2>ทางบ.xxx ขออนุญาตเรียนแจ้งให้ท่านทราบว่า</h2>
												<p class="max">สำหรับการใช้บริการ....คุณลูกค้ากรุณาแสดงความยินยอม หรือ consent สำหรับการขอใช้บริการ ดูรายละเอียด consent form ได้ที่นี่ค่ะ [เงื่อนไขการยินยอม]
												</p>
											</div>
											<ul>
												<li class="p-1"><a href="https://ui.beurguide.net/LNS/microsite/consent1.html" target="_blank">เงื่อนไขและรายละเอียด</a>
												</li>
												<li class="p-1"><a class="btn btn-block bg-gray2 p-1" href="#Action1">ยินยอม</a>
												</li>
												<li class="p-1"><a class="btn btn-block bg-gray2 p-1" href="#Action2">ไม่ยินยอม</a>
												</li>
											</ul>
										</div>
									</li>
									<li class="messageScreen2-reply">
										<div class="direct-chat-msg right">
											<img class="direct-chat-img" alt="message user image" src="http://ui.beurguide.net/LNS/dist/img/user7-128x128.jpg">
											<div class="direct-chat-text">					
											ข้าพเจ้ายินยอมเงื่อนไขการให้บริการ
											</div>
											<div class="direct-chat-infos clearfix">
											  <span class="direct-chat-timestamp float-right">23 May 5:37 pm</span>
											</div>
										</div>
									</li>
									<li class="result-imgmap messageScreen4">
										<div class="direct-chat-msg">
											<div class="direct-chat-img"></div>
										</div>
										<div class="card-button w-100">
	
											<div class="detail">
												<h2>การขอความยินยอม (Consent Form)</h2>
												<div class="max">

<p>วัตถุประสงค์</p>
<p>1. เพื่อให้ประชาชน ผู้รับบริการ ทุกพื้นที่สามารถเข้าถึงบริการด้านการให้ค าแนะน าด้านสุขภาพ
วินิจฉัยโรคเบื้องต้น ดูแลในกรณีฉุกเฉิน และดูแลในกรณีพิเศษ (ส่งต่อ)</p>
<p>2. เพื่อให้ประชาชนผู้รับบริการสามารถเข้าถึงและบริหารจัดการข้อมูลสุขภาพทางอิเล็กทรอนิกส์
ของตนได้</p>
<p>3. เพื่อให้บุคลากรที่ได้รับ</p>


												</div>
											</div>
											
										</div>
									</li>
									<li class="messageScreen5"><input type="hidden" class="done messageScreen5" id="valScreen5" value="txt|text">
										<div class="direct-chat-msg">
											<div class="direct-chat-img"></div>
											<div class="direct-chat-text">ขอบคุณค่ะ</div>
										</div>
									</li>
								</ul>
</div>
</div>
							<?php /*?><div id="main-log" class="bx-log active">
							<div id="live-screen">
								<ul class="list-sr-chat show">
									<li id="result-msg1">
										<div class="direct-chat-msg">
											<!--<div class="direct-chat-infos clearfix">
											  <span class="direct-chat-name float-left">Alexander Pierce</span>
											  <span class="direct-chat-timestamp float-right">23 Jan 2:00 pm</span>
											</div>-->
											<!-- /.direct-chat-infos -->
											<img class="direct-chat-img" src="https://ui.beurguide.net/LNS/dist/img/avatar-admin.png" alt="Message User Image">
											<!-- /.direct-chat-img -->
											<div class="direct-chat-text">
											  Is this template really for free? That's unbelievable!
											</div>
											<!-- /.direct-chat-text -->
										  </div>
									</li>

									<li id="result-msg2">
										<div class="direct-chat-msg">
											<!-- /.direct-chat-infos -->
											<img class="direct-chat-img" src="https://ui.beurguide.net/LNS/dist/img/avatar-admin.png" alt="Message User Image">
											<!-- /.direct-chat-img -->
											<div class="direct-chat-text direct-chat-image">
											  <img class="img-fluid pad" src="https://ui.beurguide.net/LNS/dist/img/photo3.jpg" alt="Photo">
											</div>
											<!-- /.direct-chat-text -->
										  </div>
									</li>

									
								</ul>
							</div>
						</div><?php */?>
							<!-- /live Mobile -->
						</div>
					</div>
				</div>
					</form>

			</div>
			
			
			
			
		</section>
    </div>
</div>

<!--<div id="skin-loading" class="bg-wh" onclick="$(this).fadeOut();">
	<div class="lds-hourglass"></div>
</div>-->
<script>
	window.setTimeout(function(){
		$('#skin-loading').fadeOut();
	}, 3000);
</script>

<!-- footer -->
<?php include("incs/footer-web.html") ?>
<!-- /footer -->

<div class="popup thm-lite" id="modal-consent">
				<div class="box-middle">
				  <div class="modal-content">
					<div class="modal-header">
					  <h2 class="t-black">Consent Form</h2>
					</div>
					<div class="modal-body">
					  
					  <div class="text-body txt-l">
					  	

							<div class="detail">
									<h3 class="text-md t-black">การขอความยินยอม (Consent Form)</h3>
									<div class="max">
									<p>วัตถุประสงค์</p>
									<p>1. เพื่อให้ประชาชน ผู้รับบริการ ทุกพื้นที่สามารถเข้าถึงบริการด้านการให้ค าแนะน าด้านสุขภาพ
									วินิจฉัยโรคเบื้องต้น ดูแลในกรณีฉุกเฉิน และดูแลในกรณีพิเศษ (ส่งต่อ)</p>
									<p>2. เพื่อให้ประชาชนผู้รับบริการสามารถเข้าถึงและบริหารจัดการข้อมูลสุขภาพทางอิเล็กทรอนิกส์
									ของตนได้</p>
									<p>3. เพื่อให้บุคลากรที่ได้รับ</p>
									<p>4. เพื่อให้ประชาชน ผู้รับบริการ ทุกพื้นที่สามารถเข้าถึงบริการด้านการให้ค าแนะน าด้านสุขภาพ
									วินิจฉัยโรคเบื้องต้น ดูแลในกรณีฉุกเฉิน และดูแลในกรณีพิเศษ (ส่งต่อ)</p>
									<p>5. เพื่อให้ประชาชนผู้รับบริการสามารถเข้าถึงและบริหารจัดการข้อมูลสุขภาพทางอิเล็กทรอนิกส์
									ของตนได้</p>
									<p>6. เพื่อให้บุคลากรที่ได้รับ</p>

									</div>
								</div>

					  </div>
	
					</div>
					<div class="modal-footer p-0 center-xs">
					  <a data-fancybox-close="" class="ui-btn-green2 btn-sm" title="Close" href="javascript:;" onclick="parent.jQuery.fancybox.close();">Close</a>
					</div>
				  </div>
				  <!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			  </div>
			  <!-- /.modal -->
			  
			  <div class="popup thm-lite" id="modal-edit-name">
				<div class="box-middle">
				  <div class="modal-content">
					<div class="modal-header">
					  <h4 class="modal-title w-100 text-center">Edit Name</h4>
					  
					</div>
					<div class="modal-body txt-l">
					  <div class="form-group bg-light rounded p-2">
                        <label class="d-block">Name</label>
                        <input type="text" class="form-control form-control-lg text-lg text-center bg-white _self-pa30" value="Nadia">
                      </div>
					  
					   <div class="form-group bg-light rounded p-2">
                        <label class="d-block">Surname</label>
                        <input type="text" class="form-control form-control-lg text-lg text-center bg-white _self-pa30" value="Carmichael">
                      </div>
	
					</div>
					<div class="modal-footer justify-content-between mt10-xs">
					   <a data-fancybox-close="" class="ui-btn-gray btn-sm" title="Cancel" href="javascript:;" onclick="parent.jQuery.fancybox.close();">Cancel</a>
					   <a data-fancybox-close="" class="ui-btn-green2 btn-sm" title="Save" href="javascript:;" onclick="parent.jQuery.fancybox.close();">Save</a>
					</div>
				  </div>
				  <!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			  </div>
			  <!-- /.modal -->
			  
			  <div class="popup thm-lite" id="modal-edit-user">
				<div class="box-middle">
				  <div class="modal-content">
					<div class="modal-header">
					  <h4 class="modal-title w-100 text-center">Edit Phone Number / UID</h4>
					  
					</div>
					<div class="modal-body txt-l">
					  <div class="form-group bg-light rounded p-2">
                        <label class="d-block">Phone Number</label>
                        <input type="text" class="form-control form-control-lg text-lg text-center bg-white _self-pa30" value="0899599955">
                      </div>
					  
					   <div class="form-group bg-light rounded p-2">
                        <label class="d-block">User ID</label>
                        <input type="text" class="form-control form-control-lg text-lg text-center bg-white _self-pa30" value="Ub8c6ef4a7880161aa490bd7bd11d0133">
                      </div>
	
					</div>
					<div class="modal-footer justify-content-between mt10-xs">
					   <a data-fancybox-close="" class="ui-btn-gray btn-sm" title="Cancel" href="javascript:;" onclick="parent.jQuery.fancybox.close();">Cancel</a>
					   <a data-fancybox-close="" class="ui-btn-green2 btn-sm" title="Save" href="javascript:;" onclick="parent.jQuery.fancybox.close();">Save</a>
					</div>
				  </div>
				  <!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			  </div>
			  <!-- /.modal -->
			  
			  <div class="popup thm-lite" id="modal-history">
				<div class="box-middle">
				  <div class="modal-content">
					<div class="modal-header">
					  <h4 class="modal-title w-100 text-center">Call History</h4>
					  
					</div>
					<div class="modal-body">
					  <ul class="products-list product-list-in-card pl-2 pr-2">
						  <li class="item">
							
							<div class="product-info ml-0">
							  <a href="javascript:;" data-fancybox="historyChat" data-modal="modal" data-src="#modal-view-chat" title="edit" class="product-title text-dark" data-dismiss="modal">
							  	20 เมษายน 2563
								<span class="badge badge-info float-right">view chat</span></a>
							  <span class="product-description">
								12.00 น. | <span class="text-muted">เวลาทำรายการ 12.38 นาที</span>
							  </span>
							</div>
						  </li>
						  <!-- /.item -->
						  <li class="item">
							
							<div class="product-info ml-0">
							  <a href="javascript:;" data-fancybox="historyChat" data-modal="modal" data-src="#modal-view-chat" title="edit" class="product-title text-dark" data-dismiss="modal">
							  	15 มีนาคม 2563
								<span class="badge badge-info float-right">view chat</span></a>
							  <span class="product-description">
								09.53 น. | <span class="text-muted">เวลาทำรายการ 5.14 นาที</span>
							  </span>
							</div>
						  </li>
						  <!-- /.item -->
						  <li class="item">
							
							<div class="product-info ml-0">
							  <a href="javascript:;" data-fancybox="historyChat" data-modal="modal" data-src="#modal-view-chat" title="edit" class="product-title text-dark" data-dismiss="modal">
							  	7 มกราคม 2563
								<span class="badge badge-info float-right">view chat</span></a>
							  <span class="product-description">
								16.05 น. | <span class="text-muted">เวลาทำรายการ 2.46 นาที</span>
							  </span>
							</div>
						  </li>
						  <!-- /.item -->
						</ul>
	
					</div>
					<div class="modal-footer center-xs mt10-xs">
					   <a data-fancybox-close="" class="ui-btn-green2 btn-sm" title="Close" href="javascript:;" onclick="parent.jQuery.fancybox.close();">Close</a>
					  <!--<button type="button" class="btn btn-primary" data-dismiss="modal">Save</button>-->
					</div>
				  </div>
				  <!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			  </div>
			  <!-- /.modal -->
			  
			  <div class="popup thm-lite" id="modal-view-chat">
				<div class="box-middle">
				  <div class="modal-content">
					<div class="modal-header">
					  <h4 class="modal-title w-100 text-center">Chat at  20 เม.ย. 2563</h4>
					  
					</div>
					<div class="modal-body d-flex center-xs">
					  						
						<div id="main-log" class="bx-log active">
							<div id="live-screen" class="live-chat">
								<ul class="list-sr-chat show">

									<li class="messageScreen1"><input type="hidden" class="done messageScreen1" id="valScreen1" value="txt|text">
										<div class="direct-chat-msg">
											<div class="direct-chat-img"></div>
											<div class="direct-chat-text">กรุณาแชร์โลเคชั่นคุณลูกค้ามาให้ทางเราด้วยค่ะ</div>
										</div>
									</li>
									<li class="messageScreen1-reply">
										<div class="direct-chat-msg right">
											<img class="direct-chat-img" alt="message user image" src="http://ui.beurguide.net/LNS/dist/img/user7-128x128.jpg">
											<div class="direct-chat-text display-map">
											<figure class="map">
											<img src="http://ui.beurguide.net/LNS/dist/img/googlemap-demo.png">
											</figure>
											<div class="address">
											ถนน แจ้งวัฒนะ ตำบลคลองเกลือ อำเภอปากเกร็ด นนทบุรี 11120 ประ
											</div>
											</div>
											<div class="direct-chat-infos clearfix">
											  <span class="direct-chat-timestamp float-right">23 May 5:24 pm</span>
											</div>
										</div>
										
									</li>
									<li class="messageScreen2"><input type="hidden" class="done messageScreen2" id="valScreen2" value="txt|text">
										<div class="direct-chat-msg">
											<div class="direct-chat-img"></div>
											<div class="direct-chat-text">กรุณาส่ง Location ที่เกิดเหตุด้วยค่ะ</div>
										</div>
										<div class="button-quick-reply d-flex center-xs mt-3">
											<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> กด แจ้ง Location</a>
										</div>
									</li>
									<li class="messageScreen2-reply">
										<div class="direct-chat-msg right">
											<img class="direct-chat-img" alt="message user image" src="http://ui.beurguide.net/LNS/dist/img/user7-128x128.jpg">
											<div class="direct-chat-text display-map">
											<figure class="map">
											<img src="http://ui.beurguide.net/LNS/dist/img/googlemap-demo.png">
											</figure>
											<div class="address">
											ถนน รังสิต-นครนายก คลอง6 ตำบลรังสิต อำเภอธัญบุรี ปทุมธานี 12110 ประเทศไทย
											</div>
											</div>
											<div class="direct-chat-infos clearfix">
											  <span class="direct-chat-timestamp float-right">23 May 5:27 pm</span>
											</div>
										</div>
										
									</li>
									
									<li class="result-carousel messageScreen3">
										<div class="direct-chat-msg">
											<div class="direct-chat-img"></div>
										</div>
										<div class="card-button">
											<figure class="image"><a href="#listaction"><img src="https://dummyimage.com/500x500/fea3b8/ffffff.png&amp;text=Consent form"></a>
											</figure>
											<div class="detail">
												<h2>ทางบ.xxx ขออนุญาตเรียนแจ้งให้ท่านทราบว่า</h2>
												<p class="max">สำหรับการใช้บริการ....คุณลูกค้ากรุณาแสดงความยินยอม หรือ consent สำหรับการขอใช้บริการ ดูรายละเอียด consent form ได้ที่นี่ค่ะ [เงื่อนไขการยินยอม]
												</p>
											</div>
											<ul>
												<li class="p-1"><a href="https://ui.beurguide.net/LNS/microsite/consent1.html" target="_blank">เงื่อนไขและรายละเอียด</a>
												</li>
												<li class="p-1"><a class="btn btn-block bg-gray2 p-1" href="#Action1">ยินยอม</a>
												</li>
												<li class="p-1"><a class="btn btn-block bg-gray2 p-1" href="#Action2">ไม่ยินยอม</a>
												</li>
											</ul>
										</div>
									</li>
									<li class="messageScreen2-reply">
										<div class="direct-chat-msg right">
											<img class="direct-chat-img" alt="message user image" src="http://ui.beurguide.net/LNS/dist/img/user7-128x128.jpg">
											<div class="direct-chat-text">					
											ข้าพเจ้ายินยอมเงื่อนไขการให้บริการ
											</div>
											<div class="direct-chat-infos clearfix">
											  <span class="direct-chat-timestamp float-right">23 May 5:37 pm</span>
											</div>
										</div>
									</li>
									<li class="result-imgmap messageScreen4">
										<div class="direct-chat-msg">
											<div class="direct-chat-img"></div>
										</div>
										<div class="card-button w-100">
	
											<div class="detail">
												<h2>การขอความยินยอม (Consent Form)</h2>
												<div class="max">

<p>วัตถุประสงค์</p>
<p>1. เพื่อให้ประชาชน ผู้รับบริการ ทุกพื้นที่สามารถเข้าถึงบริการด้านการให้ค าแนะน าด้านสุขภาพ
วินิจฉัยโรคเบื้องต้น ดูแลในกรณีฉุกเฉิน และดูแลในกรณีพิเศษ (ส่งต่อ)</p>
<p>2. เพื่อให้ประชาชนผู้รับบริการสามารถเข้าถึงและบริหารจัดการข้อมูลสุขภาพทางอิเล็กทรอนิกส์
ของตนได้</p>
<p>3. เพื่อให้บุคลากรที่ได้รับ</p>


												</div>
											</div>
											
										</div>
									</li>
									<li class="messageScreen5"><input type="hidden" class="done messageScreen5" id="valScreen5" value="txt|text">
										<div class="direct-chat-msg">
											<div class="direct-chat-img"></div>
											<div class="direct-chat-text">ขอบคุณค่ะ</div>
										</div>
									</li>
								</ul>
							</div>
						</div>
						
					</div>
					<div class="modal-footer center-xs mt10-xs">
					  <a data-fancybox-close="" class="ui-btn-green2 btn-sm" title="Close" href="javascript:;" onclick="parent.jQuery.fancybox.close();">Close</a>
					</div>
				  </div>
				  <!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			  </div>
			  <!-- /.modal -->
			  
			  <div class="popup thm-lite" id="modal-map">
				<div class="box-middle">
				  <div class="modal-content">
					<div class="modal-header">
					  <h4 class="modal-title w-100 text-center">พิกัด 13.904478, 100.530079</h4>
					  
					</div>
					<div class="modal-body d-flex center-xs">
					 <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1219.889347675032!2d100.53007896713898!3d13.904477974903562!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x1febf57be0e63088!2z4Lit4Liy4LiE4Liy4Lij4LiL4Lit4Lif4LiV4LmM4LmB4Lin4Lij4LmM4Lib4Liy4Lij4LmM4LiE!5e0!3m2!1sth!2sth!4v1588137777781!5m2!1sth!2sth" width="600" height="450" frameborder="0" style="border:0; max-width: 100%" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
					</div>
					<div class="modal-footer center-xs mt10-xs">
					  <a data-fancybox-close="" class="ui-btn-green2 btn-sm" title="Close" href="javascript:;" onclick="parent.jQuery.fancybox.close();">Close</a>
					</div>
				  </div>
				  <!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			  </div>
			  <!-- /.modal -->


<!-- js -->
<?php include("incs/js-web.html") ?>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<link href="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.css" rel="stylesheet"/>
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>

<!--<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>-->
<script>
  $( function() {
     $( ".todo-list" ).sortable({
      placeholder: "ui-state-highlight",
	  items: "li:not(.ui-state-disabled)",
	  //cancel: ".ui-state-disabled"
    });
    $( ".todo-list li" ).disableSelection();
  } );
</script>

<script>
/*upload*/
$( document ).ready( function () {
	var count_message = 0;
	
	$('.custom-select').on('change', function() {

		if ($(this)[0].selectedIndex == 0) { 
			$(this).parents('.wrap-upload').find('.f-url').show();
			$(this).parents('.wrap-upload').find('.f-upload').hide();
			
		} else if ($(this)[0].selectedIndex == 1)  {
			$(this).parents('.wrap-upload').find('.f-upload').show();
			$(this).parents('.wrap-upload').find('.f-url').hide();
		}

	});
	
	$('input[type=radio][name=chk-send]').change(function() {
		if (this.value == 'now') {
			$("#show-schedule").hide();
		}
		else if (this.value == 'set') {
			$("#show-schedule").show();
		}
	});
	
	
	// jQuery UI sortable for the todo list
	  /*$('.todo-list').sortable({
		placeholder         : 'sort-highlight',
		handle              : '.handle',
		forcePlaceholderSize: true,
		zIndex              : 999999
	  })*/
	  
	  // Add Text
	$('.btn-add-msg').click(function() {
		var message_text = $("#message_text").val();
		var count_items = 0;
		$("ul.list-sr-chat>li").each(function () {
			count_items++;
		});
		if ( count_items < 5 && message_text != '' ) {
			count_message++;
			if ( $('#newpush .todo-list').hasClass('show') ) {
				
			} else {
				$('.todo-list,.list-sr-chat,.save-template').addClass('show');
			}
			$('.todo-list').append('<li class="done messageList'+count_message+'">'+
				'<span class="handle ui-sortable-handle">'+
					' <i class="fas fa-ellipsis-v"></i>'+
					' <i class="fas fa-ellipsis-v"></i>'+
				'</span>'+
				'<div class="icheck-primary d-inline ml-2">'+
					' <input type="checkbox" id="chklist'+count_message+'" onclick="checkboxShowHide('+count_message+')" checked>'+
					' <label for="chklist'+count_message+'"></label>'+
				'</div>'+
				'<span class="text">'+message_text+'</span>'+
				'<small class="badge badge-info"><i class="fas fa-text-height"></i> Text</small>'+
				'<div class="tools">'+
					'<i class="fas fa-edit text-gray" onclick="editMessage('+count_message+')"></i>'+
					'<i class="fas fa-trash" onclick="removeMessage('+count_message+')"></i>'+
				'</div>'+
			'</li>');
			$('.list-sr-chat').append('<li class="messageScreen'+count_message+'">'+
				'<input type="hidden" class="messageScreen'+count_message+'" id="valScreen'+count_message+'" value="txt|'+message_text+'"/>'+
				'<div class="direct-chat-msg">'+
					'<div class="direct-chat-img"></div>'+
					'<div class="direct-chat-text">'+message_text+'</div>'+
				'</div>'+
			'</li>');
			$("#message_text").val('');
		} else {
			if ( count_items >= 5 ) {
				alert('Maximum 5 Message');
			} else {
				alert('Please input text');
			}
		}
	});
	
	//add Quick reply 
	$('#select-replay-action').on('change', function() {

		if ($(this)[0].selectedIndex == 0) { 
			$('#replay-action1').show();
			$('#replay-action2').hide();
			$('#replay-action3').hide();
			$('#replay-action4').hide();
			$('#replay-action5').hide();
			$('#replay-action6').hide();
			
		} else if ($(this)[0].selectedIndex == 1)  {
			$('#replay-action1').show();
			$('#replay-action2').show();
			$('#replay-action3').hide();
			$('#replay-action4').hide();
			$('#replay-action5').hide();
			$('#replay-action6').hide();
		} else if ($(this)[0].selectedIndex == 2)  {
			$('#replay-action1').show();
			$('#replay-action2').show();
			$('#replay-action3').show();
			$('#replay-action4').hide();
			$('#replay-action5').hide();
			$('#replay-action6').hide();
		} else if ($(this)[0].selectedIndex == 3)  {
			$('#replay-action1').show();
			$('#replay-action2').show();
			$('#replay-action3').show();
			$('#replay-action4').show();
			$('#replay-action5').hide();
			$('#replay-action6').hide();
		} else if ($(this)[0].selectedIndex == 4)  {
			$('#replay-action1').show();
			$('#replay-action2').show();
			$('#replay-action3').show();
			$('#replay-action4').show();
			$('#replay-action5').show();
			$('#replay-action6').hide();
		} else if ($(this)[0].selectedIndex == 5)  {
			$('#replay-action1').show();
			$('#replay-action2').show();
			$('#replay-action3').show();
			$('#replay-action4').show();
			$('#replay-action5').show();
			$('#replay-action6').show();
		}
	});
	$('.btn-add-qreply').click(function() {
		var message_replay_text = $("#message_replay_text").val();
		var isAction = $('#select-replay-action')[0].selectedIndex;
		var myLabelAction1 = $("#labelQreply1").val();
		var myLabelAction2 = $("#labelQreply2").val();
		var myLabelAction3 = $("#labelQreply3").val();
		var myLabelAction4 = $("#labelQreply4").val();
		var myLabelAction5 = $("#labelQreply5").val();
		var myLabelAction6 = $("#labelQreply6").val();
		
		var count_items = 0;
		$("ul.list-sr-chat>li").each(function () {
			count_items++;
		});
		if ( count_items < 5 && message_text != '' ) {
			count_message++;
			if ( $('#newpush .todo-list').hasClass('show') ) {
				
			} else {
				$('.todo-list,.list-sr-chat,.save-template').addClass('show');
			}
			$('.todo-list').append('<li class="done messageList'+count_message+'">'+
				'<span class="handle ui-sortable-handle">'+
					' <i class="fas fa-ellipsis-v"></i>'+
					' <i class="fas fa-ellipsis-v"></i>'+
				'</span>'+
				'<div class="icheck-primary d-inline ml-2">'+
					' <input type="checkbox" id="chklist'+count_message+'" onclick="checkboxShowHide('+count_message+')" checked>'+
					' <label for="chklist'+count_message+'"></label>'+
				'</div>'+
				'<span class="text">'+message_replay_text+'</span>'+
				'<small class="badge bg-purple"><i class="fas fa-reply"></i> Reply</small>'+
				'<div class="tools">'+
					'<i class="fas fa-edit text-gray" onclick="editMessage('+count_message+')"></i>'+
					'<i class="fas fa-trash" onclick="removeMessage('+count_message+')"></i>'+
				'</div>'+
			'</li>');
			if(isAction == 0) {
			$('.list-sr-chat').append('<li class="messageScreen'+count_message+'">'+
				'<input type="hidden" class="messageScreen'+count_message+'" id="valScreen'+count_message+'" value="txt|'+message_replay_text+'"/>'+
				'<div class="direct-chat-msg">'+
					'<div class="direct-chat-img"></div>'+
					'<div class="direct-chat-text">'+message_replay_text+'</div>'+
				'</div>'+
				'<div class="button-quick-reply d-flex justify-content-start mt-3">'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction1+'</a>'+
				'</div>'+
			'</li>');
			} else if(isAction == 1) {
			$('.list-sr-chat').append('<li class="messageScreen'+count_message+'">'+
				'<input type="hidden" class="messageScreen'+count_message+'" id="valScreen'+count_message+'" value="txt|'+message_replay_text+'"/>'+
				'<div class="direct-chat-msg">'+
					'<div class="direct-chat-img"></div>'+
					'<div class="direct-chat-text">'+message_replay_text+'</div>'+
				'</div>'+
				'<div class="button-quick-reply d-flex justify-content-start mt-3">'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction1+'</a>'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction2+'</a>'+
				'</div>'+
			'</li>');
			} else if(isAction == 2) {
			$('.list-sr-chat').append('<li class="messageScreen'+count_message+'">'+
				'<input type="hidden" class="messageScreen'+count_message+'" id="valScreen'+count_message+'" value="txt|'+message_replay_text+'"/>'+
				'<div class="direct-chat-msg">'+
					'<div class="direct-chat-img"></div>'+
					'<div class="direct-chat-text">'+message_replay_text+'</div>'+
				'</div>'+
				'<div class="button-quick-reply d-flex justify-content-start mt-3">'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction1+'</a>'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction2+'</a>'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction3+'</a>'+
				'</div>'+
			'</li>');
			} else if(isAction == 3) {
			$('.list-sr-chat').append('<li class="messageScreen'+count_message+'">'+
				'<input type="hidden" class="messageScreen'+count_message+'" id="valScreen'+count_message+'" value="txt|'+message_replay_text+'"/>'+
				'<div class="direct-chat-msg">'+
					'<div class="direct-chat-img"></div>'+
					'<div class="direct-chat-text">'+message_replay_text+'</div>'+
				'</div>'+
				'<div class="button-quick-reply d-flex justify-content-start mt-3">'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction1+'</a>'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction2+'</a>'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction3+'</a>'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction4+'</a>'+
				'</div>'+
			'</li>');
			} else if(isAction == 4) {
			$('.list-sr-chat').append('<li class="messageScreen'+count_message+'">'+
				'<input type="hidden" class="messageScreen'+count_message+'" id="valScreen'+count_message+'" value="txt|'+message_replay_text+'"/>'+
				'<div class="direct-chat-msg">'+
					'<div class="direct-chat-img"></div>'+
					'<div class="direct-chat-text">'+message_replay_text+'</div>'+
				'</div>'+
				'<div class="button-quick-reply d-flex justify-content-start mt-3">'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction1+'</a>'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction2+'</a>'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction3+'</a>'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction4+'</a>'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction5+'</a>'+
				'</div>'+
			'</li>');
			} else if(isAction == 5) {
			$('.list-sr-chat').append('<li class="messageScreen'+count_message+'">'+
				'<input type="hidden" class="messageScreen'+count_message+'" id="valScreen'+count_message+'" value="txt|'+message_replay_text+'"/>'+
				'<div class="direct-chat-msg">'+
					'<div class="direct-chat-img"></div>'+
					'<div class="direct-chat-text">'+message_replay_text+'</div>'+
				'</div>'+
				'<div class="button-quick-reply d-flex justify-content-start mt-3">'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction1+'</a>'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction2+'</a>'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction3+'</a>'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction4+'</a>'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction5+'</a>'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction6+'</a>'+
				'</div>'+
			'</li>');
			}
			
			$("#message_text").val('');
		} else {
			if ( count_items >= 5 ) {
				alert('Maximum 5 Message');
			} else {
				alert('Please input text');
			}
		}
	});
	

	//toggle action number
	
	function callToggleNum() {
		$('.select-action-chd').on('change', function() {

			if ($(this)[0].selectedIndex == 0) { 
				$(this).parent().next('.main-apm-action').children('.g-action1').show();
				$(this).parent().next('.main-apm-action').children('.g-action2').hide();
				$(this).parent().next('.main-apm-action').children('.g-action3').hide();

			} else if ($(this)[0].selectedIndex == 1)  {
				$(this).parent().next('.main-apm-action').children('.g-action1').show();
				$(this).parent().next('.main-apm-action').children('.g-action2').show();
				$(this).parent().next('.main-apm-action').children('.g-action3').hide();
			} else if ($(this)[0].selectedIndex == 2)  {
				$(this).parent().next('.main-apm-action').children('.g-action1').show();
				$(this).parent().next('.main-apm-action').children('.g-action2').show();
				$(this).parent().next('.main-apm-action').children('.g-action3').show();
			}
		});
	}
	callToggleNum();
	
	//add appointment
	$('#select-apmNum-action').on('change', function() {

		if ($(this)[0].selectedIndex == 0) { 
			$('#group-a1').show();
			$('#group-a2').hide();
			$('#group-a3').hide();
			
		} else if ($(this)[0].selectedIndex == 1)  {
			$('#group-a1').show();
			$('#group-a2').show();
			$('#group-a3').hide();
		} else if ($(this)[0].selectedIndex == 2)  {
			$('#group-a1').show();
			$('#group-a2').show();
			$('#group-a3').show();
		}
	});

	
	$('.btn-add-appointment').click(function() {
		var isAction = $('#select-apmNum-action')[0].selectedIndex;
		var myApmTitle = $("#titleApm").val();
		var myApmText = $("#textApm").val();
		var myApmText2 = $("#textApm2").val();
		var myApmAction1 = $("#actionApm1").val();
		var myApmAction2 = $("#actionApm2").val();
		var myApmAction3 = $("#actionApm3").val();
		
		var count_items = 0;
		$("ul.list-sr-chat>li").each(function () {
			count_items++;
		});
		if ( count_items < 5 && myApmTitle != '' ) {
			count_message++;
			if ( $('#newpush .todo-list').hasClass('show') ) {
				
			} else {
				$('.todo-list,.list-sr-chat,.save-template').addClass('show');
			}
			$('.todo-list').append('<li class="done messageList'+count_message+'">'+
				'<span class="handle ui-sortable-handle">'+
					' <i class="fas fa-ellipsis-v"></i>'+
					' <i class="fas fa-ellipsis-v"></i>'+
				'</span>'+
				'<div class="icheck-primary d-inline ml-2">'+
					' <input type="checkbox" id="chklist'+count_message+'" onclick="checkboxShowHide('+count_message+')" checked>'+
					' <label for="chklist'+count_message+'"></label>'+
				'</div>'+
				'<span class="text">'+myApmTitle+'</span>'+
				'<small class="badge bg-primary"><i class="fas fa-user-clock"></i> Appointment</small>'+
				'<div class="tools">'+
					'<i class="fas fa-edit text-gray" onclick="editMessage('+count_message+')"></i>'+
					'<i class="fas fa-trash" onclick="removeMessage('+count_message+')"></i>'+
				'</div>'+
			'</li>');
			if(isAction == 0) {
			$('.list-sr-chat').append('<li class="result-carousel messageScreen'+count_message+'">'+
				'<div class="direct-chat-msg">'+
					'<div class="direct-chat-img"></div>'+
				 '</div>'+
				'<div class="card-button">'+
				  '<figure class="image">'+
					  '<a href="#listaction"><img src="https://dummyimage.com/500x500/fea3b8/ffffff.png&text=demo" /></a>'+
				  '</figure>'+
					'<div class="detail">'+
					 ' <h2>'+myApmTitle+'</h2>'+
					 ' <p>'+myApmText+'</p>'+
					 ' <p>'+myApmText2+'</p>'+
					'</div>'+
					'<ul>'+
					'<li class="p-1">'+
					'<a href="https://ui.beurguide.net/LNS/microsite/consent1.html" target="_blank">'+myApmAction1+'</a>'+
					'</li>'+
				  '</ul>'+
				'</div>'+
			'</li>');
			} else if(isAction == 1) {
			$('.list-sr-chat').append('<li class="result-carousel messageScreen'+count_message+'">'+
				'<div class="direct-chat-msg">'+
					'<div class="direct-chat-img"></div>'+
				 '</div>'+
				'<div class="card-button">'+
				  '<figure class="image">'+
					  '<a href="#listaction"><img src="https://dummyimage.com/500x500/fea3b8/ffffff.png&text=demo" /></a>'+
				  '</figure>'+
					'<div class="detail">'+
					 ' <h2>'+myApmTitle+'</h2>'+
					 ' <p>'+myApmText+'</p>'+
					 ' <p>'+myApmText2+'</p>'+
					'</div>'+
					'<ul>'+
					'<li class="p-1">'+
					'<a href="https://ui.beurguide.net/LNS/microsite/consent1.html" target="_blank">'+myApmAction1+'</a>'+
					'</li>'+
					'<li class="p-1">'+
					'<a class="btn btn-block bg-gray2 p-1" href="#Action2">'+myApmAction2+'</a>'+
					'</li>'+
				  '</ul>'+
				'</div>'+
			'</li>');
			} else if(isAction == 2) {
			$('.list-sr-chat').append('<li class="result-carousel messageScreen'+count_message+'">'+
				'<div class="direct-chat-msg">'+
					'<div class="direct-chat-img"></div>'+
				 '</div>'+
				'<div class="card-button">'+
				  '<figure class="image">'+
					  '<a href="#listaction"><img src="https://dummyimage.com/500x500/fea3b8/ffffff.png&text=demo" /></a>'+
				  '</figure>'+
					'<div class="detail">'+
					 ' <h2>'+myApmTitle+'</h2>'+
					 ' <p>'+myApmText+'</p>'+
					 ' <p>'+myApmText2+'</p>'+
					'</div>'+
					'<ul>'+
					'<li>'+
					'<a href="https://ui.beurguide.net/LNS/microsite/consent1.html" target="_blank">'+myApmAction1+'</a>'+
					'</li>'+
					'<li class="p-1">'+
					'<a class="btn btn-block bg-gray2 p-1" href="#Action2">'+myApmAction2+'</a>'+
					'</li>'+
					'<li class="p-1">'+
					'<a class="btn btn-block bg-gray2 p-1" href="#Action3">'+myApmAction3+'</a>'+
					'</li>'+
				  '</ul>'+
				'</div>'+
			'</li>');
			}

			$("#titleApm").val('');
			$("#textApm").val('');
			
		} else {
			if ( count_items >= 5 ) {
				alert('Maximum 5 Message');
			} else {
				alert('Please input text');
			}
		}
	});
	//add payload
	$('.btn-add-payload').click(function() {
		
		var myCustomText = $("#myCustomText").val();
		var count_items = 0;
		$("ul.list-sr-chat>li").each(function () {
			count_items++;
		});
		if ( count_items < 5 && myCustomText != '' ) {
			count_message++;
			if ( $('#newpush .todo-list').hasClass('show') ) {
				
			} else {
				$('.todo-list,.list-sr-chat,.save-template').addClass('show');
			}
			$('.todo-list').append('<li class="done messageList'+count_message+'">'+
				'<span class="handle ui-sortable-handle">'+
					' <i class="fas fa-ellipsis-v"></i>'+
					' <i class="fas fa-ellipsis-v"></i>'+
				'</span>'+
				'<div class="icheck-primary d-inline ml-2">'+
					' <input type="checkbox" id="chklist'+count_message+'" onclick="checkboxShowHide('+count_message+')" checked>'+
					' <label for="chklist'+count_message+'"></label>'+
				'</div>'+
				'<span class="text">'+myCustomText+'</span>'+
				'<small class="badge badge-info"><i class="fas fa-comment-dots"></i> Payload</small>'+
				'<div class="tools">'+
					'<i class="fas fa-edit text-gray" onclick="editMessage('+count_message+')"></i>'+
					'<i class="fas fa-trash" onclick="removeMessage('+count_message+')"></i>'+
				'</div>'+
			'</li>');
			$('.list-sr-chat').append('<li class="messageScreen'+count_message+'">'+
				'<input type="hidden" class="messageScreen'+count_message+'" id="valScreen'+count_message+'" value="txt|'+myCustomText+'"/>'+
				'<div class="direct-chat-msg">'+
					'<div class="direct-chat-img"></div>'+
					'<div class="direct-chat-text">Custom Message!</div>'+
				'</div>'+
			'</li>');
			$("#myCustomText").val('');
			
		} else {
			if ( count_items >= 3 ) {
				alert('Maximum 3 Message');
			} else {
				alert('Please input text');
			}
		}
	});
	
	  
	  
	$('#message_urlimg').change(function () {
		$('#displayimg').prop('disabled', false);
	});
	// Upload image
	$("#message_img").change(function () {
		var file = $('#message_img')[0].files[0];
		$('#labelimg').find('p').remove();
		$('#labelimg').append('<p>'+file.name+'</p>');
	});
	$('#displayimg').click(function () {
		$('#labelimg').find('p').remove();
		$('#labelimg').append('<p>Choose file</p>');
	});
	$('#uploadimg').click(function () {
		var error_upload = [];
		error_upload[1] = 'Only file type JPG or PNG';
		error_upload[2] = 'File size Maximum 3 MB';
		var url = "https://api.innohub.me/pnp/upload";
		var chk_error = 0;
		var file = $('#message_img')[0].files[0];
		if (file.type != 'image/jpeg' && file.type != 'image/png') {
			chk_error = 1;
		} else if (file.size > 5*1024000) {
			chk_error = 2;
		}
		if (chk_error != 0) {
			alert(error_upload[chk_error]);
			return;
		}
		var reader = new FileReader();
		reader.onload = function(e) {
			$('#temp_image_src').attr('src', e.target.result);
		}
		reader.readAsDataURL(file);
		var formdata = new FormData();
		formdata.append("upload", file);
		$('#displayimg').append(' <i class="fa fa-spinner fa-spin"></i>');
		$.ajax({
			type: 'POST',
			url: url,
			data: formdata,
			success: function(data){
				if ( data.statusdesc == 'SUCCESS' ) {
					$('#displayimg').prop('disabled', false);
					$('#displayimg').find('i').remove();
					$('#temp_image_src').prop('src', data.url);
				}
			},
			cache: false,
			contentType: false,
			processData: false,
			dataType: 'json'
		});
	});
	
	
	
	//live 
	/*$('.call-log').click(function() {
		if ($('#dxy').hasClass( 'open' )) {
			$('#main-log').removeClass('active');
			$('#dxy').removeClass('open');
			$('#dxy').addClass('close');
			$('.call-log').removeClass('active');
		} else {
		   $('#main-log').addClass('active');
		   $('#dxy').toggleClass('open');
		   $('#dxy').removeClass('close');
		   $(this).toggleClass('active');
		}
    });*/
	

		/*toggle save template*/
		$('.save-as').change( function() {
			var isChecked = this.checked;
			if(isChecked) {
				$(".set-name-tp").fadeIn(100);
				$("#tpl-name").prop("disabled",false); 
				$("#tpl-name").prop("required",true); 
			} else {
				$(".set-name-tp").fadeOut(100);
				$("#tpl-name").prop("disabled",true);
				$("#tpl-name").prop("required",false);
			}
		});
		/*toggle Receiver*/
		$('.rcv-addfriend').change( function() {
			var isChecked = this.checked;
			if(isChecked) {
				$("#phone-chk").addClass('bg-light');
				$("#number-check").removeClass('hid');
			} else {
				$("#phone-chk").removeClass('bg-light');
				$("#number-check").addClass('hid');
			}
		});
		function removeBtnCheck() {
		  $('#number-check').find('i').removeClass('fa-spinner').removeClass('fa-spin');
		  $('#showPhone').fadeIn(100);
		}
		$('#number-check').click( function() {
			$(this).find('i').addClass('fa-spinner').addClass('fa-spin');
			setTimeout(function () {
				removeBtnCheck();
			}, 1800);
			
		});

	  
});

</script>


<script type="text/javascript">
$( document ).ready( function () {
	 
      $('.carousel').flexslider({
        animation: "slide",
        animationLoop: false,
        itemWidth: 210,
        itemMargin: 5,
        minItems: 1,
        maxItems: 3,
		move: 1,
        /*start: function(slider){
          $('body').removeClass('loading');
        }*/
      });
	  /*send check type*/
	  $('input[type=radio][name=chk-send]').change(function() {
		if (this.value == 'now') {
			$("#show-schedule").hide();
		}
		else if (this.value == 'set') {
			$("#show-schedule").show();
		}
	});
	//select2
	$(".select2").select2();
	$(".select-box").select2({dropdownAutoWidth : '70',minimumResultsForSearch: -1});
	$('.keep-select-group').select2({
    	placeholder: "Please select",
    	//allowClear: true,
		//tags: true,
		dropdownAutoWidth : true,
		width: '100%'
	});
	$('.js-select-multi').select2({
		//maximumSelectionLength: 3,
    	placeholder: "Select",
    	//allowClear: true,
		//tags: true,
		dropdownAutoWidth : true,
		width: '100%'
	});
	$(".js-adv-select").select2({
	   //dropdownAutoWidth : true,
	  width: '100%',
	  tags: true,
	  insertTag: function (data, tag) {
		// Insert the tag at the end of the results
		data.push(tag);
	  }
	  });

	/*Groups all*/
	$("#checkAll").click(function(){
		if($("#checkAll").is(':checked') ){
			$("#e1 > option").prop("selected","selected");
			$("#e1").trigger("change");
		}else{
			$("#e1 > option").removeAttr("selected");
			$("#e1").trigger("change");
		 }
	});
	/*users all*/
	$('.master-chk').click(function() {
		if($(this).is(':checked')) {
			$("input[class*='chd-chk']").prop("checked", true);
			$(this).prop("checked", true);
		} else {
			$("input[class*='chd-chk']").prop("checked", false);
			$(this).prop("checked", false);
		}
	});
	$("input[class*='chd-chk']").change( function() {
		$('.master-chk').prop("checked", false);
	});
	/*count remain*/
	//var credit = 3;
	//var count = 1;
	var checkboxes = $('.bubble-chk');
	var remain = $('#countRmainUID').val();
	checkboxes.change(function(){
        var current = checkboxes.filter(':checked').length;
		//alert(current);
		$('#countRmainUID').val(remain - current);
		if(current >= remain) {
		   this.checked = false;
		   checkboxes.filter(':not(:checked)').prop("disabled", true);
		   alert("ขออภัยค่ะเครดิตคุณเหลือ 0 ");
	   }
        //checkboxes.filter(':not(:checked)').prop('disabled', current >= max);
    });
	
	/*$('.bubble-chk').on('change', function(evt) {
	   if($(this).siblings(':checked').length >= remain) {
		   this.checked = false;
		   alert("ขออภัยค่ะเครดิตคุณเหลือ 0 ");
	   }
	   count = $(this).siblings(':checked').length + 1;
	   alert(count);
	   $('#countRmainUID').val(remain - count);
	});*/

});
  </script>
  
  <script>
$( document ).ready( function () {

/*tab carousel*/
$(".cs-flex-tab").on("click", "a", function (e) {
        e.preventDefault();
        if (!$(this).hasClass('add-tab')) {
            $(this).tab('show');
        }
    })
    .on("click", "span", function () {
        var anchor = $(this).siblings('a');
        $(anchor.attr('href')).remove();
        $(this).parent().remove();
        $(".cs-flex-tab li").children('a').first().click();
    });


$('.add-tab').click(function (e) {
    e.preventDefault();
    var id = $(".cs-flex-tab").children().length; //think about it ;)
    var tabId = 'card_' + id;
	/*var startId = 'start: ' + id;
	var settings = { startId , change:false }; 
	alert ( startId );
	alert ( settings );*/
    $(this).closest('li').before('<li class="nav-item"><a class="nav-link" href="#card_' + id + '">Card ' + id + ' </a> <span> x </span></li>');
    //$('.tab-content').append('<div class="tab-pane" id="' + tabId + '">Contact Form: New Contact ' + id + '</div>');
	var clonecard= '<div id="main-card3" class="main-card wrap-upload">';
	clonecard+='<div class="form-group p-0">';
	clonecard+='<label>Select Type Image ' + id + '</label>';
	clonecard+='<select class="custom-select" id="select-card' + id + '-type">';
	clonecard+='<option value="0">URL</option>';
	clonecard+='<option value="1">Local file</option>';
	clonecard+='</select>';
	clonecard+='</div>';

	clonecard+='<div id="f-url_card' + id + '" class="f-url form-group">  ';
	clonecard+='<label for="card_urlimg' + id + '">From image URL</label>';
	clonecard+='<div class="input-group mb-3">';
	clonecard+='<div class="input-group-prepend">';
	clonecard+='<span class="input-group-text"><i class="fas fa-link"></i></span>';
	clonecard+='</div>';
	clonecard+='<input type="text" class="form-control" placeholder="URL Image" id="card_urlimg' + id + '">';
	clonecard+='</div>';
	clonecard+='</div>';

	clonecard+='<div id="f-upload_card' + id + '" class="f-upload form-group" style="display: none">';
	clonecard+='<label for="card_img' + id + '-1">Upload image</label>';
	clonecard+='<div class="input-group">';
	clonecard+='<div class="custom-file">';
	clonecard+='<input type="file" class="card-file-input' + id + '" id="card_img' + id + '" accept=".jpg,.jpeg,.png">';
	clonecard+='<img id="temp_card' + id + '_src" hidden/>';
	clonecard+='<label class="custom-file-label" id="card_labelimg' + id + '" for="card_img' + id + '"><p>Choose file</p></label>';
	clonecard+='</div>';
	clonecard+='<div class="input-group-append">';
	clonecard+='<button class="input-group-text" id="card_uploadimg' + id + '">Upload</button>';
	clonecard+='</div>';

	clonecard+='</div>';
	clonecard+='</div>';
	clonecard+='</div>';
	clonecard+='<div class="form-group pa10-xs bg-light rounded">';
	clonecard+='<label>Link URL</label>';
	clonecard+='<div class="input-group">';
	clonecard+='<input type="text" class="form-control" placeholder="Action URL">';
	clonecard+='<div class="input-group-append">';
	clonecard+='<span class="input-group-text"><i class="fas fa-link"></i></span>';
	clonecard+='</div>';
	clonecard+='</div>';
	clonecard+='</div>';
	

	clonecard+='<div class="form-group"><label>Title Card</label>';
	clonecard+='<input type="text" class="form-control" id="titleCard' + id + '" placeholder="Title">';
	clonecard+='</div>';

	clonecard+='<div class="form-group">';
	clonecard+='<textarea id="textCard' + id + 'D1" class="form-control mb10-xs" rows="2" placeholder="Description"></textarea>';
	clonecard+='<textarea id="textCard' + id + 'D2" class="form-control" rows="2" placeholder="Description 2"></textarea>';
	clonecard+='</div>';

	clonecard+='<div class="form-group"><label>Number of Actions</label>';
	clonecard+='<select class="form-control select-action-chd" id="select-card' + id + 'Num-action"><option value="0">1</option> <option value="1">2</option><option value="2">3</option></select>';
	clonecard+='</div>';

	clonecard+='<div class="main-apm-action bg-light pa10-xs">';
	clonecard+='<div id="card' + id + '-a1" class="g-action1"><div class="form-group"><label>Action1</label><div class="row middle-xs mb10-xs"><span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Type</span><div class="col">';
	clonecard+='<select class="form-control" id="select-card' + id + 'Num-action1"><option value="0">Message Action</option><option value="1">URI Action</option><option value="2">Postback Action</option></select>';
	clonecard+='</div>';
	clonecard+='</div>';

	clonecard+='<div class="row middle-xs mb10-xs"><span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Label</span><div class="col">';
	clonecard+='<input type="text" class="form-control" id="actionCard' + id + '" placeholder="Action 1">';
	clonecard+='</div>';
	clonecard+='</div>';

	clonecard+='<div class="row middle-xs mb10-xs"><span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Text</span><div class="col">';
	clonecard+='<input type="text" class="form-control" id="actionLinkCard' + id + '" placeholder="Action 1">';
	clonecard+='</div>';
	clonecard+='</div>';
	clonecard+='</div>';
	clonecard+='</div>';

	clonecard+='<div id="card' + id + '-a2" class="g-action2"><div class="form-group"><label>Action2</label><div class="row middle-xs mb10-xs"><span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Type</span><div class="col">';
	clonecard+='<select class="form-control" id="select-card' + id + 'Num2-action2"><option value="0">Message Action</option><option value="1" selected>URI Action</option><option value="2">Postback Action</option></select>';
	clonecard+='</div>';
	clonecard+='</div>';

	clonecard+='<div class="row middle-xs mb10-xs"><span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Label</span><div class="col">';
	clonecard+='<input type="text" class="form-control" id="actionCard' + id + '-2" placeholder="Action 2">';
	clonecard+='</div>';
	clonecard+='</div>';

	clonecard+='<div class="row middle-xs mb10-xs"><span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">URI</span><div class="col">';
	clonecard+='<input type="text" class="form-control" id="actionLinkCard' + id + '-2" placeholder="URI">';
	clonecard+='</div>';
	clonecard+='</div>';
	clonecard+='</div>';
	clonecard+='</div>';

	clonecard+='<div id="card' + id + '-a3" class="g-action3"><div class="form-group"><label>Action3</label><div class="row middle-xs mb10-xs"><span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Type</span><div class="col">';
	clonecard+='<select class="form-control" id="select-card' + id + 'Num3-action3"><option value="0">Message Action</option><option value="1" selected>URI Action</option><option value="2">Postback Action</option></select>';
	clonecard+='</div>';
	clonecard+='</div>';

	clonecard+='<div class="row middle-xs mb10-xs"><span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Label</span><div class="col">';
	clonecard+='<input type="text" class="form-control" id="actionCard' + id + '-3" placeholder="Action 3">';
	clonecard+='</div>';
	clonecard+='</div>';

	clonecard+='<div class="row middle-xs mb10-xs"><span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">URI</span><div class="col">';
	clonecard+='<input type="text" class="form-control" id="actionLinkCard' + id + '-3" placeholder="URI">';
	clonecard+='</div>';
	clonecard+='</div>';
	clonecard+='</div>';
	clonecard+='</div>';

	clonecard+='</div>';

	//write
	//$('.tab-content').append('<div class="tab-pane" id="' + tabId + '">' + clonecard + '</div>');
    //$('.cs-flex-tab li:nth-child(' + id + ') a').click();
	//write
	$('#vert-tabs-flex .tab-content').append('<div class="tab-pane" id="' + tabId + '">' + clonecard + '</div>');
	$('#custom-carousel-tab').idTabs(); 
    $('#custom-carousel-tab li:nth-child(' + id + ') a').click();
	function callToggleNum() {
		$('.select-action-chd').on('change', function() {

			if ($(this)[0].selectedIndex == 0) { 
				$(this).parent().next('.main-apm-action').children('.g-action1').show();
				$(this).parent().next('.main-apm-action').children('.g-action2').hide();
				$(this).parent().next('.main-apm-action').children('.g-action3').hide();

			} else if ($(this)[0].selectedIndex == 1)  {
				$(this).parent().next('.main-apm-action').children('.g-action1').show();
				$(this).parent().next('.main-apm-action').children('.g-action2').show();
				$(this).parent().next('.main-apm-action').children('.g-action3').hide();
			} else if ($(this)[0].selectedIndex == 2)  {
				$(this).parent().next('.main-apm-action').children('.g-action1').show();
				$(this).parent().next('.main-apm-action').children('.g-action2').show();
				$(this).parent().next('.main-apm-action').children('.g-action3').show();
			}
		});
	}
	callToggleNum();
	
});

});
</script>
  

<script>
$( function() {
	
    //$( "#calendarshow" ).datepicker();

		$( "#datepicker" ).datepicker({
		  showOtherMonths: true,
		  selectOtherMonths: true
		});
	//set time	
	var timepicker = new TimePicker('time', {
  lang: 'en',
  theme: 'dark'
});
timepicker.on('change', function(evt) {
  
  var value = (evt.hour || '00') + ':' + (evt.minute || '00');
  evt.element.value = value;

});

  } );
</script>



<!--Plugin CSS file with desired skin-->
<link rel="stylesheet" href="js/tagator/fm.tagator.jquery.css">
<!--Plugin JavaScript file-->
<script src="js/tagator/fm.tagator.jquery.js"></script>
<!-- /js -->

</body>
</html>
