<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top-web.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden">
<!-- Headbar -->
<?php include("incs/header-v2.html") ?>
<!-- /Headbar -->
<div class="page-checkout">

    
    <div id="toc">
		<section class="z-success txt-c _self-pt30">

			<div class="bg-gray2">
				<div class="container msg">

					<h2><span class="t-red2">Test Popup Click</span></h2>	
					<p>กรุณาลองคลิกเพื่อเปิดป๊อบอัพ</p>
					
					<div class="ctrl-btn txt-c _self-mt20">
						<p><a href="javascript:;" data-fancybox="trial" data-src="#popupGetCredit" data-modal="true" title="popup" class="ui-btn-black btn-sm">รับสิทธิ์ Free</a></p>
					</div>
					
					<div class="ctrl-btn txt-c _self-mt20">
						<p><a href="javascript:;" data-fancybox="trialEN" data-src="#popupGetCreditEN" data-modal="true" title="popup" class="ui-btn-black btn-sm">30 Days Free</a></p>
					</div>
					
					<div class="ctrl-btn txt-c _self-mt20">
						<p><a href="javascript:;" data-fancybox="Change" data-src="#popupChangePK" data-modal="true" title="popup" class="ui-btn-black btn-sm">Change Package Success</a></p>
					</div>
					
					<div class="ctrl-btn txt-c _self-mt20">
						<p><a href="javascript:;" data-fancybox="ChangeError" data-src="#popupChangeError" data-modal="true" title="popup" class="ui-btn-black btn-sm">Change Package Error</a></p>
					</div>
					
				</div>
			</div>
			
			

			
		</section>
    </div>
</div>

<!-- Popup -->
<div class="popup thm-lite" id="popupGetCredit">
	<div class="box-middle">
		<!--<i class="fas fa-times-circle fa-5x t-red"></i>-->
		<i class="icon-head"><img src="di/icon-error.png"></i>
		<h2 class="t-red">ขออภัยครับ!</h2>	
		<p>คุณได้รับสิทธิ์ทดลองใช้ฟรีไปแล้วครับ.</p>
		<p>	
			<a data-fancybox-close class="ui-btn-green2 btn-sm" title="ตกลง" href="javascript:;" onClick="parent.jQuery.fancybox.close();">ตกลง</a>
		</p>
	</div>
</div>
<!-- /Popup -->

<!-- Popup -->
<div class="popup thm-lite" id="popupGetCreditEN">
	<div class="box-middle">
		<!--<i class="fas fa-times-circle fa-5x t-red"></i>-->
		<i class="icon-head"><img src="di/icon-error.png"></i>
		<h2 class="t-red">SORRY!</h2>	
		<p>You can have only one free trial.</p>
		<p>	
			<a data-fancybox-close class="ui-btn-green2 btn-sm" title="OK" href="javascript:;" onClick="parent.jQuery.fancybox.close();">OK</a>
		</p>
	</div>
</div>
<!-- /Popup -->

<!-- Popup -->
<div class="popup thm-lite" id="popupChangePK">
	<div class="box-middle">
		<!--<i class="fas fa-times-circle fa-5x t-red"></i>-->
		<i class="icon-head"><img src="di/icon-change-pk.png"></i>
		<h2 class="t-black">เปลี่ยนแพ็กเกจ</h2>	
		<p>คุณกำลังจะเปลี่ยนแพ็กเกจการใช้งาน</p>
		<p class="_self-mt10">	
			<a data-fancybox-close class="ui-btn-green2 btn-sm _self-mh10" title="Confirm" href="javascript:;" onClick="parent.jQuery.fancybox.close();">ยืนยัน</a>
			<a data-fancybox-close class="ui-btn-red btn-sm _self-mh10" title="Cancel" href="javascript:;" onClick="parent.jQuery.fancybox.close();">ยกเลิก</a>
		</p>
	</div>
</div>
<!-- /Popup -->

<!-- Popup -->
<div class="popup thm-lite" id="popupChangeError">
	<div class="box-middle">
		<!--<i class="fas fa-times-circle fa-5x t-red"></i>-->
		<i class="icon-head"><img src="di/icon-change-pk.png"></i>
		<h2 class="t-red">ขออภัย <br class="visible-xs">คุณไม่สามารถทำรายการได้</h2>	
		<p>เนื่องจากการใช้งาน Keep a line ของคุณ <br class="visible-xs">ไม่อยู่ตามเงื่อนไขแพ็กเกจ <br>กรุณาจัดการบัญชีของคุณใหม่</p>
		<p class="_self-mt10">	
			<a data-fancybox-close class="ui-btn-green2 btn-sm _self-mh10" title="จัดการบัญชี" href="javascript:;" onClick="parent.jQuery.fancybox.close();">จัดการบัญชี</a>
			<a data-fancybox-close class="ui-btn-red btn-sm _self-mh10" title="Cancel" href="javascript:;" onClick="parent.jQuery.fancybox.close();">ยกเลิก</a>
		</p>
	</div>
</div>
<!-- /Popup -->


<!--<div id="skin-loading" class="bg-wh" onclick="$(this).fadeOut();">
	<div class="lds-hourglass"></div>
</div>-->
<script>
	window.setTimeout(function(){
		$('#skin-loading').fadeOut();
	}, 3000);
</script>

<!-- footer -->
<?php include("incs/footer-web.html") ?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js-web.html") ?>
<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>
<!-- /js -->

</body>
</html>
