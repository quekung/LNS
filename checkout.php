<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top-web.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden">
<!-- Headbar -->
<?php include("incs/header-v2.html") ?>
<!-- /Headbar -->
<div class="page-checkout">

    
    <div id="toc" class="container">
		<section class="z-checkout _self-pt30">
			<div class="bx-stepbar _self-pv20 cb-af container">
				<ul class="progressbar">
						  <li class="active"><span>Choose your plan</span></li>
						  <li class="active"><span>Payment</span></li>
						  <li><span>Account setting</span></li>
						  <li><span>Get started</span></li>
				  </ul>
			</div>
			
			<div class="progress-title txt-c">
				<h3 class="t-gray head">กรุณากรอกข้อมูลส่วนตัว และชำระค่าบริการ</h3>
			</div>
        	<form class="bx-keep form-signin form-checkout" method="post" action="success.php">
				<fieldset class="fix-label">
					<!-- box-->
					<div class="box-san">
						<!--<div class="head-title">
						<h2 class="hd">Order summary</h2>
						</div>-->
						<div class="cover pd0">
							<h3 class="head txt-l">Order summary</h3>
						</div>
						<div class="body">
							<!--<ul class="list-checkout">
								<li class="_flex between-xs"><span>Package</span> <div>Individuals / up to 5 groups</div></li>
								<li class="_flex between-xs"><span>Start Date : </span> <div>01/12/19</div></li>
								<li class="_flex between-xs"><span>Expire Date : </span> <div><span class="t-red">31/12/19</span> (1 month)</div></li>
								<li class="_flex between-xs"><span>Payment amount</span> <div><span class="t-red">59 THB</span> / month</div></li>
								<li class="_flex between-xs"><span>Payment channel</span> <div>Credit/Debit Card</div></li>
							</ul>-->
							<ul data-v-3b901b54="">
								<li data-v-3b901b54="" class="_flex between-xs"><span data-v-3b901b54="">Package</span>
								<div data-v-3b901b54="">Teams Package Up to 5</div>
								</li>
								<li data-v-3b901b54="" class="_flex between-xs"><span data-v-3b901b54="">Start Date: </span>
								<div data-v-3b901b54="">07/05/2020</div>
								</li>
								<li data-v-3b901b54="" class="_flex between-xs"><span data-v-3b901b54="">Expire Date: </span>
								<div data-v-3b901b54="">06/06/2020 (1 month)</div>
								</li>
								<li data-v-3b901b54="" class="_flex between-xs"><span data-v-3b901b54="">Package price</span>
								<div data-v-3b901b54="">250</div>
								</li>
								<li data-v-3b901b54="" class="_flex between-xs"><span data-v-3b901b54="">Payment discount</span>
								<div data-v-3b901b54="">0</div>
								</li>
								<li data-v-3b901b54="" class="_flex between-xs"><span data-v-3b901b54="">Payment amount</span>
								<div data-v-3b901b54="" class="t-red">250</div>
								</li>
								<li data-v-3b901b54="" class="_flex end-xs">
									<small>*Charging as <b>recurring bill payments</b></small>
								</li>
								<!--<li data-v-3b901b54="" class="_flex between-xs"><span data-v-3b901b54="">Next Bill price</span>
								<div data-v-3b901b54="">250</div>
								</li>-->
								<li data-v-3b901b54="" class="_flex between-xs"><span data-v-3b901b54="">Payment channel</span>
								<div data-v-3b901b54="">Credit/Debit Card</div>
								</li>
								</ul>
						</div>
					 </div>
					 <!-- /box-->
					<div class="enterprise">
						<div id="check-enterprise" class="col-xs-12 mz-chk"><input type="checkbox" id="want-tax" name="want-tax" class="Blocked"><label for="want-tax"> กรุณากดเลือก ถ้าต้องการใบเสร็จรับเงิน (สำหรับองค์กร)</label></div>
						<div class="add-detail " style="padding: 0; display: none">

						<!-- box-->
						<div class="box-san">
							<!--<div class="head-title">
							<h2 class="hd">สำหรับองค์กร</h2>
							</div>-->
							<div class="cover pd0">
								<h3 class="head txt-l">สำหรับองค์กร</h3>
							</div>
							<div class="body">
								<div class="row">
									<div class="left _self-cl-xs-12-sm-06">
										<div class="wr">
											<input type="text" class="txt-box" id="tax_name" name="tax_name" disabled>
											<label for="tax_name">ชื่อบริษัท</label>
										</div>
									</div>
									<div class="right _self-cl-xs-12-sm-06">
										<div class="wr">
											<input type="text" class="txt-box" id="tax_ID" name="tax_ID" disabled>
											<label for="tax_ID">Tax ID</label>
										</div>
									</div>
								</div>
								<div class="wr">
									<input type="text" class="txt-box" id="tax_address" name="tax_address" disabled>
									<label for="tax_address">ที่อยู่</label>
								</div>
								<div class="row">
									<div class="left _self-cl-xs-12-sm-04">
										<div class="wr">
											<input type="text" class="txt-box" id="tax_province" name="tax_province" disabled>
											<label for="tax_province">เมือง</label>
										</div>
									</div>
									<div class="mid _self-cl-xs-12-sm-04">
										<div class="wr">
											<input type="text" class="txt-box" id="tax_country" name="tax_country" disabled>
											<label for="tax_country">ประเทศ</label>
										</div>
									</div>
									<div class="right _self-cl-xs-12-sm-04">
										<div class="wr">
											<input type="tel" class="txt-box" id="staff_ctax_zipcodeompany" name="tax_zipcode" disabled maxlength="5">
											<label for="tax_zipcode">รหัสไปรษณีย์</label>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="left _self-cl-xs-12-sm-06">
										<div class="wr">
											<input type="text" class="txt-box" id="tax_tel" name="tax_tel" disabled maxlength="10">
											<label for="tax_tel">เบอร์โทรศัพท์</label>
										</div>
									</div>
									<div class="right _self-cl-xs-12-sm-06">
										&nbsp;
									</div>
								</div>
								
							</div>
						 </div>
						 <!-- /box-->


						</div>
					</div>
					
					<!-- box-->
					<div class="box-san">
						<!--<div class="head-title">
						<h2 class="hd">Credit/Debit Card</h2>
						</div>-->
						<div class="cover pd0">
							<h3 class="head txt-l">Credit/Debit Card</h3>
						</div>
						<div class="body lg-visa">
							<div class="wr">
								<input type="tel" class="txt-box" id="card-num" name="card-num" required maxlength="16">
								<label for="card-num">Card number</label>
							</div>
							
							<div class="wr">
								<input type="text" class="txt-box" id="card-name" name="card-name" required>
								<label for="card-name">Name on card</label>
							</div>
							
							<div class="row">
								<div class="_self-cl-xs-12"><label for="expire-date" class="_self-pl15">Expiry date</label></div>
								<div class="left _self-cl-xs-12-sm-06">
									<div class="js-select">
										<select class="select2" id="expire-month" name="expire-month" data-placeholder="MM">
											<option></option>
											<?php for($i=1;$i<=9;$i++) { ?>
											<option value="<?php echo $i; ?>">0<?php echo $i; ?></option>
											<?php } ?>
											<option value="10">10</option>
											<option value="11">11</option>
											<option value="12">12</option>
										</select>
										
									</div>
								</div>
								<div class="right _self-cl-xs-12-sm-06">
									<div class="js-select">
										<select class="select2" id="expire-year" name="expire-year" data-placeholder="YYYY">
											<option></option>
											<?php for($i=2025;$i>=1950;$i--) { ?>
											<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
	
											<?php } ?>
										</select>
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="left _self-cl-xs-12-sm-06">
									<div class="wr">
										<input type="tel" class="txt-box" id="card-cvv" name="card-cvv" required maxlength="3">
										<label for="staff_mobile">CVV</label>
									</div>
								</div>
								<div class="right _self-cl-xs-12-sm-06">
									<a class="_self-mt30-pt15 _flex middle-xs t-gray" href="javascript:;" data-fancybox="success" data-src="#popupCard"><i class="far fa-question-circle"></i></a>
								</div>
							</div>
							
							<!-- Popup -->
							<div class="popup thm-trans" id="popupCard">
								<div class="box-middle">
								<div class="head">เลข CVV</div>
								<figure><img src="di/cvv-demo.png" alt="CVV"></figure>
								<p>เลข CVV คือ เลขรหัสหลังบัตรเครดิตหรือบัตรเดบิต</p>
								<p>	
									<a data-fancybox-close class="ui-btn-gray2-mini" title="Close" href="javascript:;" onClick="parent.jQuery.fancybox.close();"> Close</a>
								</p>
								</div>
							</div>
							<!-- /Popup -->
							
						</div>
					 </div>
					 <!-- /box-->
					
					<!--<div class="agree">
						<div id="check-accept" class="col-xs-12 mz-chk"><input type="checkbox" id="iam-gree" name="iam-gree" ><label for="iam-gree"> I agree to the Keep a line <a href="javascript:;" data-fancybox="terms" data-type="iframe" data-src="https://www.santakeep.com/terms.html">Agreement and Terms.</a></label></div>
					</div>-->
					
					<div class="agree">
						<div id="check-accept" class="col-xs-12 mz-chk"><input type="checkbox" id="iam-gree" name="iam-gree" required="required"><label for="iam-gree"> I agree to the Keep a line <a class="t-blue" href="javascript:;" data-fancybox="login2" data-src="#popup-terms"> Terms and Conditions.</a></label></div>
						<div id="check-accept" class="col-xs-12 mz-chk"><input type="checkbox" id="iam-gree-policy" name="iam-gree-policy" required="required"><label for="iam-gree-policy"> I agree to the Keep a line <a class="t-blue" href="javascript:;" data-fancybox="login2" data-src="#popup-policy"> Privacy policy.</a></label></div>
					</div>
					
					
					<div class="ctrl-btn txt-c _self-mt20 sticky-bottom">
						<!--<input type="submit" id="Cancel" class="ui-btn-green2 btn-sm" value="เลือกแพ็คเกจอื่น"> --><a href="package.php" title="เลือกแพ็คเกจอื่น" class="ui-btn-green2 btn-sm" ><i class="fas fa-angle-left"></i> เลือกแพ็คเกจอื่น</a> <input type="submit" id="Submit" class="ui-btn-green btn-sm" value="ชำระเงิน">
					</div>
				</fieldset>
			</form>
		</section>
    </div>
</div>

<!--<div id="skin-loading" class="bg-wh" onclick="$(this).fadeOut();">
	<div class="lds-hourglass"></div>
</div>-->
<script>
	window.setTimeout(function(){
		$('#skin-loading').fadeOut();
	}, 3000);
</script>

<!-- footer -->
<?php include("incs/footer-web.html") ?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js-web.html") ?>
<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
<script>
//Dropdown plugin data
$(document).ready(function() {
     $('.select2').select2({
    	placeholder: "Please select",
    	//allowClear: true,
		minimumResultsForSearch: -1,
		dropdownAutoWidth : true,
		width: '100%'
	});
	/*toggle boarding pass*/
	$('.Blocked').change( function() {
		var isChecked = this.checked;
		if(isChecked) {
			$(".add-detail").fadeIn(300);
			$(".add-detail .txt-box").prop("disabled",false);
			$(".add-detail .txt-box").prop("required",true);
		} else {
			$(".add-detail").fadeOut(300);
			$(".add-detail .txt-box").prop("disabled",true); 
			$(".add-detail .txt-box").prop("required",false); 
		}
	});
	
});
</script>
<!-- /js -->

</body>
</html>
