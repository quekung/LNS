<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top-web.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden">
<!-- Headbar -->
<?php include("incs/header-v2.html") ?>

<!-- /Headbar -->
<div id="app" class="container view-atn">
    <div class="close-page"><a href="javascript:;" onclick="clkExit()" title="close">ปิด</a></div>
    
    <div id="app-main">
			
		
        	<form class="bx-keep form-keep form-checkout" method="post" action="#">
				<fieldset class="full">
				
				<div id="profile-tabs">
						<div class="cover">
							<figure class="user-avatar">
								<div class="img"><img src="di/avatar-new.png" alt="Avatar"></div>
								<div class="pk-detail">
									<h2 class="name">James Brown</h2>
									<p>Lite Package</p>
									<p>Pro Package</p>
								</div>
							</figure>
							<ul class="idTabs _self-mb20">
								<li><a href="#profile-t1" class="selected">My Profile</a></li>
								<li><a href="#profile-t2">Team's Profile</a></li>
								<li><a href="#profile-t3">Managing Roles</a></li>
								<li><a href="#profile-t4">Digital Libary</a></li>
							</ul>
						</div>
						<div class="contentTabs">
							<div id="profile-t1" class="wrap-tab">
							<!-- box-->
							<div class="box-san">
	
							<div class="body">

							<div class="user full">
								
								<div>
									<p><b>Name : </b> Penny</p>
									<p><b>Surname : </b> Lane</p>
									<p><b>Employee ID : </b> 12345</p>
									<p><b>Credit : </b> 2/10 Groups</p>
								</div>
							</div>
							<?php /*?><div class="wr">
								<input type="text" class="txt-box" id="group_name" name="group_name" required="">
								<label for="group_name">Group Name</label>
								<span class="line"></span>
							</div><?php */?>

							<div id="tabs">
								<ul class="idTabs">
									<li><a href="#tabs-1" class="selected">Active</a></li>
									<li><a href="#tabs-2">Inactive</a></li>

								</ul>
								<div id="tabs-1" class="bx-tab">
									<div class="box-result list-group acc">
										<ul>
											<li class="_flex between-xs middle-xs">
												<!--<a href="javascript:;" data-fancybox="" data-src="#onOffBot" title="Remove bots from the group">-->
													<span>group A <a class="del" href="javascript:;" title="Delect Group" data-fancybox="" data-src="#leaveGroup" data-modal="true"><i class="far fa-trash-alt"></i></a></span>
													<div class="grp-r">
														<?php /*?><label class="toggle">
														  <span class="hidden-xs hidden-sm toggle-label">BOT ON</span>
														  <input class="toggle-checkbox" type="checkbox" name="status" checked="">
														  <div class="toggle-switch"></div>
														</label><?php */?>
														<span class="g-menu idTabs">
															<!--<a class="del" href="javascript:;" title="Delect Group" data-fancybox="" data-src="#leaveGroup" data-modal="true"><i class="far fa-trash-alt"></i></a>-->
															<a class="m selected" href="#bx-set1-1"><i class="fas fa-file-import fa-2x"></i></a>
															<a class="m" href="#bx-set1-2"><i class="fas fa-user-check fa-2x"></i></a>
														</span>
														<a href="javascript:;" class="expand"><i class="ic-setting"></i></a>
													</div>
													
													<div class="bx-setting">
														<!-- file type -->
														<div id="bx-set1-1" class="file-type mt10">
														<form method="post">
														<label>
														<div class="_flex between-xs middle-xs">
															<span>Choose file type allow bot to store </span>
															<span class="mz-chk">
																<input type="checkbox" id="chk-01" name="chk-01" class="master-chk"> <label for="chk-01">Select All</label>
															</span>
														</div>
														</label>
														<div class="g-chk">

															<div class="row mb0 _chd-cl-xs-04-xsh-03-sm-02-md-02">
															<span class="mz-chk">
																<input type="checkbox" id="chk-02" name="chk-02" class="chd-chk" checked=""> <label for="chk-02">.doc</label>
															</span>
															<span class="mz-chk">
																<input type="checkbox" id="chk-03" name="chk-03" class="chd-chk" checked=""> <label for="chk-03">.xls</label>
															</span>
															<span class="mz-chk">
																<input type="checkbox" id="chk-04" name="chk-04" class="chd-chk" checked=""> <label for="chk-04">.ppt</label>
															</span>
															</div>

															<div class="row mb0 _chd-cl-xs-04-xsh-03-sm-02-md-02">
															<span class="mz-chk">
																<input type="checkbox" id="chk-05" name="chk-05" class="chd-chk"> <label for="chk-05">.pdf</label>
															</span>
															<span class="mz-chk">
																<input type="checkbox" id="chk-06" name="chk-06" class="chd-chk" checked=""> <label for="chk-06">image</label>
															</span>
															<span class="mz-chk">
																<input type="checkbox" id="chk-07" name="chk-07" class="chd-chk"> <label for="chk-07">others</label>
															</span>
															</div>

														</div>
														<div class="_self-mt10 txt-r"><input type="submit" class="ui-btn-green btn-xs" value="Save"></div>
														</form>
														</div>
														<!-- /file type -->
														
														<!-- User allow -->
														<div id="bx-set1-2" class="file-type u-allow mt10">
															<span>All users allowed in this group</span>
															<ol>
																<?php for($i=1;$i<=200;$i++){ ?>
																<li>
																	<div class="user">
																		<img src="di/avatar-new.png" alt="Avatar">
																		<p><?php echo $i; ?> Penny Lane</p>
																	</div>
																</li>
																<?php } ?>
															</ol>
														</div>
														<!-- /User allow -->
													</div>
													
												<!--</a>-->
											</li>
											<li class="_flex between-xs middle-xs">
													<span>group B</span>
													<div class="grp-r">
														<span class="g-menu idTabs">
															<a class="del" href="javascript:;" title="Delect Group" data-fancybox="" data-src="#leaveGroup" data-modal="true"><i class="far fa-trash-alt"></i></a>
															<a class="m selected" href="#bx-set2-1"><i class="fas fa-file-import fa-2x"></i></a>
															<a class="m" href="#bx-set2-2"><i class="fas fa-user-check fa-2x"></i></a>
														</span>
														<a href="javascript:;" class="expand"><i class="ic-setting"></i></a>
													</div>
													
													<div class="bx-setting">
														<!-- file type -->
														<div id="bx-set2-1" class="file-type mt10">
														<form method="post">
														<label>
														<div class="_flex between-xs middle-xs">
															<span>Choose file type allow bot to store </span>
															<span class="mz-chk">
																<input type="checkbox" id="chk2-01" name="chk2-01" class="master-chk"> <label for="chk2-01">Select All</label>
															</span>
														</div>
														</label>
														<div class="g-chk">

															<div class="row mb0 _chd-cl-xs-04-xsh-03-sm-02-md-02">
															<span class="mz-chk">
																<input type="checkbox" id="chk2-02" name="chk2-02" class="chd-chk" checked=""> <label for="chk-02">.doc</label>
															</span>
															<span class="mz-chk">
																<input type="checkbox" id="chk2-03" name="chk2-03" class="chd-chk" checked=""> <label for="chk-03">.xls</label>
															</span>
															<span class="mz-chk">
																<input type="checkbox" id="chk2-04" name="chk2-04" class="chd-chk" checked=""> <label for="chk-04">.ppt</label>
															</span>
															</div>

															<div class="row mb0 _chd-cl-xs-04-xsh-03-sm-02-md-02">
															<span class="mz-chk">
																<input type="checkbox" id="chk2-05" name="chk2-05" class="chd-chk"> <label for="chk-05">.pdf</label>
															</span>
															<span class="mz-chk">
																<input type="checkbox" id="chk2-06" name="chk2-06" class="chd-chk" checked=""> <label for="chk-06">image</label>
															</span>
															<span class="mz-chk">
																<input type="checkbox" id="chk2-07" name="chk2-07" class="chd-chk"> <label for="chk-07">others</label>
															</span>
															</div>

														</div>
														
														<div class="_self-mt10 txt-r"><input type="submit" class="ui-btn-green btn-xs" value="Save"></div>
														</form>
													</div>
														<!-- /file type -->
														
														<!-- User allow -->
														<div id="bx-set2-2" class="file-type u-allow mt10">
															<span>All users allowed in this group</span>
															<ol>
																<?php for($i=1;$i<=20;$i++){ ?>
																<li>
																	<div class="user">
																		<img src="di/avatar-new.png" alt="Avatar">
																		<p><?php echo $i; ?> Penny Lane</p>
																	</div>
																</li>
																<?php } ?>
															</ol>
														</div>
														<!-- /User allow -->
													</div>
											</li>
											<li class="_flex between-xs middle-xs">
													<span>group C </span>
													<div class="grp-r">
														<span class="g-menu idTabs">
															<a class="del" href="javascript:;" title="Delect Group" data-fancybox="" data-src="#leaveGroup" data-modal="true"><i class="far fa-trash-alt"></i></a>
															<a class="m selected" href="#bx-set3-1"><i class="fas fa-file-import fa-2x"></i></a>
															<a class="m" href="#bx-set3-2"><i class="fas fa-user-check fa-2x"></i></a>
														</span>
														<a href="javascript:;" class="expand"><i class="ic-setting"></i></a>
													</div>
													
													<div class="bx-setting">
														<!-- file type -->
														<div id="bx-set3-1" class="file-type mt10">
														<form method="post">
														<label>
														<div class="_flex between-xs middle-xs">
															<span>Choose file type allow bot to store </span>
															<span class="mz-chk">
																<input type="checkbox" id="chk3-01" name="chk3-01" class="master-chk"> <label for="chk3-01">Select All</label>
															</span>
														</div>
														</label>
														<div class="g-chk">

															<div class="row mb0 _chd-cl-xs-04-xsh-03-sm-02-md-02">
															<span class="mz-chk">
																<input type="checkbox" id="chk3-02" name="chk3-02" class="chd-chk" checked=""> <label for="chk-02">.doc</label>
															</span>
															<span class="mz-chk">
																<input type="checkbox" id="chk3-03" name="chk3-03" class="chd-chk" checked=""> <label for="chk-03">.xls</label>
															</span>
															<span class="mz-chk">
																<input type="checkbox" id="chk3-04" name="chk3-04" class="chd-chk" checked=""> <label for="chk-04">.ppt</label>
															</span>
															</div>

															<div class="row mb0 _chd-cl-xs-04-xsh-03-sm-02-md-02">
															<span class="mz-chk">
																<input type="checkbox" id="chk3-05" name="chk3-05" class="chd-chk"> <label for="chk-05">.pdf</label>
															</span>
															<span class="mz-chk">
																<input type="checkbox" id="chk3-06" name="chk3-06" class="chd-chk" checked=""> <label for="chk-06">image</label>
															</span>
															<span class="mz-chk">
																<input type="checkbox" id="chk3-07" name="chk3-07" class="chd-chk"> <label for="chk-07">others</label>
															</span>
															</div>

														</div>
														
														<div class="_self-mt10 txt-r"><input type="submit" class="ui-btn-green btn-xs" value="Save"></div>
														</form>
													</div>
														<!-- /file type -->
														
														<!-- User allow -->
														<div id="bx-set3-2" class="file-type u-allow mt10">
															<span>All users allowed in this group</span>
															<ol>
																<?php for($i=1;$i<=20;$i++){ ?>
																<li>
																	<div class="user">
																		<img src="di/avatar-new.png" alt="Avatar">
																		<p><?php echo $i; ?> Penny Lane</p>
																	</div>
																</li>
																<?php } ?>
															</ol>
														</div>
														<!-- /User allow -->
													</div>
											</li>

										</ul>
									</div>
								</div>
								<div id="tabs-2" class="bx-tab" style="display: none">
									<div class="box-result list-group-leave">
										<ul>
											<li>
												<a href="javascript:;">
													<span>group X</span>
													<small>Left - 21/08/2019  12:08</small>
												</a>
											</li>
											<li>
												<a href="javascript:;">
													<span>group Y</span>
													<small>Left - 11/08/2019  16:53</small>
												</a>
											</li>
											<li>
												<a href="javascript:;">
													<span>group Z</span>
													<small>Left - 31/07/2019  24:00</small>
												</a>
											</li>

										</ul>
									</div>
								</div>
							</div>




							</div>
							</div>
							<!-- /box -->
							</div>
							<!-- Tab2 -->
							<div id="profile-t2" class="wrap-tab">
							<div class="box-san">

									<div class="body">

							<div class="user full">
								<div>
									<p><b>Name : </b> Penny</p>
									<p><b>Surname : </b> Lane</p>
									<p><b>Employee ID : </b> 12345</p>
									<p><b>Credit : </b> 2/10 Groups</p>
								</div>
							</div>
							<?php /*?><div class="wr">
								<input type="text" class="txt-box" id="group_name" name="group_name" required="">
								<label for="group_name">Group Name</label>
								<span class="line"></span>
							</div><?php */?>

							<div id="tabs2">
								<ul class="idTabs">
									<li><a href="#tabs2-1" class="selected">Active</a></li>
									<li><a href="#tabs2-2">Inactive</a></li>

								</ul>
								<div id="tabs2-1" class="bx-tab">
									<div class="box-result list-group acc">
										<ul>
											<li class="_flex between-xs middle-xs">
												<!--<a href="javascript:;" data-fancybox="" data-src="#onOffBot" title="Remove bots from the group">-->
													<span>group A</span>
													<div class="grp-r">
														<span class="g-menu idTabs">
															<a class="del" href="javascript:;" title="Delect Group" data-fancybox="" data-src="#leaveGroup" data-modal="true"><i class="far fa-trash-alt"></i></a>
															<a class="m selected" href="#tbx-set1-1"><i class="fas fa-file-import fa-2x"></i></a>
															<a class="m" href="#tbx-set1-2"><i class="fas fa-user-check fa-2x"></i></a>
														</span>
														<a href="javascript:;" class="expand"><i class="ic-setting"></i></a>
													</div>
													
													<div class="bx-setting">
														<!-- file type -->
														<div id="tbx-set1-1" class="file-type mt10">
														<form method="post">
														<label>
														<div class="_flex between-xs middle-xs">
															<span>Choose file type allow bot to store </span>
															<span class="mz-chk">
																<input type="checkbox" id="chk-team-01" name="chk-team-01" class="master-chk"> <label for="chk-team-01">Select All</label>
															</span>
														</div>
														</label>
														<div class="g-chk">

															<div class="row mb0 _chd-cl-xs-04-xsh-03-sm-02-md-02">
															<span class="mz-chk">
																<input type="checkbox" id="chk-team-02" name="chk-team-02" class="chd-team-chk" checked=""> <label for="chk-team-02">.doc</label>
															</span>
															<span class="mz-chk">
																<input type="checkbox" id="chk-team-03" name="chk-team-03" class="chd-chk" checked=""> <label for="chk-team-03">.xls</label>
															</span>
															<span class="mz-chk">
																<input type="checkbox" id="chk-team-04" name="chk-team-04" class="chd-chk" checked=""> <label for="chk-team-04">.ppt</label>
															</span>
															</div>

															<div class="row mb0 _chd-cl-xs-04-xsh-03-sm-02-md-02">
															<span class="mz-chk">
																<input type="checkbox" id="chk-team-05" name="chk-team-05" class="chd-chk"> <label for="chk-team-05">.pdf</label>
															</span>
															<span class="mz-chk">
																<input type="checkbox" id="chk-team-06" name="chk-team-06" class="chd-chk" checked=""> <label for="chk-team-06">image</label>
															</span>
															<span class="mz-chk">
																<input type="checkbox" id="chk-team-07" name="chk-team-07" class="chd-chk"> <label for="chk-team-07">others</label>
															</span>
															</div>

														</div>
														<div class="_self-mt10 txt-r"><input type="submit" class="ui-btn-green btn-xs" value="Save"></div>
														</form>
													</div>
														<!-- /file type -->
														
														<!-- User allow -->
														<div id="tbx-set1-2" class="file-type u-allow mt10">
															<span>All users allowed in this group</span>
															<ol>
																<?php for($i=1;$i<=50;$i++){ ?>
																<li>
																	<div class="user">
																		<img src="di/avatar-new.png" alt="Avatar">
																		<p><?php echo $i; ?> Penny Lane</p>
																	</div>
																</li>
																<?php } ?>
															</ol>
														</div>
														<!-- /User allow -->
													</div>
													
												<!--</a>-->
											</li>
											<li class="_flex between-xs middle-xs">
													<span>group B </span>
													<div class="grp-r">
														<span class="g-menu idTabs">
															<a class="del" href="javascript:;" title="Delect Group" data-fancybox="" data-src="#leaveGroup" data-modal="true"><i class="far fa-trash-alt"></i></a>
															<a class="m selected" href="#tbx-set2-1"><i class="fas fa-file-import fa-2x"></i></a>
															<a class="m" href="#tbx-set2-2"><i class="fas fa-user-check fa-2x"></i></a>
														</span>
														<a href="javascript:;" class="expand"><i class="ic-setting"></i></a>
													</div>
													
													<div class="bx-setting">
														<!-- file type -->
														<div id="tbx-set2-1" class="file-type mt10">
														<form method="post">
														<label>
														<div class="_flex between-xs middle-xs">
															<span>Choose file type allow bot to store </span>
															<span class="mz-chk">
																<input type="checkbox" id="chk2-team-01" name="chk2-team-01" class="master-chk"> <label for="chk2-team-01">Select All</label>
															</span>
														</div>
														</label>
														<div class="g-chk">

															<div class="row mb0 _chd-cl-xs-04-xsh-03-sm-02-md-02">
															<span class="mz-chk">
																<input type="checkbox" id="chk2-team-02" name="chk2-team-02" class="chd-chk" checked=""> <label for="chk2-team-02">.doc</label>
															</span>
															<span class="mz-chk">
																<input type="checkbox" id="chk2-team-03" name="chk2-team-03" class="chd-chk" checked=""> <label for="chk2-team-03">.xls</label>
															</span>
															<span class="mz-chk">
																<input type="checkbox" id="chk2-team-04" name="chk2-team-04" class="chd-chk" checked=""> <label for="chk2-team-04">.ppt</label>
															</span>
															</div>

															<div class="row mb0 _chd-cl-xs-04-xsh-03-sm-02-md-02">
															<span class="mz-chk">
																<input type="checkbox" id="chk2-team-05" name="chk2-team-05" class="chd-chk"> <label for="chk2-team-05">.pdf</label>
															</span>
															<span class="mz-chk">
																<input type="checkbox" id="chk2-team-06" name="chk2-team-06" class="chd-chk" checked=""> <label for="chk2-team-06">image</label>
															</span>
															<span class="mz-chk">
																<input type="checkbox" id="chk2-team-07" name="chk2-team-07" class="chd-chk"> <label for="chk2-team-07">others</label>
															</span>
															</div>

														</div>
														
														<div class="_self-mt10 txt-r"><input type="submit" class="ui-btn-green btn-xs" value="Save"></div>
														</form>
													</div>
														<!-- /file type -->
														
														<!-- User allow -->
														<div id="tbx-set2-2" class="file-type u-allow mt10">
															<span>All users allowed in this group</span>
															<ol>
																<?php for($i=1;$i<=100;$i++){ ?>
																<li>
																	<div class="user">
																		<img src="di/avatar-new.png" alt="Avatar">
																		<p><?php echo $i; ?> Penny Lane</p>
																	</div>
																</li>
																<?php } ?>
															</ol>
														</div>
														<!-- /User allow -->
													</div>
											</li>
											<li class="_flex between-xs middle-xs">
													<span>group C </span>
													<div class="grp-r">
														<span class="g-menu idTabs">
															<a class="del" href="javascript:;" title="Delect Group" data-fancybox="" data-src="#leaveGroup" data-modal="true"><i class="far fa-trash-alt"></i></a>
															<a class="m selected" href="#tbx-set3-1"><i class="fas fa-file-import fa-2x"></i></a>
															<a class="m" href="#tbx-set3-2"><i class="fas fa-user-check fa-2x"></i></a>
														</span>
														<a href="javascript:;" class="expand"><i class="ic-setting"></i></a>
													</div>
													
													<div class="bx-setting">
														<!-- file type -->
														<div id="tbx-set3-1" class="file-type mt10">
														<form method="post">
														<label>
														<div class="_flex between-xs middle-xs">
															<span>Choose file type allow bot to store </span>
															<span class="mz-chk">
																<input type="checkbox" id="chk3-team-01" name="chk3-team-01" class="master-chk"> <label for="chk3-team-01">Select All</label>
															</span>
														</div>
														</label>
														<div class="g-chk">

															<div class="row mb0 _chd-cl-xs-04-xsh-03-sm-02-md-02">
															<span class="mz-chk">
																<input type="checkbox" id="chk3-team-02" name="chk3-team-02" class="chd-chk" checked=""> <label for="chk3-team-02">.doc</label>
															</span>
															<span class="mz-chk">
																<input type="checkbox" id="chk3-team-03" name="chk3-team-03" class="chd-chk" checked=""> <label for="chk3-team-03">.xls</label>
															</span>
															<span class="mz-chk">
																<input type="checkbox" id="chk3-team-04" name="chk3-team-04" class="chd-chk" checked=""> <label for="chk3-team-04">.ppt</label>
															</span>
															</div>

															<div class="row mb0 _chd-cl-xs-04-xsh-03-sm-02-md-02">
															<span class="mz-chk">
																<input type="checkbox" id="chk3-team-05" name="chk3-team-05" class="chd-chk"> <label for="chk3-team-05">.pdf</label>
															</span>
															<span class="mz-chk">
																<input type="checkbox" id="chk3-team-06" name="chk3-team-06" class="chd-chk" checked=""> <label for="chk3-team-06">image</label>
															</span>
															<span class="mz-chk">
																<input type="checkbox" id="chk3-team-07" name="chk3-team-07" class="chd-chk"> <label for="chk3-team-07">others</label>
															</span>
															</div>

														</div>
														
														<div class="_self-mt10 txt-r"><input type="submit" class="ui-btn-green btn-xs" value="Save"></div>
														</form>
													</div>
														<!-- /file type -->
														
														<!-- User allow -->
														<div id="tbx-set3-2" class="file-type u-allow mt10">
															<span>All users allowed in this group</span>
															<ol>
																<?php for($i=1;$i<=10;$i++){ ?>
																<li>
																	<div class="user">
																		<img src="di/avatar-new.png" alt="Avatar">
																		<p><?php echo $i; ?> Penny Lane</p>
																	</div>
																</li>
																<?php } ?>
															</ol>
														</div>
														<!-- /User allow -->
													</div>
											</li>

										</ul>
									</div>
								</div>
								<div id="tabs2-2" class="bx-tab" style="display: none">
									<div class="box-result list-group-leave">
										<ul>
											<li>
												<a href="javascript:;">
													<span>group X</span>
													<small>Left - 21/08/2019  12:08</small>
												</a>
											</li>
											<li>
												<a href="javascript:;">
													<span>group Y</span>
													<small>Left - 11/08/2019  16:53</small>
												</a>
											</li>
											<li>
												<a href="javascript:;">
													<span>group Z</span>
													<small>Left - 31/07/2019  24:00</small>
												</a>
											</li>

										</ul>
									</div>
								</div>
							</div>

							</div>
							</div>
							
							</div>
							<!-- Tab3-->
							<div id="profile-t3" class="wrap-tab">
							<!-- box-->
							<div class="box-san">
									
									<div class="body">

										<div class="user full">

											<div>
												<p><b>Name : </b> Penny</p>
												<p><b>Surname : </b> Lane</p>
												<p><b>Employee ID : </b> 12345</p>
												<p><b>Credit : </b> 2/10 Groups</p>
											</div>
										</div>
										
										<div id="tabs3">
										<ul class="idTabs">
											<li><a href="#tabs2-1" class="selected">Group Member</a></li>
										</ul>
										<div id="tabs3-1" class="bx-tab">
										<div class="c-detail">
										<div class="row js-select">
											<select class="select2" id="select-group-member" name="select-group-member" data-placeholder="Select Group">
												<option></option>
												<option value="1">Group A</option>
												<option value="2">Group B</option>
												<option value="3">Group C</option>
											</select>
										</div>
										<ul>
											<li><b>Member : </b> 5 Users</li>
											<li><span>Admin : </span> 1</li>
											<li><span>User : </span> 1</li>
											<li><span>Guest : </span> 3</li>
										</ul>
										</div>

									<div class="c-user-join acc">
										<ul>
											<li class="_flex between-xs middle-xs">
												<div class="user">
													<img src="di/avatar-new.png" alt="Avatar">
													<div>
														<p>Penny Lane</p>
													</div>
												</div>
												<div class="grp-r permission">
													<span class="view">Admin</span>
													<a href="javascript:;" class="expand"><i class="ic-setting"></i></a>
												</div>
												<div class="bx-setting">
														<!-- file type -->
														<div class="file-type mt10">

														<div class="_flex row _chd-cl-xs-12">
															<span class="_self-mb10">Setting profile</span>
															<p><input type="text" class="txt-box" placeholder="Name"></p>
															<p><input type="text" class="txt-box" placeholder="E-mail"></p>
															<p class="js-select">
															<select class="select2" data-placeholder="Select Type">
																<option></option>
																<option value="1">Super Admin</option>
																<option value="2" selected>Admin</option>
																<option value="3">User</option>
																<option value="4">Guest</option>
															</select>
															</p>
														</div>

														
														<div class="_self-mt10 txt-r"><input type="submit" class="ui-btn-green btn-xs" value="Save"></div>
														
														</div>
														<!-- /file type -->
												</div>
												
											</li>
											<li class="_flex between-xs middle-xs">
												<div class="user">
													<img src="di/avatar-new.png" alt="Avatar">
													<div>
														<p>Penny Lane</p>
													</div>
												</div>
												<div class="grp-r permission">
													<span class="view">User</span>
													<a href="javascript:;" class="expand"><i class="ic-setting"></i></a>
												</div>
												<div class="bx-setting">
														<!-- file type -->
														<div class="file-type mt10">

														<div class="_flex row _chd-cl-xs-12">
															<span class="_self-mb10">Setting profile</span>
															<p><input type="text" class="txt-box" placeholder="Name"></p>
															<p><input type="text" class="txt-box" placeholder="E-mail"></p>
															<p class="js-select">
															<select class="select2" data-placeholder="Select Type">
																<option></option>
																<option value="1">Super Admin</option>
																<option value="2">Admin</option>
																<option value="3" selected>User</option>
																<option value="4">Guest</option>
															</select>
															</p>
														</div>

														
														<div class="_self-mt10 txt-r"><input type="submit" class="ui-btn-green btn-xs" value="Save"></div>
														
														</div>
														<!-- /file type -->
												</div>
												
											</li>
											<li class="_flex between-xs middle-xs">
												<div class="user">
													<img src="di/avatar-new.png" alt="Avatar">
													<div>
														<p>Penny Lane</p>
													</div>
												</div>
												<div class="grp-r permission">
													<span class="view">Guest</span>
													<a href="javascript:;" class="expand"><i class="ic-setting"></i></a>
												</div>
												<div class="bx-setting">
														<!-- file type -->
														<div class="file-type mt10">

														<div class="_flex row _chd-cl-xs-12">
															<span class="_self-mb10">Setting profile</span>
															<p><input type="text" class="txt-box" placeholder="Name"></p>
															<p><input type="text" class="txt-box" placeholder="E-mail"></p>
															<p class="js-select">
															<select class="select2" data-placeholder="Select Type">
																<option></option>
																<option value="1">Super Admin</option>
																<option value="2">Admin</option>
																<option value="3">User</option>
																<option value="4" selected>Guest</option>
															</select>
															</p>
														</div>

														
														<div class="_self-mt10 txt-r"><input type="submit" class="ui-btn-green btn-xs" value="Save"></div>
														
														</div>
														<!-- /file type -->
												</div>
												
											</li>
											<li class="_flex between-xs middle-xs">
												<div class="user">
													<img src="di/avatar-new.png" alt="Avatar">
													<div>
														<p>Penny Lane</p>
													</div>
												</div>
												<div class="grp-r permission">
													<span class="view">Guest</span>
													<a href="javascript:;" class="expand"><i class="ic-setting"></i></a>
												</div>
												<div class="bx-setting">
														<!-- file type -->
														<div class="file-type mt10">

														<div class="_flex row _chd-cl-xs-12">
															<span class="_self-mb10">Setting profile</span>
															<p><input type="text" class="txt-box" placeholder="Name"></p>
															<p><input type="text" class="txt-box" placeholder="E-mail"></p>
															<p class="js-select">
															<select class="select2" data-placeholder="Select Type">
																<option></option>
																<option value="1">Super Admin</option>
																<option value="2">Admin</option>
																<option value="3">User</option>
																<option value="4" selected>Guest</option>
															</select>
															</p>
														</div>

														
														<div class="_self-mt10 txt-r"><input type="submit" class="ui-btn-green btn-xs" value="Save"></div>
														
														</div>
														<!-- /file type -->
												</div>
												
											</li>
											<li class="_flex between-xs middle-xs">
												<div class="user">
													<img src="di/avatar-new.png" alt="Avatar">
													<div>
														<p>Penny Lane</p>
													</div>
												</div>
												<div class="grp-r permission">
													<span class="view">Guest</span>
													<a href="javascript:;" class="expand"><i class="ic-setting"></i></a>
												</div>
												<div class="bx-setting">
														<!-- file type -->
														<div class="file-type mt10">

														<div class="_flex row _chd-cl-xs-12">
															<span class="_self-mb10">Setting profile</span>
															<p><input type="text" class="txt-box" placeholder="Name"></p>
															<p><input type="text" class="txt-box" placeholder="E-mail"></p>
															<p class="js-select">
															<select class="select2" data-placeholder="Select Type">
																<option></option>
																<option value="1">Super Admin</option>
																<option value="2">Admin</option>
																<option value="3">User</option>
																<option value="4" selected>Guest</option>
															</select>
															</p>
														</div>

														
														<div class="_self-mt10 txt-r"><input type="submit" class="ui-btn-green btn-xs" value="Save"></div>
														
														</div>
														<!-- /file type -->
												</div>
												
											</li>
										</ul>
									</div>
										
								</div>
								</div>


							</div>
							</div>
							<!-- /box -->
							</div>
							<!-- Tab4 -->
							<div id="profile-t4" class="wrap-tab">
							<!-- box-->
							<div class="box-san">
								

								<div class="body">

								<h2 class="mt10 txt-r">
									<a href="#" class="ui-btn-green btn-sm"><!--<i class="ic-add"></i>-->  อัพโหลดไฟล์ใหม่ </a>
								</h2>

						<div class="wr">
							<input type="text" class="txt-box" id="topic_name" name="topic_name" required="">
							<label for="topic_name">Topic :</label>
							<span class="line"></span>
						</div>

						<div class="wr">
							<input id="activate_tagator2" type="text" class="txt-box tagator" value="Mileage,document" data-tagator-show-all-options-on-focus="true" data-tagator-autocomplete="['Mileage', 'ค่าเดินทาง', 'เอกสารเบิกค่าเดินทาง', 'เบิกเงิน', 'ฟอร์มเบิกค่าเดินทาง', 'เอกสารการเงิน', 'document']">
							<label for="topic_name">Key words :</label>
							<span class="line"></span>
						</div>

						<div class="wr">
							<div class="file-upload-ar" data-text="Select File">
								<input type="file" class="file-upload-input" id="train_image-01" name="train_image-01" accept="*" required=""><!--  open camera = capture-->
								<label class="hid" for="train_image-01">เลือกอัพโหลดไฟล์</label>                                
							</div>

							<script>
							/*input file*/
							$("form").on("change", ".file-upload-input", function(){ 
								$(this).parent(".file-upload-ar").attr("data-text", $(this).val().replace(/.*(\/|\\)/, '') );
							});
							</script>													
						</div>

						<!--<div class="wr">
							<input type="text" class="txt-box" id="group_name" name="group_name" required="">
							<label for="group_name"><i class="ic-search"></i> Search Group</label>
							<span class="line"></span>
						</div>-->

						<div class="mt20 js-select">
							<select class="select2" id="select-group-train" name="select-group-train" data-placeholder="Select Group">
								<option></option>
								<option value="2">Group A</option>
								<option value="3">Group B</option>
								<option value="4">Group C</option>
							</select>
						</div>


						<div class="ctrl-btn fix-bottom">
							<input type="submit" class="ui-btn-green2 btn-block" value="Submit">
						</div>

						</div>
						</div>
						<!-- /box -->

						<div class="box-result">
							<ul>
								<li>
									<a href="#edit" title="name">
										เอกสารเบิกเงิน
										<i class="ic-edit-gray"></i>
									</a>
								</li>
								<li>
									<a href="#edit" title="name">
										เอกสารทำงานนอกเวลา
										<i class="ic-edit-gray"></i>
									</a>
								</li>
								<li>
									<a href="#edit" title="name">
										ตารางงานเดือนนี้
										<i class="ic-edit-gray"></i>
									</a>
								</li>

							</ul>


						</div>
							</div>
						</div>
				</fieldset>
			</form>
	
    </div>
</div>

<div class="popup thm-lite" id="leaveGroup">
	<div class="box-middle">
	<h2>Do you want to remove bots from the group?</h2>
	<p class="_self-mt10">	
		<a data-fancybox-close class="ui-btn-green3" title="Cancel" href="javascript:;" onClick="parent.jQuery.fancybox.close();">Cancel</a>
		<a data-fancybox-close class="ui-btn-red" title="Confirm" href="javascript:;" onClick="parent.jQuery.fancybox.close();"> Confirm</a>
	</p>
	</div>
</div>

<div class="popup thm-lite" id="onOffBot">
	<div class="box-middle">
	<h2>You are closing the BOT?</h2>
	<p class="_self-mt10">	
		<a data-fancybox-close class="ui-btn-gray" title="Cancel" href="javascript:;" onClick="parent.jQuery.fancybox.close();">Cancel</a>
		<a data-fancybox-close class="ui-btn-green" title="OK" href="javascript:;" onClick="$('.m-switch a').toggleClass('off'); parent.jQuery.fancybox.close();"> OK</a>
	</p>
	</div>
</div>


<!--<div id="skin-loading" class="bg-wh" onclick="$(this).fadeOut();">
	<div class="lds-hourglass"></div>
</div>-->
<script>
	window.setTimeout(function(){
		$('#skin-loading').fadeOut();
	}, 3000);
</script>

<!-- js -->
<?php include("incs/js.html") ?>

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
<link href="cs/fm.tagator.jquery.css" rel="stylesheet" />


<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<script src="js/fm.tagator.jquery.js"></script>
<script src="js/jquery.idTabs.min.js"></script>
<script>
//Dropdown plugin data
$(document).ready(function() {
    $('.select2').select2({
    	placeholder: "Please select",
    	//allowClear: true,
		dropdownAutoWidth : true,
		width: '120'
	});

/*toggle graph*/
$('.expand').click(function(){
		if($(this).hasClass('active')){
            $(this).removeClass('active');
			$(this).parents('li').removeClass('show');
        } else {
			$('.expand').removeClass('active');
			$('.acc li').removeClass('show');
            $(this).toggleClass('active');
			$(this).parents('li').toggleClass('show');
        }
});

/*$('.accordion .AC3').click(function() {
		if ($(this).hasClass( 'active' )) {
		   $(this).removeClass('active').next().removeClass('active').slideUp(200);
		} else {
		   $('.accordion .AC3').removeClass('active').next().removeClass('active').slideUp(200);
		   $(this).toggleClass('active').next().toggleClass('active').slideToggle(200);
		}
    });*/

	$('.master-chk').click(function() {
		if($(this).is(':checked')) {
			$("input[class*='chd-chk']").prop("checked", true);
			$(this).prop("checked", true);
		} else {
			$("input[class*='chd-chk']").prop("checked", false);
			$(this).prop("checked", false);
		}
	});
	$("input[class*='chd-chk']").change( function() {
		$('.master-chk').prop("checked", false);
	});

});
</script>
<!-- /js -->

</body>
</html>
