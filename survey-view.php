<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top-web.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden">
<!-- Headbar -->
<?php include("incs/header-v2.html") ?>
<script>
$(".main-menu .list-unstyled>li.active").removeClass('active');
$(".main-menu .list-unstyled>li:nth-child(4)").addClass('active');
</script>
<!-- /Headbar -->
<div class="page-checkout">

    
    <div id="toc">
		<section class="z-broadcast _self-pt0 mb0">
			<div class="bx-stepbar _self-pv20 cb-af container">
				<ul class="tabsbar">
						  <li><a href="broadcasts.php" title="Send Message"><i class="fas fa-bullhorn"></i> <span>Send Message</span></a></li>
						  <li><a href="broadcasts-create.php" title="Create Message"><i class="fas fa-layer-group"></i> <span>Create Template</span></a></li>
						  <li><a href="broadcasts-acc.php" title="User Detail"><i class="fas fa-users-cog"></i> <span>User Detail</span></a></li>
						  <li><a href="broadcasts-setting.php" title="Message Setting"><i class="fas fa-sliders-h"></i> <span>Message Setting</span></a></li>
						  <li><a href="broadcasts-report.php" title="Report"><i class="fas fa-file-medical-alt"></i> <span>Report</span></a></li>
						  <li><a href="broadcasts-survey.php" title="Survey" class="selected"><i class="fas fa-tasks"></i> <span>Survey</span></a></li>
				  </ul>
			</div>
					

			
			<div class="contentTabs main-sv">
				<div id="tbc-1" class="msg">

					<div class="head-bg _self-mb0">
					<div class="container d-flex between-xs bottom-xs">
						<h2>View survey question</h2>
						<div class="ctrl-btn-tools d-flex _chd-pl05 middle-xs">
							<div><a href="survey-create.php" class="ui-btn-trans-min btn-xs" title="Edit mode"><i class="fas fa-clipboard-list fa-2x t-white"></i></a></div>
							<div>
								<a href="#" class="ui-btn-trans-min btn-xs" title="setting"><i class="fas fa-cog fa-2x t-white"></i></a>
								<div class="sw-anonymous">
									  <label class="toggle">
										  <input class="toggle-checkbox" type="checkbox" name="status">
										  <div class="toggle-switch"></div>
										  <small class="toggle-label">ไม่ระบุชื่อตอบ</small>
									 </label>
								</div>
							</div>
							<div><a href="broadcasts-push-survey.php" class="ui-btn-black-min btn-xs"><i class="fas fa-paper-plane"></i> Push Survey</a></div>
						</div>
					</div>
					</div>
					
					<section class="z-success z-survey txt-c _self-pt30">
						<h2 class="t-black _self-mb20">แบบสำรวจความพึงพอใจในการปฏิบัติงานของพนักงาน ครึ่งปีแรก 2563</h2>	
						<div class="bg-gray2">
							<div class="container congrat wrap-survey">
							<form class="form" name="survey" id="survey" method="post" onsubmit="return chkSubmit()" action="javascript:;">
								<fieldset id="main-survey">
								<div class="bar txt-c">กรุณากรอกแบบสอบถามให้ครบทุกช่องค่ะ</div>
								<div class="sv">
									<h3 class="quest">สภาพการทำงานในปัจจุบัน คุณพอใจระดับไหน</h3>
									<div class="group-radio-rating">
										<label for="text-less">น้อยที่สุด</label>
										<span class="star-rating smile-5">
										  <input type="radio" name="q-sy1" value="1" id="q-sy1-1" required=""><label for="q-sy1-1"></label>
										  <input type="radio" name="q-sy1" value="2" id="q-sy1-2" required=""><label for="q-sy1-2"></label>
										  <input type="radio" name="q-sy1" value="3" id="q-sy1-3" required=""><label for="q-sy1-3"></label>
										  <input type="radio" name="q-sy1" value="4" id="q-sy1-4" required=""><label for="q-sy1-4"></label>
										  <input type="radio" name="q-sy1" value="5" id="q-sy1-5" required=""><label for="q-sy1-5"></label>
										</span>
										<label for="text-exceed">มากที่สุด</label>
									</div>
								</div>
								
								<div class="sv">
									<h3 class="quest">ความรู้ความสามารถเหมาะสมกับภารกิจและหน้าที่ที่ได้รับมอบหมาย</h3>
									<div class="group-radio-rating">
										<label for="text-less">ไม่พอใจ</label>
										<span class="star-rating smile-4">
										  <input type="radio" name="q-sy2" value="1" id="q-sy2-1" required=""><label for="q-sy2-1"></label>
										  <input type="radio" name="q-sy2" value="2" id="q-sy2-2" required=""><label for="q-sy2-2"></label>
										  <input type="radio" name="q-sy2" value="3" id="q-sy2-3" required=""><label for="q-sy2-3"></label>
										  <input type="radio" name="q-sy2" value="4" id="q-sy2-4" required=""><label for="q-sy2-4"></label>
										  <?php /*?><input type="radio" name="q-sy2" value="5" id="q-sy2-5" required=""><label for="q-sy2-5"></label><?php */?>
										</span>
										<label for="text-exceed">พอใจ</label>
									</div>
								</div>
								
								<div class="sv">
									<h3 class="quest">ความรู้ความสามารถเหมาะสมกับภารกิจและหน้าที่ที่ได้รับมอบหมาย</h3>
									<div class="group-radio-rating">
										<label for="text-less">ไม่ชอบ</label>
										<span class="star-rating smile-3">
										  <input type="radio" name="q-sy2-1" value="1" id="q-sy2-1-1" required=""><label for="q-sy2-1-1"></label>
										  <input type="radio" name="q-sy2-1" value="2" id="q-sy2-1-2" required=""><label for="q-sy2-1-2"></label>
										  <input type="radio" name="q-sy2-1" value="3" id="q-sy2-1-3" required=""><label for="q-sy2-1-3"></label>
<?php /*?>										  <input type="radio" name="q-sy2-1" value="4" id="q-sy2-1-4" required=""><label for="q-sy2-1-4"></label>
										  <input type="radio" name="q-sy2-1" value="5" id="q-sy2-1-5" required=""><label for="q-sy2-1-5"></label><?php */?>
										</span>
										<label for="text-exceed">ชอบ</label>
									</div>
								</div>
								
								<div class="sv">
									<h3 class="quest">ภารกิจและหน้าที่ที่ปฏิบัติมีลักษณะท้าทายความรู้และความสามารถ</h3>
									<div class="group-radio-rating">
										<label for="text-less">น้อยที่สุด</label>
										<span class="star-rating star-5">
										  <input type="radio" name="q-sy3" value="1" id="q-sy3-1" required=""><label for="q-sy3-1"></label>
										  <input type="radio" name="q-sy3" value="2" id="q-sy3-2" required=""><label for="q-sy3-2"></label>
										  <input type="radio" name="q-sy3" value="3" id="q-sy3-3" required=""><label for="q-sy3-3"></label>
										  <input type="radio" name="q-sy3" value="4" id="q-sy3-4" required=""><label for="q-sy3-4"></label>
										  <input type="radio" name="q-sy3" value="5" id="q-sy3-5" required=""><label for="q-sy3-5"></label>
										</span>
										<label for="text-exceed">มากที่สุด</label>
									</div>
								</div>
								
								<div class="sv">
									<h3 class="quest">ภารกิจและหน้าที่ที่ปฏิบัติมีลักษณะท้าทายความรู้และความสามารถ</h3>
									<div class="group-radio-rating">
										<label for="text-less">น้อยที่สุด</label>
										<span class="star-rating star-4">
										  <input type="radio" name="q-sy3-1" value="1" id="q-sy3-1-1" required=""><label for="q-sy3-1-1"></label>
										  <input type="radio" name="q-sy3-1" value="2" id="q-sy3-1-2" required=""><label for="q-sy3-1-2"></label>
										  <input type="radio" name="q-sy3-1" value="3" id="q-sy3-1-3" required=""><label for="q-sy3-1-3"></label>
										  <input type="radio" name="q-sy3-1" value="4" id="q-sy3-1-4" required=""><label for="q-sy3-1-4"></label>
										  <input type="radio" name="q-sy3-1" value="5" id="q-sy3-1-5" required=""><label for="q-sy3-1-5"></label>
										</span>
										<label for="text-exceed">มากที่สุด</label>
									</div>
								</div>
								
								<div class="sv">
									<h3 class="quest">ภารกิจและหน้าที่ที่ปฏิบัติมีลักษณะท้าทายความรู้และความสามารถ</h3>
									<div class="group-radio-rating">
										<label for="text-less">น้อยที่สุด</label>
										<span class="star-rating star-3">
										  <input type="radio" name="q-sy3-2" value="1" id="q-sy3-2-1" required=""><label for="q-sy3-2-1"></label>
										  <input type="radio" name="q-sy3-2" value="2" id="q-sy3-2-2" required=""><label for="q-sy3-2-2"></label>
										  <input type="radio" name="q-sy3-2" value="3" id="q-sy3-2-3" required=""><label for="q-sy3-2-3"></label>
										  <input type="radio" name="q-sy3-2" value="4" id="q-sy3-2-4" required=""><label for="q-sy3-2-4"></label>
										  <input type="radio" name="q-sy3-2" value="5" id="q-sy3-2-5" required=""><label for="q-sy3-2-5"></label>
										</span>
										<label for="text-exceed">มากที่สุด</label>
									</div>
								</div>
								
								<div class="sv">
									<h3 class="quest">มีโอกาสร่วมแสดงความคิดในการแก้ปัญหาในการปฏิบัติงาน</h3>
									<div class="group-radio-rating">
										<label for="text-less">น้อยที่สุด</label>
										<span class="q-answer normal-rating d-flex flex-nowrap">
										  <div class="icheck-primary"><input type="radio" name="q-sy4" value="1" id="q-sy4-1" required=""><label for="q-sy4-1"></label></div>
										  <div class="icheck-primary"><input type="radio" name="q-sy4" value="2" id="q-sy4-2" required=""><label for="q-sy4-2"></label></div>
										  <div class="icheck-primary"><input type="radio" name="q-sy4" value="3" id="q-sy4-3" required=""><label for="q-sy4-3"></label></div>
										  <div class="icheck-primary"><input type="radio" name="q-sy4" value="4" id="q-sy4-4" required=""><label for="q-sy4-4"></label></div>
										  <div class="icheck-primary"><input type="radio" name="q-sy4" value="5" id="q-sy4-5" required=""><label for="q-sy4-5"></label></div>
										</span>
										<label for="text-exceed">มากที่สุด</label>
									</div>
								</div>

								<div class="sv none">
									<h3 class="quest">คุณเคยติดต่อหน่วยงานภายในองค์กรฝ่ายใดบ้าง (ตอบได้มากกว่า1ข้อ)</h3>
									<div class="_flex center-sm">
									<ul class="list-reason txt-l">
										<li>
											<div class="icheck-primary">
												<input type="checkbox" name="q5-chk1" id="q5-chk1"> 
												<label for="q5-chk1"> ใช้งานยาก</label>
											</div>
										</li>
										<li>
											<div class="icheck-primary">
												<input type="checkbox" name="q5-chk2" id="q5-chk2"> 
												<label for="q5-chk2"> โครงการสำเร็จแล้ว</label>
											</div>
										</li>
										<li>
											<div class="icheck-primary">
												<input type="checkbox" name="q5-chk3" id="q5-chk3"> 
												<label for="q5-chk3"> ข้อจำกัดของฟีเจอร์</label>
											</div>
										</li>
										<li>
											<div class="icheck-primary">
												<input type="checkbox" name="q5-chk4" id="q5-chk4"> 
												<label for="q5-chk4"> ราคา</label>
											</div>
										</li>
										<li>
											<div class="icheck-primary">
												<input type="checkbox" name="q5-chk5" id="q5-chk5"> 
												<label for="q5-chk5" class="d-flex flex-nowrap"> อื่นๆ
													<input type="text" class="txt-line col _self-pa0-ml10" name="text-other" placeholder="(โปรดระบุ)">
												</label>
											</div>
										</li>
										
									</ul>
									</div>
								</div>
								
								<div class="sv none">
									<h3 class="quest">ปัจจัยใด ที่ทำให้คุณมีความสุขในการทำงาน</h3>
									<div class="_flex center-sm">
									<ul class="list-reason txt-l">
										<li>
											<div class="icheck-primary">
												<input type="radio" name="q6-chk" id="q6-chk1"> 
												<label for="q6-chk1"> เพื่อนร่วมงาน</label>
											</div>
										</li>
										<li>
											<div class="icheck-primary">
												<input type="radio" name="q6-chk" id="q6-chk2"> 
												<label for="q6-chk2"> บรรยากาศที่ทำงาน</label>
											</div>
										</li>
										<li>
											<div class="icheck-primary">
												<input type="radio" name="q6-chk" id="q6-chk3"> 
												<label for="q6-chk3"> รายละเอียดงานที่ทำ</label>
											</div>
										</li>
										<li>
											<div class="icheck-primary">
												<input type="radio" name="q6-chk" id="q6-chk4"> 
												<label for="q6-chk4" class="d-flex flex-nowrap"> อื่นๆ
													<input type="text" class="txt-line col _self-pa0-ml10" name="text-other" placeholder="(โปรดระบุ)">
												</label>
											</div>
										</li>
										
									</ul>
									</div>
								</div>
								
								<div class="sv none">
									<h3 class="quest">คุณทำงานอยู่หน่วยงานใด กรุณาระบุ</h3>
									<div class="_flex center-sm">
										<textarea class="txt-dot" type="text" name="q7-answer" id="q7-answer" placeholder="ระบุหน่วยงาน"></textarea> 
									</div>
								</div>

								<!--<div class="sv none">
									<h3 class="quest">มีอะไรเพิ่มเติมอีกไหม เราจะได้นำไปปรับให้ดีกว่าเดิม</h3>
									<textarea id="comment" name="comment" class="txt-box block" style=" text-align: left" rows="5"></textarea>
								</div>-->

								<div class="ctrl-btn txt-c _self-mt20">
									<a href="javascript:;" class="ui-btn-gray btn-sm">ยกเลิก</a>
									<a href="javascript:;" title="ส่งแบบสอบถาม" class="ui-btn-green2 btn-sm">ส่งแบบสอบถาม</a>
								</div>
								</fieldset>
							</form>
							</div>
						</div>




					</section>

				</div>
			</div>
			
			
			
		</section>
    </div>
</div>

<!--<div id="skin-loading" class="bg-wh" onclick="$(this).fadeOut();">
	<div class="lds-hourglass"></div>
</div>-->
<script>
	window.setTimeout(function(){
		$('#skin-loading').fadeOut();
	}, 3000);
</script>

<!-- footer -->
<?php include("incs/footer-web.html") ?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js-web.html") ?>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<script type="text/javascript">
$( document ).ready( function () {
	


	//select2
	$(".select-num").select2({minimumResultsForSearch: -1});
	



});
  </script>
  


<!-- /js -->

</body>
</html>
