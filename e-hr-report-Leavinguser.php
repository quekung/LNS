<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top-web.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden">
<!-- Headbar -->
<?php include("incs/header-v2.html") ?>
<script>
$(".main-menu .list-unstyled>li.active").removeClass('active');
$(".main-menu .list-unstyled>li:nth-child(4)").addClass('active');
</script>
<!-- /Headbar -->
<div class="page-checkout">

    
    <div id="toc">
		<section class="z-broadcast _self-pt0 mb0">
			<div class="bx-stepbar _self-pv20 cb-af container">
				<ul class="tabsbar">
						  <li><a href="e-hr-dashboard.php" title="Send Message"><i class="fas fa-bullhorn"></i> <span>Dashboard</span></a></li>
						  <li><a href="e-hr.php" title="Create Message"><i class="fas fa-users"></i> <span>Employee</span></a></li>
						  <li><a href="e-hr-employee.php" title="User Detail"><i class="fas fa-users-cog"></i> <span>Import User</span></a></li>
						  <li><a href="e-hr-setting.php" title="Message Setting"><i class="fas fa-sliders-h"></i> <span>Message Setting</span></a></li>
						  <li><a href="e-hr-report.php" title="Report" class="selected"><i class="fas fa-file-medical-alt"></i> <span>Report</span></a></li>
				  </ul>
			</div>
					

			
			<div class="bg-gray2 contentTabs">
				<div id="tbc-1" class="msg">
					<div class="head-bg">
					<div class="container">
						<h2>E-HR</h2>	
					</div>
					</div>
					<div class="wrap-full _chd-cl-xs-12 _chd-cl-sm">
						<div class="main row center-xs">
							<div class="container">
							
								<div class="sort-bar d-flex between-xs middle-xs">
									<div class="sort">
										<select class="form-control select2" data-placeholder="Search by" style="width:auto">
										  <option></option>
										  <option>ชื่อ-นามสกุล</option>
										  <option>ฝ่าย</option>
										  <option>แผนก</option>
										  <option>ตำแหน่ง</option>
										</select>
										<div class="search-sm d-inline">
											<input class="txt-box" placeholder="ค้นหา...">
											<button type="submit" class="fas fa-search" aria-hidden="true"></button>
										</div>
									</div>
									<div class="right">
										<div id="reportrange" class="date-range pr20-xs">
											<i class="fa fa-calendar"></i>&nbsp;
											<span></span> <i class="fa fa-caret-down"></i>
										</div>
									</div>

								</div>
							
								<!-- card -->
								<div class="card bg-white rounded mt20-xs border-0">
									<div class="card-header">
										<h3 class="card-title center-xs"><b>Leaving by User Report</b></h3>
									</div>
									<div class="card-body _self-pa30-pt0 middle-xs">
										<div class="table-resp">
											<table class="table tb-bordered tb-skin">
											  <thead>
											  <tr>
													<th rowspan="2" width="9%" align="center">ประเภท<br>
													การลา</th>
													<th rowspan="2" width="20%" align="center">รายละเอียด</th>
													<th colspan="3" height="26" align="center">จำนวนวันลาที่มีสิทธิ</th>
													</tr>
													<tr>
													<th width="15%" align="center">รวม</th>
													<th width="15%" align="center">ลาจริง</th>
													<th width="15%" align="center">คงเหลือ</th>
													</tr>
											  </thead>
											  <tbody>
													
													<tr>
													<td height="25" align="center">A1</td>
													<td align="center">ลากิจ</td>
													<td align="center">6 วัน</td>
													<td align="center">2 วัน 1 ชม. 30 นาที </td>
													<td align="center">3 วัน 6 ชม. 30 นาที </td>
													</tr>
													<tr>
													<td height="25" align="center">C1</td>
													<td align="center">ลาพักผ่อน</td>
													<td align="center">10 วัน</td>
													<td align="center">5 วัน </td>
													<td align="center">5 วัน </td>
													</tr>
													<tr>
													<td height="25" align="center">S1</td>
													<td align="center">ลาป่วย</td>
													<td align="center">30 วัน</td>
													<td align="center">3 วัน </td>
													<td align="center">27 วัน </td>
													</tr>
													<tr>
													<td height="25" align="center">E1</td>
													<td align="center">ลาอุปสมบท</td>
													<td align="center">120 วัน</td>
													<td align="center">0 วัน </td>
													<td align="center">120 วัน </td>
													</tr>
													<tr class="bg-gray">
													<td colspan="7" height="25">&nbsp;</td>
													</tr>
													</tbody>

											</table>
										  </div>
									</div>
									
									<div class="sticky-bottom card-footer">
									<div class="__chd-ph10 center-xs">
											<button type="button" class="ui-btn-green btn-md" onclick="$('.form-sending')[0].reset();"><i class="fas fa-file-excel"></i> Export Excel</button>
									</div>
								  </div>
								</div>
								<!-- /card -->
							</div>

						</div>
					</div>
				</div>
					</form>

			</div>
			
			
			
			
		</section>
    </div>
</div>

<!--<div id="skin-loading" class="bg-wh" onclick="$(this).fadeOut();">
	<div class="lds-hourglass"></div>
</div>-->
<script>
	window.setTimeout(function(){
		$('#skin-loading').fadeOut();
	}, 3000);
</script>

<!-- footer -->
<?php include("incs/footer-web.html") ?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js-web.html") ?>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<link href="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.css" rel="stylesheet"/>
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
<link href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" rel="stylesheet" />

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript">
$( document ).ready( function () {
	 $(function() {

    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end);

});


	//select2
	$(".select2").select2({minimumResultsForSearch: -1});
	//select2
	$('.keep-select-group').select2({
    	placeholder: "Please select",
    	//allowClear: true,
		dropdownAutoWidth : true,
		width: '100%'
	});
	
	$('#uid-unlimit').change( function() {
			var isChecked = this.checked;
			if(isChecked) {
				$("#uid-limit").removeClass("bg-white");
				$("#uid-limit").prop("disabled",true); 
				$("#uid-limit").prop("required",false); 
			} else {
				$("#uid-limit").addClass("bg-white");
				$("#uid-limit").prop("disabled",false);
				$("#uid-limit").prop("required",true);
			}
		});
	

});
  </script>
  


<!-- /js -->

</body>
</html>
