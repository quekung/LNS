<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top-web.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden">
<!-- Headbar -->
<?php include("incs/header-v2.html") ?>
<!-- /Headbar -->
<div class="page-checkout">

    
    <div id="toc">
		<section class="z-checkout _self-pt0 mb0">
			
			<div class="bx-stepbar _self-pv20 cb-af container">
				<ul class="tabsbar">
						  <li><a href="company.php" title="Send Message" class="selected"><i class="fas fa-archway"></i> <span>Company</span></a></li>
						  <li><a href="department.php" title="Create Message"><i class="fas fa-boxes"></i> <span>Department</span></a></li>
						  <li><a href="member-account.php" title="User management "><i class="fas fa-user-shield"></i> <span>User management</span></a></li>
						  <li><a href="broadcasts-report.php" title="Report"><i class="fas fa-file-medical-alt"></i> <span>Report</span></a></li>
						  <!--<li><a href="broadcasts-acc.php" title="User Detail"><i class="fas fa-users-cog"></i> <span>User Detail</span></a></li>
						  <li><a href="lns-setting.php" title="Message Setting"><i class="fas fa-sliders-h"></i> <span>Message Setting</span></a></li>
						  <li><a href="broadcasts-report.php" title="Report"><i class="fas fa-file-medical-alt"></i> <span>Report</span></a></li>-->
				  </ul>
			</div>
			
			<div class="bg-white">
				<div class="head-bg">
					<div class="container">
						<h2>SETTING</h2>	
					</div>
					</div>
				<div class="container msg">

					
        	<form class="bx-keep form-signin form-checkout _self-pt0" method="post" action="success.php">
				<fieldset class="fix-label _self-pt10">
					<div class="box-san">
							<!--<div class="head-title">
							<h2 class="hd">สำหรับองค์กร</h2>
							</div>-->
							<div class="cover pd0">
								<h3 class="head txt-l">Company Detail</h3>
							</div>
							<div class="body">
								<div class="row">
									<div class="left _self-cl-xs-12-sm-06">
										<div class="wr">
											<input type="text" class="txt-box" id="tax_name" name="tax_name" value="บริษัท อินโน ฮับ จำกัด ">
											<label for="tax_name">ชื่อบริษัท</label>
										</div>
									</div>
									<div class="right _self-cl-xs-12-sm-06">
										<div class="wr">
											<input type="text" class="txt-box" id="display_ID" name="display_ID" value="INNOHUB">
											<label for="display_ID">Display name</label>
										</div>
									</div>
								</div>
								<div class="wr">
									<textarea class="txt-box w100 rounded-1" id="tax_address" name="tax_address">99/10 หมู่ 4 อาคารซอฟต์แวร์ปารค์ ชั้น 26 ถนนแจ้งวัฒนะ ตำบลคลองเกลือ อำเภอปากเกร็ด จังหวัดนนทบุรี 11120</textarea>
									<label for="tax_address">ที่อยู่</label>
								</div>
								<div class="row">
									<div class="left _self-cl-xs-12-sm-04">
										<div class="wr">
											<input type="text" class="txt-box" id="tax_province" name="tax_province" value="Pakkred">
											<label for="tax_province">เมือง</label>
										</div>
									</div>
									<div class="mid _self-cl-xs-12-sm-04">
										<div class="wr">
											<input type="text" class="txt-box" id="tax_country" name="tax_country" value="Thailand">
											<label for="tax_country">ประเทศ</label>
										</div>
									</div>
									<div class="right _self-cl-xs-12-sm-04">
										<div class="wr">
											<input type="tel" class="txt-box" id="staff_ctax_zipcodeompany" name="tax_zipcode" maxlength="5" value="11120">
											<label for="tax_zipcode">รหัสไปรษณีย์</label>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="left _self-cl-xs-12-sm-06">
										<div class="wr">
											<input type="text" class="txt-box" id="tax_tel" name="tax_tel" maxlength="10" value="(662)-502-6000">
											<label for="tax_tel">เบอร์โทรศัพท์</label>
										</div>
									</div>
									<div class="right _self-cl-xs-12-sm-06">
										<div class="wr">
											<input type="email" class="txt-box" id="tax_email" name="tax_email" value="contactus@oto.samartcorp.com">
											<label for="tax_tel">Email</label>
										</div>
									</div>
								</div>
								
							</div>
						 </div>
						 
						 <div class="box-san">
							<!--<div class="head-title">
							<h2 class="hd">สำหรับองค์กร</h2>
							</div>-->
							<div class="cover pd0">
								<h3 class="head txt-l">Team Members</h3>
							</div>
							<div class="body">
								<div id="show-mem" class="row">
									<ul class="users-list w-100 clearfix">
									  <li class="col-2">
										<a href="javascript:;"  data-fancybox="" data-modal ="modal" data-src="#modal-edit-user">
										<img src="https://www.w3schools.com/w3images/avatar2.png" alt="User Image">
										<b class="users-list-name">Alexander Pierce</b>
										<span class="users-list-date">Super Admin</span>
										</a>
										<div class="tools"><i class="fas fa-trash text-white" onClick="$(this).parents('li').remove();"></i></div>
									  </li>
									  <li class="col-2">
										<a href="javascript:;"  data-fancybox="" data-modal ="modal" data-src="#modal-edit-user">
										<img src="https://www.w3schools.com/w3images/avatar4.png" alt="User Image">
										<b class="users-list-name">Norman</b>
										<span class="users-list-date">Member</span>
										</a>
										<div class="tools"><i class="fas fa-trash text-white" onClick="$(this).parents('li').remove();"></i></div>
									  </li>
									  <li class="col-2">
										<a href="javascript:;"  data-fancybox="" data-modal ="modal" data-src="#modal-edit-user">
										<img src="https://www.w3schools.com/w3images/avatar2.png" alt="User Image">
										<b class="users-list-name">Jane</b>
										<span class="users-list-date">Member</span>
										</a>
										<div class="tools"><i class="fas fa-trash text-white" onClick="$(this).parents('li').remove();"></i></div>
									  </li>
									  <li class="col-2">
										<a href="javascript:;"  data-fancybox="" data-modal ="modal" data-src="#modal-edit-user">
										<img src="https://www.w3schools.com/w3images/avatar2.png" alt="User Image">
										<b class="users-list-name">John</b>
										<span class="users-list-date">Admin</span>
										</a>
										<div class="tools"><i class="fas fa-trash text-white" onClick="$(this).parents('li').remove();"></i></div>
									  </li>
									  <li class="col-2">
										<a href="javascript:;"  data-fancybox="" data-modal ="modal" data-src="#modal-edit-user">
										<img src="https://www.w3schools.com/w3images/avatar2.png" alt="User Image">
										<b class="users-list-name">Alexander Pierce</b>
										<span class="users-list-date">Super Admin</span>
										</a>
										<div class="tools"><i class="fas fa-trash text-white" onClick="$(this).parents('li').remove();"></i></div>
									  </li>
									  <li class="col-2">
										<a href="javascript:;"  data-fancybox="" data-modal ="modal" data-src="#modal-edit-user">
										<img src="https://www.w3schools.com/w3images/avatar4.png" alt="User Image">
										<b class="users-list-name">Norman</b>
										<span class="users-list-date">Member</span>
										</a>
										<div class="tools"><i class="fas fa-trash text-white" onClick="$(this).parents('li').remove();"></i></div>
									  </li>
									  <li class="col-2">
										<a href="javascript:;"  data-fancybox="" data-modal ="modal" data-src="#modal-edit-user">
										<img src="https://www.w3schools.com/w3images/avatar4.png" alt="User Image">
										<b class="users-list-name">Jane</b>
										<span class="users-list-date">Member</span>
										</a>
										<div class="tools"><i class="fas fa-trash text-white" onClick="$(this).parents('li').remove();"></i></div>
									  </li>
									  <li class="col-2">
										<a href="javascript:;"  data-fancybox="" data-modal ="modal" data-src="#modal-edit-user">
										<img src="https://www.w3schools.com/w3images/avatar4.png" alt="User Image">
										<b class="users-list-name">John</b>
										<span class="users-list-date">Admin</span>
										</a>
										<div class="tools"><i class="fas fa-trash text-white" onClick="$(this).parents('li').remove();"></i></div>
									  </li>
									  <li id="test-show" class="col-2" style="display:none">
										<a href="javascript:;"  data-fancybox="" data-modal ="modal" data-src="#modal-edit-user">
										<img src="https://www.w3schools.com/w3images/avatar4.png" alt="User Image">
										<b class="users-list-name">Santa Keep</b>
										<span class="users-list-date">Viewer</span>
										</a>
										<div class="tools"><i class="fas fa-trash text-white" onClick="$(this).parents('li').remove();"></i></div>
									  </li>
									  <li class="add-new">
										<a href="javascript:;"  data-fancybox="" data-modal ="modal" data-src="#modal-add-user">
											<i class="fas fa-user-plus fa-3x"></i>
											<b class="users-list-name">Add New</b>
											<span class="users-list-date">User</span>
										</a>
									  </li>


									 <!-- <li>
										<img src="dist/img/user2-160x160.jpg" alt="User Image">
										<a class="users-list-name" href="#">Alexander</a>
										<span class="users-list-date">13 Jan</span>
									  </li>
									  <li>
										<img src="dist/img/user5-128x128.jpg" alt="User Image">
										<a class="users-list-name" href="#">Sarah</a>
										<span class="users-list-date">14 Jan</span>
									  </li>
									  <li>
										<img src="dist/img/user4-128x128.jpg" alt="User Image">
										<a class="users-list-name" href="#">Nora</a>
										<span class="users-list-date">15 Jan</span>
									  </li>
									  <li>
										<img src="dist/img/user3-128x128.jpg" alt="User Image">
										<a class="users-list-name" href="#">Nadia</a>
										<span class="users-list-date">15 Jan</span>
									  </li>-->
									</ul>
								</div>
							</div>
						</div>
						 

					
					
					
					<!-- box-->
						<div class="box-san">
							<!--<div class="head-title">
							<h2 class="hd">สำหรับองค์กร</h2>
							</div>-->
							<div class="cover pd0">
								<h3 class="head txt-l">Add Form ALL</h3>
							</div>
							<div class="body">
								<div class="row">
									<div class="left _self-cl-xs-12-sm-06">
										<div class="wr">
											<input type="text" class="txt-box" id="tax_name" name="tax_name">
											<label for="tax_name">Label</label>
										</div>
									</div>
									<div class="right _self-cl-xs-12-sm-06">
										<div class="wr">
											<input type="text" class="txt-box" id="text_2" name="text_2">
											<label for="text_2">Label</label>
										</div>
									</div>
								</div>
								
								<div class="row">
									<div class="left _self-cl-xs-12-sm-04">
										<div class="wr">
											<input type="text" class="txt-box" id="text_3" name="text_3">
											<label for="text_3">Label</label>
										</div>
									</div>
									<div class="left _self-cl-xs-12-sm-04">
										<div class="wr">
											<input type="text" class="txt-box" id="text_4" name="text_4">
											<label for="text_4">Label</label>
										</div>
									</div>
									<div class="right _self-cl-xs-12-sm-04">
										<div class="wr">
											<input type="text" class="txt-box" id="text_5" name="text_5">
											<label for="text_5">Label</label>
										</div>
									</div>
								</div>
								
								<div class="row mt10-xs">
								<div class="_self-cl-xs-12"><label for="select_1" class="_self-pl15">Selectbox</label></div>
								<div class="left _self-cl-xs-12">
									<div class="js-select">
										<select class="select2" id="select_1" name="select_1" data-placeholder="Select">
											<option></option>
											<option value="1">01</option>
											<option value="2">02</option>
											<option value="3">03</option>
											<option value="4">04</option>
											<option value="5">05</option>
											<option value="6">06</option>
											<option value="7">07</option>
											<option value="8">08</option>
											<option value="9">09</option>
											<option value="10">10</option>
											<option value="11">11</option>
											<option value="12">12</option>
										</select>
										
									</div>
								</div>
							</div>
							
							<div class="row mt10-xs">
								<div class="_self-cl-xs-12"><label for="textarea_1" class="_self-pl15">text Area</label></div>
								<div class="left _self-cl-xs-12">
									<textarea class="txt-box w-100 rounded1" id="textarea_1" name="textarea_1" placeholder="text here"></textarea>
								</div>
							</div>
								
								<div class="row mt10-xs">
									<label for="text_3" class="ml10-xs">Label</label>
									<div class="left _self-cl-xs-12">
										
										<div class="icheck-gray pl10-xs"><input type="checkbox" name="check1" id="check1" checked=""> <label for="check1" class="f-normal">checkbox 1</label></div>
									</div>
									<div class="right _self-cl-xs-12">
										<div class="icheck-gray pl10-xs"><input type="checkbox" name="check2" id="check2" checked=""> <label for="check2" class="f-normal">checkbox 2</label></div>
									</div>
								</div>
								
								<div class="row mt10-xs">
									<label for="text_3" class="ml10-xs">Label</label>
									<div class="_self-cl-xs-12 d-flex start-xs middle-xs">
										<div class="icheck-gray pl10-xs mr20-xs"><input type="radio" name="radio" id="radio1" checked=""> <label for="radio1" class="f-normal">radio 1</label></div>
										<div class="icheck-gray  mr20-xs"><input type="radio" name="radio" id="radio2" checked=""> <label for="radio2" class="f-normal">radio 2</label></div>
										<div class="icheck-gray  mr20-xs"><input type="radio" name="radio" id="radio3" checked=""> <label for="radio3" class="f-normal">radio 3</label></div>
									</div>
								</div>
								
								<div class="row mt10-xs d-flex middle-xs">
									<label for="text_3" class="ml10-xs mr20-xs">Label</label>
									<div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
											<input type="checkbox" class="custom-control-input" id="periodSwitch2-m1" checked="" >
											<label class="custom-control-label" for="periodSwitch2-m1" style="display: block;clear: both"></label>
											<small class="text-label"></small>
										</div>
								</div>
								
							</div>
						</div>
					<!-- /box-->
					
					
					<div class="enterprise">
						<div id="check-enterprise" class="col-xs-12 mz-chk"><input type="checkbox" id="want-tax" name="want-tax" class="Blocked"><label for="want-tax"> Check Toggle</label></div>
						<div class="add-detail " style="padding: 0; display: none">

						<!-- box-->
						<div class="box-san">
							<!--<div class="head-title">
							<h2 class="hd">สำหรับองค์กร</h2>
							</div>-->
							<div class="cover pd0">
								<h3 class="head txt-l">Compy Detail</h3>
							</div>
							<div class="body">
								<div class="row">
									<div class="left _self-cl-xs-12-sm-06">
										<div class="wr">
											<input type="text" class="txt-box" id="tax_name" name="tax_name">
											<label for="tax_name">ชื่อบริษัท</label>
										</div>
									</div>
									<div class="right _self-cl-xs-12-sm-06">
										<div class="wr">
											<input type="text" class="txt-box" id="tax_ID" name="tax_ID">
											<label for="tax_ID">Tax ID</label>
										</div>
									</div>
								</div>
								<div class="wr">
									<input type="text" class="txt-box" id="tax_address" name="tax_address">
									<label for="tax_address">ที่อยู่</label>
								</div>
								<div class="row">
									<div class="left _self-cl-xs-12-sm-04">
										<div class="wr">
											<input type="text" class="txt-box" id="tax_province" name="tax_province">
											<label for="tax_province">เมือง</label>
										</div>
									</div>
									<div class="mid _self-cl-xs-12-sm-04">
										<div class="wr">
											<input type="text" class="txt-box" id="tax_country" name="tax_country">
											<label for="tax_country">ประเทศ</label>
										</div>
									</div>
									<div class="right _self-cl-xs-12-sm-04">
										<div class="wr">
											<input type="tel" class="txt-box" id="staff_ctax_zipcodeompany" name="tax_zipcode" maxlength="5">
											<label for="tax_zipcode">รหัสไปรษณีย์</label>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="left _self-cl-xs-12-sm-06">
										<div class="wr">
											<input type="text" class="txt-box" id="tax_tel" name="tax_tel" maxlength="10">
											<label for="tax_tel">เบอร์โทรศัพท์</label>
										</div>
									</div>
									<div class="right _self-cl-xs-12-sm-06">
										&nbsp;
									</div>
								</div>
								
							</div>
						 </div>
						 <!-- /box-->


						</div>
					</div>
					
				<?php /*?>	
					<!-- box-->
					<div class="box-san">
						<!--<div class="head-title">
						<h2 class="hd">Credit/Debit Card</h2>
						</div>-->
						<div class="cover pd0">
							<h3 class="head txt-l">Credit/Debit Card</h3>
						</div>
						<div class="body lg-visa">
							<div class="wr">
								<input type="tel" class="txt-box" id="card-num" name="card-num" required maxlength="16">
								<label for="card-num">Card number</label>
							</div>
							
							<div class="wr">
								<input type="text" class="txt-box" id="card-name" name="card-name" required>
								<label for="card-name">Name on card</label>
							</div>
							
							<div class="row">
								<div class="_self-cl-xs-12"><label for="expire-date" class="_self-pl15">Expiry date</label></div>
								<div class="left _self-cl-xs-12-sm-06">
									<div class="js-select">
										<select class="select2" id="expire-month" name="expire-month" data-placeholder="MM">
											<option></option>
																						<option value="1">01</option>
																						<option value="2">02</option>
																						<option value="3">03</option>
																						<option value="4">04</option>
																						<option value="5">05</option>
																						<option value="6">06</option>
																						<option value="7">07</option>
																						<option value="8">08</option>
																						<option value="9">09</option>
																						<option value="10">10</option>
											<option value="11">11</option>
											<option value="12">12</option>
										</select>
										
									</div>
								</div>
								<div class="right _self-cl-xs-12-sm-06">
									<div class="js-select">
										<select class="select2" id="expire-year" name="expire-year" data-placeholder="YYYY">
											<option></option>
																						<option value="2025">2025</option>
	
																						<option value="2024">2024</option>
	
																						<option value="2023">2023</option>
	
																						<option value="2022">2022</option>
	
																						<option value="2021">2021</option>
	
																						<option value="2020">2020</option>
	
																						<option value="2019">2019</option>
	
																						<option value="2018">2018</option>
	
																						<option value="2017">2017</option>
	
																						<option value="2016">2016</option>
	
																						<option value="2015">2015</option>
	
																						<option value="2014">2014</option>
	
																						<option value="2013">2013</option>
	
																						<option value="2012">2012</option>
	
																						<option value="2011">2011</option>
	
																						<option value="2010">2010</option>
	
																						<option value="2009">2009</option>
	
																						<option value="2008">2008</option>
	
																						<option value="2007">2007</option>
	
																						<option value="2006">2006</option>
	
																						<option value="2005">2005</option>
	
																						<option value="2004">2004</option>
	
																						<option value="2003">2003</option>
	
																						<option value="2002">2002</option>
	
																						<option value="2001">2001</option>
	
																						<option value="2000">2000</option>
	
																						<option value="1999">1999</option>
	
																						<option value="1998">1998</option>
	
																						<option value="1997">1997</option>
	
																						<option value="1996">1996</option>
	
																						<option value="1995">1995</option>
	
																						<option value="1994">1994</option>
	
																						<option value="1993">1993</option>
	
																						<option value="1992">1992</option>
	
																						<option value="1991">1991</option>
	
																						<option value="1990">1990</option>
	
																						<option value="1989">1989</option>
	
																						<option value="1988">1988</option>
	
																						<option value="1987">1987</option>
	
																						<option value="1986">1986</option>
	
																						<option value="1985">1985</option>
	
																						<option value="1984">1984</option>
	
																						<option value="1983">1983</option>
	
																						<option value="1982">1982</option>
	
																						<option value="1981">1981</option>
	
																						<option value="1980">1980</option>
	
																						<option value="1979">1979</option>
	
																						<option value="1978">1978</option>
	
																						<option value="1977">1977</option>
	
																						<option value="1976">1976</option>
	
																						<option value="1975">1975</option>
	
																						<option value="1974">1974</option>
	
																						<option value="1973">1973</option>
	
																						<option value="1972">1972</option>
	
																						<option value="1971">1971</option>
	
																						<option value="1970">1970</option>
	
																						<option value="1969">1969</option>
	
																						<option value="1968">1968</option>
	
																						<option value="1967">1967</option>
	
																						<option value="1966">1966</option>
	
																						<option value="1965">1965</option>
	
																						<option value="1964">1964</option>
	
																						<option value="1963">1963</option>
	
																						<option value="1962">1962</option>
	
																						<option value="1961">1961</option>
	
																						<option value="1960">1960</option>
	
																						<option value="1959">1959</option>
	
																						<option value="1958">1958</option>
	
																						<option value="1957">1957</option>
	
																						<option value="1956">1956</option>
	
																						<option value="1955">1955</option>
	
																						<option value="1954">1954</option>
	
																						<option value="1953">1953</option>
	
																						<option value="1952">1952</option>
	
																						<option value="1951">1951</option>
	
																						<option value="1950">1950</option>
	
																					</select>
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="left _self-cl-xs-12-sm-06">
									<div class="wr">
										<input type="tel" class="txt-box" id="card-cvv" name="card-cvv" required maxlength="3">
										<label for="staff_mobile">CVV</label>
									</div>
								</div>
								<div class="right _self-cl-xs-12-sm-06">
									<a class="_self-mt30-pt15 _flex middle-xs t-gray" href="javascript:;" data-fancybox="success" data-src="#popupCard"><i class="far fa-question-circle"></i></a>
								</div>
							</div>
							
							<!-- Popup -->
							<div class="popup thm-trans" id="popupCard">
								<div class="box-middle">
								<div class="head">เลข CVV</div>
								<figure><img src="di/cvv-demo.png" alt="CVV"></figure>
								<p>เลข CVV คือ เลขรหัสหลังบัตรเครดิตหรือบัตรเดบิต</p>
								<p>	
									<a data-fancybox-close class="ui-btn-gray2-mini" title="Close" href="javascript:;" onClick="parent.jQuery.fancybox.close();"> Close</a>
								</p>
								</div>
							</div>
							<!-- /Popup -->
							
						</div>
					 </div>
					 <!-- /box-->
					<?php */?>

					
					<div class="agree">
						<div id="check-accept" class="col-xs-12 mz-chk"><input type="checkbox" id="iam-gree" name="iam-gree" required="required"><label for="iam-gree"> I agree to the Smart Chat <a class="t-blue" href="javascript:;" data-fancybox="login2" data-src="#popup-terms"> Terms and Conditions.</a></label></div>
						<div id="check-accept" class="col-xs-12 mz-chk"><input type="checkbox" id="iam-gree-policy" name="iam-gree-policy" required="required"><label for="iam-gree-policy"> I agree to the Smart Chat <a class="t-blue" href="javascript:;" data-fancybox="login2" data-src="#popup-policy"> Privacy policy.</a></label></div>
					</div>
					
					
					<div class="ctrl-btn txt-c _self-mt20 sticky-bottom">
						<!--<input type="submit" id="Cancel" class="ui-btn-green2 btn-sm" value="เลือกแพ็คเกจอื่น"> -->
							<a href="javascript:window.history.back();" title="เลือกแพ็คเกจอื่น" class="ui-btn-gray btn-sm" ><i class="fas fa-angle-left"></i> Back</a> 
							<input type="submit" id="Submit" class="ui-btn-green btn-sm" value="Save">
					</div>
				</fieldset>
			</form>
				</div>
			</div>
			
			

			
		</section>
    </div>
</div>

<div class="popup thm-lite" id="modal-add-user">
				<div class="box-middle">
				  <div class="modal-content">
					<div class="modal-header">
					  <h2 class="modal-title t-black mb20-xs">Add Member</h2>
					</div>
					<div class="modal-body txt-l">
					  
					  <div class="form-group p-2 bg-light rounded" data-select2-id="72">

							<div class="row">
								<div class="col-sm-6">
									  <div class="form-group">
										<label for="u-Email1">Email address</label>
										<input type="email" class="form-control" id="u-Email">
									  </div>
									   <div class="form-group">
										<label for="u-user1">Username</label>
										<input type="text" class="form-control" id="u-user">
									  </div>

									  <div class="form-group">
										<label for="u-pass">Password</label>

										<div class="input-group">
										  <input type="password" class="form-control" id="u-pass">
										  <span class="input-group-append">
											<button type="button" class="btn bg-black pl5-xs pr5-xs border-0">Gen</button>
										  </span>
										</div>
									  </div>
								</div>

								<div class="col-sm-6">
									  <div class="form-group">
										<label for="u-name">Display Name</label>
										<input type="text" class="form-control" id="exampleInputEmail">
									  </div>

									  <div class="form-group">
										<label for="exampleInputEmail1">Role</label>
										<div class="btn-group btn-group-toggle w-100" data-toggle="buttons">
										  <label class="btn bg-teal active">
											<input type="radio" name="options" id="option-add1" autocomplete="off" checked> Admin
										  </label>
										  <label class="btn bg-teal">
											<input type="radio" name="options" id="option-add2" autocomplete="off"> Agent
										  </label>
										  <label class="btn bg-teal">
											<input type="radio" name="options" id="option-add3" autocomplete="off"> Viewer
										  </label>
										</div>
									  </div>
									  
									  <div id="f-upload_avatar" class="f-upload form-group" style="">
										<label for="apm_img2">Upload Avatar</label>
										<div class="input-group">
												<div class="custom-file">
													<input type="file" class="custom-file-input1" id="apm_img1" accept=".jpg,.jpeg,.png">
													<img id="temp_apmimage_src" hidden="">
													<label class="custom-file-label" id="apm_labelimg1" for="apm_img1"><p>Choose file</p></label>
												</div>
												<div class="input-group-append">
													<button class="input-group-text" id="apm_uploadimg2">Upload</button>
												</div>

										</div>
									 </div>
									  
									  <div class="form-group">
										<label for="gender">Gender</label>
										<div class="btn-group btn-group-toggle w-100" data-toggle="buttons">
										  <label class="btn bg-teal active">
											<input type="radio" name="gender" id="male" autocomplete="off"> Male
										  </label>
										  <label class="btn bg-teal">
											<input type="radio" name="gender" id="female" autocomplete="off"> Female
										  </label>
										</div>
									  </div>
								</div>
							</div>
						</div>
					  
					</div>
					<div class="modal-footer justify-content-between">
					  <button type="button" data-fancybox-close="" class="ui-btn-gray btn-sm" title="Close" href="javascript:;" onclick="parent.jQuery.fancybox.close();">Cancel</button>
					  <button type="button" data-fancybox-close="" class="ui-btn-green2 btn-sm" title="Close" href="javascript:;" onclick="parent.jQuery.fancybox.close();">Save</button>
					</div>
				  </div>
				  <!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			  </div>
			  <!-- /.modal -->
			  
<div class="popup thm-lite" id="modal-edit-user">
				<div class="box-middle">
				  <div class="modal-content">
					<div class="modal-header">
					  <h2 class="modal-title t-black mb20-xs">Edit Member</h2>
					</div>
					<div class="modal-body txt-l">
					  
					  <div class="form-group p-2 bg-light rounded" data-select2-id="72">

							<div class="row">
								<div class="col-sm-6">
									  <div class="form-group">
										<label for="u-Email1">Email address</label>
										<input type="email" class="form-control" id="u-Email1" value="Username@mail.com">
									  </div>
									   <div class="form-group">
										<label for="u-user1">Username</label>
										<input type="text" class="form-control" id="u-user1" value="innohub1">
									  </div>

									  <div class="form-group">
										<label for="u-pass">Password</label>

										<div class="input-group">
										  <input type="password" class="form-control" id="u-pass" value="123456">
										  <span class="input-group-append">
											<button type="button" class="btn bg-black pl5-xs pr5-xs border-0">Reset</button>
										  </span>
										</div>
									  </div>
								</div>

								<div class="col-sm-6">
									  <div class="form-group">
										<label for="u-name">Display Name</label>
										<input type="text" class="form-control" id="exampleInputEmail1" value="Alexander Pierce">
									  </div>

									  <div class="form-group">
										<label for="exampleInputEmail1">Role</label>
										<div class="btn-group btn-group-toggle w-100" data-toggle="buttons">
										  <label class="btn bg-teal active">
											<input type="radio" name="options" id="option1" autocomplete="off" checked> Admin
										  </label>
										  <label class="btn bg-teal">
											<input type="radio" name="options" id="option2" autocomplete="off"> Agent
										  </label>
										  <label class="btn bg-teal">
											<input type="radio" name="options" id="option3" autocomplete="off"> Viewer
										  </label>
										</div>
									  </div>
									  
									  <div id="f-upload_avatar" class="f-upload form-group" style="">
										<label for="apm_img2">Upload Avatar</label>
										<div class="input-group">
												<div class="custom-file">
													<input type="file" class="custom-file-input2" id="apm_img2" accept=".jpg,.jpeg,.png">
													<img id="temp_apmimage_src" hidden="">
													<label class="custom-file-label" id="apm_labelimg2" for="apm_img2"><p>Choose file</p></label>
												</div>
												<div class="input-group-append">
													<button class="input-group-text" id="apm_uploadimg2">Upload</button>
												</div>

										</div>
									 </div>
									  
									  <div class="form-group">
										<label for="gender">Gender</label>
										<div class="btn-group btn-group-toggle w-100" data-toggle="buttons">
										  <label class="btn bg-teal active">
											<input type="radio" name="gender" id="male" autocomplete="off" checked> Male
										  </label>
										  <label class="btn bg-teal">
											<input type="radio" name="gender" id="female" autocomplete="off"> Female
										  </label>
										</div>
									  </div>
								</div>
							</div>
						</div>
					  
					</div>
					<div class="modal-footer justify-content-between">
					  <button type="button" data-fancybox-close="" class="ui-btn-gray btn-sm" title="Close" href="javascript:;" onclick="parent.jQuery.fancybox.close();">Cancel</button>
					  <button type="button" data-fancybox-close="" class="ui-btn-green2 btn-sm" title="Close" href="javascript:;" onclick="parent.jQuery.fancybox.close();">Save</button>
					</div>
				  </div>
				  <!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			  </div>
			  <!-- /.modal -->
		

		<div class="popup thm-lite" id="modal-close">
				<div class="box-middle">
				<div class="modal-dialog">
				  <div class="modal-content">
					<div class="modal-header">
					  <h2 class="modal-title t-black mb20-xs">Do you want to close Application?</h4>
					</div>
					<div class="modal-body txt-c">
					  <p>คุณต้องการออกจากระบบการส่งข้อความใช่ไหม?</p>
					</div>
					<div class="modal-footer p-0 center-xs">
					  <button type="button" data-fancybox-close="" class="ui-btn-gray btn-sm" title="Close" href="javascript:;" onclick="parent.jQuery.fancybox.close();">Cancel</button>
					  <button type="button" data-fancybox-close="" class="ui-btn-green2 btn-sm" title="Close" href="javascript:;" onclick="parent.jQuery.fancybox.close();">Confirm</button>
					</div>
				  </div>
				  <!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			  </div>
			  <!-- /.modal -->


<!--<div id="skin-loading" class="bg-wh" onclick="$(this).fadeOut();">
	<div class="lds-hourglass"></div>
</div>-->
<script>
	window.setTimeout(function(){
		$('#skin-loading').fadeOut();
	}, 3000);
</script>

<!-- footer -->
<?php include("incs/footer-web.html") ?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js-web.html") ?>
<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
<script>
//Dropdown plugin data
$(document).ready(function() {
     $('.select2').select2({
    	placeholder: "Please select",
    	//allowClear: true,
		minimumResultsForSearch: -1,
		dropdownAutoWidth : true,
		width: '100%'
	});
	/*toggle boarding pass*/
	$('.Blocked').change( function() {
		var isChecked = this.checked;
		if(isChecked) {
			$(".add-detail").fadeIn(300);
			$(".add-detail .txt-box").prop("disabled",false);
			$(".add-detail .txt-box").prop("required",true);
		} else {
			$(".add-detail").fadeOut(300);
			$(".add-detail .txt-box").prop("disabled",true); 
			$(".add-detail .txt-box").prop("required",false); 
		}
	});
	/*toggle radio*/
	$(".btn-group-toggle input[type=radio]").on("click", function() {
		
		if ($(this).parents('.btn').hasClass( 'active' )) {
		} else {
			$(this).parents(".btn-group-toggle").find('.btn').removeClass('active');
			$(this).parents('.btn').addClass('active');
		}
		

	})
	
});
</script>
<!-- /js -->

</body>
</html>
