FROM php:7.3-apache

WORKDIR /var/www/html/
#COPY cs/ ./cs/
#COPY di/ ./di/
#COPY js/ ./js/
#COPY incs/ ./incs/
COPY index.html ./
COPY index-en.html ./
COPY terms.html ./

ENV PORT 8080

ENTRYPOINT []

CMD sed -i "s/80/$PORT/g" /etc/apache2/sites-available/000-default.conf /etc/apache2/ports.conf && docker-php-entrypoint apache2-foreground


# docker run -p 8007:80 --name comba devio/comba:latest
# docker build . -t devio/comba:1.0.0
# docker tag devio/comba:1.0.0 gcr.io/devio-230804/comba:1.0.0
# gcloud docker -- push gcr.io/devio-230804/comba:1.0.0