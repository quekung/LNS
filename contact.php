<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top-web.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden">
<!-- Headbar -->
<?php include("incs/header-v2.html") ?>
<!-- /Headbar -->
<div class="page-login page-checkout">

    
    <div id="toc" class="container">
		<section class="z-checkout _self-pt30">
		
        	<form class="form-signin form-checkout" method="post" action="javascript:;">
				<fieldset>

					<!-- box-->
						<div class="bx-keep">
							<div class="head-title">
							<h2 class="hd t-white txt-center _self-mb30">Contact Us</h2>
							</div>
							<div class="body">
								<div class="row">
									<div class="left _self-cl-xs-12-sm-06">
										<div class="wr">
											<input type="text" class="txt-box" id="tax_name" name="tax_name" required>
											<label for="tax_name">First Name*</label>
										</div>
									</div>
									<div class="right _self-cl-xs-12-sm-06">
										<div class="wr">
											<input type="text" class="txt-box" id="tax_ID" name="tax_ID" required>
											<label for="tax_ID">Last Name*</label>
										</div>
									</div>
								</div>
								
								<div class="row">
									<div class="left _self-cl-xs-12-sm-06">
										<div class="wr">
											<input type="text" class="txt-box" id="tax_province" name="tax_province" required>
											<label for="tax_province">Business Email*</label>
										</div>
									</div>
									<div class="right _self-cl-xs-12-sm-06">
										<div class="wr">
											<input type="tel" class="txt-box" id="staff_ctax_zipcodeompany" name="tax_zipcode" required maxlength="10">
											<label for="tax_zipcode">Phone*</label>
										</div>
									</div>
								</div>
								
								<div class="row">
									<div class="left _self-cl-xs-12-sm-06">
										<div class="wr">
											<input type="text" class="txt-box" id="tax_name" name="tax_name" required>
											<label for="tax_name">Company Name*</label>
										</div>
									</div>
									<div class="right _self-cl-xs-12-sm-06">
										<div class="wr js-select">
											<select class="select2" id="industry-type" name="industry-type" data-placeholder="Industry">
												<option></option>
												<option value="in">automobiles and automotive parts </option>
												<option value="in">financial services </option>
												<option value="in">electric appliances and components</option>
												<option value="in">tourism</option>
												<option value="in">cement</option>
												
												<option value="in">auto manufacturing</option>
												<option value="in">heavy and light industries</option>
												<option value="in">appliances</option>
												<option value="in">computers and parts</option>
												<option value="in">furniture</option>
												
												<option value="in">plastics</option>
												<option value="in">textiles and garments</option>
												<option value="in">agricultural processing</option>
												<option value="in">beverages</option>
												<option value="in">tobacco</option>
											</select>
										</div>

									</div>
								</div>
								
								<div class="row">
									<div class="left _self-cl-xs-12-sm-06">
										<div class="wr js-select">
											<select class="select2" id="company-size" name="company-size" data-placeholder="Company Size">
												<option></option>
												<option value="bs1">เจ้าของธุรกิจ SME</option>
												<option value="bs2">เจ้าของธุรกิจขนาดกลาง</option>
												<option value="bs3">เจ้าของธุรกิจขนาดใหญ่</option>
											</select>
										</div>
									</div>
									<div class="right _self-cl-xs-12-sm-06"></div>
								</div>
								
								<div class="_self-mb20">
								<div class="wr">
									<textarea type="text" class="txt-box" id="tax_address" name="tax_address" rows="5" required></textarea>
									<label for="tax_address">ที่อยู่</label>
								</div>
								</div>
								
							</div>
						 </div>
						 <!-- /box-->

					
					
					
					<div class="ctrl-btn txt-c _self-mt20 fix-bottom">
						<input type="submit" id="Submit" class="ui-btn-white" value="Send">
					</div>
				</fieldset>
			</form>
		</section>
    </div>
</div>

<!--<div id="skin-loading" class="bg-wh" onclick="$(this).fadeOut();">
	<div class="lds-hourglass"></div>
</div>-->
<script>
	window.setTimeout(function(){
		$('#skin-loading').fadeOut();
	}, 3000);
</script>

<!-- footer -->
<?php include("incs/footer-web.html") ?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js-web.html") ?>
<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
<script>
//Dropdown plugin data
$(document).ready(function() {
     $('.select2').select2({
    	placeholder: "Please select",
    	//allowClear: true,
		minimumResultsForSearch: -1,
		dropdownAutoWidth : true,
		width: '100%'
	});
	/*toggle boarding pass*/
	$('.Blocked').change( function() {
		var isChecked = this.checked;
		if(isChecked) {
			$(".add-detail").fadeIn(300);
			$(".add-detail .txt-box").prop("disabled",false);
			$(".add-detail .txt-box").prop("required",true);
		} else {
			$(".add-detail").fadeOut(300);
			$(".add-detail .txt-box").prop("disabled",true); 
			$(".add-detail .txt-box").prop("required",false); 
		}
	});
	
});
</script>
<!-- /js -->

</body>
</html>
